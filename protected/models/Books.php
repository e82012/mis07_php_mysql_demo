<?php

/**
 * This is the model class for table "books".
 *
 * The followings are the available columns in table 'books':
 * @property integer $id
 * @property string $bname
 * @property string $bauthor
 * @property string $bpublish
 * @property string $bisbn
 * @property string $bdate
 * @property string $bststus
 * @property string $lender
 * @property string $lenddate
 * @property string $expiry
 * @property string $bimg
 */
class Books extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'books';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bname, bauthor, bpublish, bisbn, bdate, bstatus', 'required'),
			array('bname, bauthor, bpublish, bisbn, bdate, bstatus,lender,lenddate,expiry', 'length', 'max'=>128),
			array('bimg','file','types'=>'jpg,gif,png','allowEmpty' => true), //不可為空白
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, bname, bauthor, bpublish, bisbn, bdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'bname' => '書籍名稱',
			'bauthor' => '作者',
			'bpublish' => '出版商',
			'bisbn' => 'ISBN',
			'bdate' => '出版日期',
			'bstatus' => '借閱狀態',
			'lender' => '借閱人',
			'lenddate' => '借閱日期',
			'expiry' => '逾期日',
			'bimg' => '圖片',
			);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('bname',$this->bname,true);
		$criteria->compare('bauthor',$this->bauthor,true);
		$criteria->compare('bpublish',$this->bpublish,true);
		$criteria->compare('bisbn',$this->bisbn,true);
		$criteria->compare('bdate',$this->bdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Books the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

		

}
