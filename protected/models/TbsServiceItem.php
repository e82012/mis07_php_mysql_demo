<?php

/**
 * This is the model class for table "tbs_service_item".
 *
 * The followings are the available columns in table 'tbs_service_item':
 * @property integer $id
 * @property string $version
 * @property string $sale_no
 * @property string $sale_name
 * @property string $type
 * @property string $sv_order
 * @property string $num
 * @property string $price
 * @property string $perform_mapping
 * @property string $memo
 */
class TbsServiceItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbs_service_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('version, sale_no, sale_name, type, sv_order, num, price, perform_mapping, memo', 'required'),
			array('version', 'length', 'max'=>8),
			array('sale_no', 'length', 'max'=>10),
			array('sale_name', 'length', 'max'=>50),
			array('type, num', 'length', 'max'=>1),
			array('sv_order', 'length', 'max'=>2),
			array('price', 'length', 'max'=>4),
			array('perform_mapping, memo', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, version, sale_no, sale_name, type, sv_order, num, price, perform_mapping, memo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'version' => '版本',
			'sale_no' => 'Sale No(編號)',
			'sale_name' => 'Sale Name(項目名稱)',
			'type' => 'Type(項目類型)',
			'sv_order' => 'Sv Order(預設排序)',
			'num' => 'Num(出票數)',
			'price' => 'Price(價格)',
			'perform_mapping' => 'Perform Mapping(業績對照)',
			'memo' => 'Memo(備註)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('sale_no',$this->sale_no,true);
		$criteria->compare('sale_name',$this->sale_name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('sv_order',$this->sv_order,true);
		$criteria->compare('num',$this->num,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('perform_mapping',$this->perform_mapping,true);
		$criteria->compare('memo',$this->memo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TbsServiceItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
