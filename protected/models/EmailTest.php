<?php

/**
 * This is the model class for table "EmailTest".
 *
 * The followings are the available columns in table 'EmailTest':
 * @property integer $id
 * @property string $E_from
 * @property string $E_subject
 * @property string $E_emailadd
 * @property string $E_message
 * @property string $E_file
 */
class EmailTest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EmailTest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('E_from, E_subject, E_emailadd, E_message, E_file', 'required'),
			array('E_from, E_subject, E_emailadd, E_message, E_file', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, E_from, E_subject, E_emailadd, E_message, E_file', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'E_from' => 'E From',
			'E_subject' => 'E Subject',
			'E_emailadd' => 'E Emailadd',
			'E_message' => 'E Message',
			'E_file' => 'E File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('E_from',$this->E_from,true);
		$criteria->compare('E_subject',$this->E_subject,true);
		$criteria->compare('E_emailadd',$this->E_emailadd,true);
		$criteria->compare('E_message',$this->E_message,true);
		$criteria->compare('E_file',$this->E_file,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailTest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
