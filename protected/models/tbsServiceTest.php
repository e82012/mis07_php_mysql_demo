<?php

/**
 * This is the model class for table "tbs_service_test".
 *
 * The followings are the available columns in table 'tbs_service_test':
 * @property integer $id
 * @property string $version
 * @property string $storeCode
 * @property string $storeName
 * @property string $brand
 * @property string $sale_no
 * @property string $sale_name
 * @property string $sv_time
 * @property string $sv_timename
 * @property string $sv_type
 * @property integer $sv_order
 * @property integer $num
 * @property integer $price
 * @property string $perform_mapping
 * @property string $memo
 * @property string $ip
 */
class tbsServiceTest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbs_service_test';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('version, storeCode, storeName, brand, sale_no, sale_name, sv_time, sv_timename, sv_type, sv_order, num, price, perform_mapping, memo, ip', 'required'),
			array('sv_order, num, price', 'numerical', 'integerOnly'=>true),
			array('version, sv_timename', 'length', 'max'=>8),
			array('storeCode', 'length', 'max'=>6),
			array('storeName, brand, sale_no, sv_time', 'length', 'max'=>10),
			array('sale_name, memo', 'length', 'max'=>50),
			array('sv_type', 'length', 'max'=>2),
			array('perform_mapping', 'length', 'max'=>100),
			array('ip', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, version, storeCode, storeName, brand, sale_no, sale_name, sv_time, sv_timename, sv_type, sv_order, num, price, perform_mapping, memo, ip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'version' => 'Version',
			'storeCode' => 'Store Code',
			'storeName' => 'Store Name',
			'brand' => 'Brand',
			'sale_no' => 'Sale No',
			'sale_name' => 'Sale Name',
			'sv_time' => 'Sv Time',
			'sv_timename' => 'Sv Timename',
			'sv_type' => 'Sv Type',
			'sv_order' => 'Sv Order',
			'num' => 'Num',
			'price' => 'Price',
			'perform_mapping' => 'Perform Mapping',
			'memo' => 'Memo',
			'ip' => 'Ip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('storeCode',$this->storeCode,true);
		$criteria->compare('storeName',$this->storeName,true);
		$criteria->compare('brand',$this->brand,true);
		$criteria->compare('sale_no',$this->sale_no,true);
		$criteria->compare('sale_name',$this->sale_name,true);
		$criteria->compare('sv_time',$this->sv_time,true);
		$criteria->compare('sv_timename',$this->sv_timename,true);
		$criteria->compare('sv_type',$this->sv_type,true);
		$criteria->compare('sv_order',$this->sv_order);
		$criteria->compare('num',$this->num);
		$criteria->compare('price',$this->price);
		$criteria->compare('perform_mapping',$this->perform_mapping,true);
		$criteria->compare('memo',$this->memo,true);
		$criteria->compare('ip',$this->ip,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return tbsServiceTest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
