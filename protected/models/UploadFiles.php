<?php

/**
 * This is the model class for table "uploadFiles".
 *
 * The followings are the available columns in table 'uploadFiles':
 * @property integer $id
 * @property string $filename
 * @property string $filepath
 * @property string $type
 * @property string $user
 * @property string $ctime
 * @property string $opt1
 */
class UploadFiles extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'uploadFiles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('filename, filepath, type, user, ctime, opt1', 'required'),
			array('filename, type, user', 'length', 'max'=>20),
			array('filepath', 'length', 'max'=>255),
			array('ctime', 'length', 'max'=>8),
			array('opt1', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, filename, filepath, type, user, ctime, opt1', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'filename' => 'Filename',
			'filepath' => 'Filepath',
			'type' => 'Type',
			'user' => 'User',
			'ctime' => 'Ctime',
			'opt1' => 'Opt1',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('filepath',$this->filepath,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('opt1',$this->opt1,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UploadFiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
