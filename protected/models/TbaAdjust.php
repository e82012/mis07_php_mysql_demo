<?php

/**
 * This is the model class for table "tba_adjust".
 *
 * The followings are the available columns in table 'tba_adjust':
 * @property integer $id
 * @property string $applyName
 * @property string $applyCode
 * @property string $month
 * @property string $date_sn
 * @property string $date_eo
 * @property string $aft_days
 * @property string $daytime
 * @property string $store
 * @property string $storeCode
 * @property string $o_store
 * @property string $o_storeCode
 * @property string $empno
 * @property string $empname
 * @property string $act_emps
 * @property string $act_empn
 * @property string $user_barcode
 * @property string $user_mobile
 * @property string $reason
 * @property string $memo
 */
class TbaAdjust extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tba_adjust';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('applyName, applyCode, month, date_sn, date_eo, aft_days, daytime, store, storeCode, o_store, o_storeCode, empno, empname, act_emps, act_empn, user_barcode, user_mobile, reason, memo', 'required'),
			array('applyName, date_sn, date_eo, store, o_store, empno, user_mobile', 'length', 'max'=>10),
			array('applyCode, daytime, storeCode, o_storeCode, empname', 'length', 'max'=>6),
			array('month, act_emps, act_empn', 'length', 'max'=>2),
			array('eft_days', 'length', 'max'=>3),
			array('user_barcode', 'length', 'max'=>20),
			array('reason, memo', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, applyName, applyCode, month, date_sn, date_eo, aft_days, daytime, store, storeCode, o_store, o_storeCode, empno, empname, act_emps, act_empn, user_barcode, user_mobile, reason, memo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'applyName' => '申請項目名稱',
			'applyCode' => '申請項目編號',
			'month' => '申請月份',
			'date_sn' => '日期(起.新)',
			'date_eo' => '日期(訖.舊)',
			'aft_days' => '影響天數',
			'daytime' => '時間',
			'store' => '門市(新)',
			'storeCode' => '店編(新)',
			'o_store' => '門市(舊)',
			'o_storeCode' => '店編(舊)',
			'empno' => '員編',
			'empname' => '設計師',
			'act_emps' => '實際員額增減',
			'act_empn' => '實際員額數',
			'user_barcode' => 'custom條碼',
			'user_mobile' => 'custom電話',
			'reason' => '原因',
			'memo' => '備註',
			'applyDateTime'=>'申請時間',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('applyName',$this->applyName,true);
		$criteria->compare('applyCode',$this->applyCode,true);
		$criteria->compare('month',$this->month,true);
		$criteria->compare('date_sn',$this->date_sn,true);
		$criteria->compare('date_eo',$this->date_eo,true);
		$criteria->compare('aft_days',$this->aft_days,true);
		$criteria->compare('daytime',$this->daytime,true);
		$criteria->compare('store',$this->store,true);
		$criteria->compare('storeCode',$this->storeCode,true);
		$criteria->compare('o_store',$this->o_store,true);
		$criteria->compare('o_storeCode',$this->o_storeCode,true);
		$criteria->compare('empno',$this->empno,true);
		$criteria->compare('empname',$this->empname,true);
		$criteria->compare('act_emps',$this->act_emps,true);
		$criteria->compare('act_empn',$this->act_empn,true);
		$criteria->compare('user_barcode',$this->user_barcode,true);
		$criteria->compare('user_mobile',$this->user_mobile,true);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('memo',$this->memo,true);
		$criteria->compare('applyDateTime',$this->applyDateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TbaAdjust the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
