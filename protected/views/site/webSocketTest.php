<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"></head>



<?php

/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - testPage';
$this->breadcrumbs=array(
	'WebSocket',
);
?>

<button type="button" class="btn btn-primary"><i class="fas fa-ad"></i></button>
<h5>testPageX</h5>
<?php echo $computetime ;?>
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#menu_A">A</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_B">B</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_C">C</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane container active" id="menu_A">
		<form method="POST" id = 'form1' name = 'form1'>
			PW：<input type="text" name="inText" id="inText">
			<button type="button" name = "inTexBTN" id = "inTexBTN" onclick="inTexAjax(this)">輸入</button>
		</form>
	</div>
	<div class="tab-pane container fade" id="menu_B">
	</div>
	<div class="tab-pane container fade" id="menu_C">
	<form method="POST" id = 'form3' name = 'form3'>
		DataTHrow3 Test
	</form>
	</div>
</div>

<script>
//IO_SOCKET
var ws = new WebSocket('ws://localhost:8088/webSocketTest');
console.log(ws);
// Listen for messages
ws.addEventListener('message', function (event) {
	// console.log(event);
    console.log('Message from server :', event.data);
	document.getElementById("inText").value = event.data;
});

//AJax BTN
function inTexAjax(intex)
{
	// var setDatePw = this.setDatePw();
	var inTextCurrent = document.getElementById("inText").value;
	console.log(inTextCurrent);

	//AJAX
	$.ajax({
		url:"<?php echo Yii::app()->createUrl('Site/WebSocketAjax'); ?>", //Controller
			type:'POST',
			dataType:'JSON',
			data:{
				inTextCurrent:inTextCurrent,
				//($post[name]) :var名稱
				//php後端呼叫方式 $_POST['itme']
			},
			success:function(inText){
				// console.log(inText);
				alert("輸入成功");                
			},
			error:function(){alert("輸入失敗");}
		});

}
function setDatePw()
{
  //日期
	var date = new Date();
  var dateString = date.toISOString().slice(0, 10); 
  //6位數亂碼
  var inTextPw = Math.random().toFixed(6).slice(-6); 
  //組合
  var datePw = dateString + inTextPw;
  return datePw;
}

</script>