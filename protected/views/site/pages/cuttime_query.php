<style>
  * {
    box-sizing: border-box;
  }
  .loader {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 50px;
    height: 50px;
    background: transparent; 
    margin: 30px auto 0 auto;
    border: solid 3px #424242;
    border-top: solid 3px #1c89ff;
    border-radius: 100%;
    opacity: 0;
  }

  .check {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    transform: translate3d(-4px,50px,0);
    opacity: 0;
    span:nth-child(1) {
      display: block;
      width: 10px;
      height: 2px;
      background-color: #000;
      transform: rotate(45deg);
    }
    span:nth-child(2) {
      display: block;
      width: 20px;
      height: 2px;
      background-color: #fff;
      transform: rotate(-45deg) translate3d(14px, -4px, 0);
      transform-origin: 100%;
    }
  }

  .loader.active {
    animation: loading 5s ease-in-out; 
    animation-fill-mode: forwards;
  }

  .check.active {
    opacity: 1;
    transform: translate3d(-4px,4px,0);
    transition: all .5s cubic-bezier(.49, 1.74, .38, 1.74);
    transition-delay: .2s;
  }

  @keyframes loading {
    30% {
      opacity:1; 
    }
    
    85% {
      opacity:1;
      transform: rotate(1080deg);
      border-color: #262626;
    }
    100% {
      opacity:1;
      transform: rotate(1080deg);
      border-color: #1c89ff;
    }
}
</style>
<?php
$shop_id = isset($_POST['shop_id']) ? $_POST['shop_id'] : ('');
$date_S = isset($_POST['date_S']) ? $_POST['date_S'] : date('');
$date_E = isset($_POST['date_E']) ? $_POST['date_E'] : date('');
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Cuttime_query';
$this->breadcrumbs=array(
	'剪髮時間查詢'
);
//列出店編	--- 比對資料 db1 -------------------------------------------------------	
$stores = array();
$storescode = array();
$storeTable = array();
$storeTable = ImpExcel::model()->findAll();

foreach ($storeTable as $storeT)
{
	$stores[$storeT->storecode] = $storeT->store;
}
//列出店編 --- 使用的資料 db2
$shopidAry = array();
			$shopidSql = "SELECT shop_id FROM imgrecog_log WHERE date = '20200801' GROUP BY shop_id";
			$shopidResult = Yii::app()->cuttimedb->createCommand($shopidSql)->queryAll();
			for ($i = 0; $i < count($shopidResult); $i++) {
				$shop_id = $shopidResult[$i]['shop_id'];
        $shopidAry[$shop_id] = $stores[$shop_id];} 
        // CVarDumper::dump($shopidAry);
//概念
// store = array (
// 	'007001' => XXXX
// )
// shopid = 007001
// store[shopid] = XXXX		
//------------------------------------------------------------------
//查詢
$shopAry = array();

$shop_id = $_POST['shop_id'];
$sidnum = $_POST['sidnum']; 
$service = $_POST['service']; 
$date_S = date('yymd',strtotime($_POST['date_S']));
$date_E = date('yymd',strtotime($_POST['date_E']));

if(isset($_POST['cuttime_query']))
{
	if($_POST['date_S'] =='' or $_POST['date_E'] =='')
	{
		yii::app()->user->setFlash('error','請選擇一段時間');
	}
	else
	{
		$sql = "SELECT * FROM imgrecog_log WHERE date BETWEEN '$date_S' AND '$date_E' ";
		if($shop_id != '') 
		 {
		  $sql .= "AND shop_id = '$shop_id' ";
		 }
		if($sidnum != '') 
		 {
		  $sql .= "AND sidnum = '$sidnum' ";
		 }
		  $sql .= "AND service = '$service' " ;

	   $shopAry = Yii::app()->cuttimedb->createCommand($sql)->queryAll();
		
		// CVarDumper::dump($sql);
		// CVarDumper::dump($shopAry);

		yii::app()->user->setFlash('success','成功，'.count($shopAry).'筆資料');
	}
	
}

//匯出----------------------------------------------------------------------------------------------------
$col    = array();
//欄位顯示的中文字,'ano'=>'調撥單號'
$title  = array();
//負責儲存輸出在畫面上的陣列
$colAry = array();             
$fileName = '';


if(isset($_POST['cuttime_export']))
{	
	if($_POST['date_S'] =='' or $_POST['date_E'] =='')
		{
			yii::app()->user->setFlash('error','請選擇一段時間');
		}
	else
	{
		$sql = "SELECT * FROM imgrecog_log WHERE date BETWEEN '$date_S' AND '$date_E' ";
		if($shop_id != '') 
		 {
		  $sql .= "AND shop_id = '$shop_id' ";
		 }
		if($sidnum != '') 
		 {
		  $sql .= "AND sidnum = '$sidnum' ";
		 }
		  $sql .= "AND service = '$service' " ;
	

		$shopAry = Yii::app()->cuttimedb->createCommand($sql)->queryAll();
		$col = $this->getcol();
		$title = $this->getTitle(TRUE);
		$countrow = count($shopAry)-1;

		if($countrow>0)
			{
			$data = $shopAry[0]['date'];
			$data = $shopAry[0]['shop_id'];
			$data = $shopAry[0]['sidnum'];
			$data = $shopAry[0]['service'];
			$data = $shopAry[0]['conti'];
			}
		
		$fileName = $this->exportEXCEL($qrydates,$col,$title,$shopAry);  
		$clickUrl =  "<a href='".Yii::app()->request->baseUrl. '/' . "tmp" . '/' .$fileName. "'> 點我下載</a>";
		Yii::app()->user->setFlash('success','匯出成功，共'.$countrow.'筆資料'."<br>".$clickUrl);  
	}
}


?>
<!-- //----------------------------------- -->
<html>
  <form method="post" name='myform' id='myform'>
  
    <p>日期區間：
        <input style="width:150px;" id="date_S" type="date" value="" name="date_S" value="<?php echo $date_S ?>" />    ~
        <input style="width:150px;" id="date_E" type="date" value="" name="date_E" value="<?php echo $date_E ?>"/> 
    </p>
    	<p>
        店編：<?php echo CHtml::dropDownList('shop_id',$shop_id,$shopidAry, array('style' => 'font-size: 18px' ,'empty'=>'全公司')); ?>
	</p>	
    <p>座位：
       <input name="sidnum" type="text" id="sidnum" value="" />
    </p>
    <p>服務項目：
        <select name ="service" id="service">
          <option value="cut">剪髮</option>
          <option value="wash">洗髮</option>
        </select>
    </p>
	<?php echo CHtml::submitButton('查詢',array('name'=>'cuttime_query','id'=>'cuttime_query','class'=>'button btn btn-outline-info'))?>
	<!-- <Button type="submit" name="cuttime_query" id="cuttime_query" class="btn btn-outline-info">送出</Button> -->
	<!-- <input type="submit" name="cuttime_query" id="cuttime_query" value="送出"> -->
	<?php echo CHtml::submitButton('匯出',array('name'=>'cuttime_export','class'=>'button2 btn btn-outline-info'))?>
  <!-- <button class="button btn btn-outline-info" name="cuttime_query" id="cuttime_query">Send</button> -->

    <div class="loader">
    <div class="check">
      <span class="check-one"></span>
      <span class="check-two"></span>
    </div>
  </div>

	<?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 

                echo "<div class='flash-$key'>" . $message . "</div>\n";
			}	
    ?>
	<?php if(isset($_POST['cuttime_query'])) : ?>
		<table>
			<thead>
				<tr>
					<th>日期</th>
					<th>店編</th>
					<th>座位</th>
					<th>服務</th>
					<th>時間</th>
				</tr>
			</thead>   
		 
	<?php
	for($i=0;$i<count($shopAry);$i++)
			{
				if($i%2==0) //偶數行顏色
				echo "<tr bgcolor='#ffffe0'>";
			else
				echo "<tr>";
					//echo "<td>".$resultAry[$i]->bname."</td>"; //非model不能用箭頭(->) 改用陣列['欄位名稱']
					echo "<td><small>".$shopAry[$i]['date']."</small></td>";
					echo "<td><small>".$shopAry[$i]['shop_id']."</small></td>";
					echo "<td><small>".$shopAry[$i]['sidnum']."</small></td>";
					echo "<td><small>".$shopAry[$i]['service']."</small></td>";
					echo "<td><small>".$shopAry[$i]['conti']."</small></td>";  
				echo "</tr>";
			}   
			
	?>
	</table>
	<?php endif ?>
  </form>
</html>

<script>
document.addEventListener('DOMContentLoaded', function () {
  var btn = document.querySelector('.button'),
      loader = document.querySelector('.loader'),
      check = document.querySelector('.check');
  
  btn.addEventListener('click', function () {
    loader.classList.add('active');    
  });
 
  loader.addEventListener('animationend', function() {
    check.classList.add('active'); 
  });
});

document.addEventListener('DOMContentLoaded', function () {
  var btn = document.querySelector('.button2'),
      loader = document.querySelector('.loader'),
      check = document.querySelector('.check');
  
  btn.addEventListener('click', function () {
    loader.classList.add('active');    
  });
 
  loader.addEventListener('animationend', function() {
    check.classList.add('active'); 
  });
});
</script>