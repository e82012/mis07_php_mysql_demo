<!--  highcharts 引入 defer延遲載入 以防干擾-->
<script src="https://code.highcharts.com/highcharts.js" defer></script>
<script src="https://code.highcharts.com/modules/exporting.js" defer></script>
<script src="https://code.highcharts.com/modules/export-data.js" defer></script>
<script src="https://code.highcharts.com/modules/accessibility.js" defer></script>

<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<!-- 暫放 -->

<script lang="javascript" src="https://cdn.sheetjs.com/xlsx-0.18.10/package/dist/xlsx.full.min.js"></script>
<!-- FireBase -->
<!-- <script src="https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/9.17.2/firebase-analytics.js"></script> -->


<style>
	.select2-container--open {
		z-index: 99999999999999;
	}

	/* input:read-only { background: cyan; } */

</style>
<b><h3>功能測試</h3></b>
<?php 
	echo $computetime ;
?>

<!-- 內頁標籤 -->
<ul class="nav nav-tabs">
  <!-- 臨時測試用 -->
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#menu_Test">臨時測試</a>
  </li>
  <!-- 功能測試 -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_1" title="mune_1">
		間隔週期分析
	</a>
  </li>
  <!-- PDF -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_2" title="mune_2">PDF</a>
  </li>
  <!-- 檔案上傳分析 -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_3" title="mune_3">檔案上傳分析</a>
  </li>
  <!-- SELECT套件測試 -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_4" title="mune_4">SELECT測試</a>
  </li>
  <!-- 資產管理view demo -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_5" title="mune_5">資產管理測試</a>
  </li>
  <!-- 刷票改版測試demo -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_6" title="mune_6">刷票改版測試</a>
  </li>
  <!-- 叫修分析dashboard -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_7" title="mune_7">叫修分析dashboard</a>
  </li>
  <!-- table套件測試 -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_8" title="mune_8">table測試</a>
  </li>
  <!-- _ -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_9" title="mune_9">_</a>
  </li>
  <!-- google日曆套件測試 -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_10" title="mune_10">日曆API測試</a>
  </li>
  <!-- st_service批次更新測試用 -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_11" title="mune_11">批次更新st_service</a>
  </li>
  <!-- linepay沙盒 -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_12" title="mune_12">linePay Sandbox</a>
  </li>
  <!-- highChartMap dashboard -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_map" title="mune_map">highChartMap</a>
  </li>
  <!-- ETH測試 -->
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu_ETH" title="menu_ETH">ETH測試</a>
  </li>
</ul>
<!-- Tab panes 使用include方式-->
<div class="tab-content">
	<!-- Include -->
	<?php 
		//panesA 週期分析 
		include_once("testPageX_Panes/menu_1.php");
		// panesB PDF
		include_once("testPageX_Panes/menu_2.php");
		// panesC TXT
		include_once("testPageX_Panes/menu_3.php");
		// panesD SELECT
		include_once("testPageX_Panes/menu_4.php");
		// panesE 資產管理測試
		include_once("testPageX_Panes/menu_5.php");
		// panesF 刷票改版測試
		include_once("testPageX_Panes/menu_6.php");
		// panesG 叫修分析dashboard
		include_once("testPageX_Panes/menu_7.php");
		// bootstrap-table 測試
		include_once("testPageX_Panes/menu_8.php");
		// 刷票測試
		include_once("testPageX_Panes/menu_9.php");
		// fullcalendar + google API 測試
		include_once("testPageX_Panes/menu_10.php");
		// 批次新增/更新 服務項目 st_setvice
		include_once("testPageX_Panes/menu_11.php");
		// linePay Sandbox 測試
		include_once("testPageX_Panes/linePay/index.php");
		// highChartMap
		include_once("testPageX_Panes/menu_map.php");
		// ETH測試
		include_once("testPageX_Panes/menu_ETH.php");


	?>
	<!-- JS include -->
	<script><?php include_once("testPageX_Panes/menu_4_script.js"); ?></script>
	<script><?php include_once("testPageX_Panes/menu_7_charts.js"); ?></script>

	<!-- 測試區 -->
	<div class="tab-pane container active" id="menu_Test">
		<form id = 'testForm' method="POST" enctype="multipart/form-data">
		<h4>測試用</h4>
		<div class="input-group mb-3">
			<input type="text" class = "form-control" id = 'testBox' name = 'testBox'>
			<!--  -->
			<button class="ui button" type="button" id = 'testJSBtn' name = 'testJSBtn' onclick="exportToCsv()">Test_exportToCsv</button>

		</div>
			<br>
			<div class="row">
				<!-- 羅馬數字轉換測試 -->
				<div class = "col-3">
					<div class="ui cards">
						<div class="blue card">
							<div class="content">
								<!-- <img class="right floated mini ui image" src="/images/avatar/large/elliot.jpg"> -->
							<div class="header" id = 'romanTitle'>
								羅馬數字轉換
							</div>
							<div class="meta">
								輸入數字轉成羅馬數字
							</div>
							<div class="description">
								<input class = 'form-control' type = "textbox" id = "romanT" name = "romanT" placeholder="請輸入數字">
								<input class = 'form-control' type = "textbox" id = "romanR" name = "romanR" placeholder="結果" readonly >
							</div>
							</div>
							<div class="extra content">
								<!-- <button class = "btn btn-outline-primary" type = "button" id = "romanBTN" name = "romanBTN" onclick="romanTrans()">送出</button> -->
								<div class="ui buttons">
									<button type = 'button' class="ui button" onclick="romanRemove()">清除</button>
										<div class="or"></div>
									<button type = 'button' class="ui positive button" onclick="romanTrans()">確認</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- hover 內容 -->
				<div class = "col-3">
					<div class="ui cards">
						<div class="blue card blurring dimmable image" id = 'dimmerCard'>
							<!-- hover後的內容，需放在目標標籤內才會生效 -->
								<div class="ui inverted dimmer">
									<div class="content">
									<div class="center">
										<div class="ui vertical animated ui teal basic button" tabindex="0">
											<div class="hidden content">Hover</div>
											<div class="visible content">
												<i class="window close outline icon"></i>
											</div>
										</div>
									</div>
									</div>
								</div>
							<div class="content">
								<!-- <img class="right floated mini ui image" src="/images/avatar/large/elliot.jpg"> -->
							<div class="header" id = 'romanTitle'>
								測試hover
							</div>
							<div class="meta">
								測試hover
							</div>
							<div class="description">
							</div>
							</div>
							<div class="extra content">
								<button type = 'button' class="ui button" onclick="romanRemove()">清除</button>
							</div>
						</div>
					</div>
				</div>
				<!-- 動畫選單 + hover -->
				<div class = "col-3">
					<div class="ui cards">
						<div class="blue card">
							<div class="content">
								<div class="header" id = 'romanTitle'>
									動畫選單 + hover
									<div class="mini ui vertical animated ui teal basic button" tabindex="0">
										<div class="hidden content">Shop</div>
										<div class="visible content">
											<i class="shop icon"></i>
										</div>
									</div>
								</div>
								<div class="ui middle aligned animated selection list">
									<div class="item">
										<i class="shop icon"></i>
										<div class="content">
										<div class="header">A</div>
										</div>
									</div>
									<div class="item">
									<i class="shop icon"></i>
										<div class="content">
										<div class="header">B</div>
										</div>
									</div>
									<div class="item">
										<i class="shop icon"></i>    
										<div class="content">
										<div class="header">C</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<!-- 非同步測試 -->
				<div class = "col-3">
					<div class="ui cards">
						<div class="blue card">
							<div class="content">
								<div class="header" id = 'romanTitle'>
									非同步測試
								</div>
								<!-- 測試按鈕 -->
									<div class="meta">AJAX 測試	</div>
										<button class="ui button" type="button" id = 'testAjax' name = 'testAjax' onclick="asyncTest(this)">AJAX</button>
									<div class="meta">Fetch 測試	</div>
										<button class="ui button" type="button" id = 'testFetch' name = 'testFetch' onclick="asyncTest(this)">Fetch</button>
									<div class="meta">axios 測試	</div>
										<button class="ui button" type="button" id = 'testAxios' name = 'testAxios' onclick="asyncTest(this)">axios</button>
									<div class="meta"> 臨時	</div>
										<button class="ui button" type="button" id = 'testTemp' name = 'testTemp' onclick="firebaseAddData()">臨時</button>
							</div>
						</div>
					</div>
				</div>
			</div>

		</form>
	</div>
</div>



</div>
<!-- firebase -->
<script type="module">
	import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
  	import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-analytics.js";
	import { getFirestore, collection, addDoc ,setDoc,doc } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-firestore.js";

		const firebaseConfig = {
			apiKey: "AIzaSyCDwC-mLZwAh2DnK3v0So2okyv8cxP55VE",
			authDomain: "mis07-a8413.firebaseapp.com",
			projectId: "mis07-a8413",
			storageBucket: "mis07-a8413.appspot.com",
			messagingSenderId: "436145782570",
			appId: "1:436145782570:web:2bd3d431ea4c4be62d2853",
			measurementId: "G-16MG561NMJ"
		};
		// Initialize Firebase
		const app = initializeApp(firebaseConfig);
		  // 获取 Firestore 引用
		const db = getFirestore(app);
	// create-------
	async function firebaseAddData() {
		const docRef = doc(db, "users", "ddd"); // 自定义文档的 ID
		const subColRef = collection(docRef, "subcollection");
		const subDocRef = doc(subColRef, "authData"); //自定义子集合中的文档名称

		try {
		// 建立子集合
		await setDoc(subDocRef, {
			subName: "Sub document",
			subValue: 123
		});

		// 在主文档中设置字段，包括子集合标志
		await setDoc(docRef, {
			name: "ddd",
			age: 70,
			email: "ddd@example.com",
			subcollection: true
		}, { merge: true });
		console.log("Document successfully written!");
		} catch (e) {
		console.error("Error adding document: ", e);
		}
	}

	// script type='module' 要加入全域中，否則無法調用到function
	window.firebaseAddData = firebaseAddData;


</script>


<script>

// 載入-----------------
$(document).ready(function() {
	// semantic UI accordion開啟
	$('.ui.accordion').accordion();
	// console.log("start")
	//-------------------------
	const Toast = Swal.mixin({
	toast: true,
	position: 'top-start',
	showConfirmButton: false,
	timer: 1500,
	timerProgressBar: true,
		didOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		},
	});
		Toast.fire({
		icon: 'success',
		title: 'loading success'
	});
	//   BS5 tooltip
	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-toggle="tab"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
		return new bootstrap.Tooltip(tooltipTriggerEl)
	});
	// 測試存在
	// if(typeof testConst =='')
	// {
	// 	console.log("111");
	// }

});



//-------------------測試用-------------------------


// 羅馬數字轉換
function romanTrans()
{
	const romanRegular = [
		['IX','V','IV','I'],
		['XC','L','XL','X'],
		['CM','D','CD','C']
	]
	const inputV = document.getElementById("romanT").value;
	console.log(inputV);
	//M數量(千分位)
	var m_s = Math.floor(inputV / 1000);
	console.log("M = "+m_s);
	var m_value = '';
	for(var i = 0;i < m_s;i++)
	{
		m_value +='M';
	}
	//個 十 百 分位
	const inputV_split = inputV.split('');
	var otherValue = [];
	for(var i = inputV_split.length-1;i >inputV_split.length-4 ; i--)
	{
		otherValue.push(inputV_split[i])
	}
	console.log(otherValue)
	// 個 十 百 轉換
	var otherValueR = [];
	// 組合 個 十 百
	for(var i = 0; i<otherValue.length;i++)
	{
		otherValueR[i] = [];
		if(otherValue[i] == 9)
		{
			otherValueR[i]  = romanRegular[i][0];
		}
		if(otherValue[i] == 4)
		{
			otherValueR[i] = romanRegular[i][2];
		}
		if((otherValue[i] - 4) > 0)
		{
			if((otherValue[i] - 4) == 1)
			{
				otherValueR[i] = romanRegular[i][1];
			}
			if((otherValue[i] - 4) > 1 && (otherValue[i] - 4)<5)
			{
				otherValueR[i] = romanRegular[i][1];
				for(var j = 1;j<(otherValue[i] - 4);j ++)
				{
					otherValueR[i] += romanRegular[i][3];
				}
			}
		}
		if((otherValue[i] - 4) < 0)
		{
			for(var j =0; j<otherValue[i];j++)
			{
				otherValueR[i] += romanRegular[i][3];	
			}
		}
	}
	otherValueR.push(m_value); //加入千分(後)資料

	console.log(otherValueR);
	document.getElementById("romanR").value = otherValueR[3]+otherValueR[2]+otherValueR[1]+otherValueR[0];



	// 預寫狀況

}
function romanRemove()
{
	document.getElementById('romanT').value = '';
	document.getElementById('romanR').value = '';
}
// -------------------------------------------
/**@abstract
 * 非同步測試
 */
function asyncTest(n){
	const asyncType = n.id;
	const testvalue = asyncType;
	// 測試回傳statusCode結果
	const xhr = new XMLHttpRequest();
	const testData = {testvalue:asyncType}


	console.log(asyncType);

		switch(asyncType){
			case 'testAjax':
				$.ajax({
		        	url:"<?php echo Yii::app()->createUrl('Site/AjaxTest'); ?>",
		        	// url:"/jiteip/index.php/TbpPerform/AjaxAutoCreate",
		            type:'POST',
		            dataType:'JSON',
		            data:testData,
		            success:function(test){
						console.log("SUCCESS");
						console.log("status : " + xhr.status);
						ajax1result = test;
						// sweatalert，then再執行ajax
						Swal.fire({
							title: ajax1result+'<br>再次執行AJAX?',
							showCancelButton: true,
							confirmButtonText: '確定',
							cancelButtonText:'取消',
							}).then((result) => {
							/* Read more about isConfirmed, isDenied below */
							if (result.isConfirmed) {
								$.ajax({
									url:"<?php echo Yii::app()->createUrl('Site/AjaxTest'); ?>",
									// url:"/jiteip/index.php/TbpPerform/AjaxAutoCreate",
									type:'POST',
									dataType:'JSON',
									data:{
										testvalue:testvalue,
										//($post[name]) :var名稱
										//php後端呼叫方式 $_POST['itme']
									},
									success:function(test){
										console.log("再次執行成功");
										console.log(test);
										Swal.fire({
											title: ajax1result,
		
										})

									},
									error:function(){
										console.log("ERROR");
									}
							});
								}
							});
		            },
					// 錯誤吐event可找到backend錯誤訊息，在responseText內
		            error:function(e){
						console.log(e);
		            }
		    	});
				break;
			case 'testFetch':
				// console.log(data)
				fetch('<?php echo Yii::app()->createUrl('Site/AjaxTest'); ?>', 
				{
					method: 'POST',
					// body為fetch用來傳遞資料的地方
					body:JSON.stringify(testData),
					// mode: 'no-cors'
				})
				// 第一個then傳遞promise
				.then(response =>response.json()
				//第二個then處理 promise 資料
				).then(data=>{
					Swal.fire({
							title: data,
						})
					console.log(data)
				})
				.catch(err=>{
					console.log("Err");
					// Error :(
				})
				break;
			case 'testAxios':
				axios.post("<?php echo Yii::app()->createUrl('Site/AjaxTest'); ?>" ,{
					testvalue:asyncType,
				})
				.then(res=>{
					console.log(res)
				})
				.catch(err=>{
					console.log(err)

				})
				break;
		}
}
// -------------------------------------------

// -----

var Results = [
  ["Col1", "Col2", "Col3", "Col4"],
  ["Data", 50, 100, 500],
  ["Data", -100, 20, 100],
];

function sheet_to_workbook(sheet/*:Worksheet*/, opts)/*:Workbook*/ {
	var n = opts && opts.sheet ? opts.sheet : "Sheet1";
	var sheets = {}; sheets[n] = sheet;
	return { SheetNames: [n], Sheets: sheets };
}

function aoa_to_workbook(data/*:Array<Array<any> >*/, opts)/*:Workbook*/ {
	return sheet_to_workbook(XLSX.utils.aoa_to_sheet(data, opts), opts);
}
exportToCsv = function() {
	var wb = aoa_to_workbook(Results)
	XLSX.writeFile(wb, "test.xlsx"); // save to test.xlsx

}
// dimmer hover 畫面霧化測試
$('#dimmerCard').dimmer({
  on: 'hover'
});
$('.ui.sticky')
  .sticky({
    context: '#example1'
  })
;
</script>