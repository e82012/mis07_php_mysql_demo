<!-- fullcalendar -->
	<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5/main.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5/main.css">

	<!-- <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet'> -->
<link href='https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css' rel='stylesheet'>
<!-- <script src="https://cdn.jsdelivr.net/npm/@fullcalendar/bootstrap5@6.0.2/index.global.min.js"></script> -->
	<!-- <script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.5/index.global.min.js'></script> -->
	<!-- <script scr ='https://cdn.jsdelivr.net/npm/@fullcalendar/google-calendar@6.1.5/index.global.min.js'></script> -->

	<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js'></script>

	<!-- <script async defer src="https://apis.google.com/js/api.js" onload="gapiLoaded()"></script> -->
	<script async defer src="https://accounts.google.com/gsi/client" onload="gisLoaded()"></script>
	<div class="g-signin2" data-onsuccess="onSignIn" data-cookiepolicy="single_host_origin"></div>


<div class="tab-pane container fade" id="menu_10">
	<h3 style="display: inline;"> 
		fullcalendar<br><span class="badge bg-primary">https://fullcalendar.io/</span>
	</h3><br>
	<span class="badge bg-primary">暫時移除 gapiLoaded (包含script)</span>
	<hr>
	<div>
		<label>事件名稱</label>
			<input type = 'text' id = 'newEventTitle' name = 'newEventTitle' class='form-control w-25'placeholder = 'EventTitle' >
		<label>時間起</label>
			<input type = 'date' id = 'newEventSTime' name = 'newEventSTime' class='form-control w-25'placeholder = 'EventDate' >
		<label>時間迄</label>
			<input type = 'date' id = 'newEventETime' name = 'newEventETime' class='form-control w-25'placeholder = 'EventDate' >
		<p>Google Calendar API Quickstart事件加入</p>
			<button type='button' id = 'newCEventSub' name = 'newCEventSub' class='btn btn-outline-primary' onclick='insertEvent()'>新增事件</button>
			<button id="authorize_button" onclick="handleAuthClick()" class='btn btn-outline-primary'>Authorize</button>
			<button id="signout_button" onclick="handleSignoutClick()" class='btn btn-outline-danger'>Sign Out</button>
		<!-- <pre id="content" style="white-space: pre-wrap;"></pre> -->

	</div>
	<!-- 行事曆view -->
	<hr>

	<div id='calendarEl'></div>
	</div>


<script>
	// fullcalendar
	document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendarEl');
        var calendar = new FullCalendar.Calendar(calendarEl, {
			height: 450,
			timeZone:'UTC',
			// themeSystem: 'bootstrap5',
			initialView: 'listWeek',
			headerToolbar:{
				start: 'title', // will normally be on the left. if RTL, will be on the right
				center: '',
				end: 'listWeek dayGridMonth today prev,next' // will normally be on the right. if RTL, will be on the left
			},
			// GOOGLE API
			googleCalendarApiKey: 'AIzaSyAJlYYwSvR_ihPzOFm-_iX2npmtrPRu3_M',
			// events: {
			// 	googleCalendarId: 's15631102@stu.edu.tw',
			// }
			eventSources: [
				{
					googleCalendarId: 's15631102@stu.edu.tw',
					calendar: 'Calendar1',
					test:'sdasdasd',
				},
				{
					googleCalendarId: '70789b45eeaa000a52d880c7550fdec7e0f031ab7ecc1ea5d9b6529fdfdfcd1b@group.calendar.google.com',
					calendar: 'Calendar2'
				}
			],
			eventContent: function(e) {
				// console.log(e.event._def.title);
				// console.log(e.event._instance.range);

				return {
					// html: arg.event.title + '<br>' + arg.event.extendedProps.calendarId,
					html: e.event.title + '<br>' + e.event.source.internalEventSource.extendedProps.calendar,

				};
			},
        });
        calendar.render();

	});

	// TODO(developer): Set to client ID and API key from the Developer Console
	const CLIENT_ID = '504496424013-136bvgpltbv5pchibfj052cor8av0h56.apps.googleusercontent.com';
	const API_KEY = 'AIzaSyD6aUjl6BfEigETzqt-kDXOxWVpPejNrMU';

	// Discovery doc URL for APIs used by the quickstart
	const DISCOVERY_DOC = 'https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest';

	// Authorization scopes required by the API; multiple scopes can be
	// included, separated by spaces.
	const SCOPES = 'https://www.googleapis.com/auth/calendar';

	let tokenClient;
	let gapiInited = false;
	let gisInited = false;

	document.getElementById('authorize_button').style.visibility = 'hidden';
	document.getElementById('signout_button').style.visibility = 'hidden';
	/**
	 * Callback after api.js is loaded.
	 */
	// function gapiLoaded() {
	// 	gapi.load('client', initializeGapiClient);
	// }
	/**
	 * Callback after the API client is loaded. Loads the
	 * discovery doc to initialize the API.
	 */
	// async function initializeGapiClient() {
	// 	await gapi.client.init({
	// 	apiKey: API_KEY,
	// 	discoveryDocs: [DISCOVERY_DOC],
	// 	});
	// 	gapiInited = true;
	// 	maybeEnableButtons();
	// }
	/**
	 * Callback after Google Identity Services are loaded.
	 */
	function gisLoaded() {
		tokenClient = google.accounts.oauth2.initTokenClient({
		client_id: CLIENT_ID,
		scope: SCOPES,
		callback: '', // defined later
		});
		gisInited = true;
		maybeEnableButtons();
	}
	/**
	 * Enables user interaction after all libraries are loaded.
	 */
	function maybeEnableButtons() {
		if (gapiInited && gisInited) {
		document.getElementById('authorize_button').style.visibility = 'visible';
		}
	}
	/**
	 *  Sign in the user upon button click.
	 */
	function handleAuthClick() {
		tokenClient.callback = async (resp) => {
		if (resp.error !== undefined) {
			throw (resp);
		}
		document.getElementById('signout_button').style.visibility = 'visible';
		document.getElementById('authorize_button').innerText = 'Refresh';
		};

		if (gapi.client.getToken() === null) {
		// Prompt the user to select a Google Account and ask for consent to share their data
		// when establishing a new session.
		tokenClient.requestAccessToken({prompt: 'consent'});
		} else {
		// Skip display of account chooser and consent dialog for an existing session.
		tokenClient.requestAccessToken({prompt: ''});
		}
		
	}
		
	/**
	 *  Sign out the user upon button click.
	 */
	function handleSignoutClick() {
		const token = gapi.client.getToken();
		if (token !== null) {
		google.accounts.oauth2.revoke(token.access_token);
		gapi.client.setToken('');
		// document.getElementById('content').innerText = '';
		document.getElementById('authorize_button').innerText = 'Authorize';
		document.getElementById('signout_button').style.visibility = 'hidden';
		}
	}
	
	// 新增事件
	function insertEvent(){
		const event = {
			'summary': 'Google I/O 2023',
			'location': '800 Howard St., San Francisco, CA 94103',
			'description': 'A chance to hear more about Google\'s developer products.',
			'start': {
				'dateTime': '2023-04-01T09:00:00-07:00',
				'timeZone': 'America/Los_Angeles'
			},
			'end': {
				'dateTime': '2023-04-01T17:00:00-07:00',
				'timeZone': 'America/Los_Angeles'
			},
			'recurrence': [
				'RRULE:FREQ=DAILY;COUNT=2'
			],
			'attendees': [
				{'email': 'lpage@example.com'},
				{'email': 'sbrin@example.com'}
			],
			'reminders': {
				'useDefault': false,
				'overrides': [
				{'method': 'email', 'minutes': 24 * 60},
				{'method': 'popup', 'minutes': 10}
				]
			}
		};

		const request = gapi.client.calendar.events.insert({
			'calendarId': '70789b45eeaa000a52d880c7550fdec7e0f031ab7ecc1ea5d9b6529fdfdfcd1b@group.calendar.google.com',
			'resource': event
		});

		request.execute(function(event) {
			console.log('Event created: ' + event.htmlLink);
		});
	}



	



</script>