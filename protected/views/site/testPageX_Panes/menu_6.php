<script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script>

<div class="tab-pane container" id="menu_6">
		<form method="POST" id = 'formTest' name = 'formTest'>
			<h2>Type 1</h2>
			<div class="card">
				<div class="card-body">
					<table class="table table-hover">
					<tr>
						<th>門市名稱 :</th>
						<th>
						員工編號 :
						<div class="form-select">
							<select class="form-select">
							<option>選擇</option>
							</select>
						</div>
						</th>
					</tr>
					<?php
					for ($i = 1; $i < 6; $i++) {
						echo "<tr>";
						echo "<td width='50%'>票券條碼 #$i : ";
						echo "<input id='input_$i' name='input_$i' class='form-control form-control-sm' type='text' placeholder='票券條碼$i' onkeypress='titCheck(this,event)'>";
						echo "</td>";
						echo "<td>選擇服務:";
						echo "<p id='check_$i' name='check_$i'></p>";
						echo "</td>";
						echo "</tr>";
					}
					?>
					</table>
				</div>
			</div>
			<h2>刷票測試/讀取qrcode<h2>
			<span class="badge bg-primary">https://unpkg.com/html5-qrcode</span>
			<div class="card">
				<div class="card-body">
					<!-- qr掃描欄位 -->
					<?php 
					for($i = 0; $i < 5; $i++){
					echo "<div class='mb-3'>";
					echo "<input type='file' id='cameraCode_".$i."' name='cameraCode_".$i."' accept='image/*' capture='camera' onchange='loadfile(this)' style='display:none'>";
					echo "<label class='btn btn-outline-primary' id='scanBTN_".$i."' for='cameraCode_".$i."'>掃描</label>";
					echo "<br>刷票結果 : <p id='readResult_".$i."'></p>";
					echo "</div>";
					}
					?>
				</div>
			</div>
			<div id="reader" style="display:none"></div>

		</form>
	</div>

	<script>
		// type1 
		function titCheck(n,e)
		{
			const titId = n.id;
			const num = titId.split("_")[1];

			//票卷內的服務項目
			const p_checkbox = document.getElementById("check_"+num).innerText =''; //checkbox選項先清空
			const titContent = n.value.split(";");
			const titService = [];
			for(var i = 7;i <titContent.length;i++)
			{
				titService.push(titContent[i]);

				// 跳出服務項目選擇
				// checkbox + label
				var ser_checkbox = document.createElement("INPUT"); //checkbox
					ser_checkbox.setAttribute("type","checkbox");
					ser_checkbox.setAttribute("id",titId+"_"+titContent[i]);
					ser_checkbox.setAttribute("name",titId+"_"+titContent[i]);
					ser_checkbox.setAttribute("value",titContent[i]);
					ser_checkbox.setAttribute("checked",true);
				document.getElementById("check_"+num).appendChild(ser_checkbox);
				var ser_name = document.createTextNode(titContent[i]+"[服務名稱]"); //label TEXT
				var ser_label = document.createElement("LABEL"); //label
					ser_label.setAttribute("for",titId+"_"+titContent[i]);
					ser_label.appendChild(ser_name);
				document.getElementById("check_"+num).appendChild(ser_label);

				
			}
			console.log(titService);

			// 輸入結束換行
			// console.log(n)
			// console.log(e)
			if(e.keyCode ===13)
			{
				const nextNum = parseInt(num) + 1;
				document.getElementById("input_"+nextNum).focus();
			}
		}

		// type2
		function loadfile(n)
		{
			// 原本放在外面 document ready
			// var file = document.getElementById("cameraCode").files[0];
			const qrcodeID = n.id;
			const restltID = 'readResult_'+qrcodeID.split('_')[1];
			const scanBTN = 'scanBTN_'+qrcodeID.split('_')[1];
			const html5QrCode = new Html5Qrcode(/* element id */ "reader");

			var files = event.target.files;
			// console.log(files[0])
			html5QrCode.scanFile(files[0], true)
			.then(decodedText => {
				// success, use decodedText
				console.log(decodedText);
				document.getElementById(restltID).innerText = "票號"+decodedText
				document.getElementById(scanBTN).innerText = '掃描成功';
				document.getElementById(scanBTN).className = 'btn btn-outline-success'
			})
			.catch(err => {
				// failure, handle it.
				console.log(`Error scanning file. Reason: ${err}`)
				document.getElementById(restltID).innerText = `無法辨識: ${err}`
				document.getElementById(scanBTN).innerText = '重新掃描';
				document.getElementById(scanBTN).className = 'btn btn-danger'

			});
		}
	</script>