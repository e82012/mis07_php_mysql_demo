<div class="tab-pane container" id="menu_7">
		<form method="POST" id = 'formTest' name = 'formTest'>
			<table class="table">
				<thead>
					<tr>
						<td>年月<br>
							<input type="month" class="form-control" id = "qry_month_1" name = "qry_month_1" value = '<?php echo $ym_date ?>'>
						</td>
						<td>叫修項目
							<select class="form-select" id = "qry_mItemNo" name = "qry_mItemNo" aria-label="選擇叫修項目">
								<?php 
									echo "<option value = ''>全部</option>";
									foreach($m_itemList as $mkey => $m)
									{
										if($qry_mItemNo == $mkey)
											echo "<option value = '".$mkey."' selected >".$m."</option>";
										else
											echo "<option value = '".$mkey."' >".$m."</option>";
									}
								?>
							</select>
						</td>
						<td><br>
							<button type="submit" id = "qry_call" name = "qry_call" class="btn btn-outline-secondary">查詢</button>
						</td>
					</tr>
				</thead>
			</table>
			<?php if(isset($_POST['qry_call'])):?>
			<div class="card border-secondary mb-3">
				<div class="card-header">叫修分析DashBoard</div>
				<div class="card-body text-secondary">
					<!-- ROW1 -->
					<div class = "row">
						<div class = "col">
							<div class="card border-secondary mb-3">
								<!-- <div class="card-header">全公司 - 叫修件數總計</div> -->
								<div class="card-body text-secondary">
									<div id = "callHistory_chart"></div>
									<h5 class="card-title "><?php echo "叫修件數總計 : ".$call_allTotal['callHistory'] ?></h5><br>
								</div>
							</div>
						</div>
						<div class = "col">
							<div class="card border-secondary mb-3">
								<!-- <div class="card-header">全公司 - 尚未完修件數</div> -->
								<div class="card-body text-secondary">
									<div id = "callTime_noFix_chart"></div>
									<h5 class="card-title"><?php echo "尚未完修件數 : ".$call_allTotal['callTime_noFix'] ?></h5><br>
								</div>
							</div>
						</div>
						<div class = "col">
							<div class="card border-secondary mb-3">
								<!-- <div class="card-header">全公司 - 本月叫修件數</div> -->
								<div class="card-body text-secondary">
									<div id = "callTime_ThisMonth_chart"></div>
									<h5 class="card-title"><?php echo "本月叫修件數 : ".$call_allTotal['callTime_ThisMonth'] ?></h5>
									#平均叫修件數 : <?php if($call_allTotal['callTime_ThisMonth'] != 0) echo round($call_allTotal['callTime_ThisMonth']/180,2) ?>
								</div>
							</div>
						</div>
						<div class = "col">
							<div class="card border-secondary mb-3">
								<!-- <div class="card-header">全公司 - 本月完修件數</div> -->
								<div class="card-body text-secondary">
									<div id = "callTime_fixMonth_chart"></div>
									<h5 class="card-title"><?php echo "本月完修件數 : ".$call_allTotal['callTime_fixMonth'] ?></h5><br>
								</div>
							</div>
						</div>
					</div>
					<!-- ROW2 -->
					<div class = "row">
						<div class = "col">
						<div class="card border-secondary mb-3">
								<!-- <div class="card-header">全公司 - 叫修件數總計</div> -->
								<div class="card-body text-secondary">
									<div class="card-header bg-light">
										完修率
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">
											本月完修率 : <?php if($call_allTotal['callTime_fixMonth'] != 0 ) echo round(($call_allTotal['callTime_fixMonth']/$call_allTotal['callTime_ThisMonth'])*100,2) ?>% <br>
											總計完修率 : <?php if($call_allTotal['callTime_fixMonth'] != 0 ) echo round(($call_allTotal['callTime_fixMonth']/$call_allTotal['callTime_noFix'])*100,2) ?>%
										</li>
									</ul>
									<div class="card-header bg-light">
										超時未修
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">
											超過3個月未修件 : <?php if($call_allTotal['callTime_over3'] != 0 ) echo $call_allTotal['callTime_over3'] . " (" .round(($call_allTotal['callTime_over3']/$call_allTotal['callTime_noFix'])*100,2) ."%)" ?><br>
											超過6個月未修件 : <?php if($call_allTotal['callTime_over6'] != 0 ) echo $call_allTotal['callTime_over6'] . " (" .round(($call_allTotal['callTime_over6']/$call_allTotal['callTime_noFix'])*100,2) ."%)" ?>									
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif ?>
		</form>

	</div>

<script>
//**依據區域改變門市下拉清單 */
function getStoreList(sn)
{
	// console.log(sn.value);
	const area_id = sn.value;
	document.getElementById('qry_store_1').innerHTML = '';
	$.ajax({
		url:"<?php echo Yii::app()->createUrl('Site/AjaxGetStore'); ?>",
			type:'POST',
			dataType:'JSON',
			data:{
				area_id:area_id,
				//($post[name]) :var名稱
				//php後端呼叫方式 $_POST['itme']
			},
			success:function(data){
				// console.log("AJAX"+data);
				data.forEach(element => {
					// console.log(element);
					document.getElementById('qry_store_1').innerHTML +=
					"<option value='"+element['storecode']+"'>"+element['storename']+"</option>"
				});
				// alert("ajax成功");
			},
			error:function(){alert("ajax失敗");}
		});
}

</script>
