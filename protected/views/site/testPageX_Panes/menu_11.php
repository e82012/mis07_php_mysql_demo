<div class="tab-pane container fade" id="menu_11">
	<h3>批次更新st_service<br>
		<span class="badge bg-primary">tbp_st_service</span><br>
	</h3>

	<div id='calendarEl'></div>
	<table class = 'table'>
		<tr>
			<td>
				<div class = 'form-floating'>
                    <input type="text" name="serviceno"  id = "serviceno" class = 'form-control' placeholder="serviceno"  > 
                    <label for="performDate" class="form-label" >serviceno 服務代碼</label>
                </div>
			</td>
		</tr>
		<tr>
			<td>
				<div class = 'form-floating'>
                    <input type="text" name="storeAry"  id = "storeAry" class = 'form-control'   placeholder="storeAry"> 
                    <label for="performDate" class="form-label" >storecode 多門市請用 , 隔開</label>
                </div>
			</td>
		</tr>
		<tr>
			<td>
				<!-- <button class="ui primary basic button">檢查</button> -->
				<!-- <button name = 'st_new' id = 'st_new' class="ui primary basic button" onclick="st_check()">檢查</button> -->
				<button type="button" class="ui primary basic button" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="st_check('n1')">
					新增
				</button>
				<button type="button" class="ui primary basic button" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="st_check('u1')">
					更新opt
				</button>
			</td>

		</tr>
	</table>
</div>
<!-- The Modal 檢查用-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" data-bs-backdrop="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">確認服務項目</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
		<table class = 'table table-hover' id = 'check01_table' style = 'display:none'>
			<tr>
				<th>storecode</th>
				<th>storename</th>
				<th>memo</th>
				<th>serviceno</th>
				<th>machineno</th>
				<th>queue</th>
				<th>複製新增</th>
			</tr>
			<!-- 檢查後秀出結果 -->
			<?php 
				for($i=0;$i<3;$i++)
				{
					echo "<tr>";
						echo "<td id = 'storecode_"."$i'></td>";
						echo "<td id = 'store_"."$i'></td>";
						echo "<td id = 'memo_"."$i'></td>";
						echo "<td id = 'serviceno_"."$i'></td>";
						echo "<td id = 'machineno_"."$i'></td>";
						echo "<td id = 'queue_"."$i'></td>";
						echo "<td><button type='button' class='btn btn-primary' onclick='st_new($i)'>select</button></td>";
					echo "</tr>";
				}
			?>

		</table>
		<div  id = 'check02_table' style = 'display:none'></div>

		<!-- <table class = 'table table-hover' id = 'check02_table' style = 'display:none'>

		</table> -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">關閉</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
function testDDD(){
	console.log("fnctDDDDDD","5646546")
}

// 檢查
function st_check(i){
	// console.log("new")
	var serviceno = document.getElementById("serviceno").value;
	var storeAry = document.getElementById("storeAry").value.split(',');
	console.log(serviceno);
	console.log(storeAry);
	var tempStService = '';
	// 批次新增檢查
	if(i == 'n1')
	{
		// 顯示或隱藏報表
		document.getElementById('check01_table').style.display='';
		document.getElementById('check02_table').style.display='none';
		$.ajax({
			url:"<?php echo Yii::app()->createUrl('Site/Menu11AjaxStNew'); ?>",
				type:'POST',
				dataType:'JSON',
				data:{
					serviceno:serviceno,
					storeAry:storeAry,
					step:'n1',
					//($post[name]) :var名稱
					//php後端呼叫方式 $_POST['itme']
				},
				success:function(data){
					// console.log(data);
					tempStService = data;
					console.log(tempStService);
					// 將st_service查詢結果顯示於MODAL內確認
					for(var i = 0;i<3;i++)
					{
						document.getElementById("storecode_"+i).innerText = data[i]['storecode'];
						document.getElementById("store_"+i).innerText = data[i]['storename'];
						document.getElementById("memo_"+i).innerText = data[i]['memo'];
						document.getElementById("serviceno_"+i).innerText = data[i]['serviceno'];
						document.getElementById("machineno_"+i).innerText = data[i]['machineno'];
						document.getElementById("queue_"+i).innerText = data[i]['queue'];
					}


					// alert("ajax成功");
				},
				error:function(){alert("ajax失敗");}
			});
	}
	// 批次開啟檢查
	if(i == 'u1')
	{
		// 顯示或隱藏報表
		document.getElementById('check01_table').style.display='none';
		document.getElementById('check02_table').style.display='';
		console.log(i);
		$.ajax({
			url:"<?php echo Yii::app()->createUrl('Site/Menu11AjaxStNew'); ?>",
				type:'POST',
				dataType:'JSON',
				data:{
					serviceno:serviceno,
					storeAry:storeAry,
					step:'u1',
					//($post[name]) :var名稱
					//php後端呼叫方式 $_POST['itme']
				},
				success:function(data){
					// console.log(data);
					tempStService = data;
					console.log(tempStService);
					// 將st_service查詢結果顯示於MODAL內確認
					var tableTest = new Tabulator("#check02_table", {
							data:tempStService, //set initial table data
							// pagination:true, //啟動分頁功能enable.
							// paginationSize:10, //每頁顯示幾筆
							height:"600px",
							// height:"100%", //完全呈現
							layout:"fitColumns",
							// layout:"fitData", //欄寬根據資料自適應
							// movableRows:true, // 資料可移動
							// columnHeaderSortMulti:true, //表頭多重排序(有問題)
							// groupBy:"areaCode", //分群
							// groupClosedShowCalcs:true, //分群折疊後依然顯示計算結果
							// 表頭
							columns:[
									// {rowHandle:true, formatter:"handle", headerSort:false, frozen:true, width:30, minWidth:30}, //移動欄位icon
									{title:"店編", field:"storecode"},
									{title:"門市", field:"storename"},
									{title:"備註", field:"memo"},
									{title:"代碼", field:"serviceno"},
									{title:"mno", field:"machineno"},
									{title:"que", field:"queue"},
									{title:"狀態", field:"opt1"},
									],
							//排序 
							initialSort:[
								// {column:"areaCode", dir:"asc"}, //sort by this first
								// {column:"storecode", dir:"asc"}, //then sort by this second
							],
							footerElement:"<select class = 'form-control' id = 'optValue'><option value = '0'>關閉</option><option value = '1'>開啟</option></select><button type='button' class = 'btn btn-outline-primary' onclick = 'st_update()'>更新</button>", //add a custom button to the footer element

						});
	


					// alert("ajax成功");
				},
				error:function(){alert("ajax失敗");}
			});
	}
}
// 批次新增
function st_new(i){
	// console.log(i);
	var serviceno = document.getElementById("serviceno").value;
	var storeAry = document.getElementById("storeAry").value.split(',');
	// st服務項目資料
	var st_serviceData = []
	st_serviceData.push(document.getElementById("storecode_"+i).innerText);
	st_serviceData.push(document.getElementById("store_"+i).innerText);
	st_serviceData.push(document.getElementById("memo_"+i).innerText);
	st_serviceData.push(document.getElementById("serviceno_"+i).innerText);
	st_serviceData.push(document.getElementById("machineno_"+i).innerText);
	st_serviceData.push(document.getElementById("queue_"+i).innerText);
	console.log(st_serviceData);
	// 按下顯示LOADING
	Swal.fire({
		title:'新增中...',
		allowOutsideClick: false,
		didOpen:()=>{
			Swal.showLoading()
		}
	});

	$.ajax({
		url:"<?php echo Yii::app()->createUrl('Site/Menu11AjaxStNew'); ?>",
			type:'POST',
			dataType:'JSON',
			data:{
				serviceno:serviceno,
				storeAry:storeAry,
				st_serviceData:st_serviceData,
				step:'n2',
				//($post[name]) :var名稱
				//php後端呼叫方式 $_POST['itme']
			},
			success:function(data){
				console.log(data);
				Swal.fire(
					'新增成功',
					'共新增 '+ data.length +' 筆資料',
					'success'
				);
				// tempStService = data;
				// console.log(tempStService);


				// alert("ajax成功");
			},
			error:function(){alert("ajax失敗");}
		});

}
// 批次更新
function st_update(){
	var serviceno = document.getElementById("serviceno").value;
	var statusOpt = document.getElementById("optValue").value;
	var storeAry = document.getElementById("storeAry").value.split(',');
	console.log(statusOpt);
	Swal.fire({
		title:'更新中...',
		allowOutsideClick: false,
		didOpen:()=>{
			Swal.showLoading()
		}
	});
	$.ajax({
			url:"<?php echo Yii::app()->createUrl('Site/Menu11AjaxStNew'); ?>",
				type:'POST',
				dataType:'JSON',
				data:{
					serviceno:serviceno,
					storeAry:storeAry,
					statusOpt:statusOpt,
					step:'u2',
					//($post[name]) :var名稱
					//php後端呼叫方式 $_POST['itme']
				},
				success:function(data){
					// console.log(data);
					Swal.fire(
						'新增成功',
						'共更新 '+ data +' 筆資料',
						'success'
					);
				},
				error:function(){alert("ajax失敗");}
			});
}

</script>