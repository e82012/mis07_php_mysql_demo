<!-- bootstrap-table -->
<!-- <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.20.0/dist/bootstrap-table.min.css"> -->
<script src="https://unpkg.com/bootstrap-table@1.20.0/dist/bootstrap-table.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.20.0/dist/themes/bulma/bootstrap-table-bulma.min.css">
<script src="https://unpkg.com/bootstrap-table@1.20.0/dist/themes/bulma/bootstrap-table-bulma.min.js"></script>
<!-- BS欄位過濾 -->
<script src="https://unpkg.com/bootstrap-table@1.20.0/dist/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
<!-- 自動重整(沒用???) -->
<script src="https://unpkg.com/bootstrap-table@1.20.0/dist/extensions/auto-refresh/bootstrap-table-auto-refresh.min.js"></script>
<!-- bs匯出功能 -->
<script src="https://cdn.jsdelivr.net/npm/tableexport.jquery.plugin@1.10.21/tableExport.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/tableexport.jquery.plugin@1.10.21/libs/jsPDF/jspdf.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/tableexport.jquery.plugin@1.10.21/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.20.0/dist/extensions/export/bootstrap-table-export.min.js"></script>
<!-- cloudTable datatables-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.js"></script>
<!-- tabulator-tables-->
<!-- 5.2.7 -->
    <link href="https://unpkg.com/tabulator-tables@5.2.7/dist/css/tabulator.min.css" rel="stylesheet">
    <script type="text/javascript" src="https://unpkg.com/tabulator-tables@5.2.7/dist/js/tabulator.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/5.2.7/css/tabulator_semanticui.min.css" integrity="sha512-Bz91kTWn/mV1OieoQO16aqSo7ZnXepG7AnYqaN16lwfFaxIC1eEdrwq1GtYoC/SOrrkZ8Jp/tm/Pku8GhIPCvA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<!-- 本文 -->
<div class="tab-pane container" id="menu_8">
<h3>TABLE元件測試<br>
    <span class="badge bg-primary">bootstrap-table@1.20.0</span>
    <span class="badge bg-primary">cloudTable datatables</span>
    <span class="badge bg-primary">AJAX</span>
    <span class="badge bg-primary">tabulator-tables</span>
    <p>
    </h3>
    <form method="POST" id = 'form_H' name = 'form_H'>
    <!-- 表一 bootstrap-table 可修改內容-->
    <div class="card">
        <div class="card-header"  data-bs-toggle="collapse" href="#collapseTable1"  aria-expanded="false">【bootstrap-table】JIT門市</div>
        <div class="card-body collapse" id = "collapseTable1">
            <button type="button" id = 'jsonBTN' name = 'jsonBTN' class="btn btn-outline-secondary">更新資料 </button>
            <input type="submit" id = 'valueSubmit' name = 'valueSubmit' class="btn btn-outline-secondary" value="資料送出">
            <table
                id="table_bs"
                data-toolbar="#toolbar"

                data-show-columns="true"
                data-show-columns-toggle-all="true"

                data-detail-view="true"
                data-detail-view-icon="true"
                data-detail-view-by-click="false"

                data-show-export="true"
                data-detail-formatter="detailFormatter"
                data-pagination="true"
                data-page-list="[10, 20, 30, 100, all]"

                data-filter-control="true"
                data-url="images/storeList.json"

            >
            <!-- data-url="images/storeList.json" -->
            <!-- data-show-refresh="true" -->
            <!-- data-ajax="ajaxRequest" -->
            <!-- data-show-refresh="true" -->
            <thead>
                <tr>
                <th data-field="area_id" data-filter-control="select" data-filter-control-placeholder="篩選區域"></th>
                <th data-field="storecode" data-filter-control="input" data-filter-control-placeholder="輸入店編"></th>
                <th data-field="storename" data-filter-control="select" data-filter-control-placeholder="篩選門市"></th>
                <!-- <th data-field="memo"
                    data-formatter="inputFormatter"
                    data-events="inputEvents"
                    >memo
                </th> -->
                <!-- <th data-field="memo" data-filter-control="input" data-filter-control-placeholder="memo"></th> -->

                </tr>

            </thead>
            </table>
        </div>
    </div>
    <br>
    <!-- 表二 bootstrap-table 快篩地圖試做-->
    <div class="card">
        <div class="card-header" data-bs-toggle="collapse" href="#collapseTable2">【bootstrap-table】快篩試劑地圖</div>
        <div class="card-body collapse" id = "collapseTable2" >
            <button type="button" id = 'jsonBTN2' name = 'jsonBTN2' class="btn btn-outline-secondary">更新資料</button>
            <table
                id="table_bs2"
                data-filter-control="true"
                data-detail-formatter="fst_detailFormatter"
                data-detail-view="true"
                data-detail-view-icon="false"
                data-detail-view-by-click="true"
                data-pagination="true"
                data-page-list="[10, 20, 30, 100, all]"
                data-url="images/fstMap.json"
            >
            <thead>
                <tr>
                <!-- <th data-field="0">醫事機構代碼</th> -->
                <th data-field="10" data-filter-control="select" data-filter-control-placeholder="縣市"></th>
                <th data-field="1" >醫事機構名稱</th>
                <th data-field="2" data-filter-control="input" data-filter-control-placeholder="醫事機構地址"></th>
                <!-- <th data-field="3">經度</th> -->
                <!-- <th data-field="4">緯度</th> -->
                <!-- <th data-field="5">電話</th> -->
                <!-- <th data-field="6">廠牌</th> -->
                <th data-field="7">存貨數量</th>
                <!-- <th data-field="8">來源時間</th> -->
                <!-- <th data-field="9">備註</th> -->


            </thead>
            </table>
        </div>
    </div>
    <br>
    <!-- 表三 AJAX測試 分頁 -->
    <div class="card">
        <div class="card-header" data-bs-toggle="collapse" href="#collapseTable3">【Ajax】分頁試做</div>
        <div class="card-body collapse" id = "collapseTable3" >
            <select class = "form-select" id = 'rowSelect' name = 'rowSelect'>
                <option value = '5'>5</option>
                <option value = '10'>10</option>
                <option value = '99999999'>All</option>
            </select>
            <!-- 橫幅json按鈕 -->
            <div class="d-grid">
                <button type = "button" class = "btn btn-outline-secondary btn-sm" id = "ajax_load" name = "ajax_load" onclick="ajaxLoad()">AJAX LOAD</button>
            </div>
            <!-- <button type = "button" id = "ajax_find" name = "ajax_find" onclick="ajaxFind()">AJAX Find</button> -->
            <table id = "jsTable" name = "jsTable" class = "table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>區域編號</th>
                        <th>區域名稱</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <!-- 分頁按鈕 -->
            <div>
                <div class="btn-group btn-group-sm" id = 'pagination_js' name = 'pagination_js'>
                    <button type="button" class="btn btn-outline-primary" id = "pp" value = "pp" onclick="pageBTN(this)"> ‹ </button>
                    <button type="button" class="btn btn-outline-primary" name = "page_btn_first" value = "1" style="display:none" onclick="pageBTN(this)">...</button>
                    <button type="button" class="btn btn-primary" name = "page_btn" value = "1" onclick="pageBTN(this)" >1</button>
                    <button type="button" class="btn btn-outline-primary" name = "page_btn" value = "2" onclick="pageBTN(this)">2</button>
                    <button type="button" class="btn btn-outline-primary" name = "page_btn" value = "3" onclick="pageBTN(this)">3</button>
                    <button type="button" class="btn btn-outline-primary" name = "page_btn_last" value = "4" style="display:''" onclick="pageBTN(this)">...</button>
                    <button type="button" class="btn btn-outline-primary" id = "nn" value = "nn" onclick="pageBTN(this)"> › </button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!-- 表四 cloud table datatables 測試 -->
    <div class="card">
        <div class="card-header" data-bs-toggle="collapse" href="#collapseTable4">datatables試做</div>
        <div class="card-body collapse" id = "collapseTable4" >
            <table id="datatables_04" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>areaCode</th>
                        <th>區域名稱</th>
                        <th>區經理</th>
                        <th>員編</th>
                        <th>操作</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <br>
    <!-- 表五 tabulator-tables 測試 -->
    <div class="card">
        <div class="card-header" data-bs-toggle="collapse" href="#collapseTable5" >tabulator-tables試做</div>
        <div class="card-body collapse" id = "collapseTable5">
            <span class="badge rounded-pill bg-primary">ajaxParams參數後端使用GET方式接收</span>
            <div id="example-table"></div>
        </div>
    </div>
    <!--  -->
    <!-- <button type='button' id='tableDL' class = 'btn btn-outline-primary'>download</button> -->
    </form>
</div>



<script>
// 這段等同於$(document).ready(function(){})
$(function() {
    // 表一 boostrap table -----------------------------
    $('#table_bs').bootstrapTable();
    $('#jsonBTN').click(function(){
        console.log("jsonBTN");
        $.ajax({
            url:"<?php echo Yii::app()->createUrl('Site/AjaxGetJsonData'); ?>",
                type:'POST',
                dataType:'JSON',
                data:{
                    // selectedObj:selectedObj,
                    //($post[name]) :var名稱
                    //php後端呼叫方式 $_POST['itme']
                },
                success:function(text){
                    // console.log("AJAX"+text);
                    $('#table_bs').bootstrapTable('refresh')
                    alert("ajax成功");
                    
                },
                error:function(){alert("ajax失敗");}
            });
    });
    // 表單細項顯示
    function detailFormatter(index, row) {
        var html = []
        // console.log(row)
        var i = 0
        $.each(row, function (key, value) {
            if(i >2)
            {
                html.push('<p><b>' + key + ':</b> ' + value + '</p>')
                if(key == 'memo'){
                    html.push('<p><b>memo修改:</b><br> <textarea onchange = "memoChange(this)" cols="30" rows="3" id = "'+row['storecode']+'_memo_detail_'+index+'" name = "'+row['storecode']+'_memo_detail_'+index+'">'+value+'</textarea> </p>')
                    html.push("<button type='button' class = 'btn btn-outline-secondary'>修改送出</button>")
                    // html.push('<p><b>memo修改:</b> <input type = "textarea" id = "'+row['storecode']+'_memo" name = "'+row['storecode']+'_memo" value = "'+value+'"> </p>')
                }
            }
            i++
        })
        return html.join('')
    }
    // input欄位
    $table = $('#table_bs')
    function inputFormatter(index,row) {
        console.log(index)
        // console.log(index)
        // console.log(row['storecode'])
        // console.log(row)
        var content = row['memo'] ? row['memo'] : ''
        // console.log(detail_id)
        return '<input type="textbox" id = "'+row['storecode']+'_memo" name = "'+row['storecode']+'_memo" value = "' + content + '" />'
    }
    window.inputEvents = {
        'change :input': function (e, value, row, index) {
            row.memo = $(e.target).val()
            // console.log(row['memo'])
            $table.bootstrapTable('updateRow', {
            index: index,
            row: row,
            // content:row.content
            })
            // console.log(e)
        }
    }
    // 修改內容
    function memoChange(index){
        var memo_id = index.id
        var memo_list = memo_id.split("_")
        // console.log(memo_list[3])
        $table.bootstrapTable('updateRow', {
            index: memo_list[3],
            row:{
                memo:index.value
            }
        })
    }
    // 表二 boostrap table ------------------------
    $('#table_bs2').bootstrapTable();
    $('#jsonBTN2').click(function(){
        $.ajax({
            url:"<?php echo Yii::app()->createUrl('Site/AjaxGetFstData'); ?>",
                type:'POST',
                dataType:'JSON',
                data:{
                    // selectedObj:selectedObj,
                    //($post[name]) :var名稱
                    //php後端呼叫方式 $_POST['itme']
                },
                success:function(text){
                    // console.log("AJAX"+text);
                    $('#table_bs2').bootstrapTable('refresh')
                    alert("ajax成功");
                    
                },
                error:function(){alert("ajax失敗");}
            });
    });
    // 表二 細項顯示
    function fst_detailFormatter(index,row){
        var html = []
        var titleMatch = [
            "醫事機構代碼",
            "醫事機構名稱",
            "醫事機構地址",
            "經度",
            "緯度",
            "電話",
            "廠牌",
            "存貨數量",
            "來源時間",
            "備註",
            "縣市"
        ]
        // console.log(row)
        var i = 0
        $.each(row, function (key, value) {
            // if(i >2)
            // {
                html.push('<p><b>' + titleMatch[key] + ':</b> ' + value + '</p>')
            // }
            // i++
        })
        return html.join('')
    }
    // 表四 datatable ---------------------------------
    var datatables_04 = $('#datatables_04').DataTable({
        // data: datatables_data,
        ajax: {
            type:'POST',
            dataType:'JSON',
            url:'<?php echo Yii::app()->createUrl('Site/AreaDataAry'); ?>',
            dataSrc:"",
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.12.0/i18n/zh-HANT.json" 
            },
        columns: [
            { data: 'id' },
            { data: 'areaCode'},
            { data: 'areaname' },
            { data: 'empname1' },
            { data: 'info.empno1' }, //巢狀,第二層
            // { defaultContent: "<button type ='button' class='btn btn-outline-primary' onclick = 'table4Btn()' >Click!</button>" }, 
            {render: function (data, type, row) {
              return '<button type="button" class="btn btn-warning btn-sm" onclick = "table4Btn()">編輯</button> ' +
                      '<button type="button" class="btn btn-danger btn-sm">刪除</button>'
              }
            }
        
        ],
        });
    
        // tableTest.download("csv", "data.csv");
        // table.Tabulator("download","csv", "data.csv");
        // $('#tableDL').click(function(){
        //     console.log("DL START");
        //     // var results = tableTest.getCalcResults();
        //     // console.log(results);
        //     tableTest.download("csv", "data.csv");
        //     // tableTest.download("xlsx", "data.xlsx", {}); //download table data as a CSV formatted file with a file name of data.csv
        // })
        // document.getElementById("tableDL").addEventListener("click", function(){
        //             console.log("DDD");
        //     tableTest.download("csv", "data.csv");
        // });

});
// 表五 tabulator --------------------------------------
var tableData = <?php echo json_encode($storeResult) ;?>;
var testGet = '1234';
var tableTest = new Tabulator("#example-table", {
    // ajax
    ajaxURL:'<?php echo Yii::app()->createUrl('Site/AreaDataAry'); ?>',
    ajaxParams:{testGet:testGet}, //ajax parameters參數，後端用get接收
    // data:tableData, //set initial table data
    // pagination:true, //啟動分頁功能enable.
    // paginationSize:10, //每頁顯示幾筆
    height:"600px",
    // height:"100%", //完全呈現
    layout:"fitColumns",
    // layout:"fitData", //欄寬根據資料自適應
    movableRows:true, // 資料可移動
    // columnHeaderSortMulti:true, //表頭多重排序(有問題)
    // groupBy:"areaCode", //分群
    groupClosedShowCalcs:true, //分群折疊後依然顯示計算結果
    // 表頭
    columns:[
            {rowHandle:true, formatter:"handle", headerSort:false, frozen:true, width:30, minWidth:30}, //移動欄位icon
            {title:"id", field:"id",bottomCalc:"count",topCalc:"sum"},
            {title:"區域ID", field:"areaCode"},
            {title:"店編", field:"areaname"},
            {title:"區經理", field:"empname1"},
            {title:"員編", field:"info.empno1"},
            // {title:"確認",element:"<button type='button' id='tableDL' class = 'btn btn-outline-primary' onclick = 'dlTable()'>download</button>"}

            ],
    //排序 
    initialSort:[
        // {column:"areaCode", dir:"asc"}, //sort by this first
        // {column:"storecode", dir:"asc"}, //then sort by this second
    ],
    footerElement:"<button type='button' id='tableDL' class = 'btn btn-outline-primary' onclick = 'dlTable()'>download</button>", //add a custom button to the footer element

    
});
function dlTable()
{
    console.log("DDD");
    tableTest.download("csv", "data.csv");
}
function table4Btn()
{
    $('#datatables_04').DataTable().ajax.reload()
    console.log( $('#datatables_04').DataTable().rows(this))
    console.log("OK")
}
// ----表三 分頁按鈕測試-----------------------
    //var全域變數 
    var ajaxAry = [];
    var ajaxCount  = 0;
    var pageNow = 1;
    var perRow = 5; //預先載入的每頁資料筆數
    // 資料載入
    function ajaxLoad(){
        // AJAX
        $.ajax({
            url:"<?php echo Yii::app()->createUrl('Site/AjaxArea2'); ?>",
                type:'POST',
                dataType:'JSON',
                data:{
                    // selectedObj:selectedObj,
                    //($post[name]) :var名稱
                    //php後端呼叫方式 $_POST['itme']
                },
                success:function(text){
                    // console.log(text);
                    ajaxAry = text;
                    // 先清空
                    $("#jsTable").find('tbody').empty();
                    pageNow = 1;
                    perRow = document.getElementById('rowSelect').value;
                    if(perRow =='99999999')
                        document.getElementById('pagination_js').style= "display:none";
                    else
                        document.getElementById('pagination_js').style= "";

                    // perRow預設幾筆資料於表格上
                    for(var i = 1;i <=perRow;i++)
                    {
                        $("#jsTable").find('tbody').append(
                            "<tr id = 'col_"+i+"' onclick='trCollpase(this)' data-bs-toggle='collapse' data-bs-target='#ajaxCollapse_"+ajaxAry[i]['id']+"'>"+
                                "<td>"+ajaxAry[i]['id']+"</td>"+
                                "<td>"+ajaxAry[i]['areaCode']+"</td>"+
                                "<td>"+ajaxAry[i]['areaname']+"</td>"+
                            "</tr>"+
                            "<tr class='collapse' id = 'ajaxCollapse_"+ajaxAry[i]['id']+"'>"+
                                "<td colspan = '3'>"+
                                    "<div class='card'>"+
                                    "<div class='card-header'>"+ajaxAry[i]['areaname']+"</div>"+
                                        "<div class='card-body'>"+
                                        "13131313"+
                                        "</div>"+
                                    "</div>"+
                                "</td>"+
                            "</tr>"
                        )
                    }
                    document.getElementsByName("page_btn_last")[0].value = Math.ceil(Object.keys(ajaxAry).length/perRow);
                    // document.getElementsByName("page_btn_last")[0].value = Math.ceil(Object.keys(ajaxAry).length/5);

                    alert("ajax成功");


                },
                error:function(){alert("ajax失敗");}
            });
    }
    // 翻頁功能
    function pageBTN(p){
        const pg = p.value; //按鈕指令 / 按鈕頁碼
        // console.log(pg);
        // 資料筆數
        ajaxCount = Object.keys(ajaxAry).length
        // 頁數 >>每頁顯示幾筆資料 無條件進位
        ajaxPage = Math.ceil(ajaxCount/perRow)
        if(ajaxCount !== 0 )
        {
            // 翻頁/選擇頁碼
            switch(pg)
            {
                // 前翻
                case "pp":
                if(pageNow != 1)
                    pageNow = (pageNow-1);
                break;
                // 後翻
                case "nn":
                if(pageNow != ajaxPage)
                    pageNow = (pageNow+1);
                break;
                default:
                    pageNow = eval(pg); //按鈕的value轉數字
                    break;

            }
            // 跳至頁首與頁尾
            switch(pageNow)
            {
                case ajaxPage:
                    document.getElementsByName("page_btn_last")[0].style = "display:none";
                    document.getElementsByName("page_btn_first")[0].style = "display:''";
                    break;
                case 1:
                    document.getElementsByName("page_btn_first")[0].style = "display:none";
                    document.getElementsByName("page_btn_last")[0].style = "display:''";

                    break;
                default:
                    document.getElementsByName("page_btn_last")[0].style = "display:''";
                    document.getElementsByName("page_btn_first")[0].style = "display:''";
                    break;

            }
            // 頁數選擇器，顯示3碼
            const pageList = [];
            const n_page = pageNow+1;
            const p_page = pageNow-1;
            if(p_page < 1 )
                pageList.push(pageNow,n_page,(n_page+1));
            else if(n_page > ajaxPage)
                pageList.push((p_page-1),p_page,pageNow);
            else
                pageList.push(p_page,pageNow,n_page);
            // 變換頁碼按鈕顯示
            const pageBTNlist = document.getElementsByName("page_btn");
            for(var i =0;i<=2;i++)
            {
                pageBTNlist[i].innerText = pageList[i];
                pageBTNlist[i].value = pageList[i];
                pageBTNlist[i].className = 'btn btn-outline-primary';
                if(pageList[i] == pageNow)
                {
                    pageBTNlist[i].className = 'btn btn-primary';
                }
            }
            // 先清空，再載入該頁資料 
            $("#jsTable").find('tbody').empty();
            for(var i = ((pageNow -1 )*perRow)+1;i <= (pageNow * perRow);i++)
            {
                if(typeof ajaxAry[i] !== 'undefined')
                {
                    $("#jsTable").find('tbody').append(
                        "<tr id = 'col_"+i+"' onclick='trCollpase(this)' data-bs-toggle='collapse' data-bs-target='#ajaxCollapse_"+ajaxAry[i]['id']+"'>"+
                            "<td>"+ajaxAry[i]['id']+"</td>"+
                            "<td>"+ajaxAry[i]['areaCode']+"</td>"+
                            "<td>"+ajaxAry[i]['areaname']+"</td>"+
                        "</tr>"+
                        "<tr class='collapse' id = 'ajaxCollapse_"+ajaxAry[i]['id']+"'>"+
                            "<td colspan = '3'>"+
                                "<div class='card'>"+
                                "<div class='card-header'>"+ajaxAry[i]['areaname']+"</div>"+
                                    "<div class='card-body'>"+
                                    "13131313"+
                                    "</div>"+
                                "</div>"+
                            "</td>"+
                        "</tr>"
                    )
                }
            }
        }
        else
            console.log("沒有資料")

    }
    // 內容開啟
    function trCollpase(col){
        "ajaxCollapse_"
        "col_"
        console.log(col.id)
    }
// --------------------



</script>

