$("#ajaxBTN").click(function(){
	(async () => {

	const { value: formValues } = await Swal.fire({
	title: 'AJAX連動選單',
	html:
		'<select id="sweetSelect1" name = "sweetSelect1" class = "sweetSelect1class" onchange ="optChange()">'+
		'<option value="01">A1</option>'+
		'<option value="02">B1</option>'+
		'<option value="03">C1</option>'+
		'<option value="04">D1</option>'+
		'<option value="05">E1</option>'+
		'</select>  ' +
		'<select id="sweetSelect2" name = "sweetSelect2" class = "sweetSelect2class">'+
		'<option value="01">A1</option>'+
		'<option value="02">B1</option>'+
		'<option value="03">C1</option>'+
		'<option value="04">D1</option>'+
		'<option value="05">E1</option>'+
		'</select>  ' ,
	didOpen: function(){
		$("#sweetSelect2").select2({
			language: 'zh-TW',
			width: '20%',
			// 最多字元限制
			maximumInputLength: 10,
			// 最少字元才觸發尋找, 0 不指定
			minimumInputLength: 0,
			// 當找不到可以使用輸入的文字
			tags: false,
			placeholder: "Select a state",
		});
	},
	focusConfirm: false,
	preConfirm: () => {
		return [
		document.getElementById('sweetSelect1').value,
		document.getElementById('sweetSelect2').value,
		]
	}
	})

	if (formValues) {
	Swal.fire(JSON.stringify(formValues))
	// $('#form6').submit();
	}

	})()
});

function optChange(){
	$.ajax({
		url:"<?php echo Yii::app()->createUrl('Site/AjaxTest'); ?>",
			type:'get',
			dataType:'JSON',
			// data:{
			// 	//($post[name]) :var名稱
			// 	//php後端呼叫方式 $_POST['itme']
			// },
			success:function(text){
				// console.log(text);
				var sweetSelect2Obj = document.getElementById("sweetSelect2");
				sweetSelect2Obj.options.length = 0;
				//新增
				for(var i = 0;i<text.length;i++)
				{
					sweetSelect2Obj.options.add(new Option(text[i]['name'],text[i]['value']));
				}
				alert("ajax成功");
			},
			error:function(){alert("ajax失敗");},
	});
	// document.getElementById("sweetSelect2").empty;
	// var ass1 = document.getElementsByClassName("sweetSelect2class");
	// for(var i = 0;i<ass1.length;i++)
	// {
	// 	console.log(ass1[i]);
	// }
	// 清空
}

//SELECT選項
$("#selectABC").select2({
	language: 'zh-TW',
	width: '20%',
	// 最多字元限制
	maximumInputLength: 10,
	// 最少字元才觸發尋找, 0 不指定
	minimumInputLength: 0,
	// 當找不到可以使用輸入的文字
	tags: false,
	placeholder: "Select a state",
});
// selectABC選擇後 AJAX
$("#selectABC").change(function(){
	var selectedObj = document.getElementById("selectABC").value;
	// console.log(selectedObj);
	$.ajax({
		url:"<?php echo Yii::app()->createUrl('Site/AjaxTest'); ?>",
			type:'POST',
			dataType:'JSON',
			data:{
				selectedObj:selectedObj,
				//($post[name]) :var名稱
				//php後端呼叫方式 $_POST['itme']
			},
			success:function(text){
				console.log("AJAX"+text);
				console.log(text)
				alert("ajax成功");
				
			},
			error:function(){alert("ajax失敗");}
		});
});
//select多選
$("#selectMutilTag").select2({
        language: 'zh-TW',
        width: '20%',
        // 最多字元限制
        maximumInputLength: 10,
        // 最少字元才觸發尋找, 0 不指定
        minimumInputLength: 0,
        // 當找不到可以使用輸入的文字
        tags: false,
		placeholder: "選擇",
		// option
        data: [
            { id: 11, text: "Blue" },
            { id: 12, text: "David"},
            { id: 13, text: "Judy" },
            { id: 14, text: "Kate" },
            { id: 15, text: "John" }
        ],
});

//flatpickr
$("#dateTest").flatpickr({
	dateFormat: 'Y/m/d',
	disableMobile: "true"
});
$("#dateTest_multiple").flatpickr({
	dateFormat: 'Y/m/d',
	mode: "multiple",
	disableMobile: "true"
});