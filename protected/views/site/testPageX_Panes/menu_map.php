<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/tw/tw-all.js"></script>
<style>
.floating-toast {
    position: fixed;
    top: 40%;
    left: 80%;
    transform: translate(-50%, -50%);
    z-index: 1000;
  }
</style>

<div class="tab-pane container fade" id="menu_map">
	<form method="POST" id = 'form_Map' name = 'form_map'>
			<!-- <div class="row">
				<div class = "col-2">
					<table class = "table table is-hoverable">
						<tr>
							<th colspan="2"> <?php echo date('Y-m-d');?> </th>
						</tr>
						<tr>
							<td>今日新增</td>
							<td ><?php echo $today_Data['local']['confirm']?></td>
						</tr>
						<tr>
							<td>男性</td>
							<td><?php echo $today_Data['local']['c_male']?></td>
						</tr>
						<tr>
							<td>女性</td>
							<td><?php echo $today_Data['local']['c_female']?></td>
						</tr>
						<tr>
							<th colspan="2">地區分布</th>
							<?php 
								// $local = array_keys($today_Data['local']['c_local']);
								// for($i=0;$i<count($local);$i++)
								// {
								// 	echo "<tr>";
								// 		echo "<td>".$local[$i]."</td>";
								// 		echo "<td>".$today_Data['local']['c_local'][$local[$i]]."</td>";
								// 	echo "</tr>";
								// }
							?>
						</tr>
						<tr>
							<th colspan="2">境外移入</th>
						</tr>
						<tr>
							<td>確診人數</td>
							<td><?php echo $today_Data['foreign']['confirm']?></td>
						</tr>
						<tr>
							<td>男性</td>
							<td><?php echo $today_Data['foreign']['c_male']?></td>
						</tr>
						<tr>
							<td>女性</td>
							<td><?php echo $today_Data['foreign']['c_female']?></td>
						</tr>
					</table>
				</div>
				<div class = "col-10" id="mapCharts"></div>
			</div> -->
		<div class = "row">
			<div class = "col-10" id="mapCharts"></div>
		</div>

	</form>
	<div class="toast show floating-toast">
		<div class="toast-header">
			<?php echo $total_info['dateMonth']?>
			<button type="button" class="btn-close" data-bs-dismiss="toast"></button>
		</div>
		<div class="toast-body">
			<?php 
				echo '<ul class="list-group list-group-flush">';
					echo '<li class="list-group-item"> 總計人數 :'.($total_info['男']+$total_info['女']).'</li>';
					echo '<li class="list-group-item">男性 : '.$total_info['男'].'</li>';
					echo '<li class="list-group-item">女性 : '.$total_info['女'].'</li>';
					echo '<li class="list-group-item">境外移入 : '.$total_info['import']['是'].'</li>';
				echo '</ul>'; 
			?>
		</div>	
	</div>
</div>


<script>
	//map Data
var today = new Date();
var c_data = <?php echo json_encode($map_value); ?>;
// console.log(c_data);

// Create the chart
	Highcharts.mapChart('mapCharts', {
		chart: {
			map: 'countries/tw/tw-all',
			height: 800,

		},

		title: {
			text: '當月登革熱案例分布'
		},

		subtitle: {
			// text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/tw/tw-all.js">Taiwan</a>'
			text :today
		},

		mapNavigation: {
			enabled: true,
			buttonOptions: {
				verticalAlign: 'bottom'
			}
		},

		colorAxis: {
			minColor: '#e6f2ff',
            maxColor: '#3399ff',
			min: 0
		},

		series: [{
			data: c_data,
			name: '確診人數',
			states: {
				hover: {
					color: '#BADA55'
				}
			},
			dataLabels: {
				enabled: true,
				format: '{point.name}'
			}
		}]
	});
</script>