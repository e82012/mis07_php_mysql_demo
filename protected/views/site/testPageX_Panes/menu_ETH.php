<script src="https://cdn.jsdelivr.net/npm/web3@1.5.0/dist/web3.min.js"></script>

<div class="tab-pane container fade" id="menu_ETH">
	<form method="POST" id = 'form_ETH' name = 'form_ETH'>
		<!-- 查詢錢包 -->
		<div class="mb-3">
		<label for="walletAddress" class="form-label">Enter an Ethereum address:</label>
		<div class="input-group">
			<input type="text" class="form-control" id="walletAddress" placeholder="Ethereum address">
			<button id="balance-button" class="btn btn-primary">Get Balance</button>
		</div>
		<p id="balance"></p>
		</div>

		<!-- 查詢交易 -->
		<div class="mb-3">
		<label for="transactionHash" class="form-label">Enter an Ethereum Hash:</label>
		<div class="input-group">
			<input type="text" class="form-control" id="transactionHash" placeholder="Ethereum Hash">
			<button id="getEthTrade" class="btn btn-primary">Get ETH Trade</button>
		</div>
		<p id="balance"></p>
		</div>

		<!-- 建立合約 -->
		<div class="mb-3">
		<label for="contractAddress" class="form-label">Create Contract</label>
		<div class="input-group">
			<input type="text" class="form-control" id="contractAddress" placeholder="Contract Address">
			<button id="createEthContract" class="btn btn-primary">Create</button>
		</div>
		<p id="balance"></p>
		</div>

	</form>
</div>

<script>
	/**
 * 區塊鏈測試
 * infura
 */
window.addEventListener('load', function() {
    // 檢查Web3是否已安裝
    if (typeof web3 !== 'undefined') {
        console.log('Web3已經安裝了');
        web3 = new Web3(web3.currentProvider);
    } else {
        console.log('Web3尚未安裝');
		const infuraKey  = '2fd912193df24acebab54f205ee97a47';
        web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/v3/"+infuraKey));
    }
    // 當用戶單擊“Get Balance”按鈕時，執行此功能
    document.getElementById('balance-button').addEventListener('click', function() {
        // 獲取用戶輸入的地址
        var address = document.getElementById('address').value;
        // 使用Web3.js獲取地址的餘額
        web3.eth.getBalance(address, function(error, result) {
            if (!error) {
                // 將結果轉換為Ether
                var balance = web3.utils.fromWei(result, 'ether');
                // 將餘額顯示在網頁上
                document.getElementById('balance').innerHTML = balance + ' ETH';
				console.log(balance + ' ETH' )
            } else {
                console.error(error);
            }
        });
    });
	// 以太坊區塊查詢
	web3.eth.getBlockNumber().then((result) => {
		console.log("Latest Ethereum Block is ",result);
	});
	// 查詢交易紀錄
	document.getElementById("getEthTrade").addEventListener('click',function(){
		const ethTradeHash = document.getElementById("ethTradeHash").value;
		// console.log(ethTradeHash)
		web3.eth.getTransaction(ethTradeHash, (error, txData) => {
			if (error) {
				// console.log('查詢交易失敗:', error);
				Swal.fire({
					title: '查詢交易失敗',
					text:error
				})
			} else {
				console.log('交易資料:', txData);
				const gas = txData['gas']; //這筆交易所使用的gas數量
				const gasPrice = txData['gasPrice']; //這筆交易所設定的gas價格
				// 轉換成eth
				const cost = gasPrice * gas; 
				const costInEth = web3.utils.fromWei(String(cost), 'ether');
				// console.log(costInEth)
				// console.log(gasPrice)

				Swal.fire({
					width:'55%',
					title: '交易資料',
					html:
					'Hash : ' + txData['hash'] + '<br>' + 
					'From : ' + txData['from']+ '<br>' + 
					'To : ' + txData['to']+ '<br>' +
					'gas : ' + gas + '<br>' + 
					'gasPrice : ' + gasPrice + '<br>' + 
					'cost : ' + cost + '<br>'+
					'costInEth : ' +  costInEth + ' (ETH)<br>'
				})
			}
		});
	})



});
</script>