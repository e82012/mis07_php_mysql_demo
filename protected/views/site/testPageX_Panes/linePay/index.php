<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LINE Pay API</title>
        <style type="text/css">
            body {
                min-width: 360px;
            }   
        </style>
    </head>
    <div class="tab-pane container fade" id="menu_12">
        <?php if($returnMsg == '') :?>
        <body>
            <div class="container">
                <!-- <form class="form-horizontal" id="reserveForm" method="POST" action="reserve.php"> -->
                <form class="form-horizontal" id="reserveForm" method="POST">
                    <!-- 商店資料 -->
                    <?php include('blocks/api_basic_info.php'); ?>
                    <!-- 訂單資料 -->
                    <div class="card">
                        <div class="card-header">訂單資料</div>
                        <div class="card-body">
                            <label class="ctrl-label col-3">商品名稱</label>
                                <div class="ctrls col-9">
                                    <input type="text" class="form-control" name="productName" value="Product A" required>
                                </div>
                                <?php 
                                // 隨機訂單
                                    $randomOrder = date("Ymd").rand(19999,99999);
                                ?>
                                <label class="ctrl-label col-3">訂單編號</label>
                                <div class="ctrls col-9">
                                    <input type="text" class="form-control" name="orderId" value="<?php echo $randomOrder ;?>" required>
                                </div>
                                <label class="ctrl-label col-3">訂單照片</label>
                                <div class="ctrls col-9">
                                    <input type="text" class="form-control" name="productImageUrl" value="image.jpeg" >
                                </div>
                                <label class="ctrl-label col-3">訂單金額 TWD </label>
                                <div class="ctrls col-9">
                                    <div class="input-grp">
                                        <input type="text" class="form-control" name="amount" value="20" required>
                                        <input type="hidden" class="form-control" name="currency" value="TWD">
                                    </div>
                                </div>

                                <label class="ctrl-label col-3">confirmUrl 類型</label>
                                <div class="ctrls col-9">
                                    <div class="kui-opts">
                                        <label class="kui-opt">
                                            <input type="radio" name="confirmUrlType" value="CLIENT" checked>
                                            <span class="kui-opt-input">CLIENT</span>
                                        </label>
                                        <label class="kui-opt">
                                            <input type="radio" name="confirmUrlType" value="SERVER">
                                            <span class="kui-opt-input">SERVER</span>
                                        </label>
                                    </div>
                                </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-outline-primary" name = "linePayment">支付</button>
                        </div>
                    </div>
                    <!--  -->
                </form>
            </div>
        </body>
        <?php else: ?>
            <div class="card">
                <div class="card-header">支付訊息</div>
                <div class="card-body">
                    <?php 
                        echo "<table class='table table-hover'>";
                            echo "<tbody>";
                                echo "<tr>";
                                    echo "<td>returnCode</td>";
                                    echo "<td>".$returnMsg[0]['returnCode']."</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td>returnMessage</td>";
                                    echo "<td>".$returnMsg[0]['returnMessage']."</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td>transactionId</td>";
                                    echo "<td>".$returnMsg[0]['info']['transactionId']."</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td>paymentAccessToken</td>";
                                    echo "<td>".$returnMsg[0]['info']['paymentAccessToken']."</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td colspan = '2'>
                                    <a target='_blank' href='".$returnMsg[1]."' class='btn btn-outline-secondary'>點此連至 Line 頁面登入帳戶</a>
                                    </td>";
                                echo "</tr>";
                            echo "</tbody>";
                        echo "</table>";


                    ?>
                </div>
                <!-- <div class="card-footer">Footer</div> -->
            </div>
        <?php endif ?>
    </div>
</html>