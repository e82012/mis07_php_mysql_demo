<div class="panel">
    <div class="panel-header">
        <h3 class="panel-title">API 位置與商家資料</h3>
    </div>
    <!-- API資料 -->
    <div class="card">
        <div class="card-header">API資料</div>
        <div class="card-body">
            <label class="ctrl-label col-3">LINE Pay API Server</label>
            <div class="ctrls col-9">
                <input type="text" class="form-control" name="apiEndpoint" value="https://sandbox-api-pay.line.me/v2/payments/" required>
            </div>
            <label class="ctrl-label col-3">商家 ID</label>
            <div class="ctrls col-9">
                <input type="text" class="form-control" name="channelId" value="1657653979" required>
            </div>
            <label class="ctrl-label col-3">商家密鑰</label>
            <div class="ctrls col-9">
                <input type="text" class="form-control" name="channelSecret" value="63ee5b857c6918cec24ecb1a61870c04" required>
            </div>
        </div>
        <!-- <div class="card-footer">Footer</div> -->
    </div>

</div>