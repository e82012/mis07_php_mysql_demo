<div class="tab-pane container fade" id="menu_5">
	<form method="POST" id = 'formE' name = 'formE'>
		<!-- 查詢列 -->
		<table class = "table table is-hoverable">
			<tr>
				<th>區域</th>
				<th>
				<div class="select">
				<select>
					<option>選擇區域</option>
				</select>
				</div>
				</th>
				<th>門市</th>
				<th>
				<div class="select">
				<select>
					<option>選擇門市</option>
				</select>
				</div>
				</th>
				<th>項目類別</th>
				<th>
				<div class="select">
				<select>
					<option>選擇項目</option>
				</select>
				</div>
				</th>
				<th>
					<input type="submit" name = 'search' id='search' class="button is-small is-info is-outlined" value = "查詢">
				</th>
			</tr>
		</table>
		<!-- 內容 -->
		<table id = "table_ee" class = "table table-striped">
			<?php 
				echo "<thead>";
					echo "<tr>";
						echo "<th>資產編號</th>";
						echo "<th>項目類別</th>";
						echo "<th>歸屬門市</th>";
						echo "<th>修繕次數</th>";
						echo "<th>使用狀況</th>";
						echo "<th>內容</th>";
						echo "<th>展開</th>";
					echo "</tr>";
				echo "</thead>";
				// 假設資產編號用
				$randomItemNumber = mt_rand(10000000000, 99999999999);
				echo "<tbody>";
				for($i=0;$i < 5;$i++)
				{
					echo "<tr>";
						echo "<td>".$randomItemNumber.$i."</td>";
						echo "<td>電腦設備</td>";
						echo "<td>豐原成功</td>";
						echo "<td>0</td>";
						echo "<td>使用中</td>";
						echo "<td><button type='button' name = 'modal_test' id='modal_test' class='button is-small is-info is-outlined' data-bs-toggle='modal' data-bs-target='#staticBackdrop'>內容</button></td>";
						echo "<td><button type='button' class='button is-small is-info is-outlined' data-bs-toggle='collapse' data-bs-target='#extItem_$i' >more...</button></td>";
					echo "</tr>";
					// 展開
					echo "<tr id = 'extItem_$i' class='collapse'>";
						echo "<td>電腦設備</td>";
						echo "<td>豐原成功</td>";
					echo "</tr>";

				}
				echo "</tbody>";
			
			?>
		</table>
	</form>

	<!-- The Modal -->
	<div class="modal fade" id="staticBackdrop" data-bs-backdrop="false" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" style="width:1020px;">
		<div class="modal-header">
			<h5 class="modal-title" id="staticBackdropLabel">J003055Z020191103A 電腦設備</h5>
			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		</div>
		<div class="modal-body">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			<li class="nav-item">
				<a class="nav-link active" data-bs-toggle="tab" href="#home">基本資料</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-bs-toggle="tab" href="#menu1">零組件資料</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-bs-toggle="tab" href="#menu2">叫修紀錄</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-bs-toggle="tab" href="#menu3">移轉紀錄</a>
			</li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane container active" id="home">
					資產編號:<input class="input" type="text" placeholder="objectNo" value="J003055Z020191103A"><br>
					項目類別:
					<div class = "row">
						<div class="form-floating mb-3 col-sm-4">
							<select class="form-control" id="qry_mitem" placeholder="選擇主項">
								<option>電腦設備</option>
							</select>
							<label for="floatingInput">主項</label>
						</div>
						<div class="form-floating mb-3 col-sm-4">
							<select class="form-control" id="qry_oitem" placeholder="選擇次項">
								<option>電腦設備</option>
							</select>
							<label for="floatingInput">次項</label>
						</div>
					</div>
					歸屬門市:<br>
					<div class = "row">
						<div class="form-floating mb-3 col-sm-4">
							<select class="form-control form-control-sm" id="qry_area" placeholder="選擇區域">
								<option>中一區</option>
							</select>
							<label for="floatingInput">區域</label>
						</div>
						<div class="form-floating mb-3 col-sm-4">
							<select class="form-control form-control-sm" id="qry_area" placeholder="選擇門市">
								<option>豐原成功</option>
							</select>
							<label for="floatingInput">門市</label>
						</div>
					</div>
					修繕次數:<input class="input" type="text" placeholder="fixtimes" value="1"><br>
					使用狀況:<br>			
							<div class="select">
							<select>
								<option>使用中</option>
								<option>備用品</option>
								<option>已報廢</option>
							</select>
							</div><br>
				</div>
				<div class="tab-pane container fade" id="menu1">
					<table id = "table_1">
						<tr id = "title_1">
							<th>資產編號</th>
							<th>物件名稱</th>
							<th>型號</th>
							<th>建立日期</th>
							<th>修繕次數</th>
							<th>修繕內容</th>
							<th>功能</th>
						</tr>
						<!-- <tr id = "item_1">
							<td><input class="input" type="text" placeholder="objectNo" value="A123456789" size = "12" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="投幣機" size = "8" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="A111" size = "6" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="2022-02-11" size = "10" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="0" size = "1" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="0" size = "1" readonly></td>
							<td>
							<button class="button is-link is-inverted">
								<span class="icon is-small">
								<i class="far fa-edit"></i>
								</span>
							</button>
							<button class="button is-danger is-inverted" onclick = 'item_delete()'>
								<span class="icon is-small">
								<i class="fas fa-trash-alt"></i>
								</span>
							</button>
							</td>
						</tr>
						<tr id = "item_2">
							<td><input class="input" type="text" placeholder="objectNo" value="A123456789" size = "12" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="紙鈔機" size = "8" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="B111" size = "6" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="2022-02-11" size = "10" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="0" size = "1" readonly></td>
							<td><input class="input" type="text" placeholder="objectNo" value="0" size = "1" readonly></td>
							<td>
							<button class="button is-link is-inverted">
								<span class="icon is-small">
								<i class="far fa-edit"></i>
								</span>
							</button>
							<button class="button is-danger is-inverted">
								<span class="icon is-small">
								<i class="fas fa-trash-alt"></i>
								</span>
							</button>
							</td>
						</tr> -->
					</table>
					<!-- 新增零組件 -->
					<button type="button" name = 'newItem' id='newItem' class="button is-link is-inverted" >新增零組件資料  <i class="far fa-plus-square"></i></button>
					<table>
						<tr>
							<td>
								<div class="form-floating mb-3">
									<input type="text" class="form-control form-control-sm" id="text_in1" placeholder="資產編號">
									<label for="floatingInput">資產編號</label>
								</div>
							</td>
							<td>
								<div class="form-floating mb-3">
									<input type="text" class="form-control form-control-sm" id="text_in2" placeholder="物件名稱">
									<label for="floatingInput">物件名稱</label>
								</div>
							</td>
							<td>
								<div class="form-floating mb-3">
									<input type="text" class="form-control form-control-sm" id="text_in3" placeholder="型號">
									<label for="floatingInput">型號</label>
								</div>
							</td>
							<!-- <td><input class="input" type="text" id = "text_in1" placeholder="資產編號" value="" size = "12" ></td> -->
							<!-- <td><input class="input" type="text" id = "text_in2" placeholder="物件名稱" value="" size = "8" ></td> -->
							<!-- <td><input class="input" type="text" id = "text_in3" placeholder="型號" value="" size = "6" ></td> -->
						</tr>
					</table>
				</div>
				<!-- 叫修紀錄 -->
				<div class="tab-pane container fade" id="menu2">
					<table>
						<tr>
							<th>叫修日期</th>
							<th>叫修單號</th>
							<th>叫修狀態</th>
							<th>叫修細項</th>
						</tr>
						<tr>
							<td>2022/02/06</td>
							<td>20220206003</td>
							<td>已完成</td>
							<td>電腦啟動修復，無法開機</td>
						</tr>
					</table>
				</div>
				<!-- 移轉紀錄 -->
				<div class="tab-pane container fade" id="menu3">
					<table>
		
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary">更新資料</button>
		</div>
		</div>
	</div>
	</div>
</div>

<script>
	$(document).ready( function () {
    	// $('#table_ee').DataTable();
	} );
	// 資產設備管理 零組件 新增UI
	$("#newItem").click(function(){
		var table_1 = document.getElementById("table_1");
		var row = table_1.insertRow(-1);
		var col1 = document.getElementById("text_in1");
		var col2 = document.getElementById("text_in2");
		var col3 = document.getElementById("text_in3");
		if(col1.value =='' || col2.value == '' || col3.value == '')
		{
			alert("新增資料有誤");
		}
		else
		{
			// 寫入td (insert方式)
			var c_date = new Date().toISOString().split('T')[0];
				row.insertCell(0).innerHTML = "<input class='input' type='text'  value = '"+col1.value+"' size = '12' readonly>";
				row.insertCell(1).innerHTML = "<input class='input' type='text'  value = '"+col2.value+"' size = '8' readonly >";
				row.insertCell(2).innerHTML = "<input class='input' type='text'  value = '"+col3.value+"' size = '6' readonly >";
				row.insertCell(3).innerHTML = "<input class='input' type='date'  value = '"+c_date+"' readonly >";
				row.insertCell(4).innerHTML = "<input class='input' type='text'  value ='0' size = '1' readonly >";
				row.insertCell(5).innerHTML = "<input class='input' type='text'  value ='0' size = '1' readonly >";
				// ------div方式
				// row.insertCell(0).innerHTML = "<div contenteditable = 'false'>"+col1.value+"</div>";
				// row.insertCell(1).innerHTML = "<div contenteditable = 'false'>"+col2.value+"</div>";
				// row.insertCell(2).innerHTML = "<div contenteditable = 'false'>"+col3.value+"</div>";
				// row.insertCell(3).innerHTML = "<div contenteditable = 'false'>"+new Date().toLocaleDateString()+"</div>";
				// row.insertCell(4).innerHTML = "<div contenteditable = 'false'>0</div>";
				// row.insertCell(5).innerHTML = "<div contenteditable = 'false'>0</div>";
				row.insertCell(6).innerHTML =  "<button class='button is-link is-inverted' onclick = 'item_edit(this)'>"+
								"<span class='icon is-small'>"+
								"<i class='far fa-edit'></i>"+
								"</span>"+
								"</button>"+
								"<button class='button is-danger is-inverted' onclick = 'item_delete(this)'>"+
								"<span class='icon is-small'>"+
								"<i class='fas fa-trash-alt'></i>"+
								"</span>"+
								"</button>";

				//新增欄位清空
				col1.value = "";
				col2.value = "";
				col3.value = "";
				// 新增後查看BTN
					var deleteBTN = document.getElementsByClassName('button is-danger is-inverted');
					console.log(deleteBTN)
		}


		// table_1.getElementsByTagName('tbody')[0].insertRow();
	})
	// 資產設備管理 零組件 UI - 刪除
	function item_delete(btn)
	{
		$(btn).parent().parent().remove();
		console.log(btn)
	}
	// 資產設備管理 零組件 UI - 修改(移除readonly)
	function item_edit(btn)
	{
		var item = $(btn).parent().parent();
		item.find('input').removeAttr('readonly');
		item.find('div').attr({'contenteditable':'true'});
		console.log(item)
	}


</script>

