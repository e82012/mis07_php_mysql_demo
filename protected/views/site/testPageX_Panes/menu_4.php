<!-- <script src="testPageX_Panes/menu_4_script.js"></script> -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

<div class="tab-pane container fade" id="menu_4">
	<form method="POST" id = 'form6' name = 'form6' enctype="multipart/form-data">
	<div class = "row">
		<!-- SELECT選擇測試 -->
		<div class="col-3">
			<div class="ui cards">
				<div class="blue card">
					<div class="content">
						<div class="header">
							SELECT測試
						</div>
						<div class="meta">
							選擇器
						</div>
					</div>
					<div class="ui styled fluid accordion">
						<div class="title">
							<i class="dropdown icon"></i>
							1.搭配AJAX
						</div>
						<div class="content">
							<div class="meta">Site/AjaxTest</div>
							<select id="selectABC" name="selectABC" class="form-select mb-3">
								<option value="01">A1</option>
								<option value="02">B1</option>
								<option value="03">C1</option>
								<option value="04">D1</option>
								<option value="05">E1</option>
							</select>
						</div>
					</div>
					
					<!-- 2 -->
					<div class="ui styled fluid accordion">
						<div class="title">
							<i class="dropdown icon"></i>
							2.Multiple多選
						</div>
						<div class="content">
							<div class="meta">搭配select2.js , 送出後render回view</div>
							<select id="selectMutilTag" name="selectMutilTag[]" multiple class="form-select mb-3">
								<option value="01">A1</option>
								<option value="02">B1</option>
								<option value="03">C1</option>
								<option value="04">D1</option>
								<option value="05">E1</option>
							</select>
							<input type="submit" name="datalistBTN" id="datalistBTN" class="btn btn-outline-info btn-sm" value="送出">
							<?php
								echo '<ul class="list-group">';
								if ($returnSelect != "") {
									foreach ($returnSelect as $r) {
										echo '<li class="list-group-item">' . $r . '</li>';
									}
								}
								echo '</ul>';
							?>
						</div>
					</div>
					
					<!-- 3 -->
					<div class="ui styled fluid accordion">
						<div class="title">
							<i class="dropdown icon"></i>
							3.SweetAlert Modal ajax 連動選單
						</div>
						<div class="content">
							<div class="meta">連動選單測試</div>
							<button type="button" id="ajaxBTN" name="ajaxBTN" class="btn btn-outline-info btn-sm" onclick="sweetModal()">
								<i class="fas fa-ad"></i> SweetAlert Modal測試
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- flatpicker日期選擇 -->
		<div class = "col-3">
			<div class="ui cards">
				<div class="blue card">
					<div class="content">
						<!-- <img class="right floated mini ui image" src="/images/avatar/large/elliot.jpg"> -->
						<div class="header">
							flatpickr日期選擇
						</div>
						<div class="meta">
							測試flatpickr日期選擇
						</div>
					</div>
					<!-- 移除extra content標籤，直接把footer變成dropdown -->
					<!-- <div class="extra content"> -->
						<div class="ui styled fluid accordion">
							<div class="title">
								<i class="dropdown icon"></i>
								一般
							</div>
							<div class="content">
								<div class="meta">參數修改 >> #dateTest</div>
								<input class = 'form-control' id = "dateTest" name = "dateTest" placeholder="flatpickr日期" >
							</div>
						</div>
						<div class="ui styled fluid accordion">
							<div class="title">
								<i class="dropdown icon"></i>
								多選
							</div>
							<div class="content">
								<div class="meta">參數修改 >> #dateTest_multiple</div>
								<input class = 'form-control' id = "dateTest_multiple" name = "dateTest_multiple" placeholder="flatpickr日期" >
							</div>
						</div>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</div>

	</form>
</div>
