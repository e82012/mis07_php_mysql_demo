 //圖表
 const call_allTotal = <?php echo json_encode($call_allTotal); ?>;
 const call_AllDataCharts = <?php echo json_encode($call_AllDataCharts); ?>;
 const m_itemList = <?php echo json_encode($m_itemList); ?>;
// console.log(call_AllDataCharts);
// POST後有值才產出圖表 
if(call_AllDataCharts != null){
//  1 全公司 - 總計 
    Highcharts.chart('callHistory_chart', {
        chart: {
            height: 375,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie' 
        },
        tooltip: {
            //顯示資料用()內容
            formatter: function() {
            var sliceIndex = this.point.index;
            var sliceName = this.series.chart.axes[0].categories[sliceIndex];
            return sliceName +
            '</b> : <b>' + this.y + '</b>';
                }
                },
                title: {
                    text:'<b>全公司 - 叫修件數分布</b>'
                },
                xAxis: {
                    categories: call_AllDataCharts['history']['title'],
                        title:{text: '1234'}
                },

        //value on bar
        plotOptions: {
        pie: {
            size:220,
            allowPointSelect: true,
            cursor: 'pointer',
            colors: ['#ffff99', '#ccffcc', '#ffcccc', '#99ccff','#ffcc99','#ffccff','#cce6ff','#ffe6e6'],
            dataLabels: {
                style: {
                        fontSize: '12px'
                    },
                enabled: true,
                padding:0,
                //顯示資料用
                formatter: function() {
                var sliceIndex = this.point.index;
                var sliceName = this.series.chart.axes[0].categories[sliceIndex];
                return sliceName +
                '<br>' + this.y + '</b>';
                },
                
                distance: -25,
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 1
                },
            },
            // showInLegend: true, //顯示左側項目標示
        }
        },
        series: [
            {
            name:'',
            dataLabels: {
                // padding: -10
                
                },
            data: call_AllDataCharts['history']['value'],
            },
                
        ]
        
    });
    // 2 全公司 - 未完修
    Highcharts.chart('callTime_noFix_chart', {
        chart: {
            height: 375,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie' 
        },
        tooltip: {
            //顯示資料用()內容
            formatter: function() {
            var sliceIndex = this.point.index;
            var sliceName = this.series.chart.axes[0].categories[sliceIndex];
            return sliceName +
            '</b> : <b>' + this.y + '</b>';
                }
                },
                title: {
                    text:'<b>全公司 - 尚未完修分布</b>'
                },
                xAxis: {
                    categories: call_AllDataCharts['noFix']['title'],
                        title:{text: '1234'}
                },

        //value on bar
        plotOptions: {
        pie: {
            size:220,
            allowPointSelect: true,
            cursor: 'pointer',
            colors: ['#ffff99', '#ccffcc', '#ffcccc', '#99ccff','#ffcc99','#ffccff','#cce6ff','#ffe6e6'],
            dataLabels: {
                style: {
                        fontSize: '12px'
                    },
                enabled: true,
                padding:0,
                //顯示資料用
                formatter: function() {
                var sliceIndex = this.point.index;
                var sliceName = this.series.chart.axes[0].categories[sliceIndex];
                return sliceName +
                '<br>' + this.y + '</b>';
                },
                
                distance: -25,
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 1
                },
            },
            // showInLegend: true, //顯示左側項目標示
        }
        },
        series: [
            {
            name:'',
            dataLabels: {
                // padding: -10
                
                },
            data: call_AllDataCharts['noFix']['value'],
            },
                
        ]
        
    });
    // 3全公司 - 本月叫修
    Highcharts.chart('callTime_ThisMonth_chart', {
        chart: {
            height: 375,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie' 
        },
        tooltip: {
            //顯示資料用()內容
            formatter: function() {
            var sliceIndex = this.point.index;
            var sliceName = this.series.chart.axes[0].categories[sliceIndex];
            return sliceName +
            '</b> : <b>' + this.y + '</b>';
                }
                },
                title: {
                    text:'<b>全公司 - 本月叫修分布</b>'
                },
                xAxis: {
                    categories: call_AllDataCharts['month']['title'],
                        title:{text: '1234'}
                },

        //value on bar
        plotOptions: {
        pie: {
            size:220,
            allowPointSelect: true,
            cursor: 'pointer',
            colors: ['#ffff99', '#ccffcc', '#ffcccc', '#99ccff','#ffcc99','#ffccff','#cce6ff','#ffe6e6'],
            dataLabels: {
                style: {
                        fontSize: '12px'
                    },
                enabled: true,
                padding:0,
                //顯示資料用
                formatter: function() {
                var sliceIndex = this.point.index;
                var sliceName = this.series.chart.axes[0].categories[sliceIndex];
                return sliceName +
                '<br>' + this.y + '</b>';
                },
                
                distance: -25,
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 1
                },
            },
            // showInLegend: true, //顯示左側項目標示
        }
        },
        series: [
            {
            name:'',
            dataLabels: {
                // padding: -10
                
                },
            data: call_AllDataCharts['month']['value'],
            },
                
        ]
        
    });
    // 4全公司 - 本月完修
    Highcharts.chart('callTime_fixMonth_chart', {
        chart: {
            height: 375,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie' 
        },
        tooltip: {
            //顯示資料用()內容
            formatter: function() {
            var sliceIndex = this.point.index;
            var sliceName = this.series.chart.axes[0].categories[sliceIndex];
            return sliceName +
            '</b> : <b>' + this.y + '</b>';
                }
                },
                title: {
                    text:'<b>全公司 - 本月完修分布</b>'
                },
                xAxis: {
                    categories: call_AllDataCharts['fixed']['title'],
                        title:{text: '1234'}
                },

        //value on bar
        plotOptions: {
        pie: {
            size:220,
            allowPointSelect: true,
            cursor: 'pointer',
            colors: ['#ffff99', '#ccffcc', '#ffcccc', '#99ccff','#ffcc99','#ffccff','#cce6ff','#ffe6e6'],
            dataLabels: {
                style: {
                        fontSize: '12px'
                    },
                enabled: true,
                padding:0,
                //顯示資料用
                formatter: function() {
                var sliceIndex = this.point.index;
                var sliceName = this.series.chart.axes[0].categories[sliceIndex];
                return sliceName +
                '<br>' + this.y + '</b>';
                },
                
                distance: -25,
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 1
                },
            },
            // showInLegend: true, //顯示左側項目標示
        }
        },
        series: [
            {
            name:'',
            dataLabels: {
                // padding: -10
                
                },
            data: call_AllDataCharts['fixed']['value'],
            },
                
        ]
        
    });
}