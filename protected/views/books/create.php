<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs=array(
	'書籍管理'=>array('index'),
	'新增',
);

$this->menu=array(
	array('label'=>'清單', 'url'=>array('index')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>新增</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>