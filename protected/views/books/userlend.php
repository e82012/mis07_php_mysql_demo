<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs=array(
    '使用者借閱'=>array('userlend_list'),
    '書籍搜尋'
);

$this->menu=array(
    array('label'=>'書籍清單', 'url'=>array('userlend_list')),
    array('label'=>'還書', 'url'=>array('userlend_return')),
);
?>

<h1>書籍搜尋</h1>

<form id="book-form" method="post" class="form-horizontal" >

    <div class="container-fluid">
    <div class="form-group" >書名：<input type="text" name="qrybname" value="<?php echo $qrybname ?>" size="10"></div>
    <div class="form-group">作者：<input type="text" name="qrybauthor" value="<?php echo $qrybauthor ?>"size="10"></div>
    <div class="form-group">ISBN：<input type="text" name="qrybisbn" value="<?php echo $qrybisbn ?>"size="10"></div>
    <div class="form-group"><input type="submit" name="submit" value="送出"></div>
    <input type="checkbox" name="checkNoReturn" <?php if($checkNoReturn) echo "checked"; ?> >逾期未還
    </div>

</form>


<div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>書名</th>
				<th>作者</th>
				<th>ISBN</th>
				<th>狀態</th>
                <th>借出日</th>
                <th>歸還日</th>
            </tr>
        </thead>
        <tbody>
        <?php

            for($i=0;$i<count($resultAry);$i++){
                if($i%2==0)
                    echo "<tr bgcolor='#ECFFFF'>";
                else
                    echo "<tr>";
                //echo "<td>".$resultAry[$i]->bname."</td>";
                echo "<td><small>".CHtml::link(($resultAry[$i]->bname),array('userlend_view', 'id'=>$resultAry[$i]->id)) ."<small></td>";
				echo "<td><small>".$resultAry[$i]->bauthor."</small></td>";
				echo "<td><small>".$resultAry[$i]->bisbn."</small></td>";
				echo "<td><small>".$resultAry[$i]->bstatus."</small></td>";
                echo "<td><small>".$resultAry[$i]->lenddate."</small></td>";
                echo "<td><small>".$resultAry[$i]->expiry."</small></td>";
                echo "</tr>";
            }

			?>

			</tbody>
		</table>
	
	</div>
	