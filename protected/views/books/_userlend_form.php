<?php
/* @var $this BooksController */
/* @var $model Books */
/* @var $form CActiveForm */
?>

<div class="form">


<?php 
	$form=$this->beginWidget('CActiveForm', array(
	'id'=>'books-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
	<?php //echo $form->labelEx($model,'執行動作'); ?>
	<?php
	echo $form->hiddenField($model,'bstatus',array('value'=>'借出','readonly'=>true));
	//echo $model->bstatus;
	?>
	</div>
<!-- form借閱表單 -->
<?php 
	$row = Yii::app()->db->createCommand(array(
		'select'=>'*',
		'from'=>'tbl_user',
		'where'=>'username=:username',
		'params'=>array('username'=>Yii::app()->user->id),
		))->queryRow();

	if($row['tbl_books']==null)	//判斷是否已借閱書籍
	{
		if ($model->bstatus =="在架")	//判斷書籍是否在架上
		{	
		echo $form->textField($model,'bname',array('size'=>60,'maxlength'=>128,'readonly'=>true,'input type'=>hidden)); 
		
		echo $form->labelEx($model,'lender'); 
		echo $form->textField($model,'lender',array('size'=>20,'maxlength'=>128,'value'=>Yii::app()->user->name,'readonly'=>true)); 
		echo $form->error($model,'lender'); 

		echo $form->labelEx($model,'lenddate'); 
		echo $form->textField($model,'lenddate',array('value'=>date('Ymd'),'size'=>20,'maxlength'=>128,'readonly'=>true));
		echo $form->error($model,'lenddate'); 

		echo $form->labelEx($model,'逾期日(借閱日+7天)'); 
		echo $form->textField($model,'expiry',array('size'=>20,'maxlength'=>128,'value'=>date('Ymd')+7,'readonly'=>true));
		echo $form->error($model,'expiry'); 
		echo '<br>';
		
		
		echo '<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#notice1">借閱登記</button>';
		//<!-- Modal -->
		echo '<div class="modal fade" id="notice1" data-keyboard="false" tabindex="-1" aria-hidden="true">';
		echo '<div class="modal-dialog">';
			echo '<div class="modal-content">';
			echo '<div class="modal-header">';
				echo '<h5 class="modal-title" id="staticBackdropLabel">::JIT_MIS::</h5>';
				echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
				echo '<span aria-hidden="true">&times;</span>';
			echo '</button>';
			echo '</div>';
			echo '<div class="modal-body">';
			echo '確定借閱??????';
			echo '</div>';
			echo '<div class="modal-footer">';
				echo '<button type="button" class="btn btn-outline-dark" data-dismiss="modal">取消</button>';
				echo  CHtml::submitButton($model->isNewRecord ? '新增' : '確定',array('button class'=>'btn btn-outline-primary'));
			echo '</div>';
			echo '</div>';
		echo '</div>';
		echo '</div>';	


		}
		else	
		{
			echo '此書籍已借出，無法借閱<br>';
			echo '歸還日:',$model->expiry;
		}
	}	
	else
	{
		echo '<B>每人限借閱一本</B><br>';
		echo '<B>您已借閱:',$row['tbl_books'],'</B>';
	}
		
	//CVarDumper::dump($row['tbl_books']);
?>
<!-- form借閱表單END-->
<?php $this->endWidget(); ?>


</div>