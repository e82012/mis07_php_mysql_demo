<?php 
/* @var $this BooksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'使用者借閱'=>array('userlend_list'),
	'書籍清單',
);

$this->menu=array(
	array('label'=>'書籍搜尋','url'=>array('userlend')),
	array('label'=>'還書', 'url'=>array('userlend_return')),
);
?>

<h1>書籍清單</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'userlend_listview',
)); ?>

