<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs=array(
	'使用者借閱'=>array('userlend_list'),
	$model->bname,
);

//---檢查借閱狀態 MENU顯示
if ($model->bstatus =="在架")
{
$this->menu=array(
	array('label'=>'返回', 'url'=>array('userlend_list')),
	array('label'=>'借閱', 'url'=>array('userlendupdate', 'id'=>$model->id)),
);
}
else
$this->menu=array(
	array('label'=>'返回', 'url'=>array('userlend_list')),
);	
?>

<h1><?php echo $model->bname; ?></h1>
<table class="table table-hover">
<?php 
	$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'bname',
		'bauthor',
		'bpublish',
		'bisbn',
		'bdate',
		'bstatus',
		'lenddate',
		'expiry',
		),
	
)); ?>
<img src='<?php echo $model->bimg ?>' width='300'>
</table>
