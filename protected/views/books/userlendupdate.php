<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs=array(
	'使用者借閱'=>array('userlend_list'),
	$model->bname,
);

$this->menu=array(
	array('label'=>'返回', 'url'=>array('userlend_list')),
	array('label'=>'預覽', 'url'=>array('userlend_view', 'id'=>$model->id)),
);
?>

<h1>借閱登記</h1>
<br><?php echo '書名：';echo $model->bname; ?></br>
<?php echo '作者：'; echo $model->bauthor; ?></br>
<?php echo '出版商：'; echo $model->bpublish; ?></br>
<?php echo '借閱狀態：'; echo $model->bstatus; ?></br>
<p></p>


<?php $this->renderPartial('_userlend_form', array('model'=>$model)); ?>