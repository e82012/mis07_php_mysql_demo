<?php
/* @var $this BooksController */
/* @var $model Books */
$row = Yii::app()->db->createCommand(array(
	'select'=>'*',
	'from'=>'tbl_user',
	'where'=>'username=:username',
	'params'=>array('username'=>Yii::app()->user->id),
))->queryRow();

$brow = Yii::app()->db->createCommand(array(
	'select'=>'*',
	'from'=>'books',
	'where'=>'bname=:bname',
	'params'=>array('bname'=>$row['tbl_books']),
))->queryRow();

$this->breadcrumbs=array(
	'使用者借閱'=>array('index'),
	'還書'
);
$this->menu=array(
    array('label'=>'書籍清單', 'url'=>array('userlend_list')),
);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'books-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<?php 
echo '使用者 : ',Yii::app()->user->id;
echo '<br>';
echo '<br>';
	if($row['tbl_books']==null)
	{
	//echo '您借閱的書籍 : ',$row['tbl_books'];
	echo '您目前沒有借閱中的書籍';
	}
	//echo CHtml::submitButton('確定還書',array('name'=>'breturn'));
?>
<table class="table table-hover">
        <thead>
            <tr>
   				<th>借閱書籍</th>
				<th>借出日</th>
				<th>歸還日</th> 
				<th></th>  
            </tr>
        </thead>
        <tbody>
        <?php
                echo "<tr bgcolor='#ECFFFF'>";
                echo "<tr>";
				echo "<td><B>".$row['tbl_books']."</B></td>";
				echo "<td><B>".$brow['lenddate']."</B></td>";
				echo "<td><B>".$brow['expiry']."</B></td>";
				if($row['tbl_books']!=null)
				{
					echo "<td>".CHtml::submitButton('確定還書',array('name'=>'breturn','button class'=>'btn btn-warning'))."</td>";
				}
				else
				{
					echo "<td>".''."</td>";
				}
                echo "</tr>";
			?>

			</tbody>
		</table>


<?php $this->endWidget(); ?>

<!-- <input type="submit" > -->
<!-- echo CHtml::button('Button Text', array('submit' => array('Books/userlend_return'))); -->

	

