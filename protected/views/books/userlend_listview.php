<?php
/* @var $this BooksController */
/* @var $data Books */
?>

<div class="view">
	<!--
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('userlend_view', 'id'=>$data->id)); ?>
	<br />
	-->
	<b><?php echo CHtml::encode($data->getAttributeLabel('bname')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->bname), array('userlend_view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bauthor')); ?>:</b>
	<?php echo CHtml::encode($data->bauthor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bpublish')); ?>:</b>
	<?php echo CHtml::encode($data->bpublish); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bisbn')); ?>:</b>
	<?php echo CHtml::encode($data->bisbn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bstatus')); ?>:</b>
	<?php echo CHtml::encode($data->bstatus); ?>
	<br />


</div>