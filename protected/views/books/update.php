<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs=array(
	'書籍管理'=>array('index'),
	'更新',
);

$this->menu=array(
	array('label'=>'清單', 'url'=>array('index')),
	array('label'=>'新增', 'url'=>array('create')),
	array('label'=>'預覽', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>更新資料</h1>
<?php echo $model->bname; ?>
<p></p>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>