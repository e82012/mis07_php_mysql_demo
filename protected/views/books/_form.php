<?php
/* @var $this BooksController */
/* @var $model Books */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'id'=>'books-form',

	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,

)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'bname'); ?>
		<?php echo $form->textField($model,'bname',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'bname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bauthor'); ?>
		<?php echo $form->textField($model,'bauthor',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'bauthor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bpublish'); ?>
		<?php echo $form->textField($model,'bpublish',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'bpublish'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bisbn'); ?>
		<?php echo $form->textField($model,'bisbn',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'bisbn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bdate'); ?>
		<?php echo $form->textField($model,'bdate',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'bdate'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'bimg'); ?>
		<!-- activefilefield -->
		<?php echo CHtml::activefileField($model,'bimg',array('onchange'=>'loadfile()')); ?> 
		<!-- filefield-->
		<!-- <?php echo $form->fileField($model,'bimg'); ?>  -->
		<?php echo $form->error($model,'bimg'); ?>
		<script>
    		function loadfile()	//預覽function
			{
				var output=document.getElementById('preimg');
				output.src=URL.createObjectURL(event.target.files[0]);
			};
		</script>
		<!-- <?php echo "<img src='$model->bimg' width='150'>" ?> -->
	</div>
		<!-- 預覽圖片 -->
		<img id='preimg'' width='100'>	
	<!--- 借閱資料 --->

	<!-- select-->
	<div class="row">
	<?php
	echo $form->dropDownList($model,'bstatus',array('在架'=>'在架','借出'=>'借出'));
	?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'lender'); ?>
		<?php echo $form->textField($model,'lender',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'lender'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'lenddate'); ?>
		<?php echo $form->textField($model,'lenddate',array('input type'=>date,'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'lenddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expiry'); ?>
		<?php echo $form->textField($model,'expiry',array('input type'=>date,'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'expiry'); ?>
	</div>
		<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '新增' : '更新'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->