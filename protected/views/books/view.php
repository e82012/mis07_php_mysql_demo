<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs=array(
	'書籍管理'=>array('index'),
	$model->bname,
);

// $bookimage = Yii::getPathOfAlias('webroot').'/images/';

$this->menu=array(
	array('label'=>'清單', 'url'=>array('index')),
	array('label'=>'新增', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'刪除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'確定刪除書籍?')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->bname; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'bname',
		'bauthor',
		'bpublish',
		'bisbn',
		'bdate',
		'bstatus',
		'lender',
		'lenddate',
		'expiry',
		//'bimg',
		),
	
		
)); 
// echo $model->bimg;
//  CVarDumper::dump($model->bimg);
//  CVarDumper::dump($model->bimg);
 

?>
<img src='<?php echo $model->bimg ?>' width='300'>
