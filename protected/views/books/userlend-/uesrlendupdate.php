<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs=array(
	'書籍管理'=>array('index'),
	'借閱',
);

$this->menu=array(
	array('label'=>'清單', 'url'=>array('index')),
	array('label'=>'預覽', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1>借閱登記</h1>
<br><?php echo '書名：';echo $model->bname; ?></br>
<?php echo '作者：'; echo $model->bauthor; ?></br>
<?php echo '出版商：'; echo $model->bpublish; ?></br>
<?php echo '借閱狀態：'; echo $model->bstatus; ?></br>
<p></p>


<?php $this->renderPartial('_lend_form', array('model'=>$model)); ?>