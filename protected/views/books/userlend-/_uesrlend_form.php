<?php
/* @var $this BooksController */
/* @var $model Books */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'books-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
	<?php echo $form->labelEx($model,'bstatus'); ?>
	<?php
	echo $form->dropDownList($model,'bstatus',array('在架'=>'在架','借出'=>'借出'));
	?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lender'); ?>
		<?php echo $form->textField($model,'lender',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'lender'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'lenddate'); ?>
		<?php echo $form->textField($model,'lenddate',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'lenddate'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'expiry'); ?>
		<?php echo $form->textField($model,'expiry',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'expiry'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '新增' : '送出'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->