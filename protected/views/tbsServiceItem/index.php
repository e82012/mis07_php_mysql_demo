<?php
/* @var $this TbsServiceItemController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tbs Service Items',
);

$this->menu=array(
	array('label'=>'Create TbsServiceItem', 'url'=>array('create')),
	array('label'=>'Manage TbsServiceItem', 'url'=>array('admin')),
);
?>

<h1>Tbs Service Items</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
