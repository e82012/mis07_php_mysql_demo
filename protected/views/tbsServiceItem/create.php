<?php
/* @var $this TbsServiceItemController */
/* @var $model TbsServiceItem */

$this->breadcrumbs=array(
	'Tbs Service Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TbsServiceItem', 'url'=>array('index')),
	array('label'=>'Manage TbsServiceItem', 'url'=>array('admin')),
);
?>

<h1>Create TbsServiceItem</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>