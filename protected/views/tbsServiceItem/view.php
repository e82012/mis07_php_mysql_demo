<?php
/* @var $this TbsServiceItemController */
/* @var $model TbsServiceItem */

$this->breadcrumbs=array(
	'Tbs Service Items'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TbsServiceItem', 'url'=>array('index')),
	array('label'=>'Create TbsServiceItem', 'url'=>array('create')),
	array('label'=>'Update TbsServiceItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TbsServiceItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TbsServiceItem', 'url'=>array('admin')),
);
?>

<h1>View TbsServiceItem #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'version',
		'sale_no',
		'sale_name',
		'type',
		'sv_order',
		'num',
		'price',
		'perform_mapping',
		'memo',
	),
)); ?>
