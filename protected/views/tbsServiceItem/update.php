<?php
/* @var $this TbsServiceItemController */
/* @var $model TbsServiceItem */

$this->breadcrumbs=array(
	'Tbs Service Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TbsServiceItem', 'url'=>array('index')),
	array('label'=>'Create TbsServiceItem', 'url'=>array('create')),
	array('label'=>'View TbsServiceItem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TbsServiceItem', 'url'=>array('admin')),
);
?>

<h1>Update TbsServiceItem <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>