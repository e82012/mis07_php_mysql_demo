<?php
/* @var $this TbaAdjustController */
/* @var $model TbaAdjust */

$this->breadcrumbs=array(
	'Tba Adjusts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TbaAdjust', 'url'=>array('index')),
	array('label'=>'Create TbaAdjust', 'url'=>array('create')),
	array('label'=>'Update TbaAdjust', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TbaAdjust', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TbaAdjust', 'url'=>array('admin')),
);
?>

<h1>View TbaAdjust #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'applyName',
		'applyCode',
		'month',
		'date_sn',
		'date_eo',
		'aft_days',
		'daytime',
		'store',
		'storeCode',
		'o_store',
		'o_storeCode',
		'empno',
		'empname',
		'act_emps',
		'act_empn',
		'user_barcode',
		'user_mobile',
		'reason',
		'memo',
	),
)); ?>
