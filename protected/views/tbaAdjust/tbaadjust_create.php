<style>
	#tdd th, td {
	text-align: left;
	font-size:medium;
	height: 35px;
	border-right-style:hidden;
	border-left-style:hidden;
	}
	#tdd tr:hover {background-color: #ebf9fc;}
	#tdd th {
	background-color: #6caace;
	color: white;
	}
	#tdd td{
		border-bottom: 1px solid #ddd;
	}
	input[type=submit]
    {
        transition-duration: 0.4s;
        background-color: white; 
        color: black; 
        border: 1px solid #6caace;
        font-size:15px;
        font-weight:bold;
    } 
    input[type=submit]:hover
    {
        background-color: #6caace;
        color: white;
    }
    #textA /* inputtext用(原因、備註)*/
    {
        height: 60px;
        width: 60%;
    } 

</style>
<?php
/* @var $this TbaAdjustController */
/* @var $model TbaAdjust */

$this->breadcrumbs=array(
	'Tba Adjusts'=>array('index'),
	'Create',
);

?>
<h1>稽核群組-新增申請項目</h1>
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $msg) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 
		echo "<div class='flash-$key' style='font-size:large'>" . $msg . "</div>\n";}
?>
<?php echo CHtml::beginForm('','post',array('enctype' => 'multipart/form-data')); ?> 
<table id=tdd>
<tr>
    <th>項目名稱<th>
    <th>
    <?php echo CHtml::dropDownList('adjustItem', $adjustItemQry,$adjustItemAry, array('onchange'=>'adjustitem()','empty'=>'選擇調整項目')); ?>
    </th>
    <th>申請月份</th>
    <th>
    <?php
	echo "<select name=month>";
    for($i=0;$i<count($monthlist);$i++)
        {
            echo "<option value=".$i.">".$monthlist[$i]."</option>";
        }
	echo "</select>";
	?>
    </th>
</tr>
</table>
<!-- type1 店業績、問卷、洗髮比 -->
<?php $this->renderPartial('_type1form', array('model'=>$model)); ?>
<!-- type2 拓展店、轉移店-->
<!-- type3 績效獎金名單-->
<!-- type4 票卷輸入-->


<?php echo CHtml::endForm(); ?>

<script>
    //將selectedㄉvalue與text帶入
	function adjustitem(){    
	var e = document.getElementById("adjustItem");
	var dspText = e.options[e.selectedIndex].text;
	var dspValue = e.options[e.selectedIndex].value;
	document.getElementById("applyCode").value = dspValue;
	document.getElementById("applyName").value = dspText;
	}

    function storeMap(){    
	var e = document.getElementById("storeCode");
	var storeName = e.options[e.selectedIndex].text;
	// var storeCode = e.options[e.selectedIndex].value;
	// document.getElementById("applyCode").value = dspValue;
	document.getElementById("storeName").value = storeName;
	}
</script>