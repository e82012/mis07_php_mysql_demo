<?php
/* @var $this TbaAdjustController */
/* @var $model TbaAdjust */

$this->breadcrumbs=array(
	'Tba Adjusts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TbaAdjust', 'url'=>array('index')),
	array('label'=>'Create TbaAdjust', 'url'=>array('create')),
	array('label'=>'View TbaAdjust', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TbaAdjust', 'url'=>array('admin')),
);
?>

<h1>Update TbaAdjust <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>