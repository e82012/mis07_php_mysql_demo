<?php
/* @var $this TbaAdjustController */
/* @var $data TbaAdjust */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('applyName')); ?>:</b>
	<?php echo CHtml::encode($data->applyName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('applyCode')); ?>:</b>
	<?php echo CHtml::encode($data->applyCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('month')); ?>:</b>
	<?php echo CHtml::encode($data->month); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_sn')); ?>:</b>
	<?php echo CHtml::encode($data->date_sn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_eo')); ?>:</b>
	<?php echo CHtml::encode($data->date_eo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aft_days')); ?>:</b>
	<?php echo CHtml::encode($data->aft_days); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('daytime')); ?>:</b>
	<?php echo CHtml::encode($data->daytime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('store')); ?>:</b>
	<?php echo CHtml::encode($data->store); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('storeCode')); ?>:</b>
	<?php echo CHtml::encode($data->storeCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('o_store')); ?>:</b>
	<?php echo CHtml::encode($data->o_store); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('o_storeCode')); ?>:</b>
	<?php echo CHtml::encode($data->o_storeCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('empno')); ?>:</b>
	<?php echo CHtml::encode($data->empno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('empname')); ?>:</b>
	<?php echo CHtml::encode($data->empname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('act_emps')); ?>:</b>
	<?php echo CHtml::encode($data->act_emps); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('act_empn')); ?>:</b>
	<?php echo CHtml::encode($data->act_empn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_barcode')); ?>:</b>
	<?php echo CHtml::encode($data->user_barcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->user_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason')); ?>:</b>
	<?php echo CHtml::encode($data->reason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('memo')); ?>:</b>
	<?php echo CHtml::encode($data->memo); ?>
	<br />

	*/ ?>

</div>