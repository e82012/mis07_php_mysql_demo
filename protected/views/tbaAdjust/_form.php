<?php
/* @var $this TbaAdjustController */
/* @var $model TbaAdjust */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tba-adjust-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'applyName'); ?>
		<?php echo $form->textField($model,'applyName',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'applyName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'applyCode'); ?>
		<?php echo $form->textField($model,'applyCode',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'applyCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'month'); ?>
		<?php echo $form->textField($model,'month',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'month'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_sn'); ?>
		<?php echo $form->textField($model,'date_sn',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'date_sn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_eo'); ?>
		<?php echo $form->textField($model,'date_eo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'date_eo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aft_days'); ?>
		<?php echo $form->textField($model,'aft_days',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'aft_days'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'daytime'); ?>
		<?php echo $form->textField($model,'daytime',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'daytime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'store'); ?>
		<?php echo $form->textField($model,'store',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'store'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'storeCode'); ?>
		<?php echo $form->textField($model,'storeCode',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'storeCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'o_store'); ?>
		<?php echo $form->textField($model,'o_store',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'o_store'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'o_storeCode'); ?>
		<?php echo $form->textField($model,'o_storeCode',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'o_storeCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'empno'); ?>
		<?php echo $form->textField($model,'empno',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'empno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'empname'); ?>
		<?php echo $form->textField($model,'empname',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'empname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'act_emps'); ?>
		<?php echo $form->textField($model,'act_emps',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'act_emps'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'act_empn'); ?>
		<?php echo $form->textField($model,'act_empn',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'act_empn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_barcode'); ?>
		<?php echo $form->textField($model,'user_barcode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'user_barcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_mobile'); ?>
		<?php echo $form->textField($model,'user_mobile',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'user_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reason'); ?>
		<?php echo $form->textField($model,'reason',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'memo'); ?>
		<?php echo $form->textField($model,'memo',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'memo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->