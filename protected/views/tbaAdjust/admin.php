<?php
/* @var $this TbaAdjustController */
/* @var $model TbaAdjust */

$this->breadcrumbs=array(
	'Tba Adjusts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TbaAdjust', 'url'=>array('index')),
	array('label'=>'Create TbaAdjust', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tba-adjust-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tba Adjusts</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tba-adjust-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'applyName',
		'applyCode',
		'month',
		'date_sn',
		'date_eo',
		/*
		'aft_days',
		'daytime',
		'store',
		'storeCode',
		'o_store',
		'o_storeCode',
		'empno',
		'empname',
		'act_emps',
		'act_empn',
		'user_barcode',
		'user_mobile',
		'reason',
		'memo',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
