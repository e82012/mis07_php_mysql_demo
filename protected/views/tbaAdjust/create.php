<?php
/* @var $this TbaAdjustController */
/* @var $model TbaAdjust */

$this->breadcrumbs=array(
	'Tba Adjusts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TbaAdjust', 'url'=>array('index')),
	array('label'=>'Manage TbaAdjust', 'url'=>array('admin')),
);
?>

<h1>Create TbaAdjust</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>