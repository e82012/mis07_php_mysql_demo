<?php
/* @var $this TbaAdjustController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tba Adjusts',
);

$this->menu=array(
	array('label'=>'Create TbaAdjust', 'url'=>array('create')),
	array('label'=>'Manage TbaAdjust', 'url'=>array('admin')),
);
?>

<h1>Tba Adjusts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
