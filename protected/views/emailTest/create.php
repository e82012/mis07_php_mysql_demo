<?php
/* @var $this EmailTestController */
/* @var $model EmailTest */

$this->breadcrumbs=array(
	'Email Tests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmailTest', 'url'=>array('index')),
	array('label'=>'Manage EmailTest', 'url'=>array('admin')),
);
?>

<h1>Create EmailTest</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>