
<?php
/* @var $this EmailTestController */
/* @var $dataProvider CActiveDataProvider */
// header('refresh: 3600;url="index.php?r=emailTest/Emailtools2"'); 
$this->breadcrumbs=array(
	'Email',
);
?>
<H3>自動封存PSM資料 </H3>
<div class="toast" data-autohide="false">
    <div class="toast-header">
      <strong class="mr-auto text-primary">Toast Header</strong>
      <!-- <small class="text-muted">5 mins ago</small> -->
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
    </div>
    <div class="toast-body">
      <p id ='toastP' name ='toastP'>Some text inside the toast body</p>
      <p id ='toastC' name ='toastC'>Some text inside the toast body</p>
    </div>
  </div>
<?php



?>


<script>
$(document).ready(function(){
    var today = new Date();
    var iPassAry = [<?php echo json_encode($dat_data) ;?>];
    var pdate = [<?php echo json_encode($downloadDate) ;?>];

    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
        document.getElementById('toastP').innerText = '更新時間'+'\n'+dateTime+'\n ---ipass一卡通--- \n 下載日期: ' + pdate;
        // console.log(iPassMsg[0][1]);
        // console.log(iPassMsg[0].length);
        var ipassMsg = "";
        for(var i =0;i<iPassAry[0].length;i++)
        {
          // console.log(iPassMsg[0][1]);
          // console.log(iPassMsg[i]);
          ipassMsg = ipassMsg + '【'+iPassAry[0][i]['storename']+'】票數：'+iPassAry[0][i]['tit']+'\n';
        }
        document.getElementById('toastC').innerText = ipassMsg;
        $('.toast').toast('show');
    });

</script>