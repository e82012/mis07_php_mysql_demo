<style>
/* The Modal (background) */
    .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
    animation: fadeIn 0.5s;
    }

    @keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
    }
    .btn-outline-dark:hover {
        /* 時間 */
        transition: 0.5s; 
                /* transition-property: rotate(20deg); */
        transform: rotate(5deg) scale(1.1);
        
    }
</style>

<H3>AJAX測試用</H3>
<form method="post">
    <select id = 'seT_A' name = 'seT_A'>
        <?php
        for($i=0;$i<count($aaAry);$i++)
        {
            echo "<option value =".$aaAry[$i]['applyCode'].">".$aaAry[$i]['applyName']."</option>";
        }
        ?>
    </select>
    <select id = 'seT_B' name = 'seT_B'>
        <?php
        if($abAry == '')
        {
            echo "<option value =''>'無'</option>";
        }
        else
        {
            for($i=0;$i<count($abAry);$i++)
                {
                    echo "<option value =".$abAry[$i]['applyCode'].">".$abAry[$i]['applyName']."</option>";
                }
            }
        ?>
    </select>
    <input type='text' id = 'ts1' name ='ts1' value = '<?php echo $ts1?>'>
    <input type='text' id = 'ts2' name ='ts2' value = '<?php echo $ts2?>'>

    <input type ='submit' id = 'tsB' name = 'tsB'>
    <!-- The Modal -->
    <?php 
        for($i=0;$i<count($aaAry);$i++)
        {
            echo "<div id='myModal_".$i."' class='modal'>";
                // <!-- Modal content -->
                echo "<div class='modal-content'>";
                    // echo '<span class="close" id = "close_'.$i.'"'.' name ="close_'.$i.'"'.' onclick="p_close('."'"."myModal_".$i."'".')"> &times;</span>';
                    // echo '<span class="close" id = "close_'.$i.'"'.' name ="close_'.$i.'"'.'"> &times;</span>';
                    echo "<p>修正代號(".$aaAry[$i]['applyName'].")</p>";
                    echo "<input type ='text' id='tx_$i' name='tx_$i' value = '".$aaAry[$i]['applyCode']."'>";
                    echo '<button type="button" class="btn btn-light" id = "ok_'.$i.'"'.' name ="ok_'.$i.'"'.' onclick="ok_btn('."'"."myModal_".$i."'".')">確定</button>';
                    echo '<button type="button" class="btn btn-light" id = "re_'.$i.'"'.' name ="re_'.$i.'"'.' onclick="re_btn('."'"."myModal_".$i."'".",'".$aaAry[$i]['applyCode']."',"."'"."tx_".$i."'".')">重設</button>';
                echo "</div>";
            echo "</div>";
        }
    ?>
</form>
<!-- popup form -->
<?php
        echo '<table><tr>
            <td>名稱</td>
            <td>代號</td>
            <td>PopForm</td>';
        for($i=0;$i<count($aaAry);$i++)
        {
            echo '<tr>';
            echo '<td>'.$aaAry[$i]['applyName'].'</td>';
            echo '<td>'.$aaAry[$i]['applyCode'].'</td>';
            echo '<td><button id = "pop_'.$i.'"'.' name ="pop_'.$i.'"'.' class = "btn btn-outline-dark" onclick="pop('."'"."myModal_".$i."'".')">'.'pop_'.$i.'</td>';
            echo '</tr>';
        }
        echo '</tr></table>';
        ?>




<script>
$('#seT_A').change(function(){
    aa = document.getElementById("seT_A").value;
    document.getElementById("ts1").value = aa;
    // 傳ts1值出來到tba_adjust_item比對中文名稱，print到ts2
    $.ajax({
        // url:'index.php?r=emailTest/Emailtools1/AjaxTest',
        // url: '/EmailTest/AjaxTest',
        // url: '@Url.Action("AjaxTest","Emailtools1")',
        url:"<? echo Yii::app()->createUrl('EmailTest/AjaxTest'); ?>",
        type:'POST',
        data:{
            item:aa
            //php後端呼叫方式 $_POST['itme']
        },
        // dataType:'html',
        success: function(ts2){
          console.log('查詢tba_adjust_item:'+ts2);
          document.getElementById("ts2").value = ts2;
          // alert('ajax success!'+name);
          alert('success');
          },
        error: function(){alert('Ajax Error!');}
    });
    
    //更新連結
    $.ajax({
        url:"<? echo Yii::app()->createUrl('EmailTest/AjaxTest2'); ?>",
        type:'POST',
        dataType:'json',
        data:{
            item:aa
            //php後端呼叫方式 $_POST['itme']
        },
        success: function(ts3){
            for(i = 0;i<ts3.length;i++)
            {
                document.getElementById("seT_B").options[i] = new Option(ts3[i]['applyName'],ts3[i]['applyDateTime']);
            }
          alert('success');
          },
        error: function(){alert('Ajax Error!');}
    });

});


// Get the modal
// var aaAry = <?php echo json_encode($aaAry) ; ?> ;
    //彈出視窗
function pop(popn) {
    console.log(popn);
    var modal = document.getElementById(popn);
    modal.style.display = "block";
    
}

// function p_close(popc)
// {
//     console.log(popc)
//     var modal = document.getElementById(popc);
//     modal.style.display = "none";
// }
    //確定
function ok_btn(popc)
{
    console.log(popc)
    var modal = document.getElementById(popc);
    modal.style.display = "none"; 
}
    //重設
function re_btn(popc,code,tx)
{
    console.log(popc);
    console.log(code);
    console.log(tx);

    var modal = document.getElementById(popc);
    var txbox = document.getElementById(tx);
    txbox.value=code;
    // document.getElementById()
    // modal.style.display = "none"; 
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    console.log(event);
    var modal = document.getElementsByClassName("modal");
    for(var i =0;i< modal.length;i++)
    {
        if(event.target == modal[i])
        {
            modal[i].style.display = "none";
        }
    }
    // p_close();
}
</script>

