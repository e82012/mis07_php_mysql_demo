<?php
/* @var $this EmailTestController */
/* @var $data EmailTest */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('E_from')); ?>:</b>
	<?php echo CHtml::encode($data->E_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('E_subject')); ?>:</b>
	<?php echo CHtml::encode($data->E_subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('E_emailadd')); ?>:</b>
	<?php echo CHtml::encode($data->E_emailadd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('E_message')); ?>:</b>
	<?php echo CHtml::encode($data->E_message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('E_file')); ?>:</b>
	<?php echo CHtml::encode($data->E_file); ?>
	<br />


</div>