<?php
/* @var $this EmailTestController */
/* @var $model EmailTest */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'email-test-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'E_from'); ?>
		<?php echo $form->textField($model,'E_from',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'E_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'E_subject'); ?>
		<?php echo $form->textField($model,'E_subject',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'E_subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'E_emailadd'); ?>
		<?php echo $form->textField($model,'E_emailadd',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'E_emailadd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'E_message'); ?>
		<?php echo $form->textField($model,'E_message',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'E_message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'E_file'); ?>
		<?php echo $form->textField($model,'E_file',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'E_file'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->