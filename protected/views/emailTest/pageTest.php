<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<h4>分頁</h4>
<?php   
// 檢查PHP版本
echo PHP_VERSION;
echo " ".phpversion();
echo phpversion('tidy'); 
?>
<form method="post">
    <button type = 'button' class="badge badge-primary" id = 'previousPageBTN' name ='previousPageBTN'>上一頁</button>
    <!-- 分頁選單 -->
    <select id ='pageA' name = 'pageA'>
        <?php
        for($i=1;$i<=$total_pages;$i++)
        {
            if($page == $i)
                echo "<option value =$i selected >$i</option>";
            else
                echo "<option value =$i >$i</option>";
        }
        ?>
    </select>
    <button type = 'button' class="badge badge-primary" id = 'nextPageBTN' name ='nextPageBTN'>下一頁</button>
    <input type = 'submit' id = 'subPage' name ='subPage'>
    <p>資料比數: <?php echo $data_nums ?></p>
    <t id = 'currentP' name ='currentP' >目前第<?php echo $page ?>頁</t><t>，共 <?php echo $total_pages ?> 頁 </t>
</form>
<p id = 'contentA' name = 'contentA'>
<p id = 'modalA' name = 'modalA'>

<!-- The Modal -->
<div class="modal" id="modal_0">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<?php
    if(isset($_POST['subPage']))
    {
        CVarDumper::dump($pageAry,10,true);
    }
?>
<div class="toast" data-autohide="false">
    <div class="toast-header">
      <strong class="mr-auto text-primary">Toast Header</strong>
      <!-- <small class="text-muted">5 mins ago</small> -->
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
    </div>
    <div class="toast-body">
      <p id ='toastP' name ='toastP'>Some text inside the toast body</p>
    </div>
  </div>

<script>
    // $(document).ready(function(){
    //     $('.toast').toast('show');
    // });
    //選單page AJAX
    $('#pageA').change(function(){
        var jsPage = document.getElementById('pageA').value;
            console.log(jsPage);
            document.getElementById('toastP').innerText = '目前第'+jsPage+'頁';
            $('.toast').toast('show');

        //更新頁面資料
        $.ajax({
            // url:'index.php?r=emailTest/pageTest/AjaxPage',
            // url: '/EmailTest/AjaxPage',
            // url: '@Url.Action("AjaxPage","pageTest")',
            url:"<?php echo Yii::app()->createUrl('EmailTest/AjaxPage'); ?>",
            type:'POST',
            dataType:'json',
            data:{
                pag:jsPage
                //php後端呼叫方式 $_POST['itme']
            },
            success: function(pageAry){
                console.log(pageAry);
                document.getElementById('contentA').innerText = '';
                document.getElementById('modalA').innerText = '';

                document.getElementById('currentP').innerText = '目前第'+jsPage+'頁';
                for(i = 0;i<pageAry.length;i++)
                {
                    // document.getElementById("ts_A").options[i] = new Option(pageAry[i]['sn'],pageAry[i]['sn']);
                    // document.getElementById('contentA').innerText += "\n"+"流水號:"+pageAry[i]['sn']+"\n"+"店編:"+pageAry[i]['storecode']+"\n"+"時間:"+pageAry[i]['ctime']+"\n";
                    document.getElementById('contentA').innerHTML += "<br>"+"流水號:"+pageAry[i]['sn']+"<br>"+"店編:"+pageAry[i]['storecode']+"<br>"+"時間:"+pageAry[i]['ctime']+"<br>"+
                    "<button id = 'modal_"+i+"' name = 'modal_"+i+"' class='btn btn-primary' data-toggle='modal' data-target='#modal_"+i+"' >內容</button>";
                    // document.getElementById('contentA').innerHTML += "<button>123</button>";
                }
                // document.getElementById('tx').value = pageAry;
                alert('success');
            },
            error: function(){alert('Ajax Error失敗!');}
        });

    });
    $('#nextPageBTN').click(function(){
        var jsPage = document.getElementById('pageA').value;
        jsPage = parseFloat(jsPage)+1;
        var select_p = jsPage-1;
        console.log(jsPage);
        document.getElementById('pageA').options[select_p].selected = true;
        //更新頁面資料
        $.ajax({
            url:"<?php echo Yii::app()->createUrl('EmailTest/AjaxPage'); ?>",
            type:'POST',
            dataType:'json',
            data:{
                pag:jsPage
                //php後端呼叫方式 $_POST['itme']
            },
            success: function(pageAry){
                console.log(pageAry);
                document.getElementById('contentA').innerText = '';
                document.getElementById('currentP').innerText = '目前第'+jsPage+'頁';
                for(i = 0;i<pageAry.length;i++)
                {
                    // document.getElementById("ts_A").options[i] = new Option(pageAry[i]['sn'],pageAry[i]['sn']);
                    document.getElementById('contentA').innerText += "\n"+"流水號:"+pageAry[i]['sn']+"\n"+"店編:"+pageAry[i]['storecode']+"\n"+"時間:"+pageAry[i]['ctime']+"\n";
                }
                // document.getElementById('tx').value = pageAry;
                // alert('success');
            },
            error: function(){alert('Ajax Error!');}
        });

    });
    $('#previousPageBTN').click(function(){
        var jsPage = document.getElementById('pageA').value;
        jsPage = parseFloat(jsPage)-1;
        var select_p = jsPage-1;
        console.log(jsPage);
        document.getElementById('pageA').options[select_p].selected = true;
        //更新頁面資料
        $.ajax({
            url:"<?php echo Yii::app()->createUrl('EmailTest/AjaxPage'); ?>",
            type:'POST',
            dataType:'json',
            data:{
                pag:jsPage
                //php後端呼叫方式 $_POST['itme']
            },
            success: function(pageAry){
                console.log(pageAry);
                document.getElementById('contentA').innerText = '';
                document.getElementById('currentP').innerText = '目前第'+jsPage+'頁';
                for(i = 0;i<pageAry.length;i++)
                {
                    // document.getElementById("ts_A").options[i] = new Option(pageAry[i]['sn'],pageAry[i]['sn']);
                    document.getElementById('contentA').innerText += "\n"+"流水號:"+pageAry[i]['sn']+"\n"+"店編:"+pageAry[i]['storecode']+"\n"+"時間:"+pageAry[i]['ctime']+"\n";
                }
                // document.getElementById('tx').value = pageAry;
                // alert('success');
            },
            error: function(){alert('Ajax Error!');}
        });

    });


</script>