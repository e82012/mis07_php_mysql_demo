<?php
/* @var $this EmailTestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Email Tests',
);

$this->menu=array(
	array('label'=>'Create EmailTest', 'url'=>array('create')),
	array('label'=>'Manage EmailTest', 'url'=>array('admin')),
);
?>

<h1>Email Tests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
