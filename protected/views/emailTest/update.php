<?php
/* @var $this EmailTestController */
/* @var $model EmailTest */

$this->breadcrumbs=array(
	'Email Tests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmailTest', 'url'=>array('index')),
	array('label'=>'Create EmailTest', 'url'=>array('create')),
	array('label'=>'View EmailTest', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmailTest', 'url'=>array('admin')),
);
?>

<h1>Update EmailTest <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>