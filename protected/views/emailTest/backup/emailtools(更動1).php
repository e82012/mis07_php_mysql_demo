
<?php
/* @var $this EmailTestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Email',
);
?>

    <h1>Gmail Inbox</h1>
    <form method="post">
    DateRange :
    <div class="row">
        <div class="col-sm-12">
            <input name="dateS" type="date" id="dateS" value="<?php echo $dateS ?>"/>  ～  <input name="dateE" type="date" id="dateS" value="<?php echo $dateE ?>"/>
        </div>    
    </div> 
    <br>
    EmailFrom : 
    <div class="row">
        <div class="col-sm-4">
            <input style="width:345px;" name="emailfrom" type="text" id="emailfrom" value="<?php echo $emailfrom ?>" />
        </div>
        <div class="col-sm-2">
        <?php echo CHtml::submitButton('查詢', array('name'=>'submit' ,'class'=>'btn btn-outline-info')); ?>
        </div>
    </div>
    <br>    
    <div class="row">
        <div class="col-sm-4">
        <input type="file" class="custom-file-input" id="customFile" name="filename" >
        <label class="custom-file-label" for="customFile">選擇檔案</label>
        </div>
        <div class="col-sm-4">
        <?php echo CHtml::submitButton('上傳', array('name'=>'import' ,'class'=>'btn btn-outline-info')); ?>
        </div>
    </div> 
    <br>
    <?php
            foreach(Yii::app()->user->getFlashes() as $key => $msg) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 
                echo "<div class='flash-$key'>" . $msg . "</div>\n";
            }

            CVarDumper::dump($emailData);   
            CVarDumper::dump(count($emailData));   
    ?>
    </form>
    <?php if(count($emailData)>0) : ?>
    <table>
    <thead>
            <tr>
                <th>From</th>	
                <th>Subject</th>
                <th>attachment</th>
                <th>Date</th>
            </tr>
    </thead> 
        <tr>
                <td> <?php echo imap_utf8($overview[0]->from); ?></td>
                <td> <?php echo imap_utf8($overview[0]->subject); ?></td>
                <td><a href="<?php echo $pathfilename ?>"><?php echo imap_utf8($filename) ?></a></td>
                <td> <?php echo $date; ?></td>
        </tr>
    </table>
               
<?php endif ?>
    

