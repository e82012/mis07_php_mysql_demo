
<?php
/* @var $this EmailTestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Email',
);
$emailfrom = isset($_POST['emailfrom']) ? $_POST['emailfrom'] : ('');
$dateS = isset($_POST['dateS']) ? $_POST['dateS'] : date('');
$dateE = isset($_POST['dateE']) ? $_POST['dateE'] : date('');
?>

    <h1>Gmail Inbox</h1>
    <form method="post">
    DateRange :
    <div class="row">
        <div class="col-sm-12">
            <input name="dateS" type="date" id="dateS" value="<?php echo $dateS ?>"/>  ～  <input name="dateE" type="date" id="dateS" value="<?php echo $dateE ?>"/>
        </div>    
    </div> 
    <br>
    EmailFrom : 
    <div class="row">
        <div class="col-sm-4">
            <input style="width:345px;" name="emailfrom" type="text" id="emailfrom" value="<?php echo $emailfrom ?>" />
        </div>
        <div class="col-sm-2">
        <?php echo CHtml::submitButton('查詢', array('name'=>'submit' ,'class'=>'btn btn-outline-info')); ?>
        </div>
    </div>
    <br>    
    <div class="row">
        <div class="col-sm-4">
        <input type="file" class="custom-file-input" id="customFile" name="filename" >
        <label class="custom-file-label" for="customFile">選擇檔案</label>
        </div>
        <div class="col-sm-4">
        <?php echo CHtml::submitButton('上傳', array('name'=>'import' ,'class'=>'btn btn-outline-info')); ?>
        </div>
    </div> 
    <br>
    <?php
            foreach(Yii::app()->user->getFlashes() as $key => $msg) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 
                echo "<div class='flash-$key'>" . $msg . "</div>\n";
            }
    ?>
    </form>

    <?php
    if(isset($_POST['submit']))
    {
        // CVarDumper::dump($emailfrom);
        // CVarDumper::dump($dateS);
        // CVarDumper::dump($dateE);
    
        if (! function_exists('imap_open')) {
            echo "IMAP is not configured.";
            exit();
        } else {
    ?>
     
        <?php            
            $hostname = '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX';
            $username = 'jitmis2020@gmail.com';
            $password = '4180000000';
            /* try to connect */
            $connection = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());


            /* Search Emails having the specified keyword in the email subject */
            // $emailData = imap_search($connection, "FROM '$emailfrom'"); //from who send mail ap-public@i-pass.com.tw;
            // $emailData = imap_search($connection,'FROM "ap-public@i-pass.com.tw" SINCE "10-August-2020" BEFORE "15-August-2020"');
            $emailData = imap_search($connection,"FROM $emailfrom SINCE $dateS BEFORE $dateE");
        
            if (! empty($emailData)) {
        ?>
                <table>
                <thead>
                        <tr>
                            <th>From</th>	
                            <th>Subject</th>
                            <th>attachment</th>
                            <th>Date</th>
                        </tr>
                </thead> 
                    <?php
                    foreach ($emailData as $emailIdent) {
                    
                    $overview = imap_fetch_overview($connection, $emailIdent, 0);
                    $message = imap_fetchbody($connection, $emailIdent,'1.1');
                    $structure = imap_fetchstructure($connection,$emailIdent); 
                    $messageExcerpt = substr($message, 0, 150);
                    $partialMessage = trim(quoted_printable_decode($messageExcerpt)); 
                    $date = date("Ymd", strtotime($overview[0]->date));

                    $attachments = array(); 
                    if(isset($structure->parts) && count($structure->parts)) 
                        { 
                            for($i = 0; $i < count($structure->parts); $i++) 
                            { 
                            $attachments[$i] = array(
                                'is_attachment' => false, 
                                'filename' => '', 
                                'name' => '', 
                                'attachment' => '' 
                            ); 

                            if($structure->parts[$i]->ifdparameters) 
                            { 
                                foreach($structure->parts[$i]->dparameters as $object) 
                                { 
                                if(strtolower($object->attribute) == 'filename') 
                                { 
                                $attachments[$i]['is_attachment'] = true; 
                                $attachments[$i]['filename'] = $object->value; 
                                } 
                                } 
                            } 

                            if($structure->parts[$i]->ifparameters) 
                            { 
                                foreach($structure->parts[$i]->parameters as $object) 
                                { 
                                if(strtolower($object->attribute) == 'name') 
                                { 
                                $attachments[$i]['is_attachment'] = true; 
                                $attachments[$i]['name'] = $object->value; 
                                } 
                                } 
                            } 

                            if($attachments[$i]['is_attachment']) 
                            { 
                                $attachments[$i]['attachment'] = imap_fetchbody($connection,$emailIdent, $i+1); 

                                /* 3 = BASE64 encoding */ 
                                if($structure->parts[$i]->encoding == 3) 
                                { 
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']); 
                                } 
                                /* 4 = QUOTED-PRINTABLE encoding */ 
                                elseif($structure->parts[$i]->encoding == 4) 
                                { 
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']); 
                                } 
                            } 
                            } 
                            } 

                            /* iterate through each attachment and save it */ 
                            foreach($attachments as $key=>$attachment) 
                            { 
                            if($attachment['is_attachment'] == 1) 
                            { 
                            $filename = $attachment['name']; 
                            if(empty($filename)) $filename = $attachment['filename']; 

                            if(empty($filename)) $filename = time() . ".dat"; 

                            /* prefix the email number to the filename in case two emails 
                            * have the attachment with the same file name. 
                            */
                            $pathfilename =  "./attachment/" . $date.'-'.$emailIdent.'.xls';
                            $fp = fopen($pathfilename, "w+"); // "-" . imap_utf8($filename) 
                            fwrite($fp, $attachment['attachment']); 
                            fclose($fp); 

                            $attachments[$key]['filename'] = $filename; 
                            } 
                        } 
                    ?>

                    <tr>
                            <td> <?php echo imap_utf8($overview[0]->from); ?></td>
                            <td> <?php echo imap_utf8($overview[0]->subject); ?></td>
                            <td><a href="<?php echo $pathfilename ?>"><?php echo imap_utf8($filename) ?></a></td>
                            <td> <?php echo $date; ?></td>
                    </tr>
                    <?php
                } // End foreach
                ?>
                </table>
                <?php
            } // end if 
            imap_close($connection);
        }
    }
    ?>

    

