<?php
/* @var $this EmailTestController */
/* @var $model EmailTest */

$this->breadcrumbs=array(
	'Email Tests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmailTest', 'url'=>array('index')),
	array('label'=>'Create EmailTest', 'url'=>array('create')),
	array('label'=>'Update EmailTest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmailTest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmailTest', 'url'=>array('admin')),
);
?>

<h1>View EmailTest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'E_from',
		'E_subject',
		'E_emailadd',
		'E_message',
		'E_file',
	),
)); ?>
