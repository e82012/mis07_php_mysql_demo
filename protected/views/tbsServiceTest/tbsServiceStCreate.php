<style>
	th, td {
		border-bottom: 1px solid #ddd;
	}
	tr:hover {background-color: #f5f5f5;}
	th {
	background-color: #6caace;
	color: white;
	}
	input[type=submit]
    {
        transition-duration: 0.4s;
        background-color: white; 
        color: black; 
        border: 1px solid #6caace;
        font-size:15px;
        font-weight:bold;
    } 
    input[type=submit]:hover
    {
        background-color: #6caace;
        color: white;
    }
</style>
<?php
/* @var $this TbsServiceTestController */

$this->breadcrumbs=array(
	'Tbs Service Test',
);
?>
<h1>門市服務產生</h1>
<?php echo CHtml::beginForm('','post',array('enctype' => 'multipart/form-data')); ?> 
<input type ="hidden" name="myip" Value="<?php echo $myip; ?>" readonly >
<input type ="hidden" name="storecode" Value="<?php echo $storecode; ?>" readonly >
<input type ="hidden" name="storename" Value="<?php echo $storename; ?>" readonly >

版本日期 : <?php echo date('Ymd') ?><input type ="hidden" name="version" Value="<?php echo date('Ymd') ?>" readonly >
登入門市 : (<?php echo $myip; ?>)  <?php echo $storecode; ?> <?php echo $storename; ?> 
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $msg) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 
		echo "<div class='flash-$key'>" . $msg . "</div>\n";}
?>
<?php
echo "<table>";
echo "<tr>";
	echo "<th width='40%'>品牌</th>";
	echo "<th width='20%'></th>";
	echo "<th width='20%''></th>";
	echo "<th width='20%'></th>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width='20%'><input type='radio' name='brand' value='JIT' required>JIT</td>";
    echo "<td width='20%'><input type='radio' name='brand' value='精剪' required>精剪達人</td>";
	echo "<td></td>";
	echo "<td width='20%'><input type='radio' name='brand' value='PK100' required>PK100</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<th>服務項目Type 1</th>";
	echo "<th>全天</th>";
	echo "<th>離峰</th>";
	echo "<th>尖峰</th>";
	echo "</tr>";
		for($i=0;$i<count($itemtype_1);$i++)
		{
		echo "<tr>
		<td><label><input type='checkbox' id=sv_1_".$i." name=sv_1_".$i." value=".$itemtype_1[$i]['sale_no']." onchange='changeThis(this)'>　".$itemtype_1[$i]['sale_name']."</label></td>
		<td><input type='radio' name='svt_1_".$i."' id = svt_1_".$i."_1 value='1' disabled ></td>
		<td><input type='radio' name='svt_1_".$i."' id = svt_1_".$i."_2 value='2' disabled ></td>
		<td><input type='radio' name='svt_1_".$i."' id = svt_1_".$i."_3 value='3' disabled ></td>
		</tr>";
		}
	echo "<tr>";
	echo "<th>髮品項目Type 2</th>";
	echo "<th>全天</th>";
	echo "<th>離峰</th>";
	echo "<th>尖峰</th>";
	echo "</tr>";
		for($i=0;$i<count($itemtype_2);$i++)
		{
		echo "<tr>
		<td><label><input type='checkbox' id=sv_2_".$i." name=sv_2_".$i." value=".$itemtype_2[$i]['sale_no']." onchange='changeThis2(this)'>　".$itemtype_2[$i]['sale_name']."</label></td>
		<td><input type='radio' name='svt_2_".$i."' id = svt_2_".$i."_1 value='1' disabled ></td>
		<td><input type='radio' name='svt_2_".$i."' id = svt_2_".$i."_2 value='2' disabled ></td>
		<td><input type='radio' name='svt_2_".$i."' id = svt_2_".$i."_3 value='3' disabled ></td>
		</tr>";
		}
	echo "<tr>";
	echo "<th>優惠項目Type 3</th>";
	echo "<th>全天</th>";
	echo "<th>離峰</th>";
	echo "<th>尖峰</th>";
	echo "</tr>";
	for($i=0;$i<count($itemtype_3);$i++)
		{
		echo "<tr>
		<td><label><input type='checkbox' id=sv_3_".$i." name=sv_3_".$i." value=".$itemtype_3[$i]['sale_no']." onchange='changeThis3(this)'>　".$itemtype_3[$i]['sale_name']."</label></td>
		<td><input type='radio' name='svt_3_".$i."' id = svt_3_".$i."_1 value='1' disabled ></td>
		<td><input type='radio' name='svt_3_".$i."' id = svt_3_".$i."_2 value='2' disabled ></td>
		<td><input type='radio' name='svt_3_".$i."' id = svt_3_".$i."_3 value='3' disabled ></td>
		</tr>";
		}
		echo "<tr>";
	echo "<th>其他項目Type 4</th>";
	echo "<th>全天</th>";
	echo "<th>離峰</th>";
	echo "<th>尖峰</th>";
	echo "</tr>";
	for($i=0;$i<count($itemtype_4);$i++)
		{
		echo "<tr>
		<td><label><input type='checkbox' id=sv_4_".$i." name=sv_4_".$i." value=".$itemtype_4[$i]['sale_no']." onchange='changeThis4(this)'>　".$itemtype_4[$i]['sale_name']."</label></td>
		<td><input type='radio' name='svt_4_".$i."' id = svt_4_".$i."_1 value='1' disabled ></td>
		<td><input type='radio' name='svt_4_".$i."' id = svt_4_".$i."_2 value='2' disabled ></td>
		<td><input type='radio' name='svt_4_".$i."' id = svt_4_".$i."_3 value='3' disabled ></td>
		</tr>";
		}
echo"</table>";

?>
<div align="center">
<?php echo CHtml::submitButton('　　新　增　　', array('name'=>'import' ,'class'=>'btn')); ?>
</div>
<?php echo CHtml::endForm(); ?> 



<script>
//radiobutton取消選取
	$(function(){
		$('input[type="radio"]').on('mousedown',function(evt){
			evt.preventDefault();
			this.checked=!this.checked;
		}).on('mouseup',function(evt){
			evt.preventDefault();
		}).on('click',function(evt){
			evt.preventDefault();
		});
	});
//checked before radio true
	function changeThis(sender) 
	{ 
		for(var i=0;i<=100;i++)
		{
			if(document.getElementById("sv_1_"+i).checked)
			{
				document.getElementById("svt_1_"+i+"_1").removeAttribute('disabled');
				document.getElementById("svt_1_"+i+"_2").removeAttribute('disabled');
				document.getElementById("svt_1_"+i+"_3").removeAttribute('disabled');
				document.getElementById("svt_1_"+i+"_1").setAttribute('required','');
				document.getElementById("svt_1_"+i+"_2").setAttribute('required','');
				document.getElementById("svt_1_"+i+"_3").setAttribute('required','');
			}
			else
			{
				document.getElementById("svt_1_"+i+"_1").disabled = true;
				document.getElementById("svt_1_"+i+"_2").disabled = true;
				document.getElementById("svt_1_"+i+"_3").disabled = true;
			}
		}	
	}
	function changeThis2(sender) 
	{
		for(var i=0;i<=100;i++)
		{
			if(document.getElementById("sv_2_"+i).checked)
			{
				document.getElementById("svt_2_"+i+"_1").removeAttribute('disabled');
				document.getElementById("svt_2_"+i+"_2").removeAttribute('disabled');
				document.getElementById("svt_2_"+i+"_3").removeAttribute('disabled');
				document.getElementById("svt_2_"+i+"_1").setAttribute('required','');
				document.getElementById("svt_2_"+i+"_2").setAttribute('required','');
				document.getElementById("svt_2_"+i+"_3").setAttribute('required','');
			}
			else
			{
				document.getElementById("svt_2_"+i+"_1").disabled = true;
				document.getElementById("svt_2_"+i+"_2").disabled = true;
				document.getElementById("svt_2_"+i+"_3").disabled = true;
			}
		}	

	}
	function changeThis3(sender) 
	{
		for(var i=0;i<=100;i++)
		{
			if(document.getElementById("sv_3_"+i).checked)
			{
				document.getElementById("svt_3_"+i+"_1").removeAttribute('disabled');
				document.getElementById("svt_3_"+i+"_2").removeAttribute('disabled');
				document.getElementById("svt_3_"+i+"_3").removeAttribute('disabled');
				document.getElementById("svt_3_"+i+"_1").setAttribute('required','');
				document.getElementById("svt_3_"+i+"_2").setAttribute('required','');
				document.getElementById("svt_3_"+i+"_3").setAttribute('required','');
			}
			else
			{
				document.getElementById("svt_3_"+i+"_1").disabled = true;
				document.getElementById("svt_3_"+i+"_2").disabled = true;
				document.getElementById("svt_3_"+i+"_3").disabled = true;
			}
		}	

	}
	function changeThis4(sender) 
	{
		for(var i=0;i<=100;i++)
		{
			if(document.getElementById("sv_4_"+i).checked)
			{
				document.getElementById("svt_4_"+i+"_1").removeAttribute('disabled');
				document.getElementById("svt_4_"+i+"_2").removeAttribute('disabled');
				document.getElementById("svt_4_"+i+"_3").removeAttribute('disabled');
				document.getElementById("svt_4_"+i+"_1").setAttribute('required','');
				document.getElementById("svt_4_"+i+"_2").setAttribute('required','');
				document.getElementById("svt_4_"+i+"_3").setAttribute('required','');
			}
			else
			{
				document.getElementById("svt_4_"+i+"_1").disabled = true;
				document.getElementById("svt_4_"+i+"_2").disabled = true;
				document.getElementById("svt_4_"+i+"_3").disabled = true;
			}
		}	

	}
	
</script>