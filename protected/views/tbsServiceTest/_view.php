<?php
/* @var $this TbsServiceTestController */
/* @var $data tbsServiceTest */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('version')); ?>:</b>
	<?php echo CHtml::encode($data->version); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('storeCode')); ?>:</b>
	<?php echo CHtml::encode($data->storeCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('storeName')); ?>:</b>
	<?php echo CHtml::encode($data->storeName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brand')); ?>:</b>
	<?php echo CHtml::encode($data->brand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sale_no')); ?>:</b>
	<?php echo CHtml::encode($data->sale_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sale_name')); ?>:</b>
	<?php echo CHtml::encode($data->sale_name); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sv_time')); ?>:</b>
	<?php echo CHtml::encode($data->sv_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sv_timename')); ?>:</b>
	<?php echo CHtml::encode($data->sv_timename); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sv_type')); ?>:</b>
	<?php echo CHtml::encode($data->sv_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sv_order')); ?>:</b>
	<?php echo CHtml::encode($data->sv_order); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num')); ?>:</b>
	<?php echo CHtml::encode($data->num); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perform_mapping')); ?>:</b>
	<?php echo CHtml::encode($data->perform_mapping); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('memo')); ?>:</b>
	<?php echo CHtml::encode($data->memo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ip')); ?>:</b>
	<?php echo CHtml::encode($data->ip); ?>
	<br />

	*/ ?>

</div>