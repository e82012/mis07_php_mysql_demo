<?php
/* @var $this TbsServiceTestController */
/* @var $model tbsServiceTest */

$this->breadcrumbs=array(
	'Tbs Service Tests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List tbsServiceTest', 'url'=>array('index')),
	array('label'=>'Manage tbsServiceTest', 'url'=>array('admin')),
);
?>

<h1>Create tbsServiceTest</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>