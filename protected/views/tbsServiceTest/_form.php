<?php
/* @var $this TbsServiceTestController */
/* @var $model tbsServiceTest */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tbs-service-test-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'version'); ?>
		<?php echo $form->textField($model,'version',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'version'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'storeCode'); ?>
		<?php echo $form->textField($model,'storeCode',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'storeCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'storeName'); ?>
		<?php echo $form->textField($model,'storeName',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'storeName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'brand'); ?>
		<?php echo $form->textField($model,'brand',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'brand'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sale_no'); ?>
		<?php echo $form->textField($model,'sale_no',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'sale_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sale_name'); ?>
		<?php echo $form->textField($model,'sale_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'sale_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sv_time'); ?>
		<?php echo $form->textField($model,'sv_time',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'sv_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sv_timename'); ?>
		<?php echo $form->textField($model,'sv_timename',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'sv_timename'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sv_type'); ?>
		<?php echo $form->textField($model,'sv_type',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'sv_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sv_order'); ?>
		<?php echo $form->textField($model,'sv_order'); ?>
		<?php echo $form->error($model,'sv_order'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'num'); ?>
		<?php echo $form->textField($model,'num'); ?>
		<?php echo $form->error($model,'num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'perform_mapping'); ?>
		<?php echo $form->textField($model,'perform_mapping',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'perform_mapping'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'memo'); ?>
		<?php echo $form->textField($model,'memo',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'memo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ip'); ?>
		<?php echo $form->textField($model,'ip',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ip'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->