<?php
/* @var $this TbsServiceTestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tbs Service Tests',
);

$this->menu=array(
	array('label'=>'Create tbsServiceTest', 'url'=>array('create')),
	array('label'=>'Manage tbsServiceTest', 'url'=>array('admin')),
);
?>

<h1>Tbs Service Tests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
