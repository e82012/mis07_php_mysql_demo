<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    #sortable li
    {
        counter-increment:step-counter;
         margin-bottom: 10px;
    }
    #sortable li:before
    {
        content: counter(step-counter);
        margin-right: 5px;
        font-size: 80%;
        background-color: #6caace;
        color: white;
        font-weight: bold;
        padding: 3px 8px;
        border-radius: 3px;
    }
    #sortable li:hover
    {
        background-color: #ebf9fc;
        color: black;
    }
</style>
<?php
/* @var $this TbsServiceTestController */

$this->breadcrumbs=array(
	'Tbs Service Test',
);
?>
<h1>售票機服務排序</h1>
<?php echo CHtml::beginForm('','post',array('enctype' => 'multipart/form-data','id'=>'form1')); ?> 
<input type ="hidden" name="myip" Value="<?php echo $myip; ?>" readonly >
<input type ="hidden" name="storecode" Value="<?php echo $storecode; ?>" readonly >
<input type ="hidden" name="storename" Value="<?php echo $storename; ?>" readonly >

<!-- 版本日期 : <?php echo date('Ymd') ?><input type ="hidden" name="version" Value="<?php echo date('Ymd') ?>" readonly > -->
登入門市 : (<?php echo $myip; ?>)  <?php echo $storecode; ?> <?php echo $storename; ?> 
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $msg) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 
		echo "<div class='flash-$key'>" . $msg . "</div>\n";}
?>
<div class="row">
	<div class="col-sm-3">
        版本：<?php echo CHtml::dropDownList('qryversion', $qryversion,$versionAry, array('style' => 'font-size: 16px' ,'value'=>$qryversion)); ?>
    </div>
</div>
<div class='btn-group'>
    <?php echo CHtml::submitButton('服務排序', array('name'=>'sv_submit' ,'class'=>'btn btn-outline-secondary btn-sm')); ?>
    <?php echo CHtml::submitButton('髮品排序', array('name'=>'pd_submit' ,'class'=>'btn btn-outline-secondary btn-sm')); ?>
    <?php echo CHtml::submitButton('優惠排序', array('name'=>'ev_submit' ,'class'=>'btn btn-outline-secondary btn-sm')); ?>
    <?php echo CHtml::submitButton('其他排序', array('name'=>'ot_submit' ,'class'=>'btn btn-outline-secondary btn-sm')); ?>
</div>
<br>
<br>
<?php
	echo "<ul name = 'sortable' id='sortable' class='list-group list-group-flush'>";
		for($i=0;$i<count($sortAry);$i++){
		echo "<li name = item'".$i."' id='".$sortAry[$i]['id']."' class='list-group-item list-group-item-action' sv_sortable='".$sortAry[$i]['sv_order']."'>".$sortAry[$i]['sale_name']."</li>";
        // echo "<li><input type=textbox name = item'".$i."' itemid='".$sortAry[$i]['id']."' class='list-group-item list-group-item-action' sv_sortable='".$sortAry[$i]['sv_order']."' value='".$sortAry[$i]['sale_name']."' readonly></li>";
    }

	echo "</ul>";
?>
<input type="hidden" name ="txt" id="txt">
<?php 
?>
<pre></pre>
<?php echo CHtml::submitButton('確定排序', array('name'=>'sv_save' ,'id'=>'sv_save','class'=>'btn btn-outline-secondary', )); ?>
<!-- <input name="sv_save" id="sv_save" class="btn btn-outline-secondary" type="submit" value="確定排序"> -->

<?php echo CHtml::endForm(); ?> 
<?php 

// CVarDumper::dump($test,10,true);
// CVarDumper::dump(count($test));

?>
<script>
//sortable 移動 紀錄新排序資料---
 $(function() {

    $( "#sortable" ).sortable({
        start: function(e, ui) {
        // creates a temporary attribute on the element with the old index
        $(this).attr('data-previndex', ui.item.index());
    },
    update: function(e, ui) {
        $(this).children().each(function (index) {
                        if ($(this).attr('sv_sortable') != (index+1)) {
                            $(this).attr('sv_sortable', (index+1)).addClass('updated'); 
                        }
                
                var values = $("#sortable li").map(function() {
                return ($(this).attr("id")); 
                }).get();
                document.getElementById("txt").value = values;
                $txt.value = values;
                   });            
    },
    
        opacity: 0.6, //透明度
        }
    );
  });
</script>
<?php


?>