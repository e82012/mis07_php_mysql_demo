<?php
/* @var $this TbsServiceTestController */
/* @var $model tbsServiceTest */

$this->breadcrumbs=array(
	'Tbs Service Tests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List tbsServiceTest', 'url'=>array('index')),
	array('label'=>'Create tbsServiceTest', 'url'=>array('create')),
	array('label'=>'Update tbsServiceTest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete tbsServiceTest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage tbsServiceTest', 'url'=>array('admin')),
);
?>

<h1>View tbsServiceTest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'version',
		'storeCode',
		'storeName',
		'brand',
		'sale_no',
		'sale_name',
		'sv_time',
		'sv_timename',
		'sv_type',
		'sv_order',
		'num',
		'price',
		'perform_mapping',
		'memo',
		'ip',
	),
)); ?>
