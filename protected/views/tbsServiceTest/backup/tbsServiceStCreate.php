<?php
/* @var $this TbsServiceTestController */

$this->breadcrumbs=array(
	'Tbs Service Test',
);
?>
<h1>門市服務產生</h1>
<?php echo CHtml::beginForm('','post',array('enctype' => 'multipart/form-data')); ?> 
<input type ="hidden" name="myip" Value="<?php echo $myip; ?>" readonly >
<input type ="hidden" name="storecode" Value="<?php echo $storecode; ?>" readonly >
<input type ="hidden" name="storename" Value="<?php echo $storename; ?>" readonly >

版本日期 : <?php echo date('Ymd') ?><input type ="hidden" name="version" Value="<?php echo date('Ymd') ?>" readonly >
登入門市 : (<?php echo $myip; ?>)  <?php echo $storecode; ?> <?php echo $storename; ?> 
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $msg) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 
		echo "<div class='flash-$key'>" . $msg . "</div>\n";}
?>
<table>
<tr>
	<th width="40%">品牌</th>	
	<th width="20%"><input type="radio" name="brand" value="JIT" required>JIT</th>
    <th width="20%"><input type="radio" name="brand" value="精剪" required>精剪達人</th>
	<th width="20%"><input type="radio" name="brand" value="PK100" required>PK100</th>
</tr>
<tr>
	<th>剪髮</th>	
	<th>全天</th>
	<th>離峰</th>
	<th>尖峰</th>
</tr>
	<tr>
		<td><label><input type ="checkbox" name="service1" id = "A0100" Value="A0100" onchange="changeThis(this)">剪髮100</label></td>
		<td><input type="radio" name="sv_time1" id = "A0100time1" value="1" disabled ></td>
		<td><input type="radio" name="sv_time1" id = "A0100time2" value="2" disabled ></td>
		<td><input type="radio" name="sv_time1" id = "A0100time3" value="3" disabled ></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service2" id="A0101" Value="A0101" onchange="changeThis(this)">剪髮100(含沖洗)</label></td>
		<td><input type="radio" name="sv_time2" id="A0101time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time2" id="A0101time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time2" id="A0101time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service3" id= "A0200" Value="A0200" onchange="changeThis(this)">剪髮110</label></td>
		<td><input type="radio" name="sv_time3" id="A0200time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time3" id="A0200time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time3" id="A0200time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service4" id="A0201" Value="A0201" onchange="changeThis(this)">剪髮110(含沖洗)</label></td>
		<td><input type="radio" name="sv_time4" id="A0201time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time4" id="A0201time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time4" id="A0201time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service5" id="A0300" Value="A0300" onchange="changeThis(this)">剪髮80</label></td>
		<td><input type="radio" name="sv_time5" id="A0300time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time5" id="A0300time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time5" id="A0300time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service6" id="A0301" Value="A0301" onchange="changeThis(this)">剪髮80(含沖洗)</label></td>
		<td><input type="radio" name="sv_time6" id="A0301time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time6" id="A0301time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time6" id="A0301time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service7" id="A0400" Value="A0400" onchange="changeThis(this)">女剪</label></td>
		<td><input type="radio" name="sv_time7" id="A0400time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time7" id="A0400time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time7" id="A0400time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service8" id="A0401" Value="A0401" onchange="changeThis(this)">女剪(含沖洗)</label></td>
		<td><input type="radio" name="sv_time8" id="A0401time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time8" id="A0401time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time8" id="A0401time3" value="3" disabled></td>
	</tr>
<tr>
	<th>洗髮</th>	
	<th>全天</th>
	<th>離峰</th>
	<th>尖峰</th>
</tr>
	<tr>
		<td><label><input type ="checkbox" name="service9" id="D000" Value="D000" onchange="changeThis(this)">洗髮0</label></td>
		<td><input type="radio" name="sv_time9" id="D0001" value="1" disabled></td>
		<td><input type="radio" name="sv_time9" id="D0002" value="2" disabled></td>
		<td><input type="radio" name="sv_time9" id="D0003" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service10" id="D100" Value="D0100" onchange="changeThis(this)">洗髮10</label></td>
		<td><input type="radio" name="sv_time10" id="D100time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time10" id="D100time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time10" id="D100time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service11" id="D200" Value="D0200" onchange="changeThis(this)">洗髮20</label></td>
		<td><input type="radio" name="sv_time11" id="D200time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time11" id="D200time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time11" id="D200time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service12" id="D300" Value="D0300" onchange="changeThis(this)">洗髮30</label></td>
		<td><input type="radio" name="sv_time12" id="D300time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time12" id="D300time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time12" id="D300time3" value="3" disabled></td>
	</tr>
<tr>
	<th>促銷</th>	
	<th>全天</th>
	<th>離峰</th>
	<th>尖峰</th>
</tr>
	<tr>
		<td><label><input type ="checkbox" name="service13" id="Z0200" Value="Z0200" onchange="changeThis(this)">VIP卷(100)</label></td>
		<td><input type="radio" name="sv_time13" id="Z0200time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time13" id="Z0200time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time13" id="Z0200time3" value="3" disabled></td>
	</tr>
	<tr>
		<td><label><input type ="checkbox" name="service14" id="Z0201" Value="Z0201" onchange="changeThis(this)">VIP卷(100)(含沖洗)</label></td>
		<td><input type="radio" name="sv_time14" id="Z0201time1" value="1" disabled></td>
		<td><input type="radio" name="sv_time14" id="Z0201time2" value="2" disabled></td>
		<td><input type="radio" name="sv_time14" id="Z0201time3" value="3" disabled></td>
	</tr>

</table>

<?php echo CHtml::submitButton('import', array('name'=>'import' ,'class'=>'btn btn-outline-secondary')); ?>
<?php echo CHtml::endForm(); ?> 
<script>
//radiobutton取消選取
$(function(){
	$('input[type="radio"]').on('mousedown',function(evt){
		evt.preventDefault();
		this.checked=!this.checked;
	}).on('mouseup',function(evt){
		evt.preventDefault();
	}).on('click',function(evt){
		evt.preventDefault();
	});
});

//checked before radio true
function changeThis(sender) { 
  	if(document.getElementById('A0101').checked){
    document.getElementById("A0101time1").removeAttribute('disabled');
	document.getElementById("A0101time2").removeAttribute('disabled');
	document.getElementById("A0101time3").removeAttribute('disabled');
	document.getElementById("A0101time1").setAttribute('required','');
	document.getElementById("A0101time2").setAttribute('required','');
	document.getElementById("A0101time3").setAttribute('required','');
  	}
  	else{
    document.getElementById("A0101time1").disabled = true;
	document.getElementById("A0101time2").disabled = true;
	document.getElementById("A0101time3").disabled = true;
  	}

  	if(document.getElementById('A0100').checked){
	document.getElementById("A0100time1").removeAttribute('disabled');
	document.getElementById("A0100time2").removeAttribute('disabled');
	document.getElementById("A0100time3").removeAttribute('disabled');
	document.getElementById("A0100time1").setAttribute('required','');
	document.getElementById("A0100time2").setAttribute('required','');
	document.getElementById("A0100time3").setAttribute('required','');
	}
	else{
	document.getElementById("A0100time1").disabled = true;
	document.getElementById("A0100time2").disabled = true;
	document.getElementById("A0100time3").disabled = true;
	}

	if(document.getElementById('A0201').checked){
	document.getElementById("A0201time1").removeAttribute('disabled');
	document.getElementById("A0201time2").removeAttribute('disabled');
	document.getElementById("A0201time3").removeAttribute('disabled');
	document.getElementById("A0201time1").setAttribute('required','');
	document.getElementById("A0201time2").setAttribute('required','');
	document.getElementById("A0201time3").setAttribute('required','');
	}
	else{
	document.getElementById("A0201time1").disabled = true;
	document.getElementById("A0201time2").disabled = true;
	document.getElementById("A0201time3").disabled = true;
	}

	if(document.getElementById('A0200').checked){
	document.getElementById("A0200time1").removeAttribute('disabled');
	document.getElementById("A0200time2").removeAttribute('disabled');
	document.getElementById("A0200time3").removeAttribute('disabled');
	document.getElementById("A0200time1").setAttribute('required','');
	document.getElementById("A0200time2").setAttribute('required','');
	document.getElementById("A0200time3").setAttribute('required','');
	}
	else{
	document.getElementById("A0200time1").disabled = true;
	document.getElementById("A0200time2").disabled = true;
	document.getElementById("A0200time3").disabled = true;
	}
	
	if(document.getElementById('A0301').checked){
	document.getElementById("A0301time1").removeAttribute('disabled');
	document.getElementById("A0301time2").removeAttribute('disabled');
	document.getElementById("A0301time3").removeAttribute('disabled');
	document.getElementById("A0301time1").setAttribute('required','');
	document.getElementById("A0301time2").setAttribute('required','');
	document.getElementById("A0301time3").setAttribute('required','');
	}
	else{
	document.getElementById("A0301time1").disabled = true;
	document.getElementById("A0301time2").disabled = true;
	document.getElementById("A0301time3").disabled = true;
	}

	if(document.getElementById('A0300').checked){
	document.getElementById("A0300time1").removeAttribute('disabled');
	document.getElementById("A0300time2").removeAttribute('disabled');
	document.getElementById("A0300time3").removeAttribute('disabled');
	document.getElementById("A0300time1").setAttribute('required','');
	document.getElementById("A0300time2").setAttribute('required','');
	document.getElementById("A0300time3").setAttribute('required','');
	}
	else{
	document.getElementById("A0300time1").disabled = true;
	document.getElementById("A0300time2").disabled = true;
	document.getElementById("A0300time3").disabled = true;
	}

	if(document.getElementById('A0401').checked){
	document.getElementById("A0401time1").removeAttribute('disabled');
	document.getElementById("A0401time2").removeAttribute('disabled');
	document.getElementById("A0401time3").removeAttribute('disabled');
	document.getElementById("A0401time1").setAttribute('required','');
	document.getElementById("A0401time2").setAttribute('required','');
	document.getElementById("A0401time3").setAttribute('required','');
	}
	else{
	document.getElementById("A0401time1").disabled = true;
	document.getElementById("A0401time2").disabled = true;
	document.getElementById("A0401time3").disabled = true;
	}

	if(document.getElementById('A0400').checked){
	document.getElementById("A0400time1").removeAttribute('disabled');
	document.getElementById("A0400time2").removeAttribute('disabled');
	document.getElementById("A0400time3").removeAttribute('disabled');
	document.getElementById("A0400time1").setAttribute('required','');
	document.getElementById("A0400time2").setAttribute('required','');
	document.getElementById("A0400time3").setAttribute('required','');
	}
	else{
	document.getElementById("A0400time1").disabled = true;
	document.getElementById("A0400time2").disabled = true;
	document.getElementById("A0400time3").disabled = true;
	}

	if(document.getElementById('D000').checked){
	document.getElementById("D0001").removeAttribute('disabled');
	document.getElementById("D0002").removeAttribute('disabled');
	document.getElementById("D0003").removeAttribute('disabled');
	document.getElementById("D0001").setAttribute('required','');
	document.getElementById("D0002").setAttribute('required','');
	document.getElementById("D0003").setAttribute('required','');
	}
	else{
	document.getElementById("D0001").disabled = true;
	document.getElementById("D0002").disabled = true;
	document.getElementById("D0003").disabled = true;
	}

	if(document.getElementById('D100').checked){
	document.getElementById("D100time1").removeAttribute('disabled');
	document.getElementById("D100time2").removeAttribute('disabled');
	document.getElementById("D100time3").removeAttribute('disabled');
	document.getElementById("D100time1").setAttribute('required','');
	document.getElementById("D100time2").setAttribute('required','');
	document.getElementById("D100time3").setAttribute('required','');
	
	}
	else{
	document.getElementById("D100time1").disabled = true;
	document.getElementById("D100time2").disabled = true;
	document.getElementById("D100time3").disabled = true;
	}

	if(document.getElementById('D200').checked){
	document.getElementById("D200time1").removeAttribute('disabled');
	document.getElementById("D200time2").removeAttribute('disabled');
	document.getElementById("D200time3").removeAttribute('disabled');
	document.getElementById("D200time1").setAttribute('required','');
	document.getElementById("D200time2").setAttribute('required','');
	document.getElementById("D200time3").setAttribute('required','');
	}
	else{
	document.getElementById("D200time1").disabled = true;
	document.getElementById("D200time2").disabled = true;
	document.getElementById("D200time3").disabled = true;
	}

	if(document.getElementById('D300').checked){
	document.getElementById("D300time1").removeAttribute('disabled');
	document.getElementById("D300time2").removeAttribute('disabled');
	document.getElementById("D300time3").removeAttribute('disabled');
	document.getElementById("D300time1").setAttribute('required','');
	document.getElementById("D300time2").setAttribute('required','');
	document.getElementById("D300time3").setAttribute('required','');
	}
	else{
	document.getElementById("D300time1").disabled = true;
	document.getElementById("D300time2").disabled = true;
	document.getElementById("D300time3").disabled = true;
	}
	
	if(document.getElementById('Z0201').checked){
	document.getElementById("Z0201time1").removeAttribute('disabled');
	document.getElementById("Z0201time2").removeAttribute('disabled');
	document.getElementById("Z0201time3").removeAttribute('disabled');
	document.getElementById("Z0201time1").setAttribute('required','');
	document.getElementById("Z0201time2").setAttribute('required','');
	document.getElementById("Z0201time3").setAttribute('required','');
	}
	else{
	document.getElementById("Z0201time1").disabled = true;
	document.getElementById("Z0201time2").disabled = true;
	document.getElementById("Z0201time3").disabled = true;
	}

	if(document.getElementById('Z0200').checked){
	document.getElementById("Z0200time1").removeAttribute('disabled');
	document.getElementById("Z0200time2").removeAttribute('disabled');
	document.getElementById("Z0200time3").removeAttribute('disabled');
	document.getElementById("Z0200time1").setAttribute('required','');
	document.getElementById("Z0200time2").setAttribute('required','');
	document.getElementById("Z0200time3").setAttribute('required','');
	}
	else{
	document.getElementById("Z0200time1").disabled = true;
	document.getElementById("Z0200time2").disabled = true;
	document.getElementById("Z0200time3").disabled = true;
	}


}
</script>