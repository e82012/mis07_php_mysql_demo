<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<?php
/* @var $this TbsServiceTestController */

$this->breadcrumbs=array(
	'Tbs Service Test',
);
?>
<h1>售票機服務排序</h1>
<?php echo CHtml::beginForm('','post',array('enctype' => 'multipart/form-data','id'=>'form1')); ?> 
<input type ="hidden" name="myip" Value="<?php echo $myip; ?>" readonly >
<input type ="hidden" name="storecode" Value="<?php echo $storecode; ?>" readonly >
<input type ="hidden" name="storename" Value="<?php echo $storename; ?>" readonly >

版本日期 : <?php echo date('Ymd') ?><input type ="hidden" name="version" Value="<?php echo date('Ymd') ?>" readonly >
登入門市 : (<?php echo $myip; ?>)  <?php echo $storecode; ?> <?php echo $storename; ?> 
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $msg) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 
		echo "<div class='flash-$key'>" . $msg . "</div>\n";}
?>
<div class="row">
	<div class="col-sm-3">
        版本：<?php echo CHtml::dropDownList('qryversion', $qryversion,$versionAry, array('style' => 'font-size: 16px' ,'value'=>$qryversion)); ?>
    </div>
</div>
<?php echo CHtml::submitButton('服務排序', array('name'=>'sv_submit' ,'class'=>'btn btn-outline-secondary')); ?>
<?php echo CHtml::submitButton('髮品排序', array('name'=>'pd_submit' ,'class'=>'btn btn-outline-secondary')); ?>
<?php echo CHtml::submitButton('優惠排序', array('name'=>'ev_submit' ,'class'=>'btn btn-outline-secondary')); ?>
<br>
<br>
<?php
	echo "<ul name = 'sortable' id='sortable' class='list-group list-group-flush'>";
		for($i=0;$i<count($sortAry);$i++){
		echo "<li name = item'".$i."' id='".$sortAry[$i]['id']."' class='list-group-item list-group-item-action' sv_sortable='".$sortAry[$i]['sv_order']."'>".$sortAry[$i]['sale_name']."</li>";
        // echo "<li><input type=textbox name = item'".$i."' itemid='".$sortAry[$i]['id']."' class='list-group-item list-group-item-action' sv_sortable='".$sortAry[$i]['sv_order']."' value='".$sortAry[$i]['sale_name']."' readonly></li>";
    }

	echo "</ul>";
?>

<?php echo CHtml::submitButton('確定排序', array('name'=>'sv_save' ,'id'=>'sv_save','class'=>'btn btn-outline-secondary', )); ?>
<!-- <input name="sv_save" id="sv_save" class="btn btn-outline-secondary" type="submit" value="確定排序"> -->

<?php echo CHtml::endForm(); ?> 
<?php 
?>
<script>

 $(function() {
    $( "#sortable" ).sortable({
        start: function(e, ui) {
        // creates a temporary attribute on the element with the old index
        $(this).attr('data-previndex', ui.item.index());
    },
    update: function(e, ui) {
        $(this).children().each(function (index) {
                        if ($(this).attr('sv_sortable') != (index+1)) {
                            $(this).attr('sv_sortable', (index+1)).addClass('updated'); //.addClass('updated')
                        }
                   });
            saveNewPositions();
            
    },
    
        opacity: 0.6, //透明度
        }
    );

    
  });
  function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
               positions.push([$(this).attr('id'), $(this).attr('sv_sortable')]);
            //    $(this).removeClass('updated');
            });
            $.ajax({
               url: 'http://localhost/mis07/index.php?r=tbsServiceTest/tbsServiceStSortable',
               method: 'POST',
               dataType: 'text',
               data: {
                //    update: 1,
                   positions: positions
               }, success: function (response) {
                    console.log('success');
               }
            });
            console.log(positions);
  }

</script>
<?php

    if (isset($_POST['sv_save'])) {
        var_dump($_POST);
        // foreach($_POST['positions'] as $position) {
        //    $index = $position[0];
        //    $newPosition = $position[1];

        //     $sortsql = "UPDATE tbs_service_test SET sv_order = $newPosition  WHERE id = $index";
		//     $sortAry = Yii::$app->db->createCommand($sortsql)->execute(); //更新
        // }

        // exit('success');
    }
?>