<?php
/* @var $this TbsServiceTestController */
/* @var $model tbsServiceTest */

$this->breadcrumbs=array(
	'Tbs Service Tests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List tbsServiceTest', 'url'=>array('index')),
	array('label'=>'Create tbsServiceTest', 'url'=>array('create')),
	array('label'=>'View tbsServiceTest', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage tbsServiceTest', 'url'=>array('admin')),
);
?>

<h1>Update tbsServiceTest <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>