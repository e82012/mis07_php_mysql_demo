<?php
/* @var $this ImpExcelController */
/* @var $model ImpExcel */

$this->breadcrumbs=array(
	'Imp Excels'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ImpExcel', 'url'=>array('index')),
	array('label'=>'Create ImpExcel', 'url'=>array('create')),
	array('label'=>'Update ImpExcel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ImpExcel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ImpExcel', 'url'=>array('admin')),
);
?>

<h1>View ImpExcel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'date',
		'area',
		'store',
		'type',
		'storecode',
	),
)); ?>
