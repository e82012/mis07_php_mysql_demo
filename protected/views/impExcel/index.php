<?php
/* @var $this ImpExcelController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Imp Excels',
);

$this->menu=array(
	array('label'=>'Create ImpExcel', 'url'=>array('create')),
	array('label'=>'Manage ImpExcel', 'url'=>array('admin')),
);
?>

<h1>Imp Excels</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
