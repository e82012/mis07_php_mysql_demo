<?php
/* @var $this ImpExcelController */
/* @var $model ImpExcel */

$this->breadcrumbs=array(
	'Imp Excels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ImpExcel', 'url'=>array('index')),
	array('label'=>'Manage ImpExcel', 'url'=>array('admin')),
);
?>

<h1>Create ImpExcel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>