<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<?php 	error_reporting(0); ?>
<h1>門市業績</h1>
<br>
<div class="" >
<?php echo CHtml::beginForm('','post',array('enctype' => 'multipart/form-data')); //'multipart/form-data' 上傳資料用 ?>  

    <div class="row">
        <div class="col-sm-3">
            年月：<?php echo CHtml::dropDownList('qrydate', $qrydate,$dateAry, array('style' => 'font-size: 18px' )); ?>
        </div>
        <div class="col-sm-3">
            區域：<?php echo CHtml::dropDownList('qryarea', $qryarea,$areaAry, array('style' => 'font-size: 18px', 'prompt'=>'選擇區域')); ?> 
        </div>
        <div class="col-sm-6" >
        <!-- <div class="float-right"> -->
            <?php echo CHtml::submitButton('查詢', array('name'=>'submit' ,'class'=>'btn btn-outline-info')); ?>
            <?php echo CHtml::submitButton('匯出',array('name'=>'export','class'=>'btn btn-outline-info'))?>
        </div>
        </div>
    </div>
    <div class="row">
    <br>
    </div>
    <div class="row">
        <div class="col-sm-6 custom-file ">       
            <!-- <?php echo CHtml::fileField('filename','' , array('style'=>'width=100%;heigh=36px','class'=>'btn btn-warning')); ?> -->
            <input type="file" class="custom-file-input" id="customFile" name="filename" >
            <label class="custom-file-label" for="customFile">選擇檔案</label>
        </div>
        <!-- <div class="col-sm-3">
        </div>  -->
        <div class="col-sm-6"> 
            <?php echo CHtml::submitButton('  上傳店編表  ',array('name'=>'import','button class'=>'btn btn-outline-warning','id'=>'import'))?>
            <!-- <input type="submit" name="import" value="上傳店編表"> -->
        </div> 
    </div>    
    
<?php echo CHtml::endForm(); ?>  
    <div class="progress" style="display:none;">
    <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
    </div>
    </div>
<div class="table">
<?php echo CHtml::beginForm('','post',array('enctype' => 'multipart/form-data')) ?>
    <table style="width:100%">
        <tr>
            <td width="10%">活頁簿</td>
            <td width="10%">筆數</td>
            <td width="50%">訊息</td>                 
        </tr>                                
        <tr>
        <td><?php 
                if($phpexcel!=NULL) {
                        $currentsheet = $phpexcel->getSheet(0); //讀取excel工作表
                        $allrow = $currentsheet->getHighestRow();//取得一共有多少行 
                            echo '<td>'. ($allrow-1) .'</td>';                                                     
                } 
            ?>
        </td>       
        <td>
            <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {     //畫面訊息,訊息提示-http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages/ 
                echo "<div class='flash-$key'>" . $message . "</div>\n";
            }
            ?>
        </td>
        </tr>    
    </table>
<?php echo CHtml::endForm(); ?>
</div>

<?php if(isset($fileName) && $fileName!='') : ?>
<div align="center">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/tmp/<?php echo $fileName; ?>">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/download01.jpg">
        </a>;
 </div>

<?php elseif(count($storeAry)>0) : ?>
<div class="table-hover" >
    <table>
        <thead>
            <tr>
                <th>年月</th>
				<th>區域</th>
				<th>店名</th>
				<th>品牌</th>
                <th>店編</th>
                <th>合計</th>
            </tr>
        </thead>    
    <?php
    //全公司--------------
    if($qryarea=='')
    {
        for($i=0;$i<=count($storeAry);$i++)
        {
                //echo "<td>".$resultAry[$i]->bname."</td>";
                $storageArea1 = $storeAry[$i][area];
                if($storageArea2 != '')
                {
                    if($storageArea1 !=$storageArea2)
                    {
                        if($jn==null)
                            {
                            $jn=0;
                            for($j=$jn;$j<count($areaResult);$j++)
                                {
                                    echo "<tr bgcolor='#66ccff'>";
                                        echo "<td></td>";
                                        echo "<td></td>";
                                        echo "<td></td>";
                                        echo "<td><small>區合計</small></td>";
                                        echo "<td><small>".$areaResult[$j][area]."</small></td>";
                                        echo "<td><small>".$areaResult[$j][aincome]."</small></td>";
                                    echo "</tr>";
                                    echo "<tr bgcolor='#66ccff'>";
                                        echo "<td></td>";
                                        echo "<td></td>";
                                        echo "<td></td>";
                                        echo "<td><small>區平均</small></td>";
                                        echo "<td><small>".$areaResult[$j][area]."</small></td>";
                                        echo "<td><small>".$areaResult[$j][vincome]."</small></td>";
                                    echo "</tr>";

                                break;
                                }
                            }
                        else
                            for($j=$jn;$j<count($areaResult);$j++)
                            {
                                echo "<tr bgcolor='#66ccff'>";
                                    echo "<td></td>";
                                    echo "<td></td>";
                                    echo "<td></td>";
                                    echo "<td><small>區合計</small></td>";
                                    echo "<td><small>".$areaResult[$j][area]."</small></td>";
                                    echo "<td><small>".$areaResult[$j][aincome]."</small></td>";
                                echo "</tr>";
                                echo "<tr bgcolor='#66ccff'>";
                                    echo "<td></td>";
                                    echo "<td></td>";
                                    echo "<td></td>";
                                    echo "<td><small>區平均</small></td>";
                                    echo "<td><small>".$areaResult[$j][area]."</small></td>";
                                    echo "<td><small>".$areaResult[$j][vincome]."</small></td>";
                                echo "</tr>";
                            break;
                            }      
                        $jn++;
                    }
                }
                echo "<tr>";
                echo "<td><small>".$storeAry[$i][date]."</small></td>";
                echo "<td><small>".$storeAry[$i][area]."</small></td>";
                echo "<td><small>".$storeAry[$i][store]."</small></td>";
                echo "<td><small>".$storeAry[$i][type]."</small></td>";
                echo "<td><small>".$storeAry[$i][storecode]."</small></td>";
                echo "<td><small>".$storeAry[$i][mincome]."</small></td>";  
                echo "</tr>";
                    $storageArea2 = $storageArea1;
        }   
            echo "<tr>";
                echo "<td><small>".'合計'."</small></td>";
                    if($iresult['area']!=null)
                    {
                    echo "<td><small>".$iresult['area']."</small></td>";
                    }
                    else
                    {
                        echo "<td><small>".'全公司'."</small></td>";
                    }
                echo "<td><small>".''."</small></td>";
                echo "<td><small>".''."</small></td>";
                echo "<td><small>".''."</small></td>";
                echo "<td><small>".$iresult['tincome']."</small></td>";  
            echo "</tr>";
    }


    //區域-------------------------
    else
    {
        for($i=0;$i<count($storeAry);$i++)
        {
            if($i%2==0) //偶數行顏色
            echo "<tr bgcolor='#e6ffff'>";
            else
            echo "<tr>";
                echo "<td><small>".$storeAry[$i][date]."</small></td>";
                echo "<td><small>".$storeAry[$i][area]."</small></td>";
                echo "<td><small>".$storeAry[$i][store]."</small></td>";
                echo "<td><small>".$storeAry[$i][type]."</small></td>";
                echo "<td><small>".$storeAry[$i][storecode]."</small></td>";
                echo "<td><small>".$storeAry[$i][mincome]."</small></td>";  
        }   
            echo "<tr bgcolor='#66ccff'>";
            echo "<td><small>"."</small></td>";
            if($iresult['area']!=null){
            echo "<td><small>".$iresult['area']."</small></td>";
            }
            else
            {
                echo "<td><small>".'全公司'."</small></td>";
            }
            echo "<td><small>".''."</small></td>";
            echo "<td><small>".''."</small></td>";
            echo "<td><small>合計</small></td>";
            echo "<td><small>".$iresult['tincome']."</small></td>";  
            echo "</tr>";
            echo "<tr bgcolor='#66ccff'>";
            echo "<td><small></small></td>";
            echo "<td><small></small></td>";
            echo "<td><small></small></td>";
            echo "<td><small></small></td>";
            echo "<td><small>".'平均'."</small></td>";
            echo "<td><small>".$iresult['vincome']."</small></td>";
            echo "</tr>";

    }
?>

    </table>
    
</div>
<div id="chartsMis" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php endif;  ?>
<script>
    //圖表Highcharts
    $(function() {

        var store_data = <?php echo json_encode($storeSqlAry) ;?>;
        // var store_data = ['一','二','三','四','五']
        var mincome_data = <?php echo json_encode($storeSqlIncome) ;?>;
        var avgincome_data = <?php echo json_encode($storeSqlAvgIncome);?>;
        var sqlName;
        if(<?php echo json_encode($qryarea) ;?>=="")
            {
                sqlName = '全公司';
            }
            else
            {
                sqlName = <?php echo json_encode($qryarea) ;?>;
            }
        
            Highcharts.setOptions({
                // colors: ['#80ffff']
            });
            Highcharts.chart('chartsMis', {
                chart: {
                    type: 'column'
                },
                title: {
                    text:sqlName
                },
                subtitle: {
                    text: sqlName+'業績常模'
                },
                // data: {
                //     csvURL: 'https://demo-live-data.highcharts.com/time-data.csv',
                //     //即時更新
                //     enablePolling: false 
                // }
                xAxis: {
                    categories: store_data
                //     categories: [500,250,0]
                //     title:{text: '1234'}
                },
 
                yAxis: {
                    // categories: [500,250,0],
                    // max:500,
                    tickAmount: 5,

                    title: {
                        text: '業績'
                    }
                },
                //value on bar
                plotOptions: {
                        series: {
                            dataLabels: {
                            enabled: true,
                            //在裡面
                            //inside: true
                            }
                        }
                    },
                series: [
                    {
                    name:'業績',
                    data: mincome_data,
                    },
                    {
                    name:'平均',
                    data:avgincome_data
                    }
                ]
               
            });
    });

    
    // 顯示檔案名稱
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    
    //上傳進度條
    $("#import").click(function() {
        var formData = new FormData();
        var file = $('input[type=file]')[0].files[0];
        formData.append('import',file);
        $('.progress').show();
        $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                $('.progress-bar').css('width',percentComplete+"%");
                $('.progress-bar').html(percentComplete+"%");
                if (percentComplete === 100) {	
                }
            }
            }, false);
            return xhr;
        },
        url: "import.php",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function(result) {
            console.log(result);
        }
        });
    });
</script>


