<?php
/* @var $this ImpExcelController */
/* @var $model ImpExcel */

$this->breadcrumbs=array(
	'Imp Excels'=>array('index'),
	'Update',
);

$this->menu=array(
	array('label'=>'List ImpExcel', 'url'=>array('index')),
	array('label'=>'Create ImpExcel', 'url'=>array('create')),
	array('label'=>'Manage ImpExcel', 'url'=>array('admin')),
);
?>

<h1>Update ImpExcel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>