<!-- 上橫幅 -->
<!-- <div class="ui top fixed menu">
  <div class="item">
    <img src="/images/logo.png">
  </div>
  <a class="item">Features</a>
  <a class="item">Testimonials</a>
  <a class="item">Sign-in</a>
</div> -->
<!-- 下橫幅 -->
<div class="ui bottom fixed menu">
  <div class="item">
    <img src="images/indeximg/ame.gif">
	<!-- git 版控清單 -->
	<button class="button is-info is-inverted" type="button" id="gitCommit" data-bs-toggle="dropdown" aria-expanded="false" style="text-align: right;"></button>
	<ul class="dropdown-menu" aria-labelledby="gitCommit">
		<li><p class="dropdown-item"  id = "gitCommitList" ></p></li>
	</ul>
  </div>
  <div class="right menu">
  <p class="item" href="" id="reactDiv">  </p> 
		<div class="item">
			<?php if(Yii::app()->user->name == 'Guest'): ?>
				<a class="ui inverted secondary button" href="index.php?r=site/login">login</a>
			<?php else :?>
				<a class="ui inverted primary button" href="index.php?r=site/logout">Logout<?php echo "(".Yii::app()->user->name.")" ?></a>
			<?php endif ?>
		</div>
  </div>
</div>
<!-- 主Menu -->
<div class="ui menu">
	<a class="item" href="index.php?r=site/index">
	Home
	</a>
	<div class="ui pointing dropdown link item">
		<span class="text">Function</span>
			<i class="dropdown icon"></i>
		<div class="menu">
			<div class="header"><i class="code icon"></i>測試</div>
			<!-- <a class="item" href="index.php?r=site/page&view=testpage">Test</a> -->
			<a class="item" href="index.php?r=site/testPageX">功能測試</a>
			<div class="divider"></div>
			<div class="header"><i class="upload icon"></i>帳務上傳</div>
			<a class="item" href="index.php?r=site/nccc_Upload">手動上傳 信用卡</a>
			<a class="item" href="index.php?r=site/easy_card_Upload">手動上傳 悠遊卡</a>
			<a class="item" href="index.php?r=site/ipass_card_Upload">手動上傳 一卡通</a>
			<div class="divider"></div>
			<div class="item">
				<i class="dropdown icon"></i>
				<span class="text"><i class="braille icon"></i>其他</span>
				<div class="menu">
					<div class="header">A</div>
					<div class="item" href="index.php?r=site/webSocketTest">webSocket</div>
					<div class="divider"></div>
					<div class="header">B</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 靠右 -->
	<div class="right menu">
		<div class="ui pointing dropdown link item">
			<span class="text">Language</span>
			<div class="menu">
				<div class="item">English</div>
				<div class="item">Russian</div>
				<div class="item">Spanish</div>
			</div>
		</div>
		<div class="ui pointing dropdown link item">
			<span class="text">Function</span>
		</div>
  	</div>
</div>
<!-- <div class="collapse navbar-collapse" id="main_nav">	
			<ul class="navbar-nav">
			<li class="nav-item"> <a class="nav-link" href="index.php?r=site/index"> 首頁 </a> </li>

			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown"> 關於 </a>
				<ul class="dropdown-menu">
				<li><a class="dropdown-item" href="index.php?r=site/page&view=about"> Profile </a></li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown"> 功能區 </a>
				<ul class="dropdown-menu">
				<li><a class="dropdown-item" href="index.php?r=site/page&view=testpage"> Test </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/testPageX"> TestPageX </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/webSocketTest"> webSocket </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/easy_card_Upload"> 2021.09手動上傳 悠遊卡 </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/ipass_card_Upload"> 2021.09手動上傳 一卡通 </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/nccc_Upload"> 2021.09手動上傳 信用卡 </a></li>
				</ul>
			</li>
			<?php if(Yii::app()->user->name == 'admin'):?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> 系統 </a>
				<ul class="dropdown-menu">
				<li><a class="dropdown-item" href="index.php?r=tbluser"> 使用者管理 </a></li>
				<li><a class="dropdown-item" href="index.php?r=books"> 書籍管理 </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/page&view=cuttime_query"> 剪髮時間查詢 </a></li>
				<li><a class="dropdown-item" href="index.php?r=emailTest/Emailtools"> E-mail功能 </a></li>
				<li><a class="dropdown-item" href="index.php?r=emailTest/Emailtools3"> GOOGLE SHEET CRUD功能 </a></li>
				<li><a class="dropdown-item" href="index.php?r=emailTest/pageTest"> 分頁測試 </a></li>
				<li><a class="dropdown-item" > 門市管理 <i class="fas fa-caret-right"></i></a>
					<ul class="submenu dropdown-menu">
						<li><a class="dropdown-item" href="index.php?r=impExcel"> 門市資料</a></li>
						<li><a class="dropdown-item" href="index.php?r=impExcel/import"> 門市業績(EXCEL)</a></li>
						<li><a class="dropdown-item" href="index.php?r=impExcel/jsontest"> JSON查詢</a></li>
					</ul>
				</li>
				<li><a class="dropdown-item" > 門市售票機管理 <i class="fas fa-caret-right"></i></a>
					<ul class="submenu dropdown-menu">
						<li><a class="dropdown-item" href="index.php?r=tbsServiceTest/tbsServiceStCreate"> 門市服務產生</a></li>
						<li><a class="dropdown-item" href="index.php?r=tbsServiceTest/tbsServiceStExport"> 售票機檔案匯出</a></li>
						<li><a class="dropdown-item" > 售票機服務修改 <i class="fas fa-caret-right"></i></a>
							<ul class="submenu dropdown-menu">
								<li><a class="dropdown-item" href="index.php?r=tbsServiceTest/tbsServiceStUpdate"> 服務內容修改</a></li>
								<li><a class="dropdown-item" href="index.php?r=tbsServiceTest/tbsServiceStSortable"> 服務排序修改</a></li>
								<li><a class="dropdown-item" href="index.php?r=tbsServiceItem/index"> 服務項目管理</a></li>
							</ul>
					</ul>
				</li>
				<li><a class="dropdown-item" href="index.php?r=tbaAdjust/tbaadjust_create"> 稽核 - 調整申請 </a></li>
				</ul>
			</li>
			<?php endif ?>
			<li class="nav-item"> <a class="nav-link" href="index.php?r=books/userlend_list"> 借閱 </a> </li>

			<?php if(Yii::app()->user->name == 'Guest'): ?>
			<li class="nav-item"> <a class="nav-link" href="index.php?r=site/login"> 登入 </a></li>
			<?php else: ?>
			<li class="nav-item"> <a class="nav-link " href="index.php?r=site/logout"> <i class="fab fa-napster"> 登出<?php echo "(".Yii::app()->user->name.")" ?> </i> </a></li>
			<?php endif?>
			<li class="nav-item"> <a class="nav-link" href="" id="reactDiv">  </a> </li>
			</ul>
		</div>  -->
<script>
// semantic-ui dropdown問題，如果不加，class就要加入simple下拉才會秀出來
$('.ui.dropdown').dropdown();

</script>