<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="https://bulma.io">
      <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
    </a>

    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="navbarBasicExample" class="navbar-menu">
	<!-- 1 -->
    <div class="navbar-start">
      <a class="navbar-item" href="index.php?r=site/index">
        Home
      </a>
<!-- 2 -->
	  <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          About
        </a>
        <div class="navbar-dropdown">
          <a class="navbar-item">
            About
          </a>
          <a class="navbar-item">
            Jobs
          </a>
          <a class="navbar-item">
            Contact
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            Report an issue
          </a>
        </div>
      </div>
<!-- 3 -->
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Features
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item" href="index.php?r=site/page&view=testpage">
            Test
          </a>
          <a class="navbar-item" href="index.php?r=site/testPageX">
            TestPageX
          </a>
          <a class="navbar-item" href="index.php?r=site/webSocketTest">
            WebSocket
          </a>
		  <a class="navbar-item" href="index.php?r=site/easy_card_Upload">
            2021.09手動上傳 悠遊卡
          </a>
		  <a class="navbar-item" href="index.php?r=site/ipass_card_Upload">
            2021.09手動上傳 一卡通
          </a>
		  <a class="navbar-item" href="index.php?r=site/nccc_Upload">
            2021.09手動上傳 信用卡
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            Report an issue
          </a>
        </div>
      </div>
<!-- 4 -->
	<div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          System
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            使用者管理
          </a>
          <a class="navbar-item">
            書籍管理
          </a>
          <a class="navbar-item">
            剪髮時間查詢
          </a>
		  <a class="navbar-item">
            剪髮時間查詢
          </a>
		  <a class="navbar-item">
            E-mail功能測試
          </a>
		  <a class="navbar-item">
            GoogleSheetFRUD功能測試
          </a>
		  <a class="navbar-item">
            SQL AJAX分頁測試
          </a>
				<li><a class="dropdown-item" > 門市管理 <i class="fas fa-caret-right"></i></a>
					<ul class="submenu dropdown-menu">
						<li><a class="dropdown-item" href="index.php?r=impExcel"> 門市資料</a></li>
						<li><a class="dropdown-item" href="index.php?r=impExcel/import"> 門市業績(EXCEL)</a></li>
						<li><a class="dropdown-item" href="index.php?r=impExcel/jsontest"> JSON查詢</a></li>
					</ul>
				</li>
		  <!-- 分隔線 -->
          <hr class="navbar-divider">
          <a class="navbar-item">
            Report an issue
          </a>
        </div>
      </div>
    
<!-- END -->
    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a class="button is-primary">
            <strong>Sign up</strong>
          </a>
          <a class="button is-light">
            Log in
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>