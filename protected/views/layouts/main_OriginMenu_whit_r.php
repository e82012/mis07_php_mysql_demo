<style>
@media (min-width: 992px){
	.dropdown-menu .dropdown-toggle:after{
		border-top: .3em solid transparent;
	    border-right: 0;
	    border-bottom: .3em solid transparent;
	    border-left: .3em solid;
	}
	.dropdown-menu .dropdown-menu{
		margin-left:0; margin-right: 0;
	}
	.dropdown-menu li{
		position: relative;
	}
	.nav-item .submenu{ 
		display: none;
		position: absolute;
		left:100%; top:-7px;
	}
	.nav-item .submenu-left{ 
		right:100%; left:auto;
	}
	.dropdown-menu > li:hover{ background-color: #f1f1f1 }
	.dropdown-menu > li:hover > .submenu{
		display: block;
	}
	}	
	.avatar {
	vertical-align: middle;
	width: 30px;
	height: 30px;
	border-radius: 20%;
	}

</style>
<!-- menu -->
<nav class="navbar nav nav-tabs navbar-expand-sm navbar-light ">
<!-- mobile版型按鈕 -->
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav">
	<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="main_nav">	
			<ul class="navbar-nav">
			<li class="nav-item"> <a class="nav-link" href="index.php?r=site/index"> 首頁 </a> </li>

			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown"> 關於 </a>
				<ul class="dropdown-menu">
				<li><a class="dropdown-item" href="index.php?r=site/page&view=about"> Profile </a></li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown"> 功能區 </a>
				<ul class="dropdown-menu">
				<li><a class="dropdown-item" href="index.php?r=site/page&view=testpage"> Test </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/testPageX"> TestPageX </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/webSocketTest"> webSocket </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/easy_card_Upload"> 2021.09手動上傳 悠遊卡 </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/ipass_card_Upload"> 2021.09手動上傳 一卡通 </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/nccc_Upload"> 2021.09手動上傳 信用卡 </a></li>
				</ul>
			</li>
			<?php if(Yii::app()->user->name == 'admin'):?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> 系統 </a>
				<ul class="dropdown-menu">
				<li><a class="dropdown-item" href="index.php?r=tbluser"> 使用者管理 </a></li>
				<li><a class="dropdown-item" href="index.php?r=books"> 書籍管理 </a></li>
				<li><a class="dropdown-item" href="index.php?r=site/page&view=cuttime_query"> 剪髮時間查詢 </a></li>
				<li><a class="dropdown-item" href="index.php?r=emailTest/Emailtools"> E-mail功能 </a></li>
				<li><a class="dropdown-item" href="index.php?r=emailTest/Emailtools3"> GOOGLE SHEET CRUD功能 </a></li>
				<li><a class="dropdown-item" href="index.php?r=emailTest/pageTest"> 分頁測試 </a></li>
				<li><a class="dropdown-item" > 門市管理 <i class="fas fa-caret-right"></i></a>
					<ul class="submenu dropdown-menu">
						<li><a class="dropdown-item" href="index.php?r=impExcel"> 門市資料</a></li>
						<li><a class="dropdown-item" href="index.php?r=impExcel/import"> 門市業績(EXCEL)</a></li>
						<li><a class="dropdown-item" href="index.php?r=impExcel/jsontest"> JSON查詢</a></li>
					</ul>
				</li>
				<li><a class="dropdown-item" > 門市售票機管理 <i class="fas fa-caret-right"></i></a>
					<ul class="submenu dropdown-menu">
						<li><a class="dropdown-item" href="index.php?r=tbsServiceTest/tbsServiceStCreate"> 門市服務產生</a></li>
						<li><a class="dropdown-item" href="index.php?r=tbsServiceTest/tbsServiceStExport"> 售票機檔案匯出</a></li>
						<li><a class="dropdown-item" > 售票機服務修改 <i class="fas fa-caret-right"></i></a>
							<ul class="submenu dropdown-menu">
								<li><a class="dropdown-item" href="index.php?r=tbsServiceTest/tbsServiceStUpdate"> 服務內容修改</a></li>
								<li><a class="dropdown-item" href="index.php?r=tbsServiceTest/tbsServiceStSortable"> 服務排序修改</a></li>
								<li><a class="dropdown-item" href="index.php?r=tbsServiceItem/index"> 服務項目管理</a></li>
							</ul>
					</ul>
				</li>
				<li><a class="dropdown-item" href="index.php?r=tbaAdjust/tbaadjust_create"> 稽核 - 調整申請 </a></li>
				</ul>
			</li>
			<?php endif ?>
			<li class="nav-item"> <a class="nav-link" href="index.php?r=books/userlend_list"> 借閱 </a> </li>

			<?php if(Yii::app()->user->name == 'Guest'): ?>
			<li class="nav-item"> <a class="nav-link" href="index.php?r=site/login"> 登入 </a></li>
			<?php else: ?>
			<li class="nav-item"> <a class="nav-link " href="index.php?r=site/logout"> <i class="fab fa-napster"> 登出<?php echo "(".Yii::app()->user->name.")" ?> </i> </a></li>
			<?php endif?>
			<li class="nav-item"> <a class="nav-link" href="" id="reactDiv">  </a> </li>
			</ul>
		</div> <!-- navbar-collapse.// -->
</nav>
<script>
// Prevent closing from click inside dropdown //多層次Menu用
$(document).on('click', '.dropdown-menu', function (e) {
  e.stopPropagation();
});
</script>