<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<style>
  /* Make the image fully responsive */
  .carousel-inner img {
    width: 40%;
    height: 40%;
  }
  </style>
<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<div id="sidebar">
	<!-- <img src='images/unnamed.png' width='120'> -->
	<div id="demo" class="carousel slide" data-ride="carousel">
	<ul class="carousel-indicators">
		<li data-target="#JIT" data-slide-to="0" class="active"></li>
		<li data-target="#JSM" data-slide-to="1"></li>
		<li data-target="#PK100" data-slide-to="2"></li>
		<li data-target="#Style" data-slide-to="3"></li>
	</ul>
	<div class="carousel-inner">
		<div class="carousel-item active">
		<img src="images/indeximg/FB-JIT.png" alt="JIT" width="100" height="100">
		<div class="carousel-caption">
			<h3>JIT</h3>
			<p>...</p>
		</div>   
		</div>
		<div class="carousel-item">
		<img src="images/indeximg/FB-JSM.png" alt="JSM" width="100" height="100">
		<div class="carousel-caption">
			<h3>JSM</h3>
			<p>...</p>
		</div>   
		</div>
		<div class="carousel-item">
		<img src="images/indeximg/FB-PK100.png" alt="PK100" width="100" height="100">
		<div class="carousel-caption">
			<h3>PK100</h3>
			<p>...</p>
		</div>   
		</div>
		<div class="carousel-item">
		<img src="images/indeximg/FB-STYLE.png" alt="Style" width="100" height="100">
		<div class="carousel-caption">
			<h3>Style</h3>
			<p>...</p>
		</div>   
		</div>
	</div>
	<a class="carousel-control-prev" href="#demo" data-slide="prev">
		<span class="carousel-control-prev-icon"></span>
	</a>
	<a class="carousel-control-next" href="#demo" data-slide="next">
		<span class="carousel-control-next-icon"></span>
	</a>
	</div>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>