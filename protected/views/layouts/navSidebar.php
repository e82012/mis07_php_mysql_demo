<style>
  #navSidebar{
    background-image:
      url('images/indeximg/ame.gif');
      /* linear-gradient(to top, rgba(230,249,255,0.5), rgba(255,255,255,0.5)); */
      background-repeat: no-repeat;
      background-position: right bottom, right bottom, center center;
      background-size: 30%;
  }
  .list-group-item {
    border: none;
    cursor: pointer;
    transition: transform 0.2s ease; /* 添加縮放效果 */
  }

  .list-group-item:hover {
    transform: scale(1.1); /* 懸停時放大 10% */
    background-color: #f8f9fa;
  }
</style>
<div class="offcanvas offcanvas-start" id="navSidebar">
  <div class="offcanvas-header">
    <h1 class="offcanvas-title"><i class="fab fa-napster"></i>NavSideBar</h1>
    <button type="button" class="delete is-medium" data-bs-dismiss="offcanvas"></button>
  </div>
  <div class="offcanvas-body">
    <!-- Link -->
    <!-- <p>Some text lorem ipsum.</p> -->
    <!-- <p>Some text lorem ipsum.</p> -->
    <!-- <p>Some text lorem ipsum.</p> -->
    <!-- Button trigger modal -->
    <ul class="list-group">
  <li class="list-group-item" data-bs-toggle="modal" data-bs-target="#phpInfo">
    PHP版本
  </li>
</ul>
    <!-- phpInfo Modal -->
    <div class="modal fade" id="phpInfo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="false">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">PHP</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
          <?php 
          echo phpinfo();
          // echo "<button type = 'button' id = 'testConsoleBTN' onclick="."testConsole(1,2,'$computetime')".">測試</button>";
        ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- bottom 登入資訊 -->
  <?php 
    if(Yii::app()->user->name == 'Guest'): ?>
    <p class="nav-item"> <a class="nav-link" href="index.php?r=site/login"> 登入 </a></p>
    <?php else: ?>
    <p class="nav-item"> <a class="nav-link " href="index.php?r=site/logout"> <i class="fab fa-napster"> 登出 <?php echo "(".Yii::app()->user->name.")" ?> </i> </a></p>
    <?php endif?>

</div>