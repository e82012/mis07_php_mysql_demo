<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="zh-TW">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	<!-- CSS only -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">

	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainMbMenu">
	
	<?php 
	if((Yii::app()->user->name)=='admin')
			{
			$this->widget('application.extensions.mbmenu.MbMenu',array(
			'items'=>array(
				array('label'=>'首頁', 'url'=>array('/site/index')),
				array('label'=>'關於', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'聯絡我們', 'url'=>array('/site/contact')),
				array('label'=>'系統管理',
				  'items'=>array(
				    array('label'=>'使用者管理', 'url'=>array('/tbluser')),
					array('label'=>'書籍管理', 'url'=>array('/books')),
					array('label'=>'門市管理',
						'items'=>array(
							array('label'=>'門市資料','url'=>array('/impExcel')),
							array('label'=>'門市業績(Excel)','url'=>array('/impExcel/import')),
						)),
				  				),
					),
				array('label'=>'借閱', 'url'=>array('/books/userlend_list')),
					  array('label'=>'登入', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					  array('label'=>'登出 ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
						),
			)); 
		}
		else
		{
			$this->widget('application.extensions.mbmenu.MbMenu',array(
				'items'=>array(
					array('label'=>'首頁', 'url'=>array('/site/index')),
					array('label'=>'關於', 'url'=>array('/site/page', 'view'=>'about')),
					array('label'=>'聯絡我們', 'url'=>array('/site/contact')),
					array('label'=>'借閱', 'url'=>array('/books/userlend_list')),
						  array('label'=>'登入', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						  array('label'=>'登出 ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
							),
				)); 


		}
	?>


	</div>

	<!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by JIT_MIS.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
