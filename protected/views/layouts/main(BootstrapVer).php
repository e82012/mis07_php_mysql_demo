<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="zh-TW">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	<!-- CSS only -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div>
	<ul class="nav nav-tabs">
		<li class="nav-item">
		<a class="nav-link"  href="index.php?r=site/index">首頁</a>

		<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle" data-toggle="dropdown" >關於</a>
		<div class="dropdown-menu">
		<a class="dropdown-item"  href="index.php?r=site/page&view=about">使用者</a>
		<a class="dropdown-item"  href="index.php?r=site/page&view=testpage">測試用</a>
		</div>
		</li>
		<?php if(Yii::app()->user->name == 'admin'):?>

		<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle" data-toggle="dropdown" >系統</a>
		<div class="dropdown-menu">
			<a class="dropdown-item" href="index.php?r=tbluser">使用者管理</a>
			<a class="dropdown-item" href="index.php?r=books">書籍管理</a>
			<a class="dropdown-item"  href="index.php?r=site/page&view=cuttime_query">剪髮時間查詢</a>
			<a class="dropdown-item" href="index.php?r=emailTest/Emailtools">EMAIL功能</a>
				<a class="nav-link dropdown-toggle" data-toggle="dropdown">門市管理</a>
				<div class="dropdown-menu ">
					<a class="dropdown-item" href="index.php?r=impExcel">門市資料</a>
					<a class="dropdown-item" href="index.php?r=impExcel/import">門市業績(EXCEL)</a>
				</div>
		</li>

		<?php endif ?>
		
		<li class="nav-item">
		<a class="nav-link"  href="index.php?r=books/userlend_list">借閱</a>
		</li>	
		<?php if(Yii::app()->user->name == 'Guest'): ?>
		<li class="nav-item">
		<a class="nav-link" href="index.php?r=site/login" visibility:hidden >登入</a>
		</li>
		<?php else: ?>
		<li class="nav-item">
		<a class="nav-link" href="index.php?r=site/logout">登出<?php echo "(".Yii::app()->user->name.")" ?></a>
		</li>
		<?php endif?>
		<li>
		</li>
		
  </ul>

	</div>

	<?php echo $content; ?>
	<div class="clear"></div>
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by JIT_MIS -> Ou.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
