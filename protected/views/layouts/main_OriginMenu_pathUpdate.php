<!-- menu -->
	<nav class="navbar nav nav-tabs navbar-expand-sm navbar-light ">
<!-- mobile版型按鈕 -->
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav">
	<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="main_nav">	
			<ul class="navbar-nav">
			<li class="nav-item"> <a class="nav-link" href="index"> 首頁 </a> </li>

			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown"> 關於 </a>
				<ul class="dropdown-menu">
				<li><a class="dropdown-item" href="page/about"> Profile </a></li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown"> 功能區 </a>
				<ul class="dropdown-menu">
				<!-- <li><a class="dropdown-item" href="page&view/testpage"> Test </a></li> -->
				<li><a class="dropdown-item" href="testPageX"> TestPageX </a></li>
				<li><a class="dropdown-item" href="webSocketTest"> webSocket </a></li>
				<li><a class="dropdown-item" href="easy_card_Upload"> 2021.09手動上傳 悠遊卡 </a></li>
				<li><a class="dropdown-item" href="ipass_card_Upload"> 2021.09手動上傳 一卡通 </a></li>
				<li><a class="dropdown-item" href="nccc_Upload"> 2021.09手動上傳 信用卡 </a></li>
				</ul>
			</li>
			<?php if(Yii::app()->user->name == 'admin'):?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> 系統 </a>
				<ul class="dropdown-menu">
				<li><a class="dropdown-item" href="index.php/tbluser"> 使用者管理 </a></li>
				<li><a class="dropdown-item" href="index.php/books"> 書籍管理 </a></li>
				<li><a class="dropdown-item" href="index.php/site/page&view=cuttime_query"> 剪髮時間查詢 </a></li>
				<li><a class="dropdown-item" href="index.php/emailTest/Emailtools"> E-mail功能 </a></li>
				<li><a class="dropdown-item" href="index.php/emailTest/Emailtools3"> GOOGLE SHEET CRUD功能 </a></li>
				<li><a class="dropdown-item" href="index.php/emailTest/pageTest"> 分頁測試 </a></li>
				<li><a class="dropdown-item" > 門市管理 <i class="fas fa-caret-right"></i></a>
					<ul class="submenu dropdown-menu">
						<li><a class="dropdown-item" href="index.php/impExcel"> 門市資料</a></li>
						<li><a class="dropdown-item" href="index.php/impExcel/import"> 門市業績(EXCEL)</a></li>
						<li><a class="dropdown-item" href="index.php/impExcel/jsontest"> JSON查詢</a></li>
					</ul>
				</li>
				<li><a class="dropdown-item" > 門市售票機管理 <i class="fas fa-caret-right"></i></a>
					<ul class="submenu dropdown-menu">
						<li><a class="dropdown-item" href="index.php/tbsServiceTest/tbsServiceStCreate"> 門市服務產生</a></li>
						<li><a class="dropdown-item" href="index.php/tbsServiceTest/tbsServiceStExport"> 售票機檔案匯出</a></li>
						<li><a class="dropdown-item" > 售票機服務修改 <i class="fas fa-caret-right"></i></a>
							<ul class="submenu dropdown-menu">
								<li><a class="dropdown-item" href="index.php/tbsServiceTest/tbsServiceStUpdate"> 服務內容修改</a></li>
								<li><a class="dropdown-item" href="index.php/tbsServiceTest/tbsServiceStSortable"> 服務排序修改</a></li>
								<li><a class="dropdown-item" href="index.php/tbsServiceItem/index"> 服務項目管理</a></li>
							</ul>
					</ul>
				</li>
				<li><a class="dropdown-item" href="index.php/tbaAdjust/tbaadjust_create"> 稽核 - 調整申請 </a></li>
				</ul>
			</li>
			<?php endif ?>
			<li class="nav-item"> <a class="nav-link" href="books/userlend_list"> 借閱 </a> </li>

			<?php if(Yii::app()->user->name == 'Guest'): ?>
			<li class="nav-item"> <a class="nav-link" href="login"> 登入 </a></li>
			<?php else: ?>
			<li class="nav-item"> <a class="nav-link " href="logout"> <i class="fab fa-napster"> 登出<?php echo "(".Yii::app()->user->name.")" ?> </i> </a></li>
			<?php endif?>
			<li class="nav-item"> <a class="nav-link" href="" id="reactDiv">  </a> </li>
			</ul>
		</div> <!-- navbar-collapse.// -->
	</nav>
	
<script>
// Prevent closing from click inside dropdown //多層次Menu用
$(document).on('click', '.dropdown-menu', function (e) {
  e.stopPropagation();
});
</script>