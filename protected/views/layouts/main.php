<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="zh-TW">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
<!-- fontawesome -->
	<!-- <link href="js/fontawesome-free-5.15.4-web/css/all.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- JAVASCRIPT -->
	<script src="js/jquery360.min.js"></script>
<!-- BootStrap -->
	<!-- <link href="js/bootstrap-5.1.3-dist/bootstrap513.min.css" rel="stylesheet"> -->
	<script src ="js/bootstrap-5.1.3-dist/bootstrap513.bundle.min.js"></script>
	<script src ="js/bootstrap-5.1.3-dist/popper1161.min.js"></script>
	<!-- 分頁用 -->
	<script src ="js/bootstrap-5.1.3-dist/bootstrap461.bundle.min.js"></script>
	<!-- 5.2.3 -->
	<link href="js/bootstrap-5.2.3-dist/css/bootstrap.css" rel="stylesheet">

<!-- React + 引入內容-->
	<script src ="js/react/babel71813.min.js"></script>
	<script src ="js/react/react1702.development.js" crossorigin></script>
	<script src ="js/react/react-dom1702.development.js" crossorigin></script>
	<!-- React引入內容 -->
	<script type="text/babel" src="reactJS/localTimeD.js" >  </script>
<!-- sweetAlert2 -->
	<script src="js/sweetalert211.js"></script>
<!-- bulma -->
	<link rel="stylesheet" href="css/bulma.min.css">

<!-- Select2 下拉選單-->
<link href="js/select2/select2.min.css" rel="stylesheet" />
<script src="js/select2/select2.min.js"></script>
<!-- Select2 i18 中文翻譯 -->
	<script src="js/select2/zh-TW.js"></script>
<!-- semantic-UI -->
	<script src="js/Semantic-UI-CSS-master/semantic.js"></script>
	<link rel="stylesheet" href="js/Semantic-UI-CSS-master/semantic.css" />

<!-- ALERTIFY JS -->
	<script src="js/alertifyJS/alertify.min.js"></script>
	<!-- CSS -->
	<link rel="stylesheet" href="js/alertifyJS/alertify.min.css"/>
	<!-- Default theme -->
	<link rel="stylesheet" href="js/alertifyJS/default.min.css"/>
<!-- style -->
<style>
/* 修正view內的form寬度大小 */
 .form
 {
	width:85%;
 }
</style>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>

<!-- <div class="container" id="page"> -->
	<!-- header 標題加入sidebar按鈕-->
	<div id="header">
		<div id="logo">
			<i class="fas fa-bars" type="button" data-bs-toggle="offcanvas" data-bs-target="#navSidebar" style='display: inline-block;'>	</i>
				<p class="" style = 'display: inline-block;'>
					MIS07
				</p>
			<!--<?php //echo CHtml::encode(Yii::app()->name); ?> -->
		</div>
	</div>

	<!-- Menu開始 -->
	<?php 
		// menu引入
		// include_once('main_OriginMenu_whit_r.php');
		// include_once('main_OriginMenu_pathUpdate.php'); //移除?=的版本
		// include_once('main_menu_Bulma.php');
		include_once('main_menu_semantic.php');
	?>
	<!-- 內容 -->
	<?php echo $content; ?>
	<!-- footer -->
	<div id="footer" class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			Copyright &copy; <?php echo date('Y'); ?> by Ou.<br>
			All Rights Reserved.<br>
			<?php echo Yii::powered(); ?>
		</div>
		<!-- git 版控清單 -->
		<div id = "bottom_right" class="col-sm-4">
			<!-- <button type="button" id = "gitCommit" class = "button is-info is-inverted" style="text-align: right;"></button> -->
		<!-- <p id="gitCommit" class="button is-info is-inverted" href=""></p> -->
			<div class="dropdown">
				<button class="button is-info is-inverted" type="button" id="gitCommit" data-bs-toggle="dropdown" aria-expanded="false" style="text-align: right;"></button>
				<ul class="dropdown-menu" aria-labelledby="gitCommit">
					<li><p class="dropdown-item"  id = "gitCommitList" ></p></li>
				</ul>
			</div>
		</div>

	</div>
<!-- </div> -->
<!-- page -->
<?php 
	// sideBar引入
	include_once('navSidebar.php');
?>
</body>
<!--toast position -->
<!-- <div class="position-fixed top-0 end-0 p-3" style="z-index: 100">
		<div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
			<div class="position-fixed top-0 end-0 p-4" style="z-index: 100">
				<button type="button" class="delete is-small" data-bs-dismiss="toast"></button>
			</div>
			<div class="toast-body">
				<p id="weatherAPI"></p>
			</div>
		</div>
</div> -->
</html>
<script>
$(document).ready(function(){
	var city = 5;
	// 取得天氣資料；chrome推送通知
	$.ajax({
		url:"<?php echo Yii::app()->createUrl('Site/AjaxWeather'); ?>",
			type:'POST',
			dataType:'JSON',
			data:{
				city:city,
				//($post[name]) :var名稱
				//php後端呼叫方式 $_POST['itme']
			},
			success:function(text){
				// console.log(text);
				var weatherSplit = text.split("\n");
				// console.log(weatherSplit);
				// document.getElementById("weatherAPI").innerHTML = weatherSplit[0]+"<br>"+weatherSplit[1]+"<br>"+weatherSplit[2]+"<br>"+weatherSplit[3];
				// alertify 提示
					alertify.set('notifier','delay', 5);
					alertify.set('notifier','position', 'top-right');
					alertify.message(weatherSplit[0]+"<br>"+weatherSplit[1]+"<br>"+weatherSplit[2]+"<br>"+weatherSplit[3]);
				// Chrome推送通知---無痕視窗不會顯示
					var notifyConfig = {
						body: text, // 設定內容
					};
					var notification = new Notification('MIS07 Noyify!', notifyConfig); // 建立通知
				// alert("ajax成功");
				
			},
			error:function(){alert("ajax失敗");}
	});
	//取得git版控
	$.ajax({
		url:"<?php echo Yii::app()->createUrl('Site/AjaxGitCommit'); ?>",
		type:'POST',
			dataType:'JSON',
			data:{
				//($post[name]) :var名稱
				//php後端呼叫方式 $_POST['itme']
			},
			success:function(text){
				// console.log(text);
				var mainGitCommit = "Recent commits : "+text[0]['commit'][0]+"<br>date : "+text[0]['date'];
				document.getElementById("gitCommit").innerHTML=mainGitCommit;
				// 版本清單
				var gitCommitList = document.getElementById("gitCommitList");
				text.forEach(element => {
					var listAdd = "date : "+element['date'] +"　Commits : "+element['commit'][0]+"<br>";
					gitCommitList.innerHTML += listAdd;
				});
				
			},
			error:function(){alert("ajax失敗");}

	});
});


// make it as accordion for smaller screens
if ($(window).width() < 992) {
  $('.dropdown-menu a').click(function(e){
    e.preventDefault();
      if($(this).next('.submenu').length){
        $(this).next('.submenu').toggle();
      }
      $('.dropdown').on('hide.bs.dropdown', function () {
     $(this).find('.submenu').hide();
  })
  });
}
//切換
function switchCase()
{
	// document.getElementById('toggle-switchCase').checked
	var switchCase = document.getElementById("switchCase").checked;
	console.log(switchCase);

	if(switchCase == false)
		// document.getElementById("page").style.backgroundColor = "black";
		document.getElementById('css1').href='css/tdd2.css';

	else
		// document.getElementById("page").style.backgroundColor = "white";
		document.getElementById('css1').href='css/tdd.css';
}

</script>