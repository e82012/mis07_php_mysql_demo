<?php

class EmailTestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','emailtools'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EmailTest;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmailTest']))
		{
			$model->attributes=$_POST['EmailTest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmailTest']))
		{
			$model->attributes=$_POST['EmailTest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('EmailTest');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new EmailTest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmailTest']))
			$model->attributes=$_GET['EmailTest'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return EmailTest the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=EmailTest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param EmailTest $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-test-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionEmailtools()
	{
		$emailfrom = isset($_POST['emailfrom']) ? $_POST['emailfrom'] : ('');
		$dateS = isset($_POST['dateS']) ? $_POST['dateS'] : date('');
		$dateE = isset($_POST['dateE']) ? $_POST['dateE'] : date('');

		if(isset($_POST['submit']))
		{
			// CVarDumper::dump($emailfrom);
			// CVarDumper::dump($dateS);
			// CVarDumper::dump($dateE);
		
			if (! function_exists('imap_open')) {
				echo "IMAP is not configured.";
				exit();
			} else {
				?>
			<div id="listData" class="list-form-container">
					<?php            
				$hostname = '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX';
				$username = 'jitmis2020@gmail.com';
				$password = '4180000000';
				/* try to connect */
				$connection = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());
	
	
				/* Search Emails having the specified keyword in the email subject */
				// $emailData = imap_search($connection, "FROM '$emailfrom'"); //from who send mail ap-public@i-pass.com.tw;
				// $emailData = imap_search($connection,'FROM "ap-public@i-pass.com.tw" SINCE "10-August-2020" BEFORE "15-August-2020"');
				$emailData = imap_search($connection,"FROM $emailfrom SINCE $dateS BEFORE $dateE");
			
				if (! empty($emailData)) {
					?>
						<?php
						foreach ($emailData as $emailIdent) {
						
						$overview = imap_fetch_overview($connection, $emailIdent, 0);
						$message = imap_fetchbody($connection, $emailIdent,'1.1');
						$structure = imap_fetchstructure($connection,$emailIdent); 
						$messageExcerpt = substr($message, 0, 150);
						$partialMessage = trim(quoted_printable_decode($messageExcerpt)); 
						$date = date("Ymd", strtotime($overview[0]->date));
	
						$attachments = array(); 
						if(isset($structure->parts) && count($structure->parts)) 
							{ 
								for($i = 0; $i < count($structure->parts); $i++) 
								{ 
								$attachments[$i] = array(
									'is_attachment' => false, 
									'filename' => '', 
									'name' => '', 
									'attachment' => '' 
								); 
	
								if($structure->parts[$i]->ifdparameters) 
								{ 
									foreach($structure->parts[$i]->dparameters as $object) 
									{ 
									if(strtolower($object->attribute) == 'filename') 
									{ 
									$attachments[$i]['is_attachment'] = true; 
									$attachments[$i]['filename'] = $object->value; 
									} 
									} 
								} 
	
								if($structure->parts[$i]->ifparameters) 
								{ 
									foreach($structure->parts[$i]->parameters as $object) 
									{ 
									if(strtolower($object->attribute) == 'name') 
									{ 
									$attachments[$i]['is_attachment'] = true; 
									$attachments[$i]['name'] = $object->value; 
									} 
									} 
								} 
	
								if($attachments[$i]['is_attachment']) 
								{ 
									$attachments[$i]['attachment'] = imap_fetchbody($connection,$emailIdent, $i+1); 
	
									/* 3 = BASE64 encoding */ 
									if($structure->parts[$i]->encoding == 3) 
									{ 
									$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']); 
									} 
									/* 4 = QUOTED-PRINTABLE encoding */ 
									elseif($structure->parts[$i]->encoding == 4) 
									{ 
									$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']); 
									} 
								} 
								} 
								} 
	
								/* iterate through each attachment and save it */ 
								foreach($attachments as $key=>$attachment) 
								{ 
								if($attachment['is_attachment'] == 1) 
								{ 
								$filename = $attachment['name']; 
								if(empty($filename)) $filename = $attachment['filename']; 
	
								if(empty($filename)) $filename = time() . ".dat"; 
	
								/* prefix the email number to the filename in case two emails 
								* have the attachment with the same file name. 
								*/
								$pathfilename =  "./attachment/" . $date.'-'.$emailIdent.'.xls';
								$fp = fopen($pathfilename, "w+"); // "-" . imap_utf8($filename) 
								fwrite($fp, $attachment['attachment']); 
								fclose($fp); 
	
								$attachments[$key]['filename'] = $filename; 
								} 
							} 
						?>
	
						
						<?php
					} // End foreach
					?>
					</table>
					<?php
				} // end if
				
				imap_close($connection);
			}
		}


		
		$this->render('emailtools',array(
			'dataProvider'=>$dataProvider,
			'dataProvider'=>$dataProvider,
			'emailData'=>$emailData,
			'overview'=>$overview,
			'message'=>$message,
			'structure'=>$structure,
			'messageExcerpt'=>$messageExcerpt,
			'partialMessage'=>$partialMessage,
			'date'=>$date,
			'pathfilename'=>$pathfilename,
			'filename'=>$filename,
			'emailfrom'=>$emailfrom,
			'dateS'=>$dateS,
			'dateE'=>$dateE,
		));
	}





} //最外圈
