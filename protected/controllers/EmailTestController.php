<?php

class EmailTestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() //權限設定
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','emailtools','emailtools1','emailtools2','emailtools3','sftp_download','AjaxTest','AjaxTest2','pageTest','AjaxPage','autoSavePSM'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EmailTest;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmailTest']))
		{
			$model->attributes=$_POST['EmailTest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmailTest']))
		{
			$model->attributes=$_POST['EmailTest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('EmailTest');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new EmailTest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmailTest']))
			$model->attributes=$_GET['EmailTest'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return EmailTest the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=EmailTest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param EmailTest $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-test-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionEmailtools()
	{
		error_reporting(0);
		ini_set('memory_limit', '256M');  
		XPHPExcel::init();
		$phpexcel = new PHPExcel;             

		//上傳檔案
		if(isset($_POST['import']))
		{  
			//檔案的名稱與暫存區名稱不等於空值,取得檔案連結
			if(isset($_FILES['filename']) && $_FILES['filename']['tmp_name']!='') {  
				//檔案路徑-置於暫存區
				$filepath = $_FILES['filename']['tmp_name'];                             
				//檔案名稱來源
				$filename = $_FILES['filename']['name'];             
				$phpreader = new PHPExcel_Reader_Excel2007();
					//不等於版本2007的時候往下讀取舊版本2003系列 
					if(!$phpreader->canRead($filepath)) 
					{                                                              
						$phpreader = new PHPExcel_Reader_Excel5();                            
					}
				//讀取EXCEL    
				$phpexcel = $phpreader->load($filepath); 
				//讀取第一張工作表
				$currentsheet = $phpexcel->getSheet(0);  
				//取得最大的列數 *垂直的是列(ABCD欄位)
				$allcolumn = $currentsheet->getHighestColumn();   
				//取得總行數  *水平的是行(1.2.3.4欄位)
				$allrow = $currentsheet->getHighestRow();  
				
					//單據總結 匯入
				for($currentRow2=20;$currentRow2<=22;$currentRow2++)
				{	
					$date = $phpexcel->getActiveSheet()->getCell("B"."11")->getValue();
					$storeid = $phpexcel->getActiveSheet()->getCell("B"."8")->getValue();
					$storename = $phpexcel->getActiveSheet()->getCell("B"."9")->getValue();	
					$dealname = $phpexcel->getActiveSheet()->getCell("A".$currentRow2)->getValue();
					$ap_total = $phpexcel->getActiveSheet()->getCell("B".$currentRow2)->getValue();

					$inssql = "INSERT INTO emailtest (date,storeid,storename,dealcode,dealname,ar_count,ar_total,ap_count,ap_total) VALUES('$date','$storeid','$storename','$dealcode','$dealname','$ar_count','$ar_total','$ap_count','$ap_total')";
						Yii::app()->db->createCommand($inssql)->execute();	

				} 			

				$imperror=0;	//匯入失敗筆數
				$impsuccess=0;		//匯入成功筆數

				//匯入DB
				for($currentRow=14;$currentRow<=17;$currentRow++)
				{
					$date = $phpexcel->getActiveSheet()->getCell("B"."11")->getValue();
					$storeid = $phpexcel->getActiveSheet()->getCell("B"."8")->getValue();
					$storename = $phpexcel->getActiveSheet()->getCell("B"."9")->getValue();
					$dealcode = $phpexcel->getActiveSheet()->getCell("B".$currentRow)->getValue();
					$dealname = $phpexcel->getActiveSheet()->getCell("C".$currentRow)->getValue();
					$ar_count = $phpexcel->getActiveSheet()->getCell("D".$currentRow)->getValue();
					$ar_total = $phpexcel->getActiveSheet()->getCell("E".$currentRow)->getValue();
					$ap_count = $phpexcel->getActiveSheet()->getCell("F".$currentRow)->getValue();
					$ap_total = $phpexcel->getActiveSheet()->getCell("G".$currentRow)->getValue();
					
					
					//檢查重複
					$query0 = "SELECT * FROM emailtest WHERE date='$date' and storeid='$storeid' and dealcode='$dealcode'";
					$qresult = Yii::app()->db->createCommand($query0)->queryAll();
					
					if(count($qresult) > 0)
					{
						$imperror++;
						yii::app()->user->setFlash('error',"::::匯入失敗!".$imperror.'筆錯誤::::'.$msg);
						$msg = $msg.'<br>('.$qresult[0]['date'].$qresult[0]['storename'].'- 交易代碼'.$qresult[0]['dealcode'].') 資料重複';
					}
					else
					{
						$impsuccess++;
						$inssql = "INSERT INTO emailtest (date,storeid,storename,dealcode,dealname,ar_count,ar_total,ap_count,ap_total) VALUES('$date','$storeid','$storename','$dealcode','$dealname','$ar_count','$ar_total','$ap_count','$ap_total')";
						Yii::app()->db->createCommand($inssql)->execute();
						yii::app()->user->setFlash('success','匯入成功!'.$impsuccess.'筆資料');
					}
				}		
																						
			} 

		}
		//查詢
		if(isset($_POST['submit']))
		{
			$emailfrom = isset($_POST['emailfrom']) ? $_POST['emailfrom'] : '';
			$dateS = isset($_POST['dateS']) ? $_POST['dateS'] : '';
			$dateE = isset($_POST['dateE']) ? $_POST['dateE'] : '';
			
		}
		$this->render('emailtools',array(
			'dataProvider'=>$dataProvider,
			// 'dataProvider'=>$dataProvider,
			// 'emailData'=>$emailData,
			// 'emailIdent'=>$emailIdent,
			// 'overview'=>$overview,
			// 'message'=>$message,
			// 'structure'=>$structure,
			// 'messageExcerpt'=>$messageExcerpt,
			// 'partialMessage'=>$partialMessage,
			// 'date'=>$date,
			// 'pathfilename'=>$pathfilename,
			// 'filename'=>$filename,
			'emailfrom'=>$emailfrom,
			'dateS'=>$dateS,
			'dateE'=>$dateE,
			'phpexcel'  =>$phpexcel,               
			'fileName'  =>$fileName,
			'allrow'    =>$allrow, 
			'dateAry' =>$dateAry,
			'areaAry' =>$areaAry, 
			'iresult'=>$iresult,

		));
	}
	// 自動下載email 下載 上傳
	public function actionEmailtools2()
	{
		date_default_timezone_set("Asia/Taipei");
		$creditAttAry = array();
		$creditAttMsg = ""; //信用卡傳送訊息
		$iPassAttAry = array();
        $emailTransTitle = "Nccc帳務上傳通知";
		//讀取一卡通xls檔案所需
			XPHPExcel::init();
			$phpexcel = new PHPExcel;     

		// $downloadDate = date('j-F-Y');
		$downloadDate = '26-May-2021'; //test

		// CVarDumper::dump($downloadDate);
		if (! function_exists('imap_open')) {
			echo "IMAP is not configured.";
			exit();
		} else 
		{
	
		//信用卡下載 /tmp/
			$hostname = '{imap.gmail.com:993/imap/ssl/novalidate-cert}Inbox';
			$username = 'pytestac1993@gmail.com';
			$password = 'se20210101';
			/* try to connect */
			$connection = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());
			/* Search Emails having the specified keyword in the email subject */
			$emailData = imap_search($connection,"FROM estore_1@nccc.com.tw ON $downloadDate");
			// $emailData = imap_search($connection,'FROM "jitmis2020@gmail.com" SINCE "10-August-2020" BEFORE "15-August-2021"');
			//如果有信件才執行
			if (! empty($emailData)) {
				foreach ($emailData as $emailIdent) {
				
				$overview = imap_fetch_overview($connection, $emailIdent, 0);
				$message = imap_fetchbody($connection, $emailIdent,'1.1');
				$structure = imap_fetchstructure($connection,$emailIdent); 
				$messageExcerpt = substr($message, 0, 150);
				$partialMessage = trim(quoted_printable_decode($messageExcerpt)); 
				$date = date("Ymd", strtotime($overview[0]->date));
				
					$subjectCredit = imap_utf8($overview[0]->subject);
					// CVarDumper::dump($overview,10,true);
					// echo "<br>".$subjectCredit.$downloadDate."<br>";
					array_push($creditAttAry,$subjectCredit.$downloadDate."-".$overview[0]->msgno);
					// CVarDumper::dump($creditAttAry,10,true);
				$attachments = array(); 
				if(isset($structure->parts) && count($structure->parts)) 
					{ 
						for($i = 0; $i < count($structure->parts); $i++) 
						{ 
							$attachments[$i] = array(
								'is_attachment' => false, 
								'filename' => '', 
								'name' => '', 
								'attachment' => '' 
							); 

							if($structure->parts[$i]->ifdparameters) 
							{ 
								foreach($structure->parts[$i]->dparameters as $object) 
								{ 
								if(strtolower($object->attribute) == 'filename') 
									{ 
										$attachments[$i]['is_attachment'] = true; 
										$attachments[$i]['filename'] = $object->value; 
									} 
								} 
							} 
							if($attachments[$i]['is_attachment']) 
							{ 
								$attachments[$i]['attachment'] = imap_fetchbody($connection,$emailIdent, $i+1); 
								/* 3 = BASE64 encoding */ 
								if($structure->parts[$i]->encoding == 3) 
								{ 
									$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']); 
								} 
								/* 4 = QUOTED-PRINTABLE encoding */ 
								elseif($structure->parts[$i]->encoding == 4) 
								{ 
									$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']); 
								} 
							} 
						} 
					} 

					/* iterate through each attachment and save it */ 
					foreach($attachments as $key=>$attachment) 
					{ 
						if($attachment['is_attachment'] == 1) 
						{ 
							$filename = $attachment['name']; 
							$downloadName = imap_utf8($attachment['name']);
							if(empty($filename)) $filename = $attachment['filename']; 
							if(empty($filename)) $filename = time() . ".dat"; 
							/* prefix the email number to the filename in case two emails 
							* have the attachment with the same file name. 
							*/
							// $pathfilename =  "./attachment/" .'(NCCC)'.$subject.$date.'-'.$emailIdent.'.xls';
							// $pathfilename =  "./attachment/" .'(NCCC)'.$subjectCredit.$date.'-'.$emailIdent.'.zip';
							$checkFileName = substr($attachment['filename'],0,3); //確認檔案名稱;REC = 電子收據暨對帳單通知 PDF檔案
							if (!is_dir("./tmp/")) {
								mkdir("./tmp/", 0777, true);
							}
							if($checkFileName != 'REC')
							{
								$pathfilename =  "./tmp/" .'(NCCC)'.$attachment['filename']; //檔名(特店代號)
								$pathfilename = $this->convertEncoding($pathfilename); //下載路徑中文
								$fp = fopen($pathfilename, "w+"); // "-" . imap_utf8($filename) 
								echo $pathfilename;
								fwrite($fp, $attachment['attachment']); 
								fclose($fp); 
								$attachments[$key]['filename'] = $filename; 
							}
							else
								echo "(REC File)";
						} 
					}  
			} // End foreach
				$creditAttMsg.= "(".$downloadDate.")download_success! ".count($attachment)." Files "."\n";
			} // end if 
			imap_close($connection);
		
				
			//自動讀取ZIP檔案 信用卡
				$zip = new ZipArchive;
				//讀取path底下的zip檔案，並解壓縮
				$files = glob("tmp/*.zip");
				CVarDumper::dump($files,10,true);
				if(count($files)>0) //有檔案再執行
				{
					$sucsZip = 0; //成功解壓縮數
					$errZip = 0; //失敗解壓縮數
					//解壓縮
					for($i=0;$i<count($files);$i++)
					{
						if($zip->open("$files[$i]") === TRUE)
						{

							$zipFileName = explode("(NCCC)",$files[$i]); //檔案分解命名
							$checkREC = substr("$zipFileName[1]",0,3); //確認檔案名稱;REC = 電子收據暨對帳單通知 PDF檔案
							if($checkREC != 'REC')
							{
								$zipNcccCode = substr("$zipFileName[1]",0,10); //取得檔案特店代號
											// echo $zipNcccCode;
									//取得解壓縮密碼 帳號末四碼
									$getNcccCode = $this->getNcccData($zipNcccCode); //連接DB fb_account
									// var_dump($getNcccCode);
									$ncccPW = $getNcccCode[0]['account'];
									$ncccPW = substr("$ncccPW",-4); 
								// CVarDumper::dump($getNcccCode,10,true);
								// echo $zipNcccCode.$getNcccCode[0]['storecode']."pw:".$ncccPW."<br>";
								$zip->setPassword("$ncccPW");
								$zip->extractTo('./tmp/');
								$zip->close();
								// echo "success!";
								$sucsZip ++;
							}
						}
						else
						{
							// echo "Error";
							$errZip ++;
						}
						
				}
				//讀取解壓縮完後的檔案txt
				$txtFiles = glob("tmp/D*.txt"); //讀D資料(每日交易明細)
				if(count($txtFiles)>0) //如果有檔案的話再執行
				{
					//交易資料串成sql
					$tit_nccc_sql = "INSERT INTO tit_nccc_pay_log(pdate,storecode,storename,nccc_code,store_Mcode,payNum,cardNum,price) VALUES ";
					for($i=0;$i<count($txtFiles);$i++)
					{
						//獲取檔名
						$txtFileName = explode("/",$txtFiles[$i]);
						$txtAry = file($txtFiles[$i]); //將檔案內容轉為array
						for($j=8;$j<count($txtAry);$j++)
						{
							//取得該檔案特店代號
								$txtNcccCodeAry = explode(" ",$txtAry[3]);
								$txtNcccCode = substr("$txtNcccCodeAry[1]",1);
							//取得特店代號資料 DB:tbs_fb_account 
								$getNcccCodeAry = $this->getNcccData($txtNcccCode);
								$storecode = $getNcccCodeAry[0]['storecode'];
								$storename = $getNcccCodeAry[0]['storename'];
							//拆解每筆交易明細
								$txtChargeAry = explode(" ",$txtAry[$j]);
								if(count($txtChargeAry) == 29) //資料拆解後ary數量，一般交易為29ㄍ，退刷為36ㄍ
								{
									$txtMcode = $txtChargeAry[0]; //端末機號
									$txtPayNum = $txtChargeAry[8]; //交易序號(票卷號碼)
									$txtCardNum = $txtChargeAry[10]; //交易卡號
									$txtPdate = "20".$txtChargeAry[15]; //交易日期
									$txtPrice = $txtChargeAry[20]*1; //交易金額
									// CVarDumper::dump($txtChargeAry,10,true);
									// echo "<br>(".$storecode.")"."(".$txtNcccCode.")"."(".$txtMcode.")"."(".$txtCardNum.")"."(".$txtPdate.")"."(".$txtPrice.")<br>";
									$tit_nccc_sql .= "('$txtPdate','$storecode','$storename','$txtNcccCode','$txtMcode','$txtPayNum','$txtCardNum','$txtPrice'),";
								}
						}
					
					}
					//執行完畢後刪除檔案
						// $zipDelete = glob("tmp/*.zip"); 
						// foreach($zipDelete as $delzip)
						// {
						// 	unlink($delzip);
						// }
						// $txtDelete = glob("tmp/*.txt"); 
						// foreach($txtDelete as $deltxt)
						// {
						// 	unlink($deltxt);
						// }
					//訊息提示
						$creditAttMsg.="ZIP success:".$sucsZip."fails:".$errZip."\n";
					//資料上傳
						$tit_nccc_sql =substr($tit_nccc_sql,0,-1);
						// Yii::app()->eip->createCommand($tit_nccc_sql)->execute(); //eip 正式機
					//寄信通知
						$emailTransLog = "; ZIP success:".$sucsZip."fails:".$errZip."\n";
						// $this->sendMail($downloadDate,$emailTransTitle,$emailTransLog);
				}
				else
				{
					echo $downloadDate." no Nccc dailyData ";
					$emailTransLog = " no Nccc dailyData ";
					// $zipDelete = glob("tmp/*.zip"); 
					// echo $zipDelete[0];
					// foreach($zipDelete as $delzip)
					// {
					//     // echo $delzip;
					//     unlink($delzip);
					// }
					//寄信通知
					// $this->sendMail($downloadDate,$emailTransTitle,$emailTransLog);
				}
				
			}

		}//

		$this->render('emailtools2',array(
			'creditAttAry'=>$creditAttAry,
			'creditAttMsg'=>$creditAttMsg,
			'iPassAttAry'=>$iPassAttAry,
		));
	}
	//ipass sftp下載 上傳
	public function actionSftp_download()
	{
		// $downloadDate = date('Ymd');
		$downloadDate = '20210526'; //test
		//email訊息內容
		$emailTitle = " iPass帳務上傳通知 ";
		$emailMsg ="";
		$logMsg = "";

		//查詢ipass_sftp帳號密碼
			$sftp_ac_sql = "SELECT * FROM tbs_ipass_account ";
			$sftp_ac_ary = Yii::app()->localEIP->createCommand($sftp_ac_sql)->queryAll(); //eip 正式機
			// CVarDumper::dump($sftp_ac_ary,10,true);
		//SFTP資料
			$host = 'sftp.i-pass.com.tw';
			$port = 2222;
			$remoteDir = "/fromipass/"; //SFTP檔案位置
			$localDir = 'tmp/'; //存放位置
		$fileList = array(); //檔案清單

		//每間門市登入SFTP抓資料
		for($i=0;$i<count($sftp_ac_ary);$i++)
		{
			//SFTP連線用--
				$username = $sftp_ac_ary[$i]['sftp_id'];
				$password = $sftp_ac_ary[$i]['sftp_pwd'];
				$connection = ssh2_connect("$host", $port);
				ssh2_auth_password($connection, "$username", "$password");
				$sftp = ssh2_sftp($connection);
				$sftpPath= intval($sftp); //轉型

			//列出SFTP位置全部檔案
				$files = scandir("ssh2.sftp://".$sftpPath.$remoteDir);
				// $fileList = array(); //檔案清單
			//下載並把ZIP檔案加進array
			for($j=0;$j<count($files);$j++)
			{
				$fileEX = explode('EVCR_',$files[$j]); //EVCR 明細帳
				// CVarDumper::dump($fileEX,10,true);
				//檔案類型 日期 檔案格式
				if($fileEX[0] == "" && substr($fileEX[1],8,8) == "$downloadDate" && substr($files[$j],-3) == "ZIP" )
				{
					array_push($fileList,$files[$j]);
					// $fileList[$sftp_ac_ary[$i]['storename']][$j] = $files[$j];
					//下載檔案
						$stream = fopen("ssh2.sftp://$sftpPath$remoteDir"."$files[$j]", 'r');
						if (! $stream) {
								// throw new \Exception("Could not open file: $files[$j]");
								echo "Could not open file: $files[$j]";
							}
						
						$contents = stream_get_contents($stream);
						file_put_contents ("tmp/".$files[$j], $contents);
						fclose($stream);
				}
				
			}
		}
		//解壓縮ZIP檔案
		$zip = new ZipArchive;
		//讀取path底下的zip檔案，並解壓縮
		$zipfiles = glob("tmp/*.ZIP");
		if(count($zipfiles)>0) //有檔案再執行
		{
			$sucsZip = 0; //成功解壓縮數
			$errZip = 0; //失敗解壓縮數
			//解壓縮
			for($i=0;$i<count($zipfiles);$i++)
			{
				if($zip->open("$zipfiles[$i]") === TRUE)
				{
					// $zipFileName = explode("(NCCC)",$files[$i]); //檔案分解命名
					// CVarDumper::dump($getNcccCode,10,true);
					$zip->extractTo('./tmp/');
					$zip->close();
					// echo "success!";
					$sucsZip ++;
				}
				else
				{
					// echo "Error";
					$errZip ++;
				}	
			}
		}
		//讀取解壓縮後的檔案並上傳交易明細資料
		$datFiles = glob("tmp/*.DAT"); //(每日交易明細)
		// CVarDumper::dump($datFiles,10,true);
		$insertIpass_sql = "INSERT INTO tit_ipass_pay_log(pdate,storename,storecode,ipass_storecode,ipass_type,ipass_CardNum,price,ptime,opt1) VALUES ";
		for($i=0;$i<count($datFiles);$i++)
		{
			$datAry = file($datFiles[$i]); //將檔案內容轉為array
			//解析檔案資料 - title
			$dat_ipass_storecode = substr($datAry[0],6,8);
			$dat_StoreAry = $this->getIpassData($dat_ipass_storecode);
			$dat_storename = $dat_StoreAry[0]['storename'];
			$dat_storecode = $dat_StoreAry[0]['storecode'];
			//讀取交易明細資料,第二行 到 倒數第二行 (第一行為title;最後一行為交易總筆數)
			for($j=1;$j<count($datAry)-1;$j++)
			{
				// CVarDumper::dump($datAry[$j],10,true);
				$pdate = substr($datAry[$j],127,8); //交易日
				$ptime = substr($datAry[$j],135,6); //交易時間
				$price = substr($datAry[$j],163,6)*1; //交易金額
				$ipass_type = substr($datAry[$j],175,2); //ipass種類 04一般一卡通 02聯名卡(?)
				$ipass_cardNum = substr($datAry[$j],67,8); //ipass卡號
				// echo $dat_storename."  ".$dat_storecode."   ".$pdate."   ".$ptime."   ".$price."  ".$ipass_type."  ".$ipass_code."<br>";
				//串成sql
				$insertIpass_sql .= "('$pdate','$dat_storename','$dat_storecode','$dat_ipass_storecode','$ipass_type','$ipass_cardNum','$price','$ptime','1'),";
			}
			//門市&&票數 MsgAry
			$tit = substr($datAry[count($datAry)-1],1,8)*1; 
			$dat_data[$i]['storename'] = $dat_storename;
			$dat_data[$i]['tit'] = $tit;
		}

		//上傳帳務資料
			$insertIpass_sql = substr($insertIpass_sql,0,-1); //去掉後面","
			// Yii::app()->eip->createCommand($insertIpass_sql)->execute(); //eip 正式機
		//刪除檔案
		//執行完畢後刪除檔案
		$zipDelete = glob("tmp/*.ZIP"); 
		foreach($zipDelete as $delzip)
		{
			unlink($delzip);
		}
		$datDelete = glob("tmp/*.DAT"); 
		foreach($datDelete as $deldat)
		{
			unlink($deldat);
		}
		//送信
		$emailMsg .="【".$downloadDate."  ".$emailTitle."】Zip files : ".count($fileList);
		$logMsg = "【".$downloadDate."  ".$emailTitle."】Zip files : ".count($fileList);
		$ipasTtitTotal = 0; //票卷數量(資料量)
		for($i=0;$i<count($dat_data);$i++)
		{
			$emailMsg .= "<br>門市:".$dat_data[$i]['storename'].";票數:".$dat_data[$i]['tit'];
			$ipasTtitTotal = $ipasTtitTotal + $dat_data[$i]['tit'];
		}
		//資料上傳
		if($ipasTtitTotal>0)
		{
			// Yii::app()->eip->createCommand($insertIpass_sql)->execute(); //eip 正式機
		}
		echo $ipasTtitTotal;
		echo $insertIpass_sql;


		$this->render('sftp_download',array(
			'downloadDate'=>$downloadDate,
			'fileList'=>$fileList,
			'dat_data'=>$dat_data,

		));
	}

	//線上叫修 練習 google表單
	public function actionEmailtools3()
	{
		
		// // Google API TEST
		// require_once(Yii::app()->basePath."/extensions/google-api-php-client/vendor/autoload.php");
		// $client = new Google_Client();
		// $client->setApplicationName("GoogleSheetApi");
		// $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
		// $client->setAuthConfig(Yii::app()->basePath."/extensions/google-api-php-client/"."APIkey.json");
		// $client->setAccessType('offine');
		// $client->setPrompt('Select');
		// // CVarDumper::dump($client,10,true);

		// // ** */
		// $service = new Google_Service_Sheets($client);
		// $spreadsheetId = '1vG0J4gMd_KYLMjsyo1iMz6E-ZJRVEk5Sn3-wFw4POAs';
		// // $spreadsheetId = '1r5tAf-VvEH01N35_3h7MgAfO_i67PN5BjMyzyOC-m4o';
		// $range = '表單回應 1!A1:C1';
		// $response = $service->spreadsheets_values->get($spreadsheetId, $range);
		// $values = $response->getValues();
		// CVarDumper::dump($values,10,true);
		// $updateRange = '工作表2!C2:D1';
		// $updateValue = [['TestInsert','Store']];
		// $update_body = new Google_Service_Sheets_ValueRange([
		// 	'values'=>$updateValue,
		// ]);
		// $params = [
		// 	'valueInputOption' => 'RAW'
		// ];
		// $result = $service->spreadsheets_values->append($spreadsheetId, $updateRange, $update_body, $params);
		// $result->getUpdates()->getUpdatedCells();


		//----------------------------------------------------------------
		$googleAry = array();
		$google_dates = isset($_POST['google_dates'])?$_POST['google_dates']:'';
		$google_datee = isset($_POST['google_datee'])?$_POST['google_datee']:'';
		// $google_date = date('Y/m/d',strtotime($google_date));
		// $view_date = isset($_POST['google_date']) ? date('Y-m-d',strtotime($google_date)):'';
		// $client = new Google_Client();
		// CVarDumper::dump($client,10,true);

		if(isset($_POST['googleBTN']))
		{
			$sheetDataAry = array(); //放資料用
			//舊版 讀取googleSheet
				// $googlesheet = file_get_contents('https://spreadsheets.google.com/feeds/list/1UEIrbtCzbkZwKJ7pUyE7_MhO3_dhXxBO1lSbBu3PaR8/ombkxhb/public/values?alt=json');//測試
				// // $googlesheet = file_get_contents('https://spreadsheets.google.com/feeds/list/1r5tAf-VvEH01N35_3h7MgAfO_i67PN5BjMyzyOC-m4o/ombkxhb/public/values?alt=json');
				// $googleJSON = json_decode($googlesheet);
				// $jSONAry = $googleJSON->{'feed'}->{'entry'}; //googleSheet全部資料
				// $num = 0; //給googleAry用的key值
				// CVarDumper::dump(count($jSONAry),10,true);
				// foreach($jSONAry as $k)
				// { 
				// 	$time = $k->{'gsx$時間戳記'}->{'$t'};
				// 	$date = $k->{'gsx$提報時間'}->{'$t'};
				// 	$area = $k->{'gsx$區域名稱'}->{'$t'};
				// 	$storename = $k->{'gsx$叫修門市'}->{'$t'};
				// 	$emp = $k->{'gsx$填單人員'}->{'$t'};
				// 	$details = $k->{'gsx$報修內容'}->{'$t'};
				// 	$status = $k->{'gsx$請選擇項目'}->{'$t'};
				// 	$type = $k->{'gsx$項目類別'}->{'$t'};
				// 	if($time != '')
				// 	{
				// 		if($date == $google_date)
				// 		{
				// 			$googleAry[$num] = isset($googleAry[$num]) ? $googleAry[$num]:array();
				// 			array_push($googleAry[$num],$time,$date,$area,$storename,$emp,$details,$status,$type);
				// 			// array_push($googleAry[$num],$time,$date,$storename,$emp,$details,$status,$type);
		
				// 			$num ++;
				// 		}
				// 	}
				// }
			
			// CVarDumper::dump($googleAry,10,true);
			// CVarDumper::dump($google_date,10,true);
			//CSV格式
				$sheetUrl = 'https://docs.google.com/spreadsheets/d/1vG0J4gMd_KYLMjsyo1iMz6E-ZJRVEk5Sn3-wFw4POAs/gviz/tq?tqx=out:csv&tq='; //測試用
				// $sheetUrl = 'https://docs.google.com/spreadsheets/d/1r5tAf-VvEH01N35_3h7MgAfO_i67PN5BjMyzyOC-m4o/gviz/tq?tqx=out:csv&tq='; //正式
				$sheetSql = "select A,B,C,D,E,F,G,H where A >= date '$google_dates' and A <= date '$google_datee'";
				$sheetEncode = urlencode($sheetSql); //將SQL指令轉碼
				$sheet = file_get_contents($sheetUrl.$sheetEncode);//測試
				// CVarDumper::dump($sheet,10,true);
				// echo $sheetUrl.$sheetEncode;
			//testJSON --->JSON格式
				// $testSheetUrl = 'https://docs.google.com/spreadsheets/d/1vG0J4gMd_KYLMjsyo1iMz6E-ZJRVEk5Sn3-wFw4POAs/gviz/tq?tq='; //測試用
				// $testSheetSql = "select A,B,C,D,E,F,G,H where A >= date '$google_dates' and A <= date '$google_datee'";
				// $testSheetEncode = urlencode($testSheetSql); //將SQL指令轉碼
				// $testSheet = file_get_contents($testSheetUrl.$testSheetEncode);//測試
				// $json_string = preg_replace("/[^(]*\((.*)\)/", "$1", $testSheet); //整理原本的JSON變為可讀
				// $json_string = substr($json_string,0,-1);
				// $jsonEN= json_decode($json_string);
				// 		// echo $json_string;
				// 	// CVarDumper::dump($jsonEN,10,true);

			//分段csv每一行
			$sheetAry = explode("\n",$sheet);
				// CVarDumper::dump($sheetAry,10,true);
			//分段每一行的欄位資料
			for($i=0;$i<count($sheetAry);$i++)
			{

				$sheetDetails = explode(",",$sheetAry[$i]);
				for($j=0;$j<count($sheetDetails);$j++)
				{
					$sheetDataAry[$i][$j] = $sheetDetails[$j];
				}
			}


		}



		$this->render('emailtools3',array(
			'googleAry'=>$googleAry,
			'google_dates'=>$google_dates,
			'google_datee'=>$google_datee,
			'sheetDataAry'=>$sheetDataAry,
		));
	}

	//分頁測試
	public function actionPageTest()
	{
		$sql = "SELECT * FROM posdata ";
		$sqlAry = Yii::app()->db->createCommand($sql)->queryAll();
		$pageAry ='';
		$limit_page = 0;
		$data_nums = count($sqlAry);
		$per = 5 ; //每頁顯示數量

		//每數
		//每頁5筆資料(per)，頁數(total_pages)，
		if($data_nums > 5){
			$total_pages = ceil($data_nums/$per);}
		else {
			$total_pages = 1;}
		
		//資料截點設定，頁碼
		isset($_POST['pageA']) ? $page = $_POST['pageA'] : $page = 1;
		if($page != 0)
		{
			$limit_page = ($page-1)*$per;
		}
		if(isset($_POST['subPage']))
		{
			$pageSql = "SELECT * FROM posdata LIMIT $limit_page,$per";
			$pageAry = Yii::app()->db->createCommand($pageSql)->queryAll();
		}

		// CVarDumper::dump($data_nums,10,true);
		// CVarDumper::dump($_POST);
		// CVarDumper::dump($pageAry,10,true);

		$this->render('pageTest',array(
			'data_nums'=>$data_nums,
			'pageAry' => $pageAry,
			'total_pages'=>$total_pages,
			'page'=>$page,

		));
	}
	// AJAX測試用
	public function actionEmailtools1()
	{
		$ts1 = isset($_POST['ts1'] )? $_POST['ts1']:'';
		$ts2 = isset($_POST['ts2'] )? $_POST['ts2']:'';


		$aaSQL = "SELECT * FROM tba_adjust_item";
		$aaAry = Yii::app()->db->createCommand($aaSQL)->queryAll();
		$abAry ='';
		
		if(isset($_POST['tsB']))
		{
			// $messagecontent = '測試變數名稱';
			CVarDumper::dump($_POST,10,true);
			// $headers = array(
			// 	// 'Content-Type:ultipart/form-data',
			// 	// 'Content-Type:application/json',
			// 	// 'Method:POST',
			// 	'Content-Type: application/x-www-form-urlencoded',
			// 	'Authorization:Bearer yjtdD9vd1KLwlI42bsEMFriiIZvfebliZCOGdqnpBjr'
			// );
			// $message = array(
			// 	'message' => $messagecontent,
			// );
			// $ch = curl_init();
			// curl_setopt($ch , CURLOPT_URL , "https://notify-api.line.me/api/notify");
			// // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			// // curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
			// // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			// curl_setopt($ch, CURLOPT_HEADER, true);
			// curl_setopt($ch, CURLOPT_POST, true);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($message));
			// $result = curl_exec($ch);
			// curl_close($ch);

			// CVarDumper::dump($result);


		}
		// $aaa = $this->actionAjaxTest();


		$this->render('emailtools1',array(
			'ts1'=>$ts1,
			'ts2'=>$ts2,
			'aaAry'=>$aaAry,
			'abAry'=>$abAry,
		));
	}
	public function actionAjaxTest()
	{
		if(isset($_POST['item']))
		{
			$item = $_POST['item'] ;
			$abSQL = "SELECT * FROM tba_adjust_item WHERE applyCode = '$item' ";
			$abAry = Yii::app()->db->createCommand($abSQL)->queryAll();
			
			// CVarDumper::dump($abAry['applyName']);
			print $abAry[0]['applyName'];
		}
		else
		{
			$aa = '654321';
			$ts2 = $aa;
			// CVarDumper::dump($aa);
			print $ts2;
		}
	}

	public function actionAjaxTest2()
	{
		$item = $_POST['item'] ;
		$newOPTsql ="SELECT * FROM tba_adjust WHERE applyCode = '$item'";
		$newOPT =  Yii::app()->db->createCommand($newOPTsql)->queryAll();
		if(count($newOPT) == 0)
		{
			$newOPTsql ="SELECT * FROM tbs_service_item ";
			$newOPT =  Yii::app()->db->createCommand($newOPTsql)->queryAll();
		}
		// CVarDumper::dump($newOPT);
		// return $newOPT;
		print json_encode($newOPT);
	}
	//分頁用ajax
	public function actionAjaxPage()
	{
		$page = $_POST['pag'] ;

		$pageAry ='';
		$limit_page = 0;
		$per = 5 ; //每頁顯示數量
		
		//資料截點設定，頁碼
		if($page != 0)
		{
			$limit_page = ($page-1)*$per;
		}
			$pageSql = "SELECT * FROM posdata LIMIT $limit_page,$per";
			$pageAry = Yii::app()->db->createCommand($pageSql)->queryAll();
		print json_encode($pageAry);

	}
	//email轉換
	public function convertEncoding($string){
		//根據系統進行配置
		$encode = stristr(PHP_OS, 'WIN') ? 'big5' : 'UTF-8';
		$string = iconv('UTF-8', $encode, $string);
		//$string = mb_convert_encoding($string, $encode, 'UTF-8');
		return $string;
	}
	//特店代號取得NCCC附件解壓縮密碼
	public function getNcccData($NcccCode)
	{
		$fb_sql = "SELECT * FROM tbs_fb_account WHERE nccc_code = '$NcccCode'";
		$nccc_code = Yii::app()->eip->createCommand($fb_sql)->queryAll();
		return $nccc_code;
	}
	//特店代號取得ipass帳務資料
	public function getIpassData($ipassCode)
	{
		$ipass_sql = "SELECT * FROM tbs_ipass_account WHERE ipass_storecode = '$ipassCode' ";
		$ipassAry = Yii::app()->localEIP->createCommand($ipass_sql)->queryAll();
		return $ipassAry;
	}
	//傳送email
    private function sendMail($pdate,$emailTitle,$dataLog)
    {
        // include ('c:\xampp\htdocs\jiteip\protected\extensions\phpmailer\phpmailer.php');
        $mail = new phpmailer();
            try {
                $mail->SMTPDebug = 1;
                // 戰國策
                $mail->isSMTP();                  //設定使用SMTP方式寄信
                $mail->SMTPAuth = TRUE;           //設定SMTP需要驗證
    
                // $mail->Host = "mail.jithouse.com.tw";     //智邦的SMTP主機
                // $mail->Port = 25;                 //智邦的SMTP主機的SMTP埠位為465埠
                // $mail->CharSet = "utf-8";          //設定郵件編碼，預設UTF-8
                // $mail->Username = "sensor@jithouse.com.tw";  //智邦帳號
                // $mail->Password = "jit3944560";  //智邦密碼
                // $mail->From = "sensor@jithouse.com.tw"; //設定寄件者信箱
        // =============================================================================			
                $mail->Host = "smtp.gmail.com";     //GCP的SMTP主機
                $mail->Port = 587;                 //智邦的SMTP主機的SMTP埠位為465埠
                $mail->SMTPSecure = "tls";        // 智邦的SMTP主機需要使用SSL連線
                $mail->CharSet = "utf-8";          //設定郵件編碼，預設UTF-8
                $mail->Username = "notify@jithouse.com";  //智邦帳號
                $mail->Password = "4rQ&3<jq";  //智邦密碼
                // $mail->From = "notify@jithouse.com"; //設定寄件者信箱
                $mail->setFrom('notify@jithouse.com', 'email');
    
        // =========================================================================
                $mail->FromName = "JIT-EIP系統";   //設定寄件者姓名
                $mail->Subject = $pdate.$emailTitle ;    //設定郵件標題
                $mail->Body = "";
                
                $mail->Body .= "$dataLog";  //設定郵件內容
                $mail->IsHTML(true);                     //設定郵件內容為HTML
                // $mail->AddAddress($email);
                
                $mail->AddAddress("jitmis2016@gmail.com");
                $mail->AddAddress("s15631102@stu.edu.tw");

                        
                if (!$mail->Send()) {
                    CVarDumper::dump($mail,10,true);
                    'Error: ' . $mail->ErrorInfo;
                }
                else {
                    // CVarDumper::dump($mail,10,true);
                }
            } catch (phpmailerException $e) {
                $errors[] = $e->errorMessage(); //Pretty error messages from PHPMailer
                CVarDumper::dump($e,10,true);
            } catch (Exception $e) {
                $errors[] = $e->getMessage(); //Boring error messages from anything else!
                CVarDumper::dump($e,10,true);
            }
    }

	//20210601 自動封存PSM資料(每月1號中午)
    public function actionAutoSavePsm()
    {
		// $nowdate = date('Ym');
		$setLM = strtotime("-1 month");
		$saveDate = date('Ym',$setLM); //本月封存上月資料
		$tbp_log_sql = "SELECT storecode,storename,serviceno,sum(num) as total FROM tbp_perform_log WHERE pdate like '$saveDate%' group by storecode,serviceno ";
	
            //執行SQL---
            $tbp_log_ary = Yii::app()->localEIP->createCommand($tbp_log_sql)->queryAll();
			CVarDumper::dump($tbp_log_ary,10,true);

		$this->render('autoSavePSM',array(

		));
	}


} //最外圈
