<?php

class BooksController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','userlend','userlend_view','userlend_list'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('userlend','userlendupdate','userlend_listview','userlend_return'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','create','update','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Books;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Books']))
		{
			$model->attributes=$_POST['Books'];

			if($model->bname!=null && $model->bauthor!=null && $model->bpublish!=null && $model->bisbn!=null && $model->bdate!=null)
			{
			$bookimage = Yii::getPathOfAlias('webroot').'/images/';	//儲存路徑
			$imgTem = CUploadedFile::getInstance($model, 'bimg');	//get檔案
			$model->bimg = 'images/'.$model->bisbn.$imgTem->name;	//資料庫儲存檔名>->image/檔名
			$imgTem->saveAs($bookimage.$model->bisbn.$imgTem->name);	//空間儲存檔名
			}

			
			// CVarDumper::dump($bookimage);
			// CVarDumper::dump($imgTem);
			


			if($model->save())

				$this->redirect(array('view','id'=>$model->id));
				// $model->bimg->saveAs($bookimage.$imgTem->name);

		}

		$this->render('create',array(
			'model'=>$model,
		));
		// CVarDumper::dump($_POST);
		CVarDumper::dump($model->bname);
	}



	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Books']))
		{
			$model->attributes=$_POST['Books'];

			$bookimage = Yii::getPathOfAlias('webroot').'/images/';	//儲存路徑
			$imgTem = CUploadedFile::getInstance($model, 'bimg');	//get檔案
			$model->bimg = 'images/'.$model->bisbn.$imgTem->name;	//資料庫儲存檔名>->image/檔名
			$imgTem->saveAs($bookimage.$model->bisbn.$imgTem->name);	//空間儲存檔名
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Books');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Books('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Books']))
			$model->attributes=$_GET['Books'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	


//---userlend--搜尋書籍
public function actionuserlend()
{

			CVarDumper::dump($_POST);
			
	$qryid='';
	$qrybname = '';
	$qrybauthor = '';
	$qrybisbn = '';
	//$checkNoReturn = FALSE;

	//
	$resultAry = array();

	if(isset($_POST['submit'])) {
			
			// $sql = "SELECT * FROM books ".
			// "WHERE ";
			// 					//"WHERE ";

		
		if(isset($_POST['qrybname']))
			$qrybname = $_POST['qrybname'];

		if(isset($_POST['qrybauthor']))
			$qrybauthor = $_POST['qrybauthor'];
			
		if(isset($_POST['qrybisbn']))
			$qrybisbn = $_POST['qrybisbn'];
//1
		if($qrybname!="" && $qrybauthor!="" && $qrybisbn!="")
			$resultAry = Books::model()->findAllByAttributes(
				array('bname'=>$qrybname, 'bauthor'=>$qrybauthor, 'bisbn'=>$qrybisbn)
			);
//2
		elseif($qrybname!="" && $qrybauthor=="" && $qrybisbn=="")
			$resultAry = Books::model()->findAllByAttributes(
				array('bname'=>$qrybname)
			);
		elseif($qrybname=="" && $qrybauthor!="" && $qrybisbn=="")
			$resultAry = Books::model()->findAllByAttributes(
				array('bauthor'=>$qrybauthor)
			);
		elseif($qrybname=="" && $qrybauthor=="" && $qrybisbn!="")
			$resultAry = Books::model()->findAllByAttributes(
				array('bisbn'=>$qrybisbn)
			);
//3
		elseif($qrybname!="" && $qrybauthor!="" && $qrybisbn=="")
			$resultAry = Books::model()->findAllByAttributes(
				array('bname'=>$qrybname,'bauthor'=>$qrybauthor)
			);
		elseif($qrybname=="" && $qrybauthor!="" && $qrybisbn!="")
			$resultAry = Books::model()->findAllByAttributes(
				array('bauthor'=>$qrybauthor,'bisbn'=>$qrybisbn)
			);
		elseif($qrybname!="" && $qrybauthor=="" && $qrybisbn!="")
			$resultAry = Books::model()->findAllByAttributes(
				array('bname'=>$qrybname,'bisbn'=>$qrybisbn)
			);

		else // 全查
			$resultAry = Books::model()->findAll();

		
	}


//        $book = new Book();
//        $book->bookname = "哈利波特";
//        $book->author = "JK";
//
//        $book->save();



	$this->render('userlend',array(
		'qryid'=>$qryid,
		'qrybname'=>$qrybname,
		'qrybauthor'=>$qrybauthor,
		'qrybisbn'=>$qrybisbn,
		'checkNoReturn'=>$checkNoReturn,
		'resultAry' => $resultAry
	));

}


	public function actionuserlend_view($id)
	{
		$this->render('userlend_view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionuserlend_list()
	{
		$dataProvider=new CActiveDataProvider('Books');
		$this->render('userlend_list',array(
			'dataProvider'=>$dataProvider,
		));
	}

	//借書更新資料
	public function actionuserlendupdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);


		if(isset($_POST['Books']))
		{
			
			$model->attributes=$_POST['Books'];
			//$model->lender=$_POST['Books']['lender']; //更新某個欄位
			//$model->tbl_books=$_POST['TblUser']['tbl_books'];

			//同步存tbl_user->tbl_books欄位
			$updateubs = Yii::app()->db->createCommand()
			->update('tbl_user',
			array('tbl_books'=>$model->bname,),
			'username=:username',
			array(':username'=>Yii::app()->user->id)
					);
				if($model->save())
				// echo '<script>alert("成功!")</script>'; 
				$this->redirect(array('userlend_view','id'=>$model->id));
				//CVarDumper::dump($model->bname);
			
		}
		$this->render('userlendupdate',array(
			'model'=>$model,
		));
	}

	public function actionuserlend_listview($id)
	{
		$this->render('userlend_listview',array(
			'model'=>$this->loadModel($id),
		));
	}
//-----------


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Books the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Books::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Books $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='books-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

		 //還書 userlend_return
	 public function actionuserlend_return()
	 {
		$row = Yii::app()->db->createCommand(array(
			'select'=>'*',
			'from'=>'tbl_user',
			'where'=>'username=:username',
			'params'=>array('username'=>Yii::app()->user->id),
		))->queryRow();
		//CVarDumper::dump($row['tbl_books']);
		//CVarDumper::dump(Yii::app()->user->id);
			
		//  $this->render('userlend_return');

		 if(isset($_POST['breturn']))
		{
			$return_books = Yii::app()->db->createCommand()		//書籍 資料更新
			->update('books',
			array('bstatus'=>'在架','lender'=>'','lenddate'=>'','expiry'=>''),
			'bname=:bname',
			array(':bname'=>$row['tbl_books'])
					);
			$return_user = Yii::app()->db->createCommand()		//使用者 借閱書籍更新
			->update('tbl_user',
			array('tbl_books'=>'',),
			'username=:username',
			array(':username'=>Yii::app()->user->id)
					);
			echo '<script>alert("成功!")</script>'; 
				}
				
				$this->render('userlend_return',array('tbl_book'=>''));
	 }
	 
}
