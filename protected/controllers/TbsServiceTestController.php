<?php

class TbsServiceTestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','tbsServiceStCreate','tbsServiceStExport','tbsServiceStSortable','tbsServiceStUpdate','tbsServiceStNewCreate'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TbsServiceTest;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TbsServiceTest']))
		{
			$model->attributes=$_POST['TbsServiceTest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TbsServiceTest']))
		{
			$model->attributes=$_POST['TbsServiceTest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TbsServiceTest');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TbsServiceTest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TbsServiceTest']))
			$model->attributes=$_GET['TbsServiceTest'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TbsServiceTest the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TbsServiceTest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TbsServiceTest $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tbs-service-test-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	//服務產生=========================
	public function actiontbsServiceStCreate()
	{
		$model=new TbsServiceTest;
		$dataProvider=new CActiveDataProvider('TbsServiceTest');
		$myip = '127.0.0.1';
		$storecode = '001001';
		$storename = 'test123';

		$version = $_POST['version'];
		$brand = $_POST['brand'];		

		$itemsql_1 = "SELECT * FROM tbs_service_item WHERE type = 1";
		$itemtype_1 = Yii::app()->db->createCommand($itemsql_1)->queryAll();
		$itemsql_2 = "SELECT * FROM tbs_service_item WHERE type = 2";
		$itemtype_2 = Yii::app()->db->createCommand($itemsql_2)->queryAll();
		$itemsql_3 = "SELECT * FROM tbs_service_item WHERE type = 3";
		$itemtype_3 = Yii::app()->db->createCommand($itemsql_3)->queryAll();
		$itemsql_4 = "SELECT * FROM tbs_service_item WHERE type = 4";
		$itemtype_4 = Yii::app()->db->createCommand($itemsql_4)->queryAll();

		if(isset($_POST['import']))
		{
			$tranSvTimeName = $this->getSvTimeName();	//轉換sv_time

			//檢查版本
			$checkVersionsql = "SELECT version FROM tbs_service_test WHERE version = $version and storeCode = $storecode";
			$checkVersionResult = Yii::app()->db->createCommand($checkVersionsql)->queryAll();
			if($checkVersionResult[0]['version'] == $version)
			{
				yii::app()->user->setFlash('error','新增失敗，版本號已重複，如需變更請至修改頁面操作');
			}
			else{
				$inssql = "INSERT INTO tbs_service_test (version,storeCode,storeName,brand,sale_no,sale_name,sv_time,sv_timename,sv_type,sv_order,num,perform_mapping,price,ip)
					VALUE ";

				//itemType1
				for($i=0;$i<=100;$i++)
				{
					if($_POST['sv_1_'.$i]!=null)
					{
						$sv1_post = $_POST['sv_1_'.$i];
						$svt1_post = $_POST['svt_1_'.$i];
						//對照項目用
						$item1_Allsql = "SELECT * FROM tbs_service_item WHERE sale_no = '$sv1_post'";
						$item1_Allresult = Yii::app()->db->createCommand($item1_Allsql)->queryAll();
						//SLQ
						$inssql .= "('$version','$storecode','$storename','$brand','$sv1_post','".$item1_Allresult[0]['sale_name']."','$svt1_post','$tranSvTimeName[$svt1_post]','".$item1_Allresult[0]['type']."','".$item1_Allresult[0]['sv_order']."','".$item1_Allresult[0]['num']."','".$item1_Allresult[0]['perform_mapping']."','".$item1_Allresult[0]['price']."','$myip'),";
						$serviceitem++;
					}
					if($_POST['sv_2_'.$i]!=null)
					{
						$sv2_post = $_POST['sv_2_'.$i];
						$svt2_post = $_POST['svt_2_'.$i];
						//對照項目用
						$item2_Allsql = "SELECT * FROM tbs_service_item WHERE sale_no = '$sv2_post'";
						$item2_Allresult = Yii::app()->db->createCommand($item2_Allsql)->queryAll();
						//SQL
						$inssql .= "('$version','$storecode','$storename','$brand','$sv2_post','".$item2_Allresult[0]['sale_name']."','$svt2_post','$tranSvTimeName[$svt2_post]','".$item2_Allresult[0]['type']."','".$item2_Allresult[0]['sv_order']."','".$item2_Allresult[0]['num']."','".$item2_Allresult[0]['perform_mapping']."','".$item2_Allresult[0]['price']."','$myip'),";
						$serviceitem++;
					}
					if($_POST['sv_3_'.$i]!=null)
					{
						$sv3_post = $_POST['sv_3_'.$i];
						$svt3_post = $_POST['svt_3_'.$i];
						//對照項目用
						$item3_Allsql = "SELECT * FROM tbs_service_item WHERE sale_no = '$sv3_post'";
						$item3_Allresult = Yii::app()->db->createCommand($item3_Allsql)->queryAll();
						//SQL
						$inssql .= "('$version','$storecode','$storename','$brand','$sv3_post','".$item3_Allresult[0]['sale_name']."','$svt3_post','$tranSvTimeName[$svt3_post]','".$item3_Allresult[0]['type']."','".$item3_Allresult[0]['sv_order']."','".$item3_Allresult[0]['num']."','".$item3_Allresult[0]['perform_mapping']."','".$item3_Allresult[0]['price']."','$myip'),";
						$serviceitem++;
					}
					if($_POST['sv_4_'.$i]!=null)
					{
						$sv4_post = $_POST['sv_4_'.$i];
						$svt4_post = $_POST['svt_4_'.$i];
						//對照項目用
						$item4_Allsql = "SELECT * FROM tbs_service_item WHERE sale_no = '$sv4_post'";
						$item4_Allresult = Yii::app()->db->createCommand($item4_Allsql)->queryAll();
						//SQL
						$inssql .= "('$version','$storecode','$storename','$brand','$sv4_post','".$item4_Allresult[0]['sale_name']."','$svt4_post','$tranSvTimeName[$svt4_post]','".$item4_Allresult[0]['type']."','".$item4_Allresult[0]['sv_order']."','".$item4_Allresult[0]['num']."','".$item4_Allresult[0]['perform_mapping']."','".$item4_Allresult[0]['price']."','$myip'),";
						$serviceitem++;
					}
				}
				if($serviceitem>0)
				{
					$insresult = substr($inssql,0,-1); //0->抓全部 ,-1 ->去掉最後一個字元(,)
					// var_dump($insresult);
					Yii::app()->db->createCommand($insresult)->execute();
					yii::app()->user->setFlash('success','新增成功!'.$serviceitem.'筆資料');
				}
				else
				{
					yii::app()->user->setFlash('error','新增失敗');
				}
			}
		}

		$this->render('tbsServiceStCreate',array(
			'dataProvider'=>$dataProvider,
			'myip'=>$myip,
			'storecode'=>$storecode,
			'storename'=>$storename,
			'itemtype_1'=>$itemtype_1,
			'itemtype_2'=>$itemtype_2,
			'itemtype_3'=>$itemtype_3,
			'itemtype_4'=>$itemtype_4,

		));
	}
	//服務匯出-----------------
	public function actiontbsServiceStExport()
	{
		$model=new TbsServiceTest;
		$dataProvider=new CActiveDataProvider('TbsServiceTest');
		$myip = '127.0.0.1';
		$storecode = '001001';
		$storename = 'test123';

		$col    = array();
		//欄位顯示的中文字,'ano'=>'調撥單號'
		$title  = array();
		//負責儲存輸出在畫面上的陣列
		$colAry = array();             
		$fileName = '';
		//撈版本
		$versionAry = array();
		$versionSql = "SELECT version FROM tbs_service_test WHERE storeCode = $storecode GROUP BY version ORDER BY version DESC";
		$versionResult = Yii::app()->db->createCommand($versionSql)->queryAll();
		for ($i = 0; $i < count($versionResult); $i++) {
			$version = $versionResult[$i]['version'];
			$versionAry[$version] = $version;} 

		$freeWashNo = array("A0101","A0201","A0301","A0401","Z0201");	//含沖洗項目.備註用
		$chargeWashNo = array("A0100","A0200","A0300","A0400","Z0200"); 	//不含沖洗項目.備註用


			if(isset($_POST['export'])) 
			{  
				$qryversion = $_POST['qryversion'];
				$storecode = $_POST['storecode'];
				$col    = array();
				$title  = array();
				$colAry = array();             
				$fileName = '';
	
				if($qryversion!="" &&  $storecode!="")
					{

					$sql = "SELECT * FROM tbs_service_test WHERE version = $qryversion and storeCode = $storecode ORDER BY sv_type,sv_order";	//型態排序 >> 項目排序

						$allocAry = yii::app()->db->createcommand($sql)->queryAll(); //saletime用
						$exportServiceAry = yii::app()->db->createcommand($sql)->queryAll(); //service用
						// CVarDumper::dump($exportServiceAry,10,true);
						$col = $this->getcol();
						$sv_col = $this->getsv_col();
						$title = $this->getTitle(TRUE);
						$sv_title = $this->getsv_title(TRUE);
						$countrow = count($allocAry)-1;
						
						// $serviceTime = $this->getServiceTime();

						if($countrow>0)
							{

							for($i=0; $i<count($allocAry); $i+=1) {
									if($allocAry[$i]['sv_time'] == '1') //全天
									{
									$serviceTime = $this->getAlldayServiceTime();
									}
									if($allocAry[$i]['sv_time'] == '2') //離峰
									{
									$serviceTime = $this->getOutBusyServiceTime();
									}
									if($allocAry[$i]['sv_time'] == '3') //尖峰
									{
									$serviceTime = $this->getInBusyServiceTime();
									}
						
								for($j=1; $j<=count($serviceTime); $j+=1) {
									$key = $allocAry[$i]['sale_no'].$j;//A01001

									$ary[$key]['version'] = $allocAry[$i]['version'];
									$ary[$key]['sale_no'] = $allocAry[$i]['sale_no'];
									$ary[$key]['sale_name'] = $allocAry[$i]['sale_name'];
									$ary[$key]['date'] = '';
									$ary[$key]['week'] = $j;
									$ary[$key]['times'] = $serviceTime[$j]['times'];
									$ary[$key]['timee'] = $serviceTime[$j]['timee'];

										if(in_array($ary[$key]['sale_no'],$freeWashNo)  && $ary[$key]['week'] <=5)
										{
											$memoinfo = '離峰剪髮送沖洗';
										}	
										else if(in_array($ary[$key]['sale_no'],$freeWashNo) && $ary[$key]['week'] >5 && $ary[$key]['week']<=7)
										{
											$memoinfo = '六日不含沖洗';
										}
										else if(in_array($ary[$key]['sale_no'],$chargeWashNo) && $ary[$key]['week'] <=5 && $ary[$key]['sv_time'] ==3)
										{
											$memoinfo = '尖峰不含沖洗';
										}
										else
											$memoinfo = '';
									$ary[$key]['memo'] =$memoinfo;	
								}
							}
							

							$fileName = $this->exportEXCEL($qryversion,$col,$title,$ary);  
							$saletimeclickUrl =  "<a href='".Yii::app()->request->baseUrl. '/' . "tmp" . '/' .$fileName. "'>Saletime下載</a>";
							$fileServiceName = $this->exportService($qryversion,$sv_col,$sv_title,$exportServiceAry);  
							$serviceclickUrl =  "<a href='".Yii::app()->request->baseUrl. '/' . "tmp" . '/' .$fileServiceName. "'>Service下載</a>";
							Yii::app()->user->setFlash('success','匯出成功，版本('.$qryversion.')<br>'.$saletimeclickUrl.'<br>'.$serviceclickUrl);   
							}
						
					else
						yii::app()->user->setFlash('error','查無資料,匯出失敗');
					}
					// CVarDumper::dump($allocAry,10,true);
					// CVarDumper::dump($serviceTime,10,true);
				}

		$this->render('tbsServiceStExport',array(
			'dataProvider'=>$dataProvider,
			'myip'=>$myip,
			'storecode'=>$storecode,
			'storename'=>$storename,
			'versionAry'=>$versionAry,


		));
	}

	//服務修改---------------------
	public function actiontbsServiceStUpdate()
	{
		$model=new TbsServiceTest;
		$dataProvider=new CActiveDataProvider('TbsServiceTest');
		$myip = '127.0.0.1';
		$storecode = '001001';
		$storename = 'test123';

		$qryversion = $_POST['qryversion'];

		$versionAry = array();
		$versionSql = "SELECT version FROM tbs_service_test WHERE storeCode = $storecode GROUP BY version ORDER BY version DESC";
		$versionResult = Yii::app()->db->createCommand($versionSql)->queryAll();
		for ($i = 0; $i < count($versionResult); $i++) {
			$version = $versionResult[$i]['version'];
			$versionAry[$version] = $version;} 

		if(isset($_POST['search']))
		{	
		$sql = "SELECT * FROM tbs_service_test WHERE version = $qryversion and storeCode = $storecode";
			//view顯示項目內容
			$itemsql_1 = "SELECT * FROM tbs_service_item WHERE type = 1";
			$itemtype_1 = Yii::app()->db->createCommand($itemsql_1)->queryAll();
			$itemsql_2 = "SELECT * FROM tbs_service_item WHERE type = 2";
			$itemtype_2 = Yii::app()->db->createCommand($itemsql_2)->queryAll();
			$itemsql_3 = "SELECT * FROM tbs_service_item WHERE type = 3";
			$itemtype_3 = Yii::app()->db->createCommand($itemsql_3)->queryAll();
			$itemsql_4 = "SELECT * FROM tbs_service_item WHERE type = 4";
			$itemtype_4 = Yii::app()->db->createCommand($itemsql_4)->queryAll();
			//全項目sale_no陣列
			$type1Ary = array();
			$type2Ary = array();
			$type3Ary = array();
			$type4Ary = array();
				//將SQL的值丟入全項目sale_no陣列
				foreach($itemtype_1 as $it1)
				{
					array_push($type1Ary,$it1['sale_no']);
				}
				foreach($itemtype_2 as $it2)
				{
					array_push($type2Ary,$it2['sale_no']);
				}
				foreach($itemtype_3 as $it3)
				{
					array_push($type3Ary,$it3['sale_no']);
				}
				foreach($itemtype_4 as $it4)
				{
					array_push($type4Ary,$it4['sale_no']);
				}
				if($storecode != null)
				{
				$sqlResult = Yii::app()->db->createCommand($sql)->queryAll();
	
					if($sqlResult == null)
					{
						yii::app()->user->setFlash('error',$storename.'<br>版本編號'.$qryversion.'查無資料');
					}
					if($sqlResult != null)
					{
					//項目值帶回欄位
					for($i=0;$i<count($sqlResult);$i++)
					{
							if(in_array($sqlResult[$i]['sale_no'],$type1Ary))
							{
								$svt1[$sqlResult[$i]['sale_no']] = $sqlResult[$i]['sv_time'];
								$checked1[$sqlResult[$i]['sale_no']] = TRUE;
								// CVarDumper::dump($svt1,10,true);
							}
							if(in_array($sqlResult[$i]['sale_no'],$type2Ary))
							{
								$svt2[$sqlResult[$i]['sale_no']] = $sqlResult[$i]['sv_time'];
								$checked2[$sqlResult[$i]['sale_no']] = TRUE;
							}
							if(in_array($sqlResult[$i]['sale_no'],$type3Ary))
							{
								$svt3[$sqlResult[$i]['sale_no']] = $sqlResult[$i]['sv_time'];
								$checked3[$sqlResult[$i]['sale_no']] = TRUE;
							}
							if(in_array($sqlResult[$i]['sale_no'],$type4Ary))
							{
								$svt4[$sqlResult[$i]['sale_no']] = $sqlResult[$i]['sv_time'];
								$checked4[$sqlResult[$i]['sale_no']] = TRUE;
							}
					}
					}
				}
				else
					yii::app()->user->setFlash('notice','請選擇門市資料');


		}
		//-----------------更新用

		$tranSvTimeName = array();
		$serviceitem = 0;

		if(isset($_POST['update'])) //更新
		{
			$tranSvTimeName = $this->getSvTimeName();	//轉換sv_time
			
			//先清空舊資料再新增新資料
			$renewsql = "DELETE FROM tbs_service_test WHERE version = $qryversion"; 
			Yii::app()->db->createCommand($renewsql)->execute();

			$inssql = "INSERT INTO tbs_service_test (version,storeCode,storeName,brand,sale_no,sale_name,sv_time,sv_timename,sv_type,sv_order,num,perform_mapping,price,ip)
					VALUE ";
				for($i=0;$i<=100;$i++)
				{
					if($_POST['sv_1_'.$i]!=null)
					{
						$sv1_post = $_POST['sv_1_'.$i];
						$svt1_post = $_POST['svt_1_'.$i];
						//對照項目用
						$item1_Allsql = "SELECT * FROM tbs_service_item WHERE sale_no = '$sv1_post'";
						$item1_Allresult = Yii::app()->db->createCommand($item1_Allsql)->queryAll();
						//SLQ
						$inssql .= "('$qryversion','$storecode','$storename','$brand','$sv1_post','".$item1_Allresult[0]['sale_name']."','$svt1_post','$tranSvTimeName[$svt1_post]','".$item1_Allresult[0]['type']."','".$item1_Allresult[0]['sv_order']."','".$item1_Allresult[0]['num']."','".$item1_Allresult[0]['perform_mapping']."','".$item1_Allresult[0]['price']."','$myip'),";
						$serviceitem++;
					}
					if($_POST['sv_2_'.$i]!=null)
					{
						$sv2_post = $_POST['sv_2_'.$i];
						$svt2_post = $_POST['svt_2_'.$i];
						//對照項目用
						$item2_Allsql = "SELECT * FROM tbs_service_item WHERE sale_no = '$sv2_post'";
						$item2_Allresult = Yii::app()->db->createCommand($item2_Allsql)->queryAll();
						//SQL
						$inssql .= "('$qryversion','$storecode','$storename','$brand','$sv2_post','".$item2_Allresult[0]['sale_name']."','$svt2_post','$tranSvTimeName[$svt2_post]','".$item2_Allresult[0]['type']."','".$item2_Allresult[0]['sv_order']."','".$item2_Allresult[0]['num']."','".$item2_Allresult[0]['perform_mapping']."','".$item2_Allresult[0]['price']."','$myip'),";
						$serviceitem++;
					}
					if($_POST['sv_3_'.$i]!=null)
					{
						$sv3_post = $_POST['sv_3_'.$i];
						$svt3_post = $_POST['svt_3_'.$i];
						//對照項目用
						$item3_Allsql = "SELECT * FROM tbs_service_item WHERE sale_no = '$sv3_post'";
						$item3_Allresult = Yii::app()->db->createCommand($item3_Allsql)->queryAll();
						//SQL
						$inssql .= "('$qryversion','$storecode','$storename','$brand','$sv3_post','".$item3_Allresult[0]['sale_name']."','$svt3_post','$tranSvTimeName[$svt3_post]','".$item3_Allresult[0]['type']."','".$item3_Allresult[0]['sv_order']."','".$item3_Allresult[0]['num']."','".$item3_Allresult[0]['perform_mapping']."','".$item3_Allresult[0]['price']."','$myip'),";
						$serviceitem++;
					}
					if($_POST['sv_4_'.$i]!=null)
					{
						$sv4_post = $_POST['sv_4_'.$i];
						$svt4_post = $_POST['svt_4_'.$i];
						//對照項目用
						$item4_Allsql = "SELECT * FROM tbs_service_item WHERE sale_no = '$sv4_post'";
						$item4_Allresult = Yii::app()->db->createCommand($item4_Allsql)->queryAll();
						//SQL
						$inssql .= "('$qryversion','$storecode','$storename','$brand','$sv4_post','".$item4_Allresult[0]['sale_name']."','$svt4_post','$tranSvTimeName[$svt4_post]','".$item4_Allresult[0]['type']."','".$item4_Allresult[0]['sv_order']."','".$item4_Allresult[0]['num']."','".$item4_Allresult[0]['perform_mapping']."','".$item4_Allresult[0]['price']."','$myip'),";
						$serviceitem++;
					}
				}

		
		if($serviceitem>0)
		{
			$insresult = substr($inssql,0,-1); //0->抓全部 ,-1 ->去掉最後一個字元(,)
			// var_dump($insresult);
			Yii::app()->db->createCommand($insresult)->execute();
			yii::app()->user->setFlash('success','更新成功!'.$serviceitem.'筆資料');
		}
			else
			{
				yii::app()->user->setFlash('error','更新失敗');
			}
		}

		$this->render('tbsServiceStUpdate',array(
			'dataProvider'=>$dataProvider,
			'myip'=>$myip,
			'storecode'=>$storecode,
			'storename'=>$storename,
			'versionAry'=>$versionAry,
			'qryversion'=>$qryversion,
			'sqlResult'=>$sqlResult,
			'svt1'=>$svt1,
			'svt2'=>$svt2,
			'svt3'=>$svt3,
			'svt4'=>$svt4,
			'checked1'=>$checked1,
			'checked2'=>$checked2,
			'checked3'=>$checked3,
			'checked4'=>$checked4,
			'itemtype_1'=>$itemtype_1,
			'itemtype_2'=>$itemtype_2,
			'itemtype_3'=>$itemtype_3,
			'itemtype_4'=>$itemtype_4,
		));
	}
	//服務排序---------------------------------------
	public function actiontbsServiceStSortable()
	{
		$myip = '127.0.0.1';
		$storecode = '001001';
		$storename = 'test123';
		
		$versionAry = array();
		$versionSql = "SELECT version FROM tbs_service_test WHERE storeCode = $storecode GROUP BY version ORDER BY version DESC";
		$versionResult = Yii::app()->db->createCommand($versionSql)->queryAll();
		for ($i = 0; $i < count($versionResult); $i++) {
			$version = $versionResult[$i]['version'];
			$versionAry[$version] = $version;} 

		$qryversion = $_POST['qryversion'];
		$sv_sortable = $_POST['sv_sortable'];
		$itemid = $_POST['itemid'];

		//顯示排列-------------
		if(isset($_POST['sv_submit']))
		{
			$sortsql = "SELECT * FROM tbs_service_test WHERE version = $qryversion and storeCode = $storecode and sv_type =1 ORDER BY sv_order";
			$sortAry = yii::app()->db->createcommand($sortsql)->queryAll();
			// CVarDumper::dump($sortAry,10,true);
		}
		if(isset($_POST['pd_submit']))
		{
			$sortsql = "SELECT * FROM tbs_service_test WHERE version = $qryversion and storeCode = $storecode and sv_type =2 ORDER BY sv_order";
			$sortAry = yii::app()->db->createcommand($sortsql)->queryAll();
		}
		if(isset($_POST['ev_submit']))
		{
			$sortsql = "SELECT * FROM tbs_service_test WHERE version = $qryversion and storeCode = $storecode and sv_type =3 ORDER BY sv_order";
			$sortAry = yii::app()->db->createcommand($sortsql)->queryAll();
		}
		if(isset($_POST['ot_submit']))
		{
			$sortsql = "SELECT * FROM tbs_service_test WHERE version = $qryversion and storeCode = $storecode and sv_type =4 ORDER BY sv_order";
			$sortAry = yii::app()->db->createcommand($sortsql)->queryAll();
		}
		//確定排列----------
		if(isset($_POST['sv_save']))
		{
			$test = explode(',',$_POST['txt']);
			// CVarDumper::dump($test,10,true);
			// CVarDumper::dump(count($test));
			if(count($test)>1)
				{
					for($i = 0;$i<count($test);$i++)
					{
						$updatesql = "UPDATE tbs_service_test SET sv_order = $i WHERE id = $test[$i]";
						Yii::app()->db->createCommand($updatesql)->execute();
					}
					yii::app()->user->setFlash('success','更新成功!');
				}
			else
				yii::app()->user->setFlash('error','更新失敗!');
			// CVarDumper::dump($updatesql);

		}



		$this->render('tbsServiceStSortable',array(
			'dataProvider'=>$dataProvider,
			'myip'=>$myip,
			'storecode'=>$storecode,
			'storename'=>$storename,
			'versionAry'=>$versionAry,
			'qryversion'=>$qryversion,
			'sortAry'=>$sortAry,
			'versionSql'=>$versionSql,
		));
	}

//----------以下private function--------------

	private function exportEXCEL($qryversion,$col, $title, $allocAry) //saletime
	{        
		// PHP EXCEL 初始化
		XPHPExcel::init();
		$fileTitle = "JIT $qryversion Export File";
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("JIT")
										->setLastModifiedBy("JIT")
										->setTitle($fileTitle)
										->setSubject("")
										->setDescription($fileTitle)
										->setKeywords("office 2007 openxml php")
										->setCategory("Export File");

		// 第一列 填入標題，由第0欄開始
		$column = 0;            
		for ($i = 0; $i < count($col); $i++) {

			if(isset($title[$col[$i]])) {

				$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($column,1, (isset($title[$col[$i]]))?
				$title[$col[$i]]:'',PHPExcel_Cell_DataType::TYPE_STRING);
				$column++;
			}
		}            
		// 由第2列開始
		$row = 2;

		foreach($allocAry as $row1){
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(0,$row, $row1['version']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(1,$row, $row1['sale_no']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(2,$row, $row1['sale_name']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(3,$row, $row1['date']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(4,$row, $row1['week']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(5,$row, $row1['times']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(6,$row, $row1['timee']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(7,$row, $row1['memo']);
			$row++;	
		}


		//sheet 表名稱
		$objPHPExcel->getActiveSheet()->setTitle($qryversion.'saletime');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a web browser (Excel5)
		$webroot = Yii::getPathOfAlias('webroot');
		//$fileName =$excelname.'-'.time().'.xls';
		$fileName = $qryversion.'saletime'.'.csv';
		$filePath = $webroot . '/' . "tmp" . '/';
		$fileUrl = $filePath.$fileName;
		// If you're serving to IE over SSL, then the following may be needed
		// header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		// header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		// header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		// header ('Pragma: public'); // HTTP/1.0
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV')->setDelimiter(';');

		$objWriter->save($fileUrl);

		return $fileName;
	}
	private function exportService($qryversion,$sv_col, $sv_title, $exportServiceAry) //service
	{        
		// PHP EXCEL 初始化
		XPHPExcel::init();
		$fileServiceTitle = "JIT $qryversion Export File";
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("JIT")
										->setLastModifiedBy("JIT")
										->setTitle($fileServiceTitle)
										->setSubject("")
										->setDescription($fileServiceTitle)
										->setKeywords("office 2007 openxml php")
										->setCategory("Export File");
		$column = 0;            
		for ($i = 0; $i < count($sv_col); $i++) {

			if(isset($sv_title[$sv_col[$i]])) {

				$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($column,1, (isset($sv_title[$sv_col[$i]]))?
				$sv_title[$sv_col[$i]]:'',PHPExcel_Cell_DataType::TYPE_STRING);
				$column++;
			}
		}            
		// 由第2列開始
		$row = 2;

		foreach($exportServiceAry as $row1){
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(0,$row, $row1['version']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(1,$row, $row1['storeCode']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(2,$row, $row1['storeName']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(3,$row, $row1['sale_no']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(4,$row, $row1['sale_name']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(5,$row, $row1['sv_type']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(6,$row, $row1['sv_order']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(7,$row, $row1['num']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(8,$row, $row1['price']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(9,$row, $row1['perform_mapping']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(10,$row, $row1['memo']);
			$row++;	
		}
		

		//sheet 表名稱
		$objPHPExcel->getActiveSheet()->setTitle($qryversion.'service');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a web browser (Excel5)
		$webroot = Yii::getPathOfAlias('webroot');
		//$fileName =$excelname.'-'.time().'.xls';
		$fileServiceName = $qryversion.'service'.'.csv';
		$fileServicePath = $webroot . '/' . "tmp" . '/';
		$fileServiceUrl = $fileServicePath.$fileServiceName;
		// If you're serving to IE over SSL, then the following may be needed
		// header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		// header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		// header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		// header ('Pragma: public'); // HTTP/1.0
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV')->setDelimiter(';');

		$objWriter->save($fileServiceUrl);

		return $fileServiceName;
	}                              
    public function getcol() 
	{
			$col = array(
				// 0 =>'id',
				0 =>'version',
				1 =>'sale_no',
				2 =>'sale_name',
				3 =>'date',
				4=>'week',
				5=>'times',
				6=>'timee',
				7=>'memo',
			);
			return $col; 
	}
	public function getsv_col() 
	{
			$sv_col = array(
				// 0 =>'id',
				0 =>'version',
				1 =>'storeCode',
				2 =>'storeName',
				3 =>'sale_no',
				4=>'sale_name',
				5=>'sv_type',
				6=>'sv_order',
				7=>'num',
				8=>'price',
				9=>'perform_mapping',
				10=>'memo',
			);
			return $sv_col; 
	}
	public function getTitle() 
	{
			$title = array(
				// 'id'=>'id',
				'version'=>'版本(改ID?)',
				'sale_no'=>'編號',
				'sale_name'=>'售票名稱',
				'date'=>'日期',
				'week'=>'星期',
				'times'=>'時間起',
				'timee'=>'時間迄',
				'memo'=>'備註',                     
			);
			return $title;
	}
	public function getsv_title() 
	{
			$sv_title = array(
				// 'id'=>'id',
				'version'=>'版號',
				'storeCode'=>'門市編號',
				'storeName'=>'門市名稱',
				'sale_no'=>'編號',
				'sale_name'=>'售票名稱',
				'sv_type'=>'類別',
				'sv_order'=>'排序',
				'num'=>'張數',
				'price'=>'金額',
				'perform_mapping'=>'業績對照',
				'memo'=>'備註',                   
			);
			return $sv_title;
	}
	//測試--------------------
	private function getServiceTime()
	{

			if($allAry['sv_time'] == 1)
			{
			$getSv_Time = array(
				'1'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
				'2'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
				'3'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
				'4'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
				'5'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
				'6'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
				'7'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
			);
			}
			//outbusy
			if($allAry['sv_time'] == 2)
			{
			$getSv_Time = array(
				'1'=> array(
					'times'=>'00:00:00',
					'timee'=>'16:59:59',
				),
				'2'=>array(
					'times'=>'00:00:00',
					'timee'=>'16:59:59',
				),
				'3'=>array(
					'times'=>'00:00:00',
					'timee'=>'16:59:59',
				),
				'4'=>array(
					'times'=>'00:00:00',
					'timee'=>'16:59:59',
				),
				'5'=>array(
					'times'=>'00:00:00',
					'timee'=>'16:59:59',
				),
				'6'=>array(
					'times'=>'00:00:00',
					'timee'=>'23:59:59',
				),
				'7'=>array(
					'times'=>'00:00:00',
					'timee'=>'23:59:59',
				),
			);
			}
			//inbusy
			if($allAry['sv_time']  == 3)
			{
			$getSv_Time = array(
				'1'=>array(
				'times'=>'17:00:00',
				'timee'=>'23:59:59',
				),
				'2'=>array(
				'times'=>'17:00:00',
				'timee'=>'23:59:59',
				),
				'3'=>array(
				'times'=>'17:00:00',
				'timee'=>'23:59:59',
				),
				'4'=>array(
				'times'=>'17:00:00',
				'timee'=>'23:59:59',
				),
				'5'=>array(
				'times'=>'17:00:00',
				'timee'=>'23:59:59',
				),
				'6'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
				'7'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
				),
			);
		}
		return $getSv_Time;
	}
	//-------------------------

	//ServiceTime----------------------------
	private function getAlldayServiceTime()
	{
		// CVarDumper::dump($allDayServiceTime,10,true);

		//全天		
		$allDayServiceTime = array(
			'1'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
			'2'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
			'3'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
			'4'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
			'5'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
			'6'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
			'7'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
		);
		
		return $allDayServiceTime;
	}
	private function getOutBusyServiceTime()
	{
		$outBusyServiceTime = array(
			'1'=> array(
				'times'=>'00:00:00',
				'timee'=>'16:59:59',
			),
			'2'=>array(
				'times'=>'00:00:00',
				'timee'=>'16:59:59',
			),
			'3'=>array(
				'times'=>'00:00:00',
				'timee'=>'16:59:59',
			),
			'4'=>array(
				'times'=>'00:00:00',
				'timee'=>'16:59:59',
			),
			'5'=>array(
				'times'=>'00:00:00',
				'timee'=>'16:59:59',
			),
			'6'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
			),
			'7'=>array(
				'times'=>'00:00:00',
				'timee'=>'23:59:59',
			),
		);
		return $outBusyServiceTime;
	}
	private function getInBusyServiceTime()
	{
		$inBusyServiceTime = array(
			'1'=>array(
			'times'=>'17:00:00',
			'timee'=>'23:59:59',
			),
			'2'=>array(
			'times'=>'17:00:00',
			'timee'=>'23:59:59',
			),
			'3'=>array(
			'times'=>'17:00:00',
			'timee'=>'23:59:59',
			),
			'4'=>array(
			'times'=>'17:00:00',
			'timee'=>'23:59:59',
			),
			'5'=>array(
			'times'=>'17:00:00',
			'timee'=>'23:59:59',
			),
			'6'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
			'7'=>array(
			'times'=>'00:00:00',
			'timee'=>'23:59:59',
			),
		);
		return $inBusyServiceTime;
	}
	//----------------------------------------
	private function getServiceName()
	{
		$servicename = array(
			'A0100'=>'剪髮100元',
			'A0101'=>'剪髮100元(含沖洗)',
			'A0200'=>'剪髮110元',
			'A0201'=>'剪髮110元(含沖洗)',
			'A0300'=>'剪髮80元',
			'A0301'=>'剪髮80元(含沖洗)',
			'A0400'=>'女剪150元',
			'A0401'=>'女剪150元(含沖洗)',
			'F0920'=>'頭皮舒壓130(限加價購)',
			'D000'=>'洗髮0元',
			'D0100'=>'洗髮10元',
			'D0200'=>'洗髮20元',
			'D0300'=>'女剪30元',
			'S0300'=>'優惠染髮500元',
			'B0100'=>'染髮500元',
			'N0310'=>'染膏200元',
			'C0100'=>'燙髮500元',
			'N0210'=>'燙劑200元',
			'N0110'=>'燙劑400元',
			'F0700'=>'頭皮舒壓150',
			'F0800'=>'頭皮隔離',
			'H0200'=>'刻線',
			'N0410'=>'深層潔淨洗髮精',
			'N0420'=>'茶樹SPA洗髮精',
			'N0430'=>'保溼修護洗髮精',
			'N0440'=>'茶樹SPA洗髮精(新)',
			'N0450'=>'薄荷深層洗髮精(新) ',
			'N0460'=>'舒活果漾洗髮精(新)',
			'N0510'=>'髮油',
			'N0520'=>'髮雕',
			'N0530'=>'髮蠟',
			'Z0200'=>'VIP券(剪髮100)',
			'Z0201'=>'VIP券(剪髮100)(含沖洗)',
		);
		return $servicename;
	}
	private function getSvTimeName()
	{
		$svtimename = array(
			'1'=>'全天',
			'2'=>'離峰',
			'3'=>'尖峰',
		);
		return $svtimename;
	}
	private function getServicePrice()
	{
		$serviceprice = array(
			'A0100'=>'100',
			'A0101'=>'100',
			'A0200'=>'110',
			'A0201'=>'110',
			'A0300'=>'80',
			'A0301'=>'80',
			'A0400'=>'150',
			'A0401'=>'150',
			'F0920'=>'130',
			'D000'=>'0',
			'D0100'=>'10',
			'D0200'=>'20',
			'D0300'=>'30',
			'S0300'=>'500',
			'B0100'=>'500',
			'N0310'=>'200',
			'C0100'=>'500',
			'N0210'=>'200',
			'N0110'=>'400',
			'F0700'=>'150',
			'F0800'=>'200',
			'H0200'=>'50',
			'N0410'=>'100',
			'N0420'=>'100',
			'N0430'=>'100',
			'N0440'=>'120',
			'N0450'=>'120',
			'N0460'=>'120',
			'N0510'=>'200',
			'N0520'=>'100',
			'N0530'=>'200',
			'Z0200'=>'0',
			'Z0201'=>'0',
		);
		return $serviceprice;
	}
	private function getServiceType()
	{
		$servicetype = array(
			'A0100'=>'1',
			'A0101'=>'1',
			'A0200'=>'1',
			'A0201'=>'1',
			'A0300'=>'1',
			'A0301'=>'1',
			'A0400'=>'1',
			'A0401'=>'1',
			'F0920'=>'1',
			'D000'=>'1',
			'D0100'=>'1',
			'D0200'=>'1',
			'D0300'=>'1',
			'S0300'=>'1',
			'B0100'=>'1',
			'N0310'=>'1',
			'C0100'=>'1',
			'N0210'=>'1',
			'N0110'=>'1',
			'F0700'=>'1',
			'F0800'=>'1',
			'H0200'=>'1',
			'N0410'=>'2',
			'N0420'=>'2',
			'N0430'=>'2',
			'N0440'=>'2',
			'N0450'=>'2',
			'N0460'=>'2',
			'N0510'=>'2',
			'N0520'=>'2',
			'N0530'=>'2',
			'Z0200'=>'3',
			'Z0201'=>'3',

		);
		return $servicetype;
	}
	private function getServiceOrder() //預設order
	{
		$serviceorder = array(
			'A0100'=>'1',
			'A0101'=>'2',
			'A0200'=>'3',
			'A0201'=>'4',
			'A0300'=>'5',
			'A0301'=>'6',
			'A0400'=>'7',
			'A0401'=>'8',
			'F0920'=>'9',
			'D000'=>'10',
			'D0100'=>'11',
			'D0200'=>'12',
			'D0300'=>'13',
			'S0300'=>'14',
			'B0100'=>'15',
			'N0310'=>'16',
			'C0100'=>'17',
			'N0210'=>'18',
			'N0110'=>'19',
			'F0700'=>'20',
			'F0800'=>'21',
			'H0200'=>'22',
			'N0410'=>'1',
			'N0420'=>'2',
			'N0430'=>'3',
			'N0440'=>'4',
			'N0450'=>'5',
			'N0460'=>'6',
			'N0510'=>'7',
			'N0520'=>'8',
			'N0530'=>'9',
			'Z0200'=>'1',
			'Z0201'=>'2',
		);
		return $serviceorder;
	}
	
}
