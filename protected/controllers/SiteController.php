<?php

use Chinwei6\LinePay;

class SiteController extends Controller
{
	

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**F
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function transTest()
	{
		$userQRsel ="SELECT * FROM userqr WHERE status = '' ";
		$userQRary = Yii::app()->db->createCommand($userQRsel)->queryAll();
		if(count($userQRary)>0)
		{
			// CVarDumper::dump($userQRary,10,true);
			//紀錄轉了那些票卷，用於更新狀態用
			$titAry = "";
			for($i=0;$i<count($userQRary);$i++)
			{
				$titAry .= "'".$userQRary[$i]['barcode']."'".",";
			}
			$titAry = substr($titAry,0,-1);
			//----------------------------------------------------------------------------------------
			$data = $userQRary;
			
			$data_string = json_encode($data);
			// CVarDumper::dump($data,10,true);

			$ch = curl_init("http://sensor.jithouse.com/userqr");
			//$ch = curl_init("http://127.0.0.1/sensor/fw/page/site/user.php");
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
			);
			//die();
			$result = curl_exec($ch);
				// CVarDumper::dump($result,10,true);
			
			//結束後將傳送的票眷設為以傳送 status = 1
			$userQRupdate ="UPDATE userqr SET status = '1' WHERE barcode in($titAry) ";
			Yii::app()->db->createCommand($userQRupdate)->execute();
			}
			// 發送訊息
				$date = date('Ymd');
				$msg = $date."QR拋送資料 共計: ".count($userQRary)." 筆 ";
				echo $msg;
				$auth = 'yjtdD9vd1KLwlI42bsEMFriiIZvfebliZCOGdqnpBjr';
				$this->lineNotify($auth,$msg);
	}
	public function actionIndex()
	{

		$msg = "測試";
		
		if(isset($_POST['txtup']))
		{
			if(isset($_FILES['nccctxt']))
			{
				
				// 檔案名稱
				$filename = $_FILES['nccctxt']['name'];
				// 檔案暫存路徑
				$tmp_name =  $_FILES["nccctxt"]["tmp_name"] ;
					echo $filename;
					echo $tmp_name;
					echo "<br>";
				// 讀取檔案arry
				$file_ary = file($tmp_name);

				//檔案資料----
				
				$fileTitle = explode('.',$filename);

				//取得特店資料
				$easyAccount = $this->getEasyAccount();
				CVarDumper::dump($easyAccount,10,true);

				// echo "  收件日期:".$fileTitle[2];
				echo "  檔案日期:".$fileTitle[2];
				echo "  檔案特店待號:".$fileTitle[1];
					$transTotla = count($file_ary)-1;
				echo "  交易總金額:".substr($file_ary[$transTotla],10,16)*1;
				echo "<br>";

				// 0行為檔案標題 從1開始，去掉最後一筆
				for($i=1;$i<count($file_ary)-1;$i++)
				{
					$transCode = substr($file_ary[$i],78,6);
					//
					if($transCode =='811599')
					{
						// $ncname = $this->ncccStorename();
						echo "日期時間:".substr($file_ary[$i],406,15)."  ";
						echo "交易處理代碼:".substr($file_ary[$i],78,6);
						echo "特店待號:"."0".(substr($file_ary[$i],367,9)*1)."  "; //靠左補0
						echo "卡號:"."0".(substr($file_ary[$i],3,17)*1)."  ";//靠左補0
						echo "消費金額:".(substr($file_ary[$i],62,8)*1)."  " ;
						echo "消費前餘額:".(substr($file_ary[$i],46,8)*1);
						echo "消費後餘額:".(substr($file_ary[$i],70,8)*1);
						echo "<br>";
					}
				}
			}
		}
		//解壓縮檔案
		$zip = new ZipArchive;
		if(isset($_POST['fileUnzip']))
		{
			$f_path = "/attachment";
			
			$f_total = count($_FILES['zipfile']['name']);
			echo $f_total;
			CVarDumper::dump($_FILES,10,true);
			for($i=0;$i<$f_total;$i++)
			{
				echo $_FILES['zipfile']['name'][$i];
				// if(isset($_FILES['zipfile']))
				// 	{
				// 		$filename = $_FILES['zipfile']['name'][$i];
				// 		$fileTmp = $_FILES['zipfile']['tmp_name'][$i];
				// 		if($zip->open($fileTmp) === TRUE ){
				// 			$zip->setPassword('2991');
				// 			$zip->extractTo('./tmp/');
				// 			$zip->close();
				// 			echo "success!";
				// 		}
				// 		else
				// 			echo "Error";

				// 		echo $filename." ".$fileTmp."<br>";
				// 	}
			}

		}
		//讀取 ZIP
		if(isset($_POST['testZip']))
		{
			//讀取path底下的zip檔案，並解壓縮
			$files = glob("attachment/*.zip"); 
			for($i=0;$i<count($files);$i++)
			{
				if($zip->open("$files[$i]") === TRUE)
				{
					$zipFileName = explode("(NCCC)",$files[$i]); //檔案分解命名
					$zipNcccCode = substr("$zipFileName[1]",0,10); //取得檔案特店代號
						//取得解壓縮密碼 帳號末四碼
						$ncccPW = $getNcccCode[$zipNcccCode]['account'];
						$ncccPW = substr("$ncccPW",-4); 
					// CVarDumper::dump($getNcccCode,10,true);
					echo $zipNcccCode.$getNcccCode[$zipNcccCode]['storename']."密碼:".$ncccPW."<br>";
					$zip->setPassword("$ncccPW");
					$zip->extractTo('./tmp/');
					$zip->close();
					echo "success!";
				}
				else
					echo "Error";
			}
			//讀取解壓縮完後的檔案txt
			$txtFiles = glob("tmp/D*.txt"); //讀D資料(每日交易明細)
			for($i=0;$i<count($txtFiles);$i++)
			{
				//獲取檔名
				$txtFileName = explode("/",$txtFiles[$i]);
				$txtAry = file($txtFiles[$i]); //將檔案內容轉為array
				for($j=8;$j<count($txtAry);$j++)
				{
					//取得該檔案特店代號
						$txtNcccCodeAry = explode(" ",$txtAry[3]);
						$txtNcccCode = substr("$txtNcccCodeAry[1]",1);
					//取得特店代號資料 DB:tbs_fb_account 
						$storecode = $getNcccCode[$txtNcccCode]['storecode'];
						$storename = $getNcccCode[$txtNcccCode]['storename'];
					//拆解每筆交易明細
						$txtChargeAry = explode(" ",$txtAry[$j]);
						if(count($txtChargeAry) == 29) //資料拆解後ary數量，一般交易為29ㄍ，退刷為36ㄍ
						{
							$txtMcode = $txtChargeAry[0]; //端末機號
							$txtCardNum = $txtChargeAry[10]; //交易卡號
							$txtPdate = "20".$txtChargeAry[15]; //交易日期
							$txtPrice = $txtChargeAry[20]*1; //交易金額
							// CVarDumper::dump($txtChargeAry,10,true);
							// echo "【".$storecode."】"."【".$storename."】".$txtAry[$j]."<br>";
							echo "【".$storecode."】"."【".$storename."】"."【".$txtNcccCode."】"."【".$txtMcode."】"."【".$txtCardNum."】"."【".$txtPdate."】"."【".$txtPrice."】<br>";
						}
				}
			
			
			}
			//執行完畢後刪除檔案
			foreach($files as $delzip)
			{
				unlink($delzip);
			}
			$txtDelete = glob("tmp/*.txt"); 
			foreach($txtDelete as $deltxt)
			{
				unlink($deltxt);
			}

		}
		$sql = "SELECT VERSION() as version";
		$mysqlqry = Yii::app()->localEIP->createCommand($sql)->queryAll();
		$mysqlVersion = isset($mysqlqry[0]['version'])?$mysqlqry[0]['version']:"查無資料";
		$this->render('index',array(
			'msg'=>$msg,
			'mysqlVersion'=>$mysqlVersion,
		));
			
	}
	//傳送lineNotify 2020.06.10
	public function lineNotify($auth,$msg)
	{
		$headers = array(
			// 'Content-Type:ultipart/form-data',
			// 'Content-Type:application/json',
			// 'Method:POST',
			'Content-Type: application/x-www-form-urlencoded',
			'Authorization:Bearer '.$auth
		);
		$message = array(
			'message' => $msg,
		);
		$ch = curl_init();
		curl_setopt($ch , CURLOPT_URL , "https://notify-api.line.me/api/notify");
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($message));
		$result = curl_exec($ch);
		curl_close($ch);
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */

	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
			$model=new LoginForm;

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	// public function getStoreArray()
	// {
	// 	$stores = array();
	// 	$storescode = array();
	// 	$storeTable = array();

	// 	$storeTable = ImpExcel::model()->findAll;

	// 	foreach ($storeTable as $storeT)
	// 	{
	// 		$stores[$storeT->$storescode] = $storeT->store;
	// 	}

	// 	return array($stores);

	// }

	function exportEXCEL($qrydate,$col, $title, $shopAry)
	{        
			// PHP EXCEL 初始化
			XPHPExcel::init();
			$fileTitle = "JIT $qrydate Export File";
			$objPHPExcel= XPHPExcel::createPHPExcel();
			$objPHPExcel->getProperties()->setCreator("JIT")
										 ->setLastModifiedBy("JIT")
										 ->setTitle($fileTitle)
										 ->setSubject("")
										 ->setDescription($fileTitle)
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Export File");

			// 第一列 填入標題，由第0欄開始
			$column = 0;            
			for ($i = 0; $i < count($col); $i++) {

				if(isset($title[$col[$i]])) {

					$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($column,1, (isset($title[$col[$i]]))?
					$title[$col[$i]]:'',PHPExcel_Cell_DataType::TYPE_STRING);
					$column++;
				}
			}            
			// 由第2列開始
			$row = 2;
			for ($j = 0; $j < count($shopAry); $j++) {

				if(isset($shopAry[$j][$col[0]])) {
					// 第幾欄. 由第0欄開始
					$column = 0; 
					for ($i = 0; $i < count($col); $i++) {
						// 若符合篩選欄位. 才進行
						if(isset($title[$col[$i]])) {                   
							$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $row)->setValueExplicit((isset($shopAry[$j][$col[$i]]))?
							$shopAry[$j][$col[$i]]:'', PHPExcel_Cell_DataType::TYPE_STRING);

							$column++;
						}
					}
				$row++;
				}
			}
			// $row = $allrow;

			//sheet 表名稱
			$objPHPExcel->getActiveSheet()->setTitle($date_S.'~'.$date_S.'剪髮時間');
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			// Redirect output to a web browser (Excel5)
			$webroot = Yii::getPathOfAlias('webroot');
			//$fileName =$excelname.'-'.time().'.xls';
			$fileName = $date_S.'~'.$date_S.time().'.xls';
			$filePath = $webroot . '/' . "tmp" . '/';
			$fileUrl = $filePath.$fileName;
			// If you're serving to IE over SSL, then the following may be needed
			// header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			// header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			// header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			// header ('Pragma: public'); // HTTP/1.0
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save($fileUrl);
			return $fileName;
	}     
	
	public function getcol() 
	{
			$col = array(
				// 0 =>'id',
				0 =>'date',
				1 =>'shop_id',
				2 =>'sidnum',
				3 =>'service',
				4 =>'conti',
			);
			return $col; 
	}
	public function getTitle() 
	{
			$title = array(
				// 'id'=>'id',
				'date'=>'年月',
				'shop_id'=>'門市店編',
				'sidnum'=>'座位',
				'service'=>'服務',
				'conti'=>'服務時間',                      
			);
			return $title;
	}
	public function testfill($name,$year)
	{
		echo $name." and ".$year;
	}
	// 特店待號名稱
	public function ncccStorename()
	{
		$ncnum = array(
		'0219011261'=>'ＪＩＴ快速剪髮－高雄聯興店',
		'0219011423'=>'ＪＩＴ快速剪髮－高雄武廟店',
		'1319012415'=>'ＪＩＴ快速剪髮－中和安平',
		'1319012457'=>'ＪＩＴ快速剪髮－中和景平',
		'1419008305'=>'ＪＩＴ快速剪髮－楊梅新中山',
		'1319012460'=>'ＪＩＴ快速剪髮－中和景平二',
		'1319012485'=>'ＪＩＴ快速剪髮－中和福祥',
		'1319012749'=>'ＪＩＴ快速剪髮－中和更生',
		);
		return $ncnum;
	}
	//特店代號取得NCCC附件解壓縮密碼
	public function getNcccData()
	{
		$fb_sql = "SELECT * FROM tbs_fb_account ";
		$nccc_code_ary = Yii::app()->localEIP->createCommand($fb_sql)->queryAll();
		$nccc_code = array();
		foreach($nccc_code_ary as $nccc)
		{
			$nccc_code[$nccc['nccc_code']] = $nccc;
		}

		return $nccc_code;
	}
	//email轉換用
	public function convertEncoding($string){
		//根據系統進行配置
		$encode = stristr(PHP_OS, 'WIN') ? 'big5' : 'UTF-8';
		$string = iconv('UTF-8', $encode, $string);
		//$string = mb_convert_encoding($string, $encode, 'UTF-8');
		return $string;
	}
	//地圖用
	public function getMapCode()
	{
		$mapAry = array(
			'屏東縣'=>'tw-pt',
			'台南市'=>'tw-tn',
			'宜蘭縣'=>'tw-il',
			'嘉義縣'=>'tw-ch',
			'台東縣'=>'tw-tt',
			'澎湖縣'=>'tw-ph',
			'金門縣'=>'tw-km',
			'連江縣'=>'tw-lk',
			'台北市'=>'tw-tw',
			'嘉義市'=>'tw-cs',
			'台中市'=>'tw-th',
			'雲林縣'=>'tw-yl',
			'高雄市'=>'tw-kh',
			'新北市'=>'tw-tp',
			'新竹市'=>'tw-hs',
			'新竹縣'=>'tw-hh',
			'基隆市'=>'tw-cl',
			'苗栗縣'=>'tw-ml',
			'桃園市'=>'tw-ty',
			'彰化縣'=>'tw-cg',
			'花蓮縣'=>'tw-hl',
			'南投縣'=>'tw-nt'
		);
		return $mapAry;
	}
	//上傳youtube測試
	public function upLoadYoutube()
	{
	}
	//testPage
	public function actionFunctionPage()
	{
		if(isset($_POST['submit']))
		{
			$storecode = $_POST['storecode'];

			$sQLtes = "SELECT * FROM tit_saletime where storeCode = '$storecode' and sv_time ='1' ORDER BY sv_type,sv_order";
			// $sQLtes = "SELECT * FROM tit_saletime where storeCode = '$storecode' ORDER BY sv_type,sv_order";

				$sQLtesAry = Yii::app()->localEIP->createCommand($sQLtes)->queryAll();
				// CVarDumper::dump($sQLtesAry,10,true);
				$totalService = count($sQLtesAry);
				$jsonAry = array(); //組合用
				//title
				$jsonTitle = array(
					"type"=> "project",
					"entry"=> "$totalService",
				);
				//組合表頭
				$jsonAry = array_merge($jsonAry,$jsonTitle);
				//資料用
				$jsonAdd = array();
				for($i=0;$i<count($sQLtesAry);$i++)
				{
					$categorySET = '';
					$idN = $i+1;
					switch($sQLtesAry[$i]['sv_type'])
						{
							case 1:
								$categorySET = '服务';
								break;
							case 2:
								$categorySET = '產品';
								break;
							case 3:
								$categorySET = '赠品';
								break;
							case 4:
								$categorySET = '其他';
								break;
						}
					$jsonAdd = array(
						"projectID".$idN=> $sQLtesAry[$i]['sale_no'],
						"name".$idN=> $sQLtesAry[$i]['sale_name'],
						"abbreviation".$idN=> "null", //簡稱
						"category".$idN=> $categorySET,
						"order".$idN=> $sQLtesAry[$i]['sv_order'],
						"Number".$idN=> $sQLtesAry[$i]['num'],
						"money".$idN=> $sQLtesAry[$i]['price'],
						"achievement".$idN=> $sQLtesAry[$i]['perform_mapping'],
						"BAK".$idN=> "OK",
					);
					$jsonAry = array_merge($jsonAry,$jsonAdd);
				}
				$json = json_encode($jsonAry,JSON_UNESCAPED_UNICODE); //中文防亂碼
				// CVarDumper::dump($json,10,true);
				// echo $json;
				//檔案
					//路徑
					$jPath = Yii::getPathOfAlias('webroot').'/images/';	//儲存路徑(外)
					$jsonDownload = file_put_contents($jPath.$storecode."_serviceJson.json",$json);
					$downloadPath = $jPath."serviceJson.json";
				//-----------

				//decode
					// $storeJsonFile = file_get_contents($downloadPath);
					// $storeJsonAry = json_decode($storeJsonFile,true);
					// CVarDumper::dump($storeJsonAry,10,true);
				$this->redirect('images/'.$storecode.'_serviceJson.json');
				
			}

		$this->render('functionPage',array(
		));
	}
	//功能測試畫面
	public function actionTestPageX()
	{
		ini_set('memory_limit', '2048M'); //記憶體設定
		set_time_limit(0); //不timeOut

		//計時
		$time_start = microtime(true);
		// //取得區域資料
		// $areaList = $this->actionAjaxArea();
		//----

		//計算剪髮頻率------------------------------------------------
			//type1
			if(isset($_POST['sub3']))
			{
				//QR
				// $setFrequencySQL = "SELECT mobile,mid(barcodeQR,11,5) as pdate FROM `userqr`  WHERE mid(barcodeQR,8,8) between '20210501' and '20210731' and mid(barcodeQR,26,1) like 'A%' group by mobile,pdate";
				// 一維
				// $setFrequencySQL = "SELECT mobile,mid(barcode,4,5) as pdate,ctime,barcode FROM `user` WHERE mid(barcode,4,5) between '10501' and '10731' and mid(barcode,13,2) = '01' group by mobile,pdate ";
				//user_analyze
				$setFrequencySQL = "SELECT mobile,pdate FROM `user_analyze` WHERE pdate between '10101' and '11131' group by mobile,pdate ";

				$setFrequency = yii::app()->localDB->createCommand($setFrequencySQL)->queryAll();
				CVarDumper::dump(count($setFrequency),10,true); //總數
				// CVarDumper::dump($setFrequency[0],10,true);

				$frequencyAry = array(); //個別資料
				$frequencyTotal = array(); //來店次數統計分析
				// //間隔天數ary
				$frequencyDayAry = array();
				$frequencyDayTotalTimes = 0; //來店次數總計


				// 結算個別顧客 來店次數及日期
				foreach($setFrequency as $fre)
				{
					//建立 電話來店次數
					if(!isset($frequencyAry[$fre['mobile']]['times'])){
						$frequencyAry[$fre['mobile']]['times'] = 0;
						$frequencyAry[$fre['mobile']]['date'] = array();
					}
					$frequencyAry[$fre['mobile']]['times'] ++;
					//紀錄 來店消費日期
					array_push($frequencyAry[$fre['mobile']]['date'],"202".$fre['pdate']);
				}
				// CVarDumper::dump($frequencyAry,10,true);

				//計算期間來店次數統計
				foreach($frequencyAry as $f=>$fary)
				{
					//來店次數統計用--
					$frequencyTotal[$fary['times']] ++;

					//每位顧客【間隔天數】、【平均天數】-- 來2次以上才有間隔天數能計算
					if($fary['times']>=2 && $fary['times']<=52)
					{
						//日期倒續相減
						$dateReverse = array_reverse($fary['date']);
						$fDayAry = array(); //間隔天數ary
						//計算間隔天數
						foreach($dateReverse as $d=>$date)
						{
							//如果下一個日期存在，則相減取得間隔天數
							if(isset($dateReverse[$d+1]))
							{
								$fday = (strtotime($date) - strtotime($dateReverse[$d+1]))/86400; //(60*60*24)計算天數
								array_push($fDayAry,$fday);
								array_push($frequencyDayAry,$fday); //全部間隔天數
								$frequencyDayTotalTimes += $fary['times']-1; //全部來店次數 -1 (來2次才有1次間隔天數能計算)
							}
						}
						//間隔日期ary加入
						$frequencyAry[$f]['fdays'] = $fDayAry;
						//計算個人平均消費天數
						$fdayTotal = 0;
						foreach($frequencyAry[$f]['fdays'] as $fday)
						{
							$fdayTotal += $fday;
						}
						$frequencyAry[$f]['fdays_Avg'] = round($fdayTotal / count($frequencyAry[$f]['fdays']));
					}
				}
				// CVarDumper::dump($frequencyAry,10,true); //個別資料
				CVarDumper::dump($frequencyTotal,10,true); //來店次數統計分析

				//計算全部平均間隔日
				$fn = 0 ;
				$f_AvgCount = 0;
				foreach($frequencyAry as $fary)
				{
					if(isset($fary['fdays_Avg']))
					{
						$fn ++;
						$f_AvgCount += $fary['fdays_Avg'];
					}
				}
				$f_countAvg = round($f_AvgCount/$fn,2);
				CVarDumper::dump($f_countAvg,10,true); //全部平均

				//全部間隔天數中位數
				// frequencyDayAry
				$mediaTimes = $this->getMedian($frequencyDayAry);
				CVarDumper::dump($mediaTimes,10,true); //中位數
				// CVarDumper::dump($frequencyDayAry,10,true); //間隔天數ary

			}
			//type2 消費週期 >>天數間隔
			if(isset($_POST['sub4']))
			{
				//************************ */
				//T1 同期有來的電話名單 >> 以去年同期名單查詢今年度還有回鍋的顧客
				$userSql = "SELECT pdate,mobile FROM user_analyze WHERE pdate between '10801' and '11131' and mobile in(
					SELECT mobile FROM user_analyze where pdate between '00801' and '01131' GROUP BY mobile
					) GROUP BY pdate,mobile ";
				//T2 篩選日期分析
				$userSql1 = "SELECT pdate,mobile FROM user_analyze where pdate between '11001' and '11131' GROUP BY pdate,mobile";

				$userAry = yii::app()->localDB->createCommand($userSql1)->queryAll();
				$totalCome = count($userAry);
				CVarDumper::dump("來店總計筆數:".$totalCome,10,true);

				//建立來電名單資料
				$userList = array();
				//統計來店星期清單
				$comeWeekList = array();
				foreach($userAry as $user)
				{	
					$comeDate = '202'.$user['pdate']; //來店日期
					$comeWeek = date('w',strtotime($comeDate)); //來店星期

					//建立來店次數
					if(!isset($userList[$user['mobile']]['times']))
						$userList[$user['mobile']]['times'] = 0;
					$userList[$user['mobile']]['times'] ++;
					//來電日期加入array
					if(!isset($userList[$user['mobile']]['pdate']))
						$userList[$user['mobile']]['pdate'] = array();
					array_push($userList[$user['mobile']]['pdate'],$comeDate);
					//建立來店星期
					if(!isset($userList[$user['mobile']]['week']))
						$userList[$user['mobile']]['week'] = array();
					array_push($userList[$user['mobile']]['week'],$comeWeek);
					array_push($comeWeekList,$comeWeek);


				}
				unset($userAry);
				//記憶體監控
				$m1 = memory_get_usage();
				// CVarDumper::dump($comeWeekList,10,true);
				CVarDumper::dump("來店人數:".count($userList),10,true);

				//計算來店頻率與間隔日期
				$comeAgainAry = array();
				$comeAgain = 0;
				$totalGapDay = array(); //全部間隔天數
				$totalAvgDays = array();
				$totalTimes = 0; // 全部來店次數
				foreach($userList as $u=>$user)
				{
					//統計來店次數資料
					if(!isset($comeAgainAry[$user['times']]))
						$comeAgainAry[$user['times']] = 0;
					$comeAgainAry[$user['times']] ++;

					//來店次數>2次以上才進行計算 ，後者判斷合理次數範圍內
					if($user['times']>=2 && $user['times']<=20)
					{
						//重複來店人數
						$comeAgain ++;
						//間隔日array
						$gap = array();
						//個人間隔天數總計
						$pDays = 0;
						//反轉pdate日期進行相減，計算來店間隔時間
						$reverse = array_reverse($user['pdate']);
						foreach($reverse as $r=>$rev)
						{
							if(isset($reverse[$r+1])) //如果有下一次，則進行相減
							{
								$gapDay = (strtotime($rev) - strtotime($reverse[$r+1]))/86400;//(60*60*24)計算天數
								array_push($gap,$gapDay);//間隔天數加入
								$pDays +=$gapDay; //總計個人間隔天數
								array_push($totalGapDay,$gapDay); //全部間隔天數
								$totalTimes += ($user['times'] -1 ); //全部來店次數總計
							}
						}
						//個人 間隔天數丟回array
						$userList[$u]['gapDays'] = $gap;
						$userList[$u]['frequency'] = round($pDays/($user['times'] -1 ),2); //平均間隔天數
						array_push($totalAvgDays,$userList[$u]['frequency']);
						$userList[$u]['mid'] = $this->getMedian($gap); //間隔天數中位數

					}
				}
				CVarDumper::dump("篩選範圍人數:".$comeAgain,10,true);
				// CVarDumper::dump($userList,10,true);

				//總計
				$totalDays = 0 ;
				foreach($totalAvgDays as $avg)
				{
					$totalDays += $avg;
				}
				$totalAvgDay = round($totalDays/count($totalAvgDays),2);
				//全部間隔天數中位數
				$totalMidDay = $this->getMedian($totalGapDay); 
				CVarDumper::dump("總計間隔天數平均".$totalAvgDay,10,true);
				CVarDumper::dump("總計間隔天數中位".$totalMidDay,10,true);
				CVarDumper::dump($comeAgainAry,10,true); //來店次數統計

				// //來店星期分析
				// $comeWeekAry = array();
				// $weekName = $this->getWeek();
				// foreach($comeWeekList as $cw)
				// {
				// 	if(!isset($comeWeekAry[$cw]))
				// 		$comeWeekAry[$cw] = 0;
				// 	$comeWeekAry[$cw]++;
				// }
				// unset($comeWeekList);
				// foreach($comeWeekAry as $k=>$cwa)
				// {
				// 	if(!isset($comeWeekAry[$weekName[$k]]['percent']))
				// 		$comeWeekAry[$weekName[$k]]['num'] = $cwa;
				// 		$comeWeekAry[$weekName[$k]]['percent'] = round(($cwa / $totalCome)*100,2);
				// 	// CVarDumper::dump($k,10,true); 

				// }
				// CVarDumper::dump($comeWeekAry,10,true); //來星期統計



				//記憶體監控
				$m2 = memory_get_usage();

				CVarDumper::dump($m1,10,true);
				CVarDumper::dump($m2,10,true);




			}
			//type3 消費週期 >>週期間隔
			if(isset($_POST['sub5']))
			{
				$log = "";
				$data_log = "";
				//************************ */

				//T1 同期有來的電話名單 >> 以去年同期名單查詢今年度還有回鍋的顧客
				$userSql = "SELECT pdate,mobile FROM user_analyze WHERE pdate between '10801' and '11131' and mobile in(
					SELECT mobile FROM user_analyze where pdate between '00801' and '01131' GROUP BY mobile
					) GROUP BY pdate,mobile ";
				//T2 篩選日期分析
				$userSql1 = "SELECT pdate,mobile FROM user_analyze where pdate between '00101' and '00431' GROUP BY pdate,mobile";
				//3 特定電話
				$userSql2 = "SELECT pdate,mobile FROM user_analyze where pdate between '10101' and '11231' and mobile = '0910671159' 
				GROUP BY pdate,mobile";
				//4 北中南
				$userSql_N = "SELECT pdate,mobile FROM user_analyze where pdate between '00301' and '00431' and storecode in 
					('002043','002069','002074','002078','002112','002123','002176','002181','002038','002040','002042',
					'002072','002081','002088','002173','002191','002221','002041','002073','002080','002157','002161',
					'002218','J02001','J02007','J02008','002051','002061','002079','002085','002086','002064','002067',
					'002075','002107','002135','002160','002200','002224','003050','003054','003055','003133','003188',
					'003196','003212','003235','003236','003056','003066','003076','003122','003132','003149','003151',
					'003155','003226') 
				GROUP BY pdate,mobile";
				$userSql_M = "SELECT pdate,mobile FROM user_analyze where pdate between '00801' and '01231' and storecode in 
					('004024','004071','004094','004096','004126','004152','004185','004197','004198','004214','004215','004220',
					'004225','004057','004087','004127','004134','004137','004171','004183','004186','004192','004209','004228',
					'049165','004103','004116','005062','005084','005105','005146','005154','005195','006180','006234','006052',
					'006129','006172','006184','006201','006202','006219','006227','006229','006232','006239','006036','006045',
					'006117','006141','006144','006179','006216','006223','006238') 
				GROUP BY pdate,mobile";
				$userSql_S = "SELECT pdate,mobile FROM user_analyze where pdate between '00801' and '01231' and storecode in 
					('007053','007114','007121','007128','007170','007178','007182','007187','007203','007211','007213','007230',
					'007222','007001','007023','007032','007101','007130','007136','007140','007167','007231','007091','007147',
					'007156','007204','007207','007217','J07002','J07003','007002','007035','007109','007120','007131','007143',
					'007148','007166','P07003','007065','007089','007095','007150','007158','007168','007169','007199','007205',
					'007027','007046','007049','007058','007099','007100','007118','007139','007142','007159','007206','007210',
					'P07002','008083','008111','008145','008162','008177','008208','008233','008237','008240','089021','089163')
				GROUP BY pdate,mobile";

				//In mobileList Array
				$mobileList = "SELECT mobile FROM (SELECT mobile,count(*) as count FROM `user_analyze` WHERE pdate BETWEEN '80101' and '81231' GROUP BY mobile )as a WHERE count > 8";
				$userSql12 = "SELECT pdate,mobile FROM user_analyze where pdate between '10901' and '11231' and mobile in 
				($mobileList)
				GROUP BY pdate,mobile";

				$userAry = yii::app()->localDB->createCommand($userSql12)->queryAll();
				$totalCome = count($userAry);
				// CVarDumper::dump("來店總計筆數:".$totalCome,10,true);
				$log .= "'來店總計筆數','".$totalCome."',";

				//建立來電名單資料
				$userList = array();
				//統計來店星期清單
				$comeWeekList = array();

				//建立每隻電話個人來店資料
				foreach($userAry as $user)
				{	
					$comeDate = '202'.$user['pdate']; //來店日期
					$comeWeek = date('W',strtotime($comeDate)); //來店週期

					//建立來店次數
					if(!isset($userList[$user['mobile']]['times']))
						$userList[$user['mobile']]['times'] = 0;
					$userList[$user['mobile']]['times'] ++;
					//來電日期加入array
					if(!isset($userList[$user['mobile']]['pdate']))
						$userList[$user['mobile']]['pdate'] = array();
					array_push($userList[$user['mobile']]['pdate'],$comeDate);
					//來電週加入array
					if(!isset($userList[$user['mobile']]['pweek']))
						$userList[$user['mobile']]['pweek'] = array();
					array_push($userList[$user['mobile']]['pweek'],$comeWeek);
				}
				unset($userAry);
				//記憶體監控
				// $m1 = memory_get_usage();
				// CVarDumper::dump($comeWeekList,10,true);
				// CVarDumper::dump("來店人數:".count($userList),10,true);
				$log .= "'來店人數','".count($userList)."(".round(count($userList)/4,0).")',";
				// CVarDumper::dump($userList,10,true);

				//計算來店頻率與間隔週期
				$comeAgainAry = array();
				$comeAgain = 0;
				$totalGapWeek = array(); //全部間隔週數
				$totalAvgWeek = array();
				$comeWeekAry = array(); //間隔週數統計
				$totalTimes = 0; // 全部來店次數
				foreach($userList as $u=>$user)
				{
					//統計來店次數資料
					if(!isset($comeAgainAry[$user['times']]))
						$comeAgainAry[$user['times']] = 0;
					$comeAgainAry[$user['times']] ++;
					//統計來店週數資料
					// foreach($user['pweek'] as $week)
					// {
					// 	if(!isset($comeWeekAry[$week]))
					// 	{
					// 		$comeWeekAry[$week] = 0;
					// 	}
					// 	$comeWeekAry[$week] ++;
					// }

					//來店次數>2次以上才進行計算 ，後者判斷合理次數範圍內************************
					if($user['times']>=2 ) 
					{
						//重複來店人數
						$comeAgain ++;
						//間隔週array
						$gap = array();
						//個人間隔週數總計
						$pWeek = 0;
						//反轉pdate日期進行相減，計算來店間隔時間
						$reverse = array_reverse($user['pweek']);
						foreach($reverse as $r=>$rev)
						{
							if(isset($reverse[$r+1])) //如果有下一次，則進行相減
							{
								$gapWeek = $rev - $reverse[$r+1];
								//以防月初第一周為week53， 1-53 = -52 計算失準
								if($gapWeek < 0)
								{
									$gapWeek = 1;
								}
								array_push($gap,$gapWeek);//間隔週數加入
								$pWeek +=$gapWeek; //總計個人間隔週數
								array_push($totalGapWeek,$gapWeek); //全部間隔週數
								$totalTimes += ($user['times'] -1 ); //全部來店次數總計
							}
						}
						//個人 間隔天數丟回array
						$userList[$u]['gapWeek'] = $gap;
						$userList[$u]['frequency'] = round($pWeek/($user['times'] -1 ),4); //平均間隔週數
						array_push($totalAvgWeek,$userList[$u]['frequency']);
						// $userList[$u]['mid'] = $this->getMedian($gap); //間隔週數中位數
						// $userList[$u]['mode'] = $this->getModeNum($gap);//間隔眾數

					}
				}

				// CVarDumper::dump("篩選範圍人數:".$comeAgain,10,true);
				$log .= "'篩選範圍人數','".$comeAgain."(".round($comeAgain/4,0).")',";
				// CVarDumper::dump($userList,10,true);//印出個人資料

				// 紀錄個人資料LOG
				$log1 = "";
				foreach($userList as $u=>$user)
				{
					foreach($user['pdate'] as $p=>$pdate)
					{
						$log1 .= $u.",".$user['times'].",".$user['pdate'][$p].",".$user['pweek'][$p].",".$user['frequency'].",".$user['mid']."\n";
					}
				

				}
				$data_log = $userList;
				unset($userList);

				//總計
				$totalWeeks = 0 ;
				foreach($totalAvgWeek as $avg)
				{
					$totalWeeks += $avg;
				}
				$totalAvgWeekResult = round($totalWeeks/count($totalAvgWeek),2);
				//全部間隔週數中位數
				// $totalMidWeek = $this->getMedian($totalGapWeek); 
				// CVarDumper::dump("總計間隔周數平均".$totalAvgWeekResult,10,true);
				// CVarDumper::dump("總計間隔周數中位".$totalMidWeek,10,true);
				// CVarDumper::dump($comeAgainAry,10,true); //來店次數統計
				// CVarDumper::dump($comeWeekAry,10,true);//來電週數統計



				$log .= "'間隔周數平均','".$totalAvgWeekResult."'";
				$log = "\n".$log;
				//匯出LOG
				CVarDumper::dump($log,10,true);
				//路徑
				$jPath = Yii::getPathOfAlias('webroot').'/images/';	//儲存路徑(外)
				$jsonDownload = file_put_contents($jPath."dataLog.txt",$log1); //FILE_APPEND加入資料，非複寫
				$jsonDownload = file_put_contents($jPath."data.txt",$log,FILE_APPEND); //FILE_APPEND加入資料，非複寫
				// $downloadPath = $jPath."data.txt";

				//記憶體監控
				// $m2 = memory_get_usage();
				// CVarDumper::dump($m1,10,true);
				// CVarDumper::dump($m2,10,true);




			}
		//----------------------------
		//解析 富邦薪轉文件
			// $codeTest = 'C760012000000816800020749700000000000001478800N224181140';
			// $codeAry['1'] = substr($codeTest,0,1);
			// $codeAry['2'] = substr($codeTest,1,7);
			// $codeAry['3'] = substr($codeTest,8,5);
			// $codeAry['4'] = substr($codeTest,13,14);
			// $codeAry['5'] = substr($codeTest,27,6);
			// $codeAry['6'] = substr($codeTest,33,13);
			// $codeAry['7'] = substr($codeTest,46,10);

			// CVarDumper::dump($codeAry,10,true);
			// $testDate = '2021-12-06';
			// $testWeek = date('W',strtotime($testDate)); //來店週期

			// CVarDumper::dump($testWeek,10,true);
		// --
		//PDF
		if(isset($_POST['subPDF']))
		{
			$sql = "SELECT * FROM tbs_fb_account WHERE account != ''";
			$sqlAry = yii::app()->localDB->createCommand($sql)->queryAll();
			$mainAry = array();
			$maincontect = ''; //內容
			$n = 0; //截斷頁面紀錄用
			// ** html內容
			$cssHtml = '<style>
						.tddclass td{
							border: 1px solid #6caace;
							text-align: center;
						}
						.tddclass th {
							border: 1px solid #6caace;
							background-color: #FFFF66;
							text-align: center;
						}
						.divclass {
							position: absolute;
							width: 50%;
							bottom: 10px;
							text-align: right;
						}
					</style>';
			foreach($sqlAry as $k=>$s)
			{				
				if($n == 0)
					$maincontect .= '<h2 style="text-align:center">門市帳戶資料</h2><table class = "tddclass"><tr><th>序號</th><th>店編</th><th>門市</th><th>Nccc特店代號</th><th>富邦帳號</th></tr>';
				$maincontect .='<tr><td>'.($k+1).'</td><td>'.$s['storecode'].'</td><td>'.$s['storename'].'</td><td>'.$s['nccc_code'].'</td><td>'.$s['account'].'</td></tr>';
				$n++;
				if($n >10)
				{
					$maincontect .= '</table>';
					array_push($mainAry,$maincontect);
					$maincontect = '';
					$n = 0;
				}
			}
				// echo $maincontect;
				// CVarDumper::dump($mainAry,10,true);

				$pdf = $this->tcpdf_include($cssHtml,$mainAry);
			}
		$mainAry = array();
		//預覽PDF於modal上
		if(isset($_POST['previewPDFSsub']))
		{
			$sql = "SELECT * FROM tbs_fb_account WHERE account != ''";
			$sqlAry = yii::app()->localDB->createCommand($sql)->queryAll();
			$maincontect = ''; //內容
			$n = 0; //截斷頁面紀錄用
			// ** html內容
			$cssHtml = '<style>
						.tddclass td{
							border: 1px solid #6caace;
							text-align: center;
						}
						.tddclass th {
							border: 1px solid #6caace;
							background-color: #FFFF66;
							text-align: center;
						}
						.divclass {
							position: absolute;
							width: 50%;
							bottom: 10px;
							text-align: right;
						}
					</style>';
			foreach($sqlAry as $k=>$s)
			{				
				if($n == 0)
					$maincontect .= '<h2 style="text-align:center">門市帳戶資料</h2><table class = "tddclass"><tr><th>序號</th><th>店編</th><th>門市</th><th>Nccc特店代號</th><th>富邦帳號</th></tr>';
				$maincontect .='<tr><td>'.($k+1).'</td><td>'.$s['storecode'].'</td><td>'.$s['storename'].'</td><td>'.$s['nccc_code'].'</td><td>'.$s['account'].'</td></tr>';
				$n++;
				if($n >10)
				{
					$maincontect .= '</table>';
					array_push($mainAry,$maincontect);
					$maincontect = '';
					$n = 0;
				}
			}
			// CVarDumper::dump($mainAry,10,true);

		}
		//datalist
		if(isset($_POST['datalistBTN']))
		{
			//DB連線 DB,user,pwd,db_table
			$con = mysqli_connect("database","root",null,"testdrive");
			//檢查DB連線
			if (mysqli_connect_errno($con)) 
			{ 
				echo "连接 MySQL 失败: " . mysqli_connect_error(); 
			} 
			$con->set_charset("utf8"); //編碼
			$sql = "SELECT * FROM tbs_fb_account"; //sql
			$results = mysqli_query($con,$sql);
			$dataAry = mysqli_fetch_all($results,MYSQLI_ASSOC); //資料array
			// $row = mysqli_fetch_array($ser,MYSQLI_NUM);

			// CVarDumper::dump($dataAry,10,true);
			// 回傳資料
			$returnSelect = isset($_POST['selectMutilTag'])?$_POST['selectMutilTag']:"";



			// CVarDumper::dump($_POST['selectABC'],10,true);
			// CVarDumper::dump($_POST['selectMutilTag'],10,true);
			// CVarDumper::dump($_POST['selectMutilTag'],10,true);
		}
		// 優化tit_pay_log
		if(isset($_POST['titPayLogBTN']))
		{

			//檢查posdata有無未拋送的資料 ;type1 售票機拋送每筆交易資料 type2售票機拋送結帳資料;防止重複拋送，GROUP BY id(2021.4.1)
			$posSql = "SELECT * FROM posdata WHERE status = 'N' and type = '1'"; 
			$posAry = Yii::app()->localDB->createCommand($posSql)->queryAll();
			count($posAry) > 0 ? $memoTag = '售票機轉入':$memoTag = '重複轉入';
			CVarDumper::dump($memoTag,10,true);


			
		}
		//TXT讀取測試
		if(isset($_POST['txtup']))
		{
			if(isset($_FILES['txtfile']))
			{
				// 檔案名稱
				$filename = $_FILES['txtfile']['name'];
				// 檔案暫存路徑
				$tmp_name =  $_FILES["txtfile"]["tmp_name"] ;
					echo $filename;
					echo $tmp_name;
					echo "<br>";
				// 讀取檔案arry
				$file_ary = file($tmp_name); //逐筆讀取
				// CVarDumper::dump($file_ary,10,true);
				// echo "INSERT INTO tbe_inventory_log (stock_date,sys_date,storeCode,storeName,itemNo,itemName,type,typeCode,status,num,opt1,memo,cemp) values ";

				foreach($file_ary as $f)
				{
					// $dataline = $this->convertEncoding($f);
					// $dataline = iconv('UTF-8','UTF-8',$f);
					// CVarDumper::dump($dataline,10,true);
					// CVarDumper::dump($f,10,true);
					// $txtLineAry = explode(",",$f);
					// echo "('20220901','20220930','".$txtLineAry[1]."','".$txtLineAry[0]."','".$txtLineAry[2]."','".$txtLineAry[3]."','期初','00','1','".$txtLineAry[4]."','1','9月補期初','sys'), <br>";
					// CVarDumper::dump($txtLineAry[0],10,true);



					// echo iconv('UTF-8', 'big5', $f);
				}
			}
		}

		// ----CALL CENTER
		//叫修項目清單
		$m_itemList = array();
		$m_itemnoSql = "SELECT m_itemNo,m_itemName FROM `tbe_call_item` GROUP BY m_itemNo";
		$m_item = Yii::app()->localEIP->createCommand($m_itemnoSql)->queryAll();
		foreach($m_item as $m)
		{
			$m_itemList[$m['m_itemNo']] = $m['m_itemName'];
		}
		// CVarDumper::dump($m_itemList,10,true);
		// 產生叫修資料分析
		if(isset($_POST['qry_call']))
		{
			$qry_area_1 = isset($_POST['qry_area_1'])?$_POST['qry_area_1']:'';
			$qry_store_1 = isset($_POST['qry_store_1'])?$_POST['qry_store_1']:'';
			$qry_mItemNo = isset($_POST['qry_mItemNo'])?$_POST['qry_mItemNo']:'';
			$ym_date = isset($_POST['qry_month_1'])?$_POST['qry_month_1']:'';
			$curMonth = isset($_POST['qry_month_1'])?date("Ym",strtotime($_POST['qry_month_1'])):'';
			// CVarDumper::dump($curMonth,10,true);

			//門市清單
			$storeListSql = "SELECT storecode,storename,area_id FROM tbs_store WHERE opt1 = '1' and depart_id = '3'";
			$storeList = Yii::app()->localEIP->createCommand($storeListSql)->queryAll();
			

			// 回傳用
			$call_areaData = array();
			$call_AllData = array();

			// 歷史叫修清單 - 門市
			$callTime_ary = array();
			$callTime_all = 0; //全公司總計
			$callSql_time = "SELECT areacode,storecode,storename,mItemNo,count(*)as time FROM `tbe_call_center` WHERE opt1 = '1'";
			 	if($qry_mItemNo != '')
					$callSql_time .=" and mItemNo = '$qry_mItemNo'";
			 	$callSql_time .= " GROUP BY storecode ,mItemNo ORDER BY `time` DESC";
			$Call_time = Yii::app()->localEIP->createCommand($callSql_time)->queryAll();
			foreach($Call_time as $ct)
			{
				if(!isset($callTime_ary[$ct['storecode']]))
					$callTime_ary[$ct['storecode']] = array();
				if(!isset($callTime_ary[$ct['storecode']]['calltime']))
					$callTime_ary[$ct['storecode']]['calltime'] = 0;
				if(!isset($callTime_ary[$ct['storecode']]['callList']))
					$callTime_ary[$ct['storecode']]['callList'] = array();
				$callTime_all +=$ct['time'];
				$callTime_ary[$ct['storecode']]['calltime'] += $ct['time'];
				array_push($callTime_ary[$ct['storecode']]['callList'],$ct);
				// 區域
				if(!isset($call_areaData[$ct['areacode']]))
					$call_areaData[$ct['areacode']] = array();
				if(!isset($call_areaData[$ct['areacode']]['history'][$ct['mItemNo']]))
					$call_areaData[$ct['areacode']]['history'][$ct['mItemNo']] = 0;
				$call_areaData[$ct['areacode']]['history'][$ct['mItemNo']] += $ct['time'];
				// 全公司
				if(!isset($call_AllData['history']))
					$call_AllData['history'] = array();
				if(!isset($call_AllData['history'][$ct['mItemNo']]))
					$call_AllData['history'][$ct['mItemNo']] = 0;
				$call_AllData['history'][$ct['mItemNo']] += $ct['time'];

			}
			// 未完修件數
			$callNoFix_ary = array();
			$callTime_noFix = 0; //總計未完修
			$callSql_noFix ="SELECT areacode,storecode,storename,mItemNo,count(*)as time FROM `tbe_call_center` WHERE opt1 = '1' and callStatus in (1,2,3,4,5) ";
			if($qry_mItemNo != '')
					$callSql_noFix .=" and mItemNo = '$qry_mItemNo'";
			 	$callSql_noFix .= " GROUP BY storecode ,mItemNo ORDER BY `time` DESC";
			$call_noFix = Yii::app()->localEIP->createCommand($callSql_noFix)->queryAll();
			foreach($call_noFix as $cn)
			{
				if(!isset($callNoFix_ary[$cn['storecode']]))
					$callNoFix_ary[$cn['storecode']] = array();
				if(!isset($callNoFix_ary[$cn['storecode']]['calltime']))
					$callNoFix_ary[$cn['storecode']]['calltime'] = 0;
				if(!isset($callNoFix_ary[$cn['storecode']]['callList']))
					$callNoFix_ary[$cn['storecode']]['callList'] = array();
				$callTime_noFix += $cn['time'];
				$callNoFix_ary[$cn['storecode']]['calltime'] += $cn['time'];
				array_push($callNoFix_ary[$cn['storecode']]['callList'],$cn);
				// 區域
				if(!isset($call_areaData[$cn['areacode']]))
					$call_areaData[$cn['areacode']] = array();
				if(!isset($call_areaData[$cn['areacode']]['noFix'][$cn['mItemNo']]))
					$call_areaData[$cn['areacode']]['noFix'][$cn['mItemNo']] = 0;
				$call_areaData[$cn['areacode']]['noFix'][$cn['mItemNo']] += $cn['time'];
				// 全公司
				if(!isset($call_AllData['noFix']))
					$call_AllData['noFix'] = array();
				if(!isset($call_AllData['noFix'][$cn['mItemNo']]))
					$call_AllData['noFix'][$cn['mItemNo']] = 0;
				$call_AllData['noFix'][$cn['mItemNo']] += $cn['time'];
			}
			// 本月新增curMonth
			$callThisMonth_ary = array();
			$callTime_ThisMonth = 0; //總計本月新增
			$callsql_THisMonth ="SELECT areacode,storecode,storename,mItemNo,count(*)as time FROM `tbe_call_center` WHERE opt1 = '1' and mid(callno,1,6) = '$curMonth'";
			if($qry_mItemNo != '')
				$callsql_THisMonth .=" and mItemNo = '$qry_mItemNo'";
			$callsql_THisMonth .= " GROUP BY storecode ,mItemNo ORDER BY `time` DESC";
			$call_THisMonth = Yii::app()->localEIP->createCommand($callsql_THisMonth)->queryAll();
			foreach($call_THisMonth as $cm)
			{
				if(!isset($callThisMonth_ary[$cm['storecode']]))
					$callThisMonth_ary[$cm['storecode']] = array();
				if(!isset($callThisMonth_ary[$cm['storecode']]['calltime']))
					$callThisMonth_ary[$cm['storecode']]['calltime'] = 0;
				if(!isset($callThisMonth_ary[$cm['storecode']]['callList']))
					$callThisMonth_ary[$cm['storecode']]['callList'] = array();
				$callTime_ThisMonth += $cm['time'];
				$callThisMonth_ary[$cm['storecode']]['calltime'] += $cm['time'];
				array_push($callThisMonth_ary[$cm['storecode']]['callList'],$cm);
				// 區域
				if(!isset($call_areaData[$cm['areacode']]))
					$call_areaData[$cm['areacode']] = array();
				if(!isset($call_areaData[$cm['areacode']]['month'][$cm['mItemNo']]))
					$call_areaData[$cm['areacode']]['month'][$cm['mItemNo']] = 0;
				$call_areaData[$cm['areacode']]['month'][$cm['mItemNo']] += $cm['time'];
				// 全公司
				if(!isset($call_AllData['month']))
					$call_AllData['month'] = array();
				if(!isset($call_AllData['month'][$cm['mItemNo']]))
					$call_AllData['month'][$cm['mItemNo']] = 0;
				$call_AllData['month'][$cm['mItemNo']] += $cm['time'];
			}
			
			// 本月完修fixMonth
			$callfixMonth_ary = array();
			$callTime_fixMonth = 0; //總計本月新增
			$callsql_fixMonth ="SELECT areacode,storecode,storename,mItemNo,count(*)as time FROM `tbe_call_center` WHERE opt1 = '1' and callStatus in (6,7,91) and mid(finisheDate,1,7) = '$ym_date'";
			if($qry_mItemNo != '')
				$callsql_fixMonth .=" and mItemNo = '$qry_mItemNo'";
			$callsql_fixMonth .= " GROUP BY storecode ,mItemNo ORDER BY `time` DESC";
			$call_fixMonth = Yii::app()->localEIP->createCommand($callsql_fixMonth)->queryAll();
			foreach($call_fixMonth as $fm)
			{
				if(!isset($callfixMonth_ary[$fm['storecode']]))
					$callfixMonth_ary[$fm['storecode']] = array();
				if(!isset($callfixMonth_ary[$fm['storecode']]['calltime']))
					$callfixMonth_ary[$fm['storecode']]['calltime'] = 0;
				if(!isset($callfixMonth_ary[$fm['storecode']]['callList']))
					$callfixMonth_ary[$fm['storecode']]['callList'] = array();
				$callTime_fixMonth += $fm['time'];
				$callfixMonth_ary[$fm['storecode']]['calltime'] += $fm['time'];
				array_push($callfixMonth_ary[$fm['storecode']]['callList'],$fm);

				// 區域
				if(!isset($call_areaData[$fm['areacode']]))
					$call_areaData[$fm['areacode']] = array();
				if(!isset($call_areaData[$fm['areacode']]['fixed'][$fm['mItemNo']]))
					$call_areaData[$fm['areacode']]['fixed'][$fm['mItemNo']] = 0;
				$call_areaData[$fm['areacode']]['fixed'][$fm['mItemNo']] += $fm['time'];
				// 全公司
				if(!isset($call_AllData['fixed']))
					$call_AllData['fixed'] = array();
				if(!isset($call_AllData['fixed'][$fm['mItemNo']]))
					$call_AllData['fixed'][$fm['mItemNo']] = 0;
				$call_AllData['fixed'][$fm['mItemNo']] += $fm['time'];
			}


			//資料加入storeList中
			foreach($storeList as $key=>$s)
			{
				//叫修總計
				if(isset($callTime_ary[$s['storecode']]))
					$storeList[$key]['calldata_history'] = $callTime_ary[$s['storecode']];
				// 未完修總計
				if(isset($callNoFix_ary[$s['storecode']]))
					$storeList[$key]['calldata_noFix'] = $callNoFix_ary[$s['storecode']];
				// 本月新增
				if(isset($callThisMonth_ary[$s['storecode']]))
					$storeList[$key]['calldata_month'] = $callThisMonth_ary[$s['storecode']];
				//本月完修
				if(isset($callfixMonth_ary[$s['storecode']]))
					$storeList[$key]['calldata_month'] = $callfixMonth_ary[$s['storecode']];
			}
			unset($callTime_ary);
			unset($callNoFix_ary);
			unset($callThisMonth_ary);
			unset($callfixMonth_ary);

			// 建立未修清單 >> 算超過3個月or6個月資料
			// 6個月清單
			$sixMonthAry = $this->getSixMonthList($ym_date);
			$noFix_sql = "";
			$noFix_sql ="SELECT callno,mid(callno,1,6) as month,areacode,storecode,storename,mItemNo FROM `tbe_call_center` WHERE opt1 = '1' and callStatus in (1,2,3,4,5) ";
			if($qry_mItemNo != '')
					$noFix_sql .=" and mItemNo = '$qry_mItemNo'";
			 	$noFix_sql .= " GROUP BY callno";
			$noFix_ary = Yii::app()->localEIP->createCommand($noFix_sql)->queryAll();
			// CVarDumper::dump($noFix_ary,10,true);
			$noFixOver3 = 0;
			$noFixOver6 = 0;
			foreach($noFix_ary as $nf)
			{
				if(!isset($call_areaData[$nf['areacode']]['over3']))
					$call_areaData[$nf['areacode']]['over3'] = 0;
				if(!isset($call_areaData[$nf['areacode']]['over6']))
					$call_areaData[$nf['areacode']]['over6'] = 0;
				
				//數量加總
				// 超過3個月
				if($nf['month'] == $sixMonthAry[0] || $nf['month'] == $sixMonthAry[1] || $nf['month'] == $sixMonthAry[2]){
					$call_areaData[$nf['areacode']]['over3'] ++;
					$noFixOver3 ++;
				}
				// 超過6個月
				if(!in_array($nf['month'],$sixMonthAry)){
					$call_areaData[$nf['areacode']]['over6'] ++;
					$noFixOver6 ++;
				}

				
			}
			// CVarDumper::dump($call_areaData,10,true); // 各區叫修紀錄
			// CVarDumper::dump($call_AllData,10,true); //callTime_all、callTime_noFix、callTime_ThisMonth

			// 總計資料
			$call_allTotal = array();
			$call_allTotal['callHistory'] = $callTime_all; //累計 總計
			$call_allTotal['callTime_noFix'] = $callTime_noFix; //尚未完修件數
			$call_allTotal['callTime_ThisMonth'] = $callTime_ThisMonth; //本月新增
			$call_allTotal['callTime_fixMonth'] = $callTime_fixMonth; //本月完修
			$call_allTotal['callTime_over3'] = $noFixOver3; //超過3個月
			$call_allTotal['callTime_over6'] = $noFixOver6; //超過6個月
			// CVarDumper::dump($call_allTotal,10,true);

			// 圖表用
				//全公司
				$call_AllDataCharts = array();
				foreach($call_AllData as $k=>$c)
				{
					foreach($call_AllData[$k] as $ki=>$i)
					{
						if(!isset($call_AllDataCharts[$k]))
						{
							$call_AllDataCharts[$k]['value'] = array();
							$call_AllDataCharts[$k]['title'] = array();
						}
						array_push($call_AllDataCharts[$k]['value'],$i);
						if(!isset($m_itemList[$ki]))
						{
							switch($ki)
							{
								case 'ZZZ':
									$kTitle = '其他';
									break;
								case 'ZAA':
									$kTitle = '設備申請';
									break;	
							}
							array_push($call_AllDataCharts[$k]['title'],$kTitle);
						}
						else
							array_push($call_AllDataCharts[$k]['title'],$m_itemList[$ki]);

					}
				}

				// 區域
				$call_areaDataCharts = array();
				// foreach($areaList as $a)
				// {
				// 	// $call_areaDataCharts[$a['areaCode']] = $call_areaData;
				// }
		}
	
		//批次服務項目修改、新增 st_service 
		if(isset($_POST['testBtn']))
		{
			// 門市資料
			$storeListA = "SELECT * FROM tbs_store";
			$storeListARy = Yii::app()->localEIP->createCommand($storeListA)->queryAll();
			$storeListMatch = array();
			// foreach($storeListARy as $aa)
			// {
			// 	$storeListMatch[$aa['storecode']] = $aa;
			// }

			$storeAry = array(
				'002038',
				'002040',
				'002041'
			);
			//--- 
			$textjsValue = isset($_POST['testBox'])?$_POST['testBox']:'';
			$testSql = "select * from tbs_store where storecode = '$textjsValue'";
			// $testSql = "SHOW TABLES";
			$testResult = Yii::app()->localEIP->createCommand($testSql)->queryAll();
			echo $testSql;

			CVarDumper::dump($testResult,10,true);
			// echo $testSql;
			// $phpE = new XPHPExcel;
			// include_once("/XPHPExcel.php");
			// CVarDumper::dump($phpE,10,true);

		}

		// -------------------
		//google回傳表單CSV 讀取統計
		if(isset($_POST['txtup_google']))
		{
			// 檔案分析---------------
			if(isset($_FILES['txtfile_google']))
			{
				// 檔案名稱
				$filename = $_FILES['txtfile_google']['name'];
				// 檔案暫存路徑
				$tmp_name =  $_FILES["txtfile_google"]["tmp_name"] ;
					// echo $filename;
					// echo $tmp_name;
					// echo "<br>";
				// 讀取檔案arry
				$file_ary = file($tmp_name); //逐筆讀取
				$file_ary_result = array();
				// 建立TITLE
				$file_ary_title = explode(",",$file_ary[0]);
				// CVarDumper::dump($file_ary_title,10,true);
				$n = 0;
				foreach($file_ary as $f)
				{
					if($n != 0)
					{
						$dataline = iconv('UTF-8','UTF-8',$f);
						$dataline_data = explode(",",$dataline);
						foreach($file_ary_title as $nn => $tt)
						{
							if($tt != '門市')
								$file_ary_result[$dataline_data[0]][$tt]['期末'] = $dataline_data[$nn];
						}
					}
					$n++;
				}
			}

			// 庫存比對---------------
			// 項目
			$itemAry = array(
				'A001001',
				'A001002',
				'A001003',
				'A001004',
				'A001005',
				'A001006',
				'A001007',
				'A001009',
				'A001010',
				'A001011',
				'A001014',
				'A001015',
				'A001016',
				'A001017',
				'A001018',
				'A001019',
				'A001020',
				'A001021',
				'A001022',
				'A001023',
				'A001024',
				'A001025',
				'A001026',
				'A001027',
				'A001028',
				'A001029',
				'A001030',
				'A001032',
				'A001033',
				'A001034',
				'A001035',
				'A001036',
				'A001037',
				'A101020',
				'A101030',
				'A101035',
				'A101049',
				'A101050',
				'A101052',
				'A101085',
				'A101087',
				'A101088',
				'A101105',
				'A101125',
				'A101130',
				'A101135',
				'A101145',
				'A101305',
				'A101320',
				'A101325',
				'A101350',
				'A101425',
				'A101435',
				'A101437',
				'A101439',
				'A101455',
				'A101459',
				'A101501',
				'A102001',
				'A102002',
				'A102003',
				'B0010016',
				'B0010017',
				'B0010018',
				'B0010019',
				'B001006',
				'B001007',
				'B001008',
				'B001009',
				'B001010',
				'B002004',
				'B002005',
				'B002006',
				'B002007',
				'C001001',
				'C001002',
				'C001003',
				'C001004',
				'D001001',
				'D001002',
				'D001004',
				'D001005',
				'D001006',
				'D001007',
				'E001001',
				'E001002',
				'E001003',
				'E001004',
				'E001005',
				'E001006',
				'E001007',
				'E001008',
				'E001009',
				'E001010',
				'E001011',
				'E001013',
				'E001014',
				'E001015',
				'E001016',
				'E001019',
				'E001020',
				'E001022',
				'E002006',
				'E002007',
				'E0020070',
				'F001001',
				'F001002',
				'F0010020',
				'F0010021',
				'F0010051',
				'F0010052',
				'F001006',
				'F001007',
				'F0010071',
				'F001008',
				'F00200101',
				'F00200102',
				'F101001',
				'F101021',
				'H001003',
				'H00101011',
				'H002001',
				'I001011',
				'J001004',
				'J001005',
				'J001006',
				'Z005051'
			);
			// 對照
			$itemAryMatch = array(
				'A001001'=>'染膏4N',
				'A001002'=>'染膏4W',
				'A001003'=>'染膏5RB',
				'A001004'=>'染膏WL-R',
				'A001005'=>'雙氧水6%',
				'A001006'=>'雙氧水9%',
				'A001007'=>'雙氧水12%',
				'A001009'=>'染膏7RB',
				'A001010'=>'染膏5M',
				'A001011'=>'染膏7M',
				'A001014'=>'染膏WL-A',
				'A001015'=>'染膏WL-RB',
				'A001016'=>'雙氧水3%',
				'A001017'=>'染膏3N',
				'A001018'=>'漂粉',
				'A001019'=>'D染膏1C',
				'A001020'=>'D染膏6C',
				'A001021'=>'D染膏7C',
				'A001022'=>'D染膏6YG',
				'A001023'=>'D染膏7YG',
				'A001024'=>'D染膏AB',
				'A001025'=>'D染膏AG',
				'A001026'=>'D染膏AV',
				'A001027'=>'D染膏SSC',
				'A001028'=>'D雙氧水6%',
				'A001029'=>'D雙氧水9%',
				'A001030'=>'D雙氧水12%',
				'A001032'=>'D染膏SSA',
				'A001033'=>'D染膏5V',
				'A001034'=>'D染膏5P',
				'A001035'=>'D染膏5TN',
				'A001036'=>'D染膏10D',
				'A001037'=>'D染膏6P',
				'A101020'=>'精剪-美絲雅 3',
				'A101030'=>'精剪-美絲雅 4',
				'A101035'=>'精剪-美絲雅4.26',
				'A101049'=>'精剪-美絲雅 5',
				'A101050'=>'精剪-美絲雅5.15',
				'A101052'=>'精剪-美絲雅 5.5',
				'A101085'=>'精剪-美絲雅 6.1',
				'A101087'=>'精剪-美絲雅 6.23',
				'A101088'=>'精剪-美絲雅 6.26',
				'A101105'=>'精剪-美絲雅 6.66',
				'A101125'=>'精剪-美絲雅 7.13',
				'A101130'=>'精剪-美絲雅 7.23',
				'A101135'=>'精剪-美絲雅 7.44',
				'A101145'=>'精剪-美絲雅 8.1',
				'A101305'=>'精剪-完美漾棕 4.05',
				'A101320'=>'精剪-完美漾棕 5.05',
				'A101325'=>'精剪-完美漾棕 5.35',
				'A101350'=>'精剪-完美漾棕 7.35',
				'A101425'=>'精剪-完美酷棕 5.8',
				'A101435'=>'精剪-完美酷棕 6.8',
				'A101437'=>'精剪-完美酷棕 6.11',
				'A101439'=>'精剪-完美酷棕 6.17',
				'A101455'=>'精剪-完美酷棕 7.8',
				'A101459'=>'精剪-完美酷棕 7.17',
				'A101501'=>'精剪-完美拜金 13',
				'A102001'=>'精剪-雙氧乳 6%',
				'A102002'=>'精剪-雙氧乳 9%',
				'A102003'=>'精剪-雙氧乳 12%',
				'B0010016'=>'茶樹SPA洗髮精-職洗',
				'B0010017'=>'薄荷深層洗髮精-職洗',
				'B0010018'=>'舒活果漾洗髮精-職洗',
				'B0010019'=>'綠茶洗髮精-職洗',
				'B001006'=>'潤絲精-2000ml',
				'B001007'=>'茶樹SPA洗髮精-販售',
				'B001008'=>'薄荷深層洗髮精-販售',
				'B001009'=>'舒活果漾洗髮精-販售',
				'B001010'=>'綠茶洗髮精-販售',
				'B002004'=>'茶樹SPA標籤 1000ml',
				'B002005'=>'薄荷深層標籤 1000ml',
				'B002006'=>'舒活果漾標籤 1000ml',
				'B002007'=>'綠茶標籤1000ml -職洗',
				'C001001'=>'拋棄式圍脖紙',
				'C001002'=>'感熱紙',
				'C001003'=>'濕紙巾',
				'C001004'=>'保鮮膜',
				'D001001'=>'酵素/JIT髮油',
				'D001002'=>'貴婦人造型雕',
				'D001004'=>'髮油-販售',
				'D001005'=>'髮雕-販售',
				'D001006'=>'髮蠟-販售',
				'D001007'=>'SOPU-90度髮蠟',
				'E001001'=>'染碗',
				'E001002'=>'染膏擠壓器',
				'E001003'=>'染髮圍巾',
				'E001004'=>'防水披肩',
				'E001005'=>'染刷',
				'E001006'=>'尖尾梳',
				'E001007'=>'大版梳',
				'E001008'=>'排骨梳',
				'E001009'=>'耳罩',
				'E001010'=>'空瓶擠壓器',
				'E001011'=>'計時器',
				'E001013'=>'JIT-毛巾(藍)',
				'E001014'=>'手拿鏡',
				'E001015'=>'電子秤',
				'E001016'=>'染膏調理棒',
				'E001019'=>'吹風機',
				'E001020'=>'L資訊架',
				'E001022'=>'髮品塑膠袋',
				'E002006'=>'空瓶-髮蠟(試用瓶)',
				'E002007'=>'空瓶-精油(分裝瓶)',
				'E0020070'=>'精油(分裝瓶)貼紙',
				'F001001'=>'凡士林凝膠',
				'F001002'=>'頭皮防護凝膠',
				'F0010020'=>'染髮去色液-matrix',
				'F0010021'=>'染髮去漬液-JOICO',
				'F0010051'=>'葉綠素髮霜(舒壓 / 涼 )4000ml',
				'F0010052'=>'葉綠素髮霜(舒壓 / 不涼 )4000m',
				'F001006'=>'賦活髮泥組',
				'F001007'=>'葉綠素髮霜(瞬護/50%涼度)-1000',
				'F0010071'=>'葉綠素髮霜(瞬護/0%涼度)-1000m',
				'F001008'=>'舒壓精油(薰衣草)100ml',
				'F00200101'=>'葉綠素髮霜 (涼) 標籤',
				'F00200102'=>'葉綠素髮霜 (不涼) 標籤',
				'F101001'=>'前置舒緩霜(頭皮隔離)',
				'F101021'=>'護髮精華(染後護髮)',
				'H001003'=>'眼鏡盒',
				'H00101011'=>'雜誌',
				'H002001'=>'髮型設計書',
				'I001011'=>'剪髮圍巾',
				'J001004'=>'髮油-空瓶',
				'J001005'=>'髮雕-空瓶',
				'J001006'=>'髮蠟-空瓶',
				'Z005051'=>'大陸刷頭',
			);
			$invenSql = "SELECT * FROM tbe_inventory_log WHERE stock_date = '20220731' and typeCode in('99','13','15')";
			$invenLog = yii::app()->localEIP->createCommand($invenSql)->queryAll();
			$invenAry = array();
			foreach($invenLog as $i)
			{
				// 只做期末
				if($i['type'] == '期末')
					$invenAry[$i['storeName']][$i['itemNo']][$i['type']] = $i['num'];
			}
			
			// 分析
			foreach($invenAry as $store=>$sAry)
			{
				foreach($itemAry as $item)
				{
					if(isset($file_ary_result[$store][$item]['期末']))
					// CVarDumper::dump($store,10,true);
						$invenAry[$store][$item]['google期末'] = $file_ary_result[$store][$item]['期末'];
						// 檢查
						if(((float)$invenAry[$store][$item]['期末'] - (float)$file_ary_result[$store][$item]['期末']) == 0)
							$invenAry[$store][$item]['核對'] = '正確';
						else
							{
								if(isset($invenAry[$store][$item]['期末']) && $file_ary_result[$store][$item]['期末'] != null)
								{
									$invenAry[$store][$item]['核對'] ="【錯誤】" .($invenAry[$store][$item]['期末'] - $file_ary_result[$store][$item]['期末']);
								}
								else
								{
									$invenAry[$store][$item]['核對'] = '【EIP:'.$invenAry[$store][$item]['期末'].' 】|| 【回報:'.$file_ary_result[$store][$item]['期末']."】";
								}
							}

				}
			}
			// CVarDumper::dump($invenAry['高雄自由'],10,true);
			// CVarDumper::dump($file_ary_result,10,true);
		}
		// ------------------
		// Json讀取測試
		if(isset($_POST['jsonup']))
		{
			// 檔案
			if(isset($_FILES['jsonfile']))
			{
				// 檔案名稱
				$filename = $_FILES['jsonfile']['name'];
				// 檔案暫存路徑
				$tmp_name =  $_FILES["jsonfile"]["tmp_name"] ;
					echo $filename;
					echo $tmp_name;
					echo "<br>";
				// 讀取檔案arry
				$file_ary = json_decode(file($tmp_name)[0],true); //逐筆讀取
				// CVarDumper::dump($file_ary,10,true);
				// foreach($file_ary as $s)
				// {
				// 	$file_result = iconv("GBK","UTF-8",$s);
				// 	CVarDumper::dump($file_result,10,true);


				// }

			}
		}

		// ------------
		// LinePay
		$returnMsg = '';
		if(isset($_POST['linePayment']))
		{
			// CVarDumper::dump($_POST,10,true);
			
			// 導向reservc
			// Store Webpage -> Store Server
			if(isset($_POST['productName'])) 
			{
				$returnMsg = $this->linepayReserve();
				// CVarDumper::dump($returnMsg,10,true);
			}
			else {
				echo "No Data";
			}
		}

		// COVID搬運------------------------------
			$map_value = array();
			$today_Data = array();
			$total_info = array();
			// 	//日期
			// 	$setYSD = strtotime("-1 days");
			// 	$setDate = date('Ymd',$setYSD);
			// 	// $setDate = '20210601';
			// 	//地圖圖表用
			// 	$map_value = array();
			// 	$mapMatch = $this->getMapCode();
			// 	// CVarDumper::dump($mapMatch,10,true);

			//https://data.cdc.gov.tw/dataset/agsdctable-day-19cov/resource/a59483fd-4b09-42bd-af15-3c123147d7e3
			// $covidFile = file_get_contents('https://od.cdc.gov.tw/eic/Day_Confirmation_Age_County_Gender_19CoV.json');
			// $covidData = json_decode($covidFile,true);
			// CVarDumper::dump($covidData,10,true);

			// 	// $setDate = '20210530';
			// 	$confirm = 0;
			// 	$f_confirm = 0;
			// 	$c_male = 0;
			// 	$fc_male = 0;
			// 	$c_female = 0;
			// 	$fc_female = 0;
			// 	$c_local = array();
			// 	$fc_local = array();
			// 	$today_Data = array();
			// 	for($i=0;$i<count($covidData);$i++)
			// 	{
			// 		if($covidData[$i]['個案研判日'] == $setDate)
			// 		{
			// 			//本土
			// 			if($covidData[$i]['是否為境外移入'] == '否')
			// 			{
			// 				$confirm+=$covidData[$i]['確定病例數'];
			// 				switch($covidData[$i]['性別'])
			// 				{
			// 					case '女':
			// 						$c_female+=$covidData[$i]['確定病例數'];
			// 						break;
			// 					case '男':
			// 						$c_male+=$covidData[$i]['確定病例數'];
			// 						break;
			// 				}
			// 				switch($covidData[$i]['縣市'])
			// 				{
			// 					case $covidData[$i]['縣市']:
			// 						$c_local[$covidData[$i]['縣市']]+=$covidData[$i]['確定病例數'];
			// 						break;
			// 				}
			// 			}
			// 			//境外
			// 			if($covidData[$i]['是否為境外移入'] == '是')
			// 			{
			// 				$f_confirm ++;
			// 				switch($covidData[$i]['性別'])
			// 				{
			// 					case '女':
			// 						$fc_female++;
			// 						break;
			// 					case '男':
			// 						$fc_male++;
			// 						break;
			// 				}
			// 				switch($covidData[$i]['縣市'])
			// 				{
			// 					case $covidData[$i]['縣市']:
			// 						$fc_local[$covidData[$i]['縣市']]++;
			// 						break;
			// 				}
			// 			}
			// 		}

			// 	}
			// 	//整理資料
			// 	$today_Data['local']['confirm'] = $confirm;
			// 	$today_Data['local']['c_male'] = $c_male;
			// 	$today_Data['local']['c_female'] = $c_female;
			// 	$today_Data['local']['c_local'] = $c_local;
			// 	$today_Data['foreign']['confirm'] = $f_confirm;
			// 	$today_Data['foreign']['c_male'] = $fc_male;
			// 	$today_Data['foreign']['c_female'] = $fc_female;
			// 	$today_Data['foreign']['c_local'] = $fc_local;
			// 	// CVarDumper::dump($today_Data,10,true);
			// // Map比對
			// 	//先產keys
			// 	$local_key = array_keys($today_Data['local']['c_local']);
			// 	for($i=0;$i<count($local_key);$i++)
			// 	{
			// 		//0地點 1數量
			// 		$map_value[$i][0] = $mapMatch[$local_key[$i]];
			// 		$map_value[$i][1] = $today_Data['local']['c_local'][$local_key[$i]];
			// 	}
			// 統計數字
			// $total_covid_file = file_get_contents("https://quality.data.gov.tw/dq_download_json.php?nid=120450&md5_url=02bd6ba50ef7d355cb8cd9e8685bec1c");
			// $total_covid_ary = json_decode($total_covid_file,true);
				// $fileDate = fgetcsv($file);	
				// CVarDumper::dump($fileLine,10,true);
				//資料丟進array
					// $total_info['確診人數總計'] = $total_covid_ary[0]['確診'];
					// $total_info['解除隔離總計']= $total_covid_ary[0]['解除隔離'];
					// $total_info['死亡總計']= $total_covid_ary[0]['死亡'];
					// $total_info['送驗總計']= $total_covid_ary[0]['送驗'];
					// $total_info['排除總計']= $total_covid_ary[0]['排除'];
					// $total_info['昨日確診']= $total_covid_ary[0]['昨日確診'];
					// $total_info['昨日排除']= $total_covid_ary[0]['昨日排除'];
					// $total_info['昨日送驗']= $total_covid_ary[0]['昨日送驗'];
				//------------------
				// CVarDumper::dump($total_info,10,true);
				// CVarDumper::dump($tttJson,10,true);

		//-----------------------------------------------------------
		// 測試搬運
		// $covSorUrl = 'https://od.cdc.gov.tw/eic/open_data_death_date_statistics_19CoV_6.json';
		// $covSorUrl ='https://od.cdc.gov.tw/eic/Day_Confirmation_Age_County_Gender_19CoV.json';
		// 直接使用file_get_contents會出現SSL認證錯誤問題，需加入參數
			$arrContextOptions=array(
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			);  
		// $covSorData = file_get_contents($covSorUrl,false,stream_context_create($arrContextOptions));
		// $covJsonDecode = json_decode($covSorData);
		// CVarDumper::dump($covJsonDecode,10,true);

		// fopen方式，用於來源資料量龐大時 
		// $handle = fopen($covSorUrl, 'r',false,stream_context_create($arrContextOptions));
		// 	while (($line = fgets($handle)) !== false) {
		// 		$jsonData = json_decode($line, true); // 將 JSON 字串轉換成陣列或物件
		// 		// 在這裡處理 $jsonData
		// 		// CVarDumper::dump($jsonData,10,true);
		// 	}
		// 	fclose($handle);

		// Dengue測試----------------------------------
			//日期
			$setYSD = strtotime("-1 days");
			$setDate = date('Y/m/d',$setYSD);
			$setMonth = date('Y/m',$setYSD);
			$total_info['dateMonth'] = $setMonth;
			// CVarDumper::dump($setMonth,10,true);
		// 	//地圖圖表用
		// 	$map_value = array();
			$mapMatch = $this->getMapCode();
		// 	// CVarDumper::dump($mapMatch,10,true);

		// $dengueSourceUrl = 'https://od.cdc.gov.tw/eic/Dengue_Daily_last12m_EN.json';
		$dengueSourceUrl = 'https://od.cdc.gov.tw/eic/Dengue_Daily_last12m.json'; //(中文資料)
		$dengueData = file_get_contents($dengueSourceUrl,false,stream_context_create($arrContextOptions));
		$dengueDecode = json_decode($dengueData);
		// CVarDumper::dump($dengueDecode,10,true);
		// 塞選當月資料
		$filteredData = array_filter($dengueDecode,function($dData) use ($setMonth){
			// 先explode YM
			$dateExplode = explode("/",$dData->個案研判日);
			$dateYM = $dateExplode[0]."/".$dateExplode[1];
			return $dateYM == $setMonth;
		}); 
		// CVarDumper::dump($filteredData,10,true);
		// 記錄用
			$localAry = array();	
		foreach($filteredData as $d)
		{
			// highChart 使用
			if(!in_array($mapMatch[$d->居住縣市],$localAry)){
				$map_value[$mapMatch[$d->居住縣市]][0] = $mapMatch[$d->居住縣市];
				$map_value[$mapMatch[$d->居住縣市]][1] = 1;
				array_push($localAry,$mapMatch[$d->居住縣市]);
			}
			else
				$map_value[$mapMatch[$d->居住縣市]][1]++;
			//性別統計 
			if(!isset($total_info[$d->性別]))
				$total_info[$d->性別] = 1;
			else
				$total_info[$d->性別] ++;
			// 境內外統計
			if(!isset($total_info['import'][$d->是否境外移入]))
				$total_info['import'][$d->是否境外移入] = 1;
			else
				$total_info['import'][$d->是否境外移入] ++;

		}
		unset($localAry);
		// 重新建立key
		$map_value = array_values($map_value);
		// CVarDumper::dump($map_value,10,true);
		// CVarDumper::dump($total_info,10,true);



	
		//計時
        $time_end = microtime(true);
        $computetime = "time : ".substr($time_end - $time_start,0,5);
		// echo date("Ymd Hi");
		// ---------


		$this->render('testPageX',array(
			'computetime'=>$computetime,
			'sqlAry'=>$sqlAry,
			'gap'=>$gap,
			'cssHtml' =>$cssHtml,
			'mainAry'=>$mainAry,
			'ym_date'=>$ym_date,
			'qry_mItemNo'=>$qry_mItemNo,
			'm_itemList'=>$m_itemList,
			'areaList'=>$areaList,
			'storeList'=>$storeList,
			'call_AllData'=>$call_AllData,
			'call_allTotal'=>$call_allTotal,
			// 圖表用
			'call_AllDataCharts'=>$call_AllDataCharts,
			// covid搬運用
			'map_value'=>$map_value,
			'today_Data'=>$today_Data,
			'total_info'=>$total_info,
			// 其他測試
			// 'AjaxAreaAry'=>$AjaxAreaAry,
			'returnSelect'=>$returnSelect,
			'storeResult'=>$storeResult,
			// 測試庫存盤點
			'itemAry'=>$itemAry,
			'invenAry'=>$invenAry,
			'itemAryMatch'=>$itemAryMatch,
			// linePay
			'returnMsg'=>$returnMsg,
			// ----



		));
	}

	//WebSocket測試
	public function actionWebSocketTest()
	{

		ini_set('memory_limit', '1024M');
		//計時
		$time_start = microtime(true);
		
		//計時
		$time_end = microtime(true);
		$computetime = substr($time_end - $time_start,0,5);

		$this->render('webSocketTest',array(
			'computetime'=>$computetime,

		));
	}
	public function actionWebSocketAjax()
	{
		$text = $_POST['inTextCurrent'];
		$sql = "update clock_pw SET opt1 = '1' where pw = '$text'";
		Yii::app()->localDB->createCommand($sql)->execute(); 
		print json_encode($text);
	}
	public function actionJson_form()
	{
		// CVarDumper::dump($_GET,10,true);
		if(isset($_GET['storecode']))
		{
			$storecode = $_GET['storecode'];
			// $storecode = '002041';

			// $sQLtes = "SELECT * FROM tit_saletime where storeCode = '$storecode' and sv_time ='1' ORDER BY sv_type,sv_order";
			$sQLtes = "SELECT * FROM tit_saletime where storeCode = '$storecode' ORDER BY sv_type,sv_order";

				$sQLtesAry = Yii::app()->localEIP->createCommand($sQLtes)->queryAll();
				// CVarDumper::dump($sQLtesAry,10,true);
				$totalService = count($sQLtesAry);
				$jsonAry = array(); //組合用
				//title
				$jsonTitle = array(
					"type"=> "project",
					"entry"=> "$totalService",
				);
				//組合表頭
				$jsonAry = array_merge($jsonAry,$jsonTitle);
				//資料用
				$jsonAdd = array();
				for($i=0;$i<count($sQLtesAry);$i++)
				{
					$categorySET = '';
					$idN = $i+1;
					switch($sQLtesAry[$i]['sv_type'])
						{
							case 1:
								$categorySET = '服务';
								break;
							case 2:
								$categorySET = '產品';
								break;
							case 3:
								$categorySET = '赠品';
								break;
							case 4:
								$categorySET = '其他';
								break;
						}
					$jsonAdd = array(
						"projectID".$idN=> $sQLtesAry[$i]['sale_no'],
						"name".$idN=> $sQLtesAry[$i]['sale_name'],
						"abbreviation".$idN=> "null", //簡稱
						"category".$idN=> $categorySET,
						"order".$idN=> $sQLtesAry[$i]['sv_order'],
						"Number".$idN=> $sQLtesAry[$i]['num'],
						"money".$idN=> $sQLtesAry[$i]['price'],
						"achievement".$idN=> $sQLtesAry[$i]['perform_mapping'],
						"BAK".$idN=> "OK",
					);
					$jsonAry = array_merge($jsonAry,$jsonAdd);
				}
				// $json = json_encode($jsonAry); //unicode
				$json = json_encode($jsonAry,JSON_UNESCAPED_UNICODE); //中文防亂碼
				// CVarDumper::dump($json,10,true);
				// echo $json;
				//檔案
					//路徑
					$jPath = Yii::getPathOfAlias('webroot').'/images/';	//儲存路徑(外)
					$jsonDownload = file_put_contents($jPath.$storecode."_serviceJson.json",$json);
					$downloadPath = $jPath."serviceJson.json";
				//-----------

				// echo $downloadPath;
				
				$this->redirect('images/'.$storecode.'_serviceJson.json');
				
		}

		$this->render('json_form',array(
		));
	}
	//get悠遊卡門市資料 
	public function getEasyAccountInfo()
	{
		$easyAccountInfoAry = array();
		$easyAccountSql = "SELECT * FROM tbs_easy_account WHERE opt1 = '1'"; //opt1 = 1 為啟用悠遊卡門市
		$easyAccountAry = Yii::app()->localEIP->createCommand($easyAccountSql)->queryAll();
		for($i=0;$i<count($easyAccountAry);$i++)
		{
			$easyAccountInfoAry[$easyAccountAry[$i]['easy_storecode']] = $easyAccountAry[$i];
		}
		return $easyAccountInfoAry;
	}
	//特店代號取得ipass帳務資料
	public function getIpassData()
	{
		$ipass_sql = "SELECT * FROM tbs_ipass_account";
		$ipassResult = Yii::app()->localEIP->createCommand($ipass_sql)->queryAll();
		$ipassAry = array();
		foreach($ipassResult as $ipass)
		{
			$ipassAry[$ipass['ipass_storecode']] = $ipass;
		}
		return $ipassAry;
	}
	//悠遊卡手動上傳 FTPS
	public function actionEasy_card_Upload()
	{
		$time_start = microtime(true); //計時

		$easy_date = date('Ymd',strtotime($_POST['easy_date'])); //下載日
		$show_date = date('Y-m-d',strtotime($_POST['easy_date'])); //view日期
		// $date =date('Ymd');
		if(isset($_POST['sub']))
		{
			//特店代號取得門市資料
			$easyAccountInfo = $this->getEasyAccountInfo();
			$easyInsertSql_INTO ="INSERT INTO tit_easy_pay_log(pdate,storename,storecode,easy_storecode,easy_cardNum,price,ptime,opt1,memo) VALUES ";
			$easyInsertSql ="";
			// 取得登入帳號資料 , 需做加密
			$get_roots = $this->getRootAccount('easy');
			//開始抓取資料(從FTPS下載)(統一目錄底下 統一檔案彙整全分店帳款資料)
			    $ftp_server = "cmas-ftp.easycard.com.tw";
			    $ftp_id = $get_roots[0]['account'];
			    $ftp_psw = $get_roots[0]['password'];
			    $easy_JIT = '09104782'; //JIT代碼
			
			    //---連接
			    $ch = curl_init();
			    //檔案名稱尾碼 01 ;資料超過20萬筆才會有02，so先設定抓01就好
			    curl_setopt($ch,CURLOPT_URL,'ftps://'.$ftp_server.'/'.$easy_JIT.'/out/TRNC/TRNC.'.$easy_JIT.'.'.$easy_date.'01');
			    // curl_setopt($ch, CURLOPT_VERBOSE, TRUE);   //DEBUG
			    //SSL
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			        // curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
			        curl_setopt($ch,CURLOPT_USERPWD,$ftp_id.':'.$ftp_psw);
			    //List FTP files and directories(只顯示檔案清單)
			        // curl_setopt($ch, CURLOPT_FTPLISTONLY, TRUE);
			    //Output to curl_exec
			        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			        $file = fopen('tmp/TRNC_file_'.$easy_date,'w'); //下載路徑、檔名
			        curl_setopt($ch, CURLOPT_FILE, $file); //下載
			    //執行
			        curl_exec($ch); 
			//讀取下載的資料
			    //檔案清單
			        // $easyFiles_ary = scandir('tmp/');
			    //上傳檔案 (檔案清單從2開始，前面0 1 為 . ..)
			    // for($i=2;$i<count($easyFiles_ary);$i++)
			    // {
			        $tit = 0;//資料筆數
			        $n = 0;//批次資料，截斷寫入用
			        $easyFile = file('tmp/TRNC_file_'.$easy_date);
			        // $easyFile = file('tmp/'.$easyFiles_ary[$i]);
			        //檔案內容 每筆交易資料
			        for($j=1;$j<count($easyFile)-1;$j++)
			        {
			            //特店代碼
			            $easy_storecode = "0".(substr($easyFile[$j],367,9)*1);
			            //測試 解析特店代碼 16倒置轉10 (6byte->3byte)--------------------先確認明碼是否跟hex16一樣
			                $easy_storecode16_ori = substr($easyFile[$j],202,6);
			                //拆解字串，前後不帶空白 PREG_SPLIT_NO_EMPTY
			                $easy_codeAry = preg_split('//',$easy_storecode16_ori,-1,PREG_SPLIT_NO_EMPTY); 
			                //16進位倒置
			                $easy_storecode16 = $easy_codeAry[4].$easy_codeAry[5].$easy_codeAry[2].$easy_codeAry[3].$easy_codeAry[0].$easy_codeAry[1];
			                $newStorecode10 = base_convert($easy_storecode16,16,10);
			            //----------------------------------------------------------------
			            //交易代號 811599 購貨 ，是購貨再紀錄，其他不要
			            $transCode = substr($easyFile[$j],78,6);
			            if($transCode =='811599') 
			            {
			                //====================
			                    // $ncname = $this->ncccStorename();
			                    // echo "日期:".substr($easyFile[$j],406,8)."  ";
			                    // echo "時間:".substr($easyFile[$j],414,6)."  ";

			                    // echo "門市:".$easyAccountInfo[$easy_storecode]['storename']."  ";
			                    // echo "店編:".$easyAccountInfo[$easy_storecode]['storecode']."  ";

			                    // echo "交易處理代碼:".substr($easyFile[$j],78,6);
			                    // echo "特店待號:".$easy_storecode."  "; //靠左補0

			                    // echo "卡號:"."0".(substr($easyFile[$j],3,17)*1)."  ";//靠左補0
			                    // echo "消費金額:".(substr($easyFile[$j],62,8)*1)."  " ;
			                    // //memo
			                    // echo "消費前餘額:".(substr($easyFile[$j],46,8)*1);
			                    // echo "消費後餘額:".(substr($easyFile[$j],70,8)*1);
			                    // echo "<br>";
			                //====================
			                $pdate = substr($easyFile[$j],406,8); //消費日期
			                $ptime = substr($easyFile[$j],414,6); //消費時間
			                $storename = $easyAccountInfo[$easy_storecode]['storename']; //店名
			                $storecode = $easyAccountInfo[$easy_storecode]['storecode']; //店編
			                $easy_cardNum = "0".(substr($easyFile[$j],3,17)*1); //悠遊卡卡號
			                $price = (substr($easyFile[$j],62,8)*1); //消費價格
			                $opt1 = '1';
			                $memo = "特店代號16:".$newStorecode10."消費前餘額:".(substr($easyFile[$j],46,8)*1)."消費後餘額:".(substr($easyFile[$j],70,8)*1);
			                //SQL串接
			                $easyInsertSql .="('$pdate','$storename','$storecode','$easy_storecode','$easy_cardNum','$price','$ptime','$opt1','$memo'),";
			                $n ++;
			                $tit ++;
			            }
			            //資料太多的話，分段執行寫入
			            if($n > 2000)
			            {
			                $easyInsertSql = substr($easyInsertSql,0,-1);
			                Yii::app()->localEIP->createCommand($easyInsertSql_INTO.$easyInsertSql)->execute(); //
			                $easyInsertSql ="";
			                $n = 0;
			            }
			        }
			        //有資料再作寫入
			        if($n != 0)
			        {
			            //SQL去掉尾巴逗號
			            $easyInsertSql = substr($easyInsertSql,0,-1);
			            Yii::app()->localEIP->createCommand($easyInsertSql_INTO.$easyInsertSql)->execute(); 
			            $easyInsertSql ="";
			        }
			        $logMsg = "手動上傳【".$easy_date." 悠遊卡】總計交易筆數:".$tit;
			        // echo $logMsg;
			        // 傳送LINE NOTIFY
			            $auth = 'yjtdD9vd1KLwlI42bsEMFriiIZvfebliZCOGdqnpBjr';
			            // $this->lineNotify($auth,$logMsg);
			        //刪除暫存資料
			            unlink('tmp/TRNC_file_'.$easy_date);

				// }
				//SQL去掉尾巴逗號
				// $easyInsertSql = substr($easyInsertSql,0,-1);
				// Yii::app()->localEIP->createCommand($easyInsertSql_INTO.$easyInsertSql)->execute(); 
				// echo $n;
		}

		Yii::app()->user->setflash('success', $logMsg);

		// CVarDumper::dump($_POST,10,true);
		//計時
		$time_end = microtime(true);
		$computetime = substr($time_end - $time_start,0,5);

		$this->render('easy_card_Upload',array(
			'logMsg'=>$logMsg,
			'easy_date'=>$easy_date,
			'show_date'=>$show_date,
			'computetime'=>$computetime

		));
	}
	//一卡通手動上傳 SFTP
	public function actionIpass_card_Upload()
	{
		// error_reporting(E_ALL);
		$time_start = microtime(true); //計時

		$downloadDate = date('Ymd',strtotime($_POST['easy_date'])); //下載日
		$show_date = date('Y-m-d',strtotime($_POST['easy_date'])); //view日期

		if(isset($_POST['sub']))
		{

			$logMsg = "";
			$transTitNum = 0;

			//查詢ipass_sftp帳號密碼
			    $sftp_ac_sql = "SELECT * FROM tbs_ipass_account WHERE opt1 = '1'"; //有啟用的門市
			    $sftp_ac_ary = Yii::app()->localEIP->createCommand($sftp_ac_sql)->queryAll(); //eip 正式機
			    // CVarDumper::dump($sftp_ac_ary,10,true);
			//SFTP資料
			    $host = 'sftp.i-pass.com.tw';
			    $port = 2222;
			    $remoteDir = "/fromipass/"; //SFTP檔案位置
			    $localDir = 'tmp/'; //存放位置
			$fileList = array(); //檔案清單

			//每間門市登入SFTP抓資料
			$error_store = ''; //錯誤紀錄
			$success_store = '';
			for($i=0;$i<count($sftp_ac_ary);$i++)
			{
			    //SFTP連線用--
			        $username = $sftp_ac_ary[$i]['sftp_id'];
			        $password = $sftp_ac_ary[$i]['sftp_pwd'];
			        $connection = ssh2_connect("$host", $port);
					//加上@避免執行，先確認sftp能否正常連線
			        $sftp_connect = @ssh2_auth_password($connection, "$username", "$password");
					if(!$sftp_connect)
					{
						$error_store .= "<br>".$sftp_ac_ary[$i]['storename']."__sftp失敗";
					}
					else
					{
						$sftp = ssh2_sftp($connection);
						$sftpPath= intval($sftp); //轉型

						//列出SFTP位置全部檔案
							$files = scandir("ssh2.sftp://".$sftpPath.$remoteDir);
							// $fileList = array(); //檔案清單
						//下載並把ZIP檔案加進array
						for($j=0;$j<count($files);$j++)
						{
							$fileEX = explode('EVCR_',$files[$j]); //EVCR 明細帳
							// CVarDumper::dump($fileEX,10,true);
							//檔案類型 日期 檔案格式
							if($fileEX[0] == "" && substr($fileEX[1],8,8) == "$downloadDate" && substr($files[$j],-3) == "ZIP" )
							{
								array_push($fileList,$files[$j]);
								// $fileList[$sftp_ac_ary[$i]['storename']][$j] = $files[$j];
								//下載檔案
									$stream = fopen("ssh2.sftp://$sftpPath$remoteDir"."$files[$j]", 'r');
									if (! $stream) {
											// throw new \Exception("Could not open file: $files[$j]");
											echo "Could not open file: $files[$j]";
										}
									
									$contents = stream_get_contents($stream);
									file_put_contents ("./tmp/".$files[$j], $contents);
									fclose($stream);
							}
							
						}
					}
			}
			//解壓縮ZIP檔案
			$zip = new ZipArchive;
			//讀取path底下的zip檔案，並解壓縮
			$zipfiles = glob("tmp/*.ZIP");

			$sucsZip = 0; //成功解壓縮數
			$errZip = 0; //失敗解壓縮數
			//有檔案再執行
			if(count($zipfiles)>0) 
			{
			    //解壓縮
			    for($i=0;$i<count($zipfiles);$i++)
			    {
			        if($zip->open("$zipfiles[$i]") === TRUE)
			        {
			            // $zipFileName = explode("(NCCC)",$files[$i]); //檔案分解命名
			            // CVarDumper::dump($getNcccCode,10,true);
			            $zip->extractTo('./tmp/');
			            $zip->close();
			            // echo "success!";
			            $sucsZip ++;
			        }
			        else
			        {
			            // echo "Error";
			            $errZip ++;
			        }	
			    }
				//讀取解壓縮後的檔案並上傳交易明細資料
				$datFiles = glob("tmp/*.DAT"); //(每日交易明細)
				// CVarDumper::dump($datFiles,10,true);
				$insertIpass_sql = "INSERT INTO tit_ipass_pay_log(pdate,storename,storecode,ipass_storecode,ipass_type,ipass_CardNum,price,ptime,opt1) VALUES ";
				for($i=0;$i<count($datFiles);$i++)
				{
					$datAry = file($datFiles[$i]); //將檔案內容轉為array
					//解析檔案資料 - title
					$dat_ipass_storecode = substr($datAry[0],6,8);
					$dat_StoreAry = $this->getIpassData();
					$dat_storename = $dat_StoreAry[$dat_ipass_storecode]['storename'];
					$dat_storecode = $dat_StoreAry[$dat_ipass_storecode]['storecode'];
					//讀取交易明細資料,第二行 到 倒數第二行 (第一行為title;最後一行為交易總筆數)
					for($j=1;$j<count($datAry)-1;$j++)
					{
						// CVarDumper::dump($datAry[$j],10,true);
						$pdate = substr($datAry[$j],127,8); //交易日
						$ptime = substr($datAry[$j],135,6); //交易時間
						$price = substr($datAry[$j],163,6)*1; //交易金額
						$ipass_type = substr($datAry[$j],175,2); //ipass種類 04一般一卡通 02聯名卡(?)
						$ipass_cardNum = substr($datAry[$j],67,8); //ipass卡號
						// echo $dat_storename."  ".$dat_storecode."   ".$pdate."   ".$ptime."   ".$price."  ".$ipass_type."  ".$ipass_code."<br>";
						//串成sql
						$transTitNum++; //總交易票數++
						$insertIpass_sql .= "('$pdate','$dat_storename','$dat_storecode','$dat_ipass_storecode','$ipass_type','$ipass_cardNum','$price','$ptime','1'),";
					}
					//門市&&票數 MsgAry
					$tit = substr($datAry[count($datAry)-1],1,8)*1; 
					$dat_data[$i]['storename'] = $dat_storename;
					$dat_data[$i]['tit'] = $tit;
				}

				//刪除檔案
				//執行完畢後刪除檔案
				$zipDelete = glob("tmp/*.ZIP"); 
				foreach($zipDelete as $delzip)
				{
					unlink($delzip);
				}
				$datDelete = glob("tmp/*.DAT"); 
				foreach($datDelete as $deldat)
				{
					unlink($deldat);
				}
			}
			
			//送信、通知
			$logMsg = "手動上傳【".$downloadDate." 一卡通】Zip files : ".count($fileList)."票數:".$transTitNum . $error_store;
			// 傳送LINE NOTIFY
			    // $auth = 'yjtdD9vd1KLwlI42bsEMFriiIZvfebliZCOGdqnpBjr';
			    // $this->lineNotify($auth,$logMsg);
			//上傳帳務資料 (有資料才做上傳)
			if($transTitNum>0)
			{
			    $insertIpass_sql = substr($insertIpass_sql,0,-1); //去掉後面","
			    Yii::app()->localEIP->createCommand($insertIpass_sql)->execute(); //db2 正式機
			}
			
			Yii::app()->user->setflash('success', $logMsg);

		}

		//計時
		$time_end = microtime(true);
		$computetime = substr($time_end - $time_start,0,5);

		$this->render('ipass_card_Upload',array(
			'show_date'=>$show_date,
			'logMsg'=>$logMsg,
			'computetime'=>$computetime,
		));
	}
	//信用卡手動上傳 email
	public function actionNccc_Upload()
	{
		$time_start = microtime(true); //計時
		$ncccStoreFilecode = array(); //檢查用，zip列表
		$ncccStoreTxtcode = array(); //檢查用，txt列表

		$downloadDate = date('j-F-Y',strtotime($_POST['easy_date'])); //下載日
		$show_date = date('Y-m-d',strtotime($_POST['easy_date'])); //view日期
		$msgLog = '';
		if(isset($_POST['sub']))
		{
			$creditAttAry = array();

			if (! function_exists('imap_open')) {
				echo "IMAP is not configured.";
				$msgLog = 'IMAP is not configured';
				exit();
			} else {
			// 取得登入帳號資料 , 需做加密
			$getNccc_roots = $this->getRootAccount('nccc');
			//信用卡下載 /tmp/
				// $hostname = '{imap.gmail.com:993/imap/ssl/novalidate-cert}Inbox';
				$hostname = '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX';
					// 2022.06.05後改版後須使用應用程式密碼
			        $username = $getNccc_roots[0]['email'];
					$password = $getNccc_roots[0]['password']; 
				/* try to connect */
				$connection = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());
				/* Search Emails having the specified keyword in the email subject */
				$emailData = imap_search($connection,"FROM estore_1@nccc.com.tw ON $downloadDate");
				// $emailData = imap_search($connection,'FROM "jitmis2020@gmail.com" SINCE "10-August-2020" BEFORE "15-August-2021"');
			    //如果有信件才執行
				if (! empty($emailData)) {
					foreach ($emailData as $emailIdent) {
					
					$overview = imap_fetch_overview($connection, $emailIdent, 0);
					$message = imap_fetchbody($connection, $emailIdent,'1.1');
					$structure = imap_fetchstructure($connection,$emailIdent); 
					$messageExcerpt = substr($message, 0, 150);
					$partialMessage = trim(quoted_printable_decode($messageExcerpt)); 
					$date = date("Ymd", strtotime($overview[0]->date));
					
						$subjectCredit = imap_utf8($overview[0]->subject);
						// CVarDumper::dump($overview,10,true);
						// echo "<br>".$subjectCredit.$downloadDate."<br>";
						array_push($creditAttAry,$subjectCredit.$downloadDate."-".$overview[0]->msgno);
						// CVarDumper::dump($creditAttAry,10,true);
					$attachments = array(); 
					if(isset($structure->parts) && count($structure->parts)) 
						{ 
							for($i = 0; $i < count($structure->parts); $i++) 
							{ 
								$attachments[$i] = array(
									'is_attachment' => false, 
									'filename' => '', 
									'name' => '', 
									'attachment' => '' 
								); 

								if($structure->parts[$i]->ifdparameters) 
								{ 
									foreach($structure->parts[$i]->dparameters as $object) 
									{ 
									if(strtolower($object->attribute) == 'filename') 
										{ 
											$attachments[$i]['is_attachment'] = true; 
											$attachments[$i]['filename'] = $object->value; 
										} 
									} 
								} 
								if($attachments[$i]['is_attachment']) 
								{ 
									$attachments[$i]['attachment'] = imap_fetchbody($connection,$emailIdent, $i+1); 
									/* 3 = BASE64 encoding */ 
									if($structure->parts[$i]->encoding == 3) 
									{ 
										$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']); 
									} 
									/* 4 = QUOTED-PRINTABLE encoding */ 
									elseif($structure->parts[$i]->encoding == 4) 
									{ 
										$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']); 
									} 
								} 
							} 
						} 

						/* iterate through each attachment and save it */ 
						foreach($attachments as $key=>$attachment) 
						{ 
							if($attachment['is_attachment'] == 1) 
							{ 
								$filename = $attachment['name']; 
								$downloadName = imap_utf8($attachment['name']);
								if(empty($filename)) $filename = $attachment['filename']; 
								if(empty($filename)) $filename = time() . ".dat"; 
								/* prefix the email number to the filename in case two emails 
								* have the attachment with the same file name. 
								*/
								// $pathfilename =  "./attachment/" .'(NCCC)'.$subject.$date.'-'.$emailIdent.'.xls';
								// $pathfilename =  "./attachment/" .'(NCCC)'.$subjectCredit.$date.'-'.$emailIdent.'.zip';
			                    $checkFileName = substr($attachment['filename'],0,3); //確認檔案名稱;REC = 電子收據暨對帳單通知 PDF檔案
			                    if (!is_dir("./tmp/")) {
									mkdir("./tmp/", 0777, true);
								}
			                    if($checkFileName != 'REC')
			                    {
			                        $pathfilename =  "./tmp/" .'(NCCC)'.$attachment['filename']; //檔名(特店代號)
			                        $pathfilename = $this->convertEncoding($pathfilename); //下載路徑中文
			                        $fp = fopen($pathfilename, "w+"); // "-" . imap_utf8($filename) 
			                        fwrite($fp, $attachment['attachment']); 
			                        fclose($fp); 
			
			                        $attachments[$key]['filename'] = $filename; 
			                    }
			                    else
			                        echo "(REC File)";
							} 
						}  
				} // End foreach
			        // echo "(".$downloadDate.")download_success! ".count($attachment)." Files ";
			            // $emailTransLog .= "【".$downloadDate."NCCC】 下載通知 ".count($attachment)." Files"."\n";
				} // end if 
				imap_close($connection);
			//
			//自動讀取ZIP檔案 信用卡 上傳、通知
				$zip = new ZipArchive;
				//取得NCCC帳戶資料;解壓縮密碼 帳號末四碼
				$getNcccCode = $this->getNcccData();
				//讀取path底下的zip檔案，並解壓縮
				$files = glob("tmp/*.zip");
				if(count($files)>0) //有檔案再執行
				{
					$sucsZip = 0; //成功解壓縮數
					$errZip = 0; //失敗解壓縮數
					//解壓縮
					for($i=0;$i<count($files);$i++)
					{
						if($zip->open("$files[$i]") === TRUE)
						{

			                $zipFileName = explode("(NCCC)",$files[$i]); //檔案分解命名
			                $checkREC = substr("$zipFileName[1]",0,3); //確認檔案名稱;REC = 電子收據暨對帳單通知 PDF檔案
			                if($checkREC != 'REC')
			                {
			                    $zipNcccCode = substr("$zipFileName[1]",0,10); //取得檔案特店代號
			                                // echo $zipNcccCode;
			                        //取得解壓縮密碼 帳號末四碼
			                        $ncccPW = $getNcccCode[$zipNcccCode]['account'];
			                        $ncccPW = substr("$ncccPW",-4); 
			                    // CVarDumper::dump($getNcccCode,10,true);
			                    // echo $zipNcccCode.$getNcccCode[0]['storecode']."pw:".$ncccPW."<br>";
									//加入array檢查用
									$ncccStoreFilecode[$i]['file'] = $zipNcccCode;
									$ncccStoreFilecode[$i]['PW'] = $ncccPW;
			                    $zip->setPassword("$ncccPW");
			                    $zip->extractTo('./tmp/');
			                    $zip->close();
			                    // echo "success!";
			                    $sucsZip ++;
			                }
						}
						else
						{
							// echo "Error";
							$errZip ++;
						}
							
					}
					//讀取解壓縮完後的檔案txt
			        $txtFiles = glob("tmp/D*.txt"); //讀D資料(每日交易明細)
			        $transDataNum = 0; //當日交易筆數
			        if(count($txtFiles)>0) //如果有檔案的話再執行
			        {
			            //交易資料串成sql
			            $tit_nccc_sql = "INSERT INTO tit_nccc_pay_log(pdate,storecode,storename,nccc_code,store_Mcode,payNum,authNum,cardNum,price) VALUES ";
			            for($i=0;$i<count($txtFiles);$i++)
			            {
			                //獲取檔名
			                $txtFileName = explode("/",$txtFiles[$i]);
			                $txtAry = file($txtFiles[$i]); //將檔案內容轉為array
			                for($j=8;$j<count($txtAry);$j++)
			                {
			                    //取得該檔案特店代號
			                        $txtNcccCodeAry = explode(" ",$txtAry[3]);
			                        $txtNcccCode = substr("$txtNcccCodeAry[1]",1);
									//檢查用
									array_push($ncccStoreTxtcode,$txtNcccCode);
			                    //取得特店代號資料 DB:tbs_fb_account 
			                        $storecode = $getNcccCode[$txtNcccCode]['storecode'];
			                        $storename = $getNcccCode[$txtNcccCode]['storename'];
			                    //拆解每筆交易明細
			                        $txtChargeAry = explode(" ",$txtAry[$j]);
			                        if(count($txtChargeAry) == 29) //資料拆解後ary數量，一般交易為29ㄍ，退刷為36ㄍ
			                        {
			                            $transDataNum ++;
			                            $txtMcode = $txtChargeAry[0]; //端末機號
			                            $txtPayNum = $txtChargeAry[8]; //交易序號(票卷號碼)
			                            $txtCardNum = $txtChargeAry[10]; //交易卡號
			                            $txtPdate = "20".$txtChargeAry[15]; //交易日期
			                            $txtAuthNum = $txtChargeAry[17];//授權碼
			                            $txtPrice = $txtChargeAry[20]*1; //交易金額
			                            // CVarDumper::dump($txtChargeAry,10,true);
			                            // echo "<br>(".$storecode.")"."(".$txtNcccCode.")"."(".$txtMcode.")"."(".$txtCardNum.")"."(".$txtPdate.")"."(".$txtPrice.")<br>";
			                            $tit_nccc_sql .= "('$txtPdate','$storecode','$storename','$txtNcccCode','$txtMcode','$txtPayNum','$txtAuthNum','$txtCardNum','$txtPrice'),";
									}
									if(count($txtChargeAry) ==30) //美國運通卡15碼ary數量30
									{
										$transDataNum ++;
										$txtMcode = $txtChargeAry[0]; //端末機號
										$txtPayNum = $txtChargeAry[8]; //交易序號(票卷號碼)
										$txtCardNum = $txtChargeAry[10]; //交易卡號
										$txtPdate = "20".$txtChargeAry[16]; //交易日期
										$txtAuthNum = $txtChargeAry[18];//授權碼
										$txtPrice = $txtChargeAry[21]*1; //交易金額
										$tit_nccc_sql .= "('$txtPdate','$storecode','$storename','$txtNcccCode','$txtMcode','$txtPayNum','$txtAuthNum','$txtCardNum','$txtPrice'),";
									}
			                }
						
			            }
			            //執行完畢後刪除檔案
			                $zipDelete = glob("tmp/*.zip"); 
			                foreach($zipDelete as $delzip)
			                {
			                    unlink($delzip);
			                }
			                $txtDelete = glob("tmp/*.txt"); 
			                foreach($txtDelete as $deltxt)
			                {
			                    unlink($deltxt);
			                }
			            //訊息提示
			                // echo "; ZIP success:".$sucsZip."fails:".$errZip."\n";
			            //資料上傳
							if($transDataNum>0)
							{
								$tit_nccc_sql =substr($tit_nccc_sql,0,-1);
								Yii::app()->localEIP->createCommand($tit_nccc_sql)->execute(); //db2 正式機
							}
			            //通知
			                $msgLog = "【上傳通知】 門市數量 : ".$sucsZip." 失敗:".$errZip."\n"."總交易筆數:".$transDataNum;
			        }
			        else
			        {
			            $msgLog = " no Nccc dailyData ";			   
			        }
			        // 傳送LINE NOTIFY
			        // $auth = 'yjtdD9vd1KLwlI42bsEMFriiIZvfebliZCOGdqnpBjr';
			        // $this->lineNotify($auth,$emailTransLog);

				}
				else
				{
					$msgLog = '當日沒有帳務檔案';
				}
			//
			} //外
			Yii::app()->user->setflash('success', $msgLog);
			// CVarDumper::dump($ncccStoreFilecode,10,true);
			// CVarDumper::dump($ncccStoreTxtcode,10,true);

		}
		//檢查txt檔案
		if(isset($_POST['sub2']))
		{
			// $txt = $_FILES;
			// CVarDumper::dump($txt,10,true);
			// $txtA = file_get_contents($_FILES['txtcheck']['tmp_name']); //讀取TXT檔案
			$txtAry = file($_FILES['txtcheck']['tmp_name']); //轉為array
			// CVarDumper::dump($txtAry,10,true);
			$txtData = 0;
			for($j=8;$j<count($txtAry);$j++)
			{
				//拆解每筆交易明細
					$txtChargeAry = explode(" ",$txtAry[$j]);
					CVarDumper::dump(count($txtChargeAry),10,true);
					if(count($txtChargeAry) == 29 ) //資料拆解後ary數量，一般交易為29ㄍ，退刷為36ㄍ
					{
						$txtMcode = $txtChargeAry[0]; //端末機號
						$txtPayNum = $txtChargeAry[8]; //交易序號(票卷號碼)
						$txtCardNum = $txtChargeAry[10]; //交易卡號
						$txtPdate = "20".$txtChargeAry[15]; //交易日期
						$txtAuthNum = $txtChargeAry[17];//授權碼
						$txtPrice = $txtChargeAry[20]*1; //交易金額
						$txtData ++;
					}
					if(count($txtChargeAry) ==30) //美國運通卡15碼ary數量30
					{
						$txtMcode = $txtChargeAry[0]; //端末機號
						$txtPayNum = $txtChargeAry[8]; //交易序號(票卷號碼)
						$txtCardNum = $txtChargeAry[10]; //交易卡號
						$txtPdate = "20".$txtChargeAry[16]; //交易日期
						$txtAuthNum = $txtChargeAry[18];//授權碼
						$txtPrice = $txtChargeAry[21]*1; //交易金額
						$txtData ++;
					}
					echo $txtMcode."__".$txtPayNum."__".$txtCardNum."__".$txtPdate."__".$txtAuthNum."__".$txtPrice."<br>";

			}
			CVarDumper::dump($txtData,10,true);



		}

		//計時
		$time_end = microtime(true);
		$computetime = substr($time_end - $time_start,0,5);

		$this->render('nccc_Upload',array(
			'show_date'=>$show_date,
			'msgLog'=>$msgLog,
			'computetime'=>$computetime,
		));
	}
	// 取得root登入帳號資料
	private function getRootAccount($type)
	{
		$root = isset($type)?$type:'';
		switch($root)
		{
			case 'nccc':
				$qryAccountName = 'NcccRoot';
				break;
			case 'easy':
				$qryAccountName = 'easyCard';
				break;
			default:
				$qryAccountName = '';
				break;
		}
		if($qryAccountName != '')
		{
			$sql = "SELECT * FROM tbs_Root_EmailAccount WHERE name = '$qryAccountName' and opt1 = '1' ";
			$accountAry = Yii::app()->localEIP->createCommand($sql)->queryAll();
		}
		else
			$accountAry = array();

		return $accountAry;
	}
	//PDF套件
	public function tcpdf_include($cssHtml,$mainAry)
	{
		require_once(Yii::app()->basePath."/extensions/TCPDF-main/tcpdf.php");
		// PDF設定-------------------------------------------------------
		//頁面方向（P =直，L =橫）、測量（mm）、頁面格式
		// PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false
		$pdf = new TCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
		$pdf->SetMargins(15, 15, 15); //邊界 右 上 左
		$pdf->setPrintHeader(false); //不要頁首
		// $pdf->setPrintFooter(false); //不要頁尾
		$pdf->SetFont('msungstdlight', '', 12, '', true); //設定字型 (字體、樣式、大小、字型檔、文字子集)
		//設定自動頁尾
		// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// $pdf->setFooterData(array(0,64,0), array(0,64,0));
		//設定密碼
		// $pdf->SetProtection(array('print', 'copy','modify'), "1234", "ourcodeworld-master", 0, null);

		//內容------------------------------------------------------------
		// $pdf->writeHTML($cssHtml, true, false, false, false, '');

		$button = '<input type="reset" class="btn btn-outline-info" name="reset" value="Reset" />
					<input type="submit" class="btn btn-outline-info" name="submit" value="Submit" />
					<input type="button" class="btn btn-outline-info" name="print" value="Print" onclick="print()" />
					<input type="hidden" class="btn btn-outline-info" name="hiddenfield" value="OK" />';
		// set style for barcode
		$style = array(
			'border' => 2,
			'vpadding' => 'auto',
			'hpadding' => 'auto',
			'fgcolor' => array(0,0,0),
			'bgcolor' => false, //array(255,255,255)
			'module_width' => 1, // width of a single module in points
			'module_height' => 1 // height of a single module in points
		);

		foreach($mainAry as $v=>$main)
		{
			$pdf->AddPage(); //新增頁面
			$code = "pdf123123";
			// $footer = '<div class = "divclass">page '.($v+1).'/'.count($mainAry).'</div>';
			$footerTxt = 'page '.($v+1).'/'.count($mainAry);

			$pdf->writeHTML($cssHtml.$main, true, false, false, false, '');
			// QRCODE,L : QR-CODE Low error correction
			$pdf->write2DBarcode('www.tcpdf.org', 'QRCODE,L', 20, 125, 25, 25, $style, 'N');
			$pdf->Text(20, 150, 'QRCODE L');
			$pdf->Text(140, 180, $footerTxt); //頁尾

			// $pdf->writeHTML($button, true, false, false, false, '');

		}
	
		// $pdf->lastPage();

		///
		//


		// $pdf->AddPage(); //新增頁面
		//產出
		$pdf->Output('contact.pdf', 'D');

		// CVarDumper::dump($pdf,10,true);	

	}
	/**
     * 依年月直接取得該年月內所有日期的假日
     * @param String $daymonth - 年月, 如: 201405
     * @return Array - array('20140501'=>true, '20140502'=>false ... )
     */
    public function getHolidayByYearMonth($daymonth) {

        $dates = array();

        $aStart = $this->getTheFirstDayByYearMonth($daymonth, "Ymd");
        $aEnd = $this->getTheLastDayByYearMonth($daymonth, "Ymd");

        for ($i = 0; ($aStart + $i) <= $aEnd; $i++) {
            array_push($dates, $aStart + $i);
        }

        return $this->getHolidayByDates($dates);
    }
	public function getTheFirstDayByYearMonth($daymonth, $format) {

        // 切割出年份
        $tmp_year = substr($daymonth, 0, 4);
        // 切割出月份
        $tmp_mon = substr($daymonth, 4, 2);

        return date($format, mktime(24, 0, 0, $tmp_mon, 0, $tmp_year));
    }
	public function getTheLastDayByYearMonth($daymonth, $format) {

        // 切割出年份
        $tmp_year = substr($daymonth, 0, 4);
        // 切割出月份
        $tmp_mon = substr($daymonth, 4, 2);

        return date($format, mktime(0, 0, 0, $tmp_mon + 1, 0, $tmp_year));
    }
	function getHolidayByDates($dates) {

        $_arr = array(); //回傳日期是否為假日的陣列
        // '2014-01-01','2014-01-02'  array轉成字串  
        if (count($dates) > 0) {
            $_str = "'" . implode("','", $dates) . "'"; //每個日期加入單引號為了sql in查詢使用
            //從DB判斷日期是否存在            
            $sql = "SELECT holiday from tba_holiday where holiday  IN ($_str) ORDER BY holiday ASC ";

            $result = Yii::app()->db->createCommand($sql)->queryAll();

            //將雙層array轉成單層array()
            $_array = array();
            for ($i = 0; $i < count($result); $i++) {
                array_push($_array, $result[$i]['holiday']);
            }

            //假日true，平常日false
            for ($i = 0; $i < count($dates); $i++) {

                $_arr[$dates[$i]] = in_array($dates[$i], $_array);
            }
        }
        return $_arr;
    }
	//array取中位數
	function getMedian($numbers)
	{
		sort($numbers);
		$totalNumbers = count($numbers);
		$mid = floor($totalNumbers / 2);

		return ($totalNumbers % 2) === 0 ? ($numbers[$mid - 1] + $numbers[$mid]) / 2 : $numbers[$mid];
	}
	//array取眾數
	function getModeNum($numbes)
	{
		// $modeAry = array();
		// //計算每個值出現次數
		// foreach($numbes as $num)
		// {
		// 	if(!isset($modeAry[$num]))
		// 		$modeAry[$num] = 0;
		// 	$modeAry[$num] ++;
		// }
		// //放置array做分析
		// $modeList = array(); //key
		// $modeList2 = array(); //次數
		// foreach($modeAry as $m=>$v)
		// {
		// 	array_push($modeList,$m);
		// 	array_push($modeList2,$v);
		// }
		// //分析眾數
		// for($i = 0;$i<count($modeList);$i++ )
		// {
		// 	if(!isset($maxV)){
		// 		$maxV = $modeList2[$i] ;
		// 	}
		// 	if(isset($modeList2[$i+1]))
		// 	{
 		// 		if($modeList2[$i+1]>$maxV)
		// 		{
		// 			$maxV = $modeList[$i+1];
		// 		}
		// 	}
		// }

		//php方式************************
		//計算出現次數
		$arys = array_count_values($numbes);
		//取array中最大值
		$maxV = array_search(max($arys), $arys);

		return $maxV;
	}
	//星期對照
	function getWeek()
	{
		$weekAry = array(
			1 => '星期一',
			2 => '星期二',
			3 => '星期三',
			4 => '星期四',
			5 => '星期五',
			6 => '星期六',
			0 => '星期日'
		);
		return $weekAry;
	}
	function actionSweetPost()
	{
		$test1 = '1';
        print json_encode($test1);
	}

	//AJAX測試用
	function actionAjaxTest()
	{
		// CVarDumper::dump("@3",10,true);
		$testPOST = $_POST;
		// $yiiPOST = Yii::app()->request->getPost('testvalue');
		$testvalue = isset($_POST['testvalue'])?$_POST['testvalue']:"沒有傳遞值";
		// var_dump("23123");
		// $ary = array('123',"abc","ABC");
		$result = "asyncType為".$testvalue;
		
		// SQL測試
		// $sql = "select * from Tbs_storeD";
		// $result = yii::app()->localEIP->createCommand($sql)->queryAll();

		print json_encode($testPOST);
		// return $result;

	}
	//AJAX取得氣溫 (
	function actionAjaxWeather()
	{
		$city = $_POST['city'];
		$nowTime = date("A");
		switch($nowTime)
		{
			case "AM":
				$nowStatus = 0;
				break;
			case "PM":
				$nowStatus = 1;
				break;
		}
		$weatherURL = "https://opendata.cwb.gov.tw/fileapi/v1/opendataapi/F-C0032-001?Authorization=rdec-key-123-45678-011121314&format=JSON";
		$weatherJson = json_decode(file_get_contents($weatherURL));
		$location = $weatherJson->cwbopendata->dataset->location;
		// 資料整理
		$kao = $location[$city]; //城市
		$cityName = $kao->locationName;
		$weatherStatus = $kao->weatherElement[0]->time[$nowStatus]->parameter->parameterName;
		$tempMax = $kao->weatherElement[1]->time[$nowStatus]->parameter->parameterName;
		$tempMin = $kao->weatherElement[2]->time[$nowStatus]->parameter->parameterName;
		$tempFeel = $kao->weatherElement[3]->time[$nowStatus]->parameter->parameterName;
		$rainPOP = $kao->weatherElement[4]->time[$nowStatus]->parameter->parameterName;

		$weatherMsg =$cityName.
		"\n"."氣候狀態:".$weatherStatus.
		"\n"."溫度區間:".$tempMin."°C~".$tempMax."°C".
		"\n"."體感狀態:".$tempFeel;

		// return $weatherMsg;
		print json_encode($weatherMsg);

	}
	 //**回推6個月清單 */
	 public function getSixMonthList($qry_month)
	 {
		 $List_6month = array();
		 $pdate = date("Ym",strtotime($qry_month));
		 array_push($List_6month,$pdate);
		 for($i = 1;$i<6;$i++)
		 {
			 $month_back = "-".$i." month";
			 $month_back_add = date('Ym',strtotime($month_back,strtotime($qry_month)));
			 array_push($List_6month,$month_back_add);
		 }
		 $List_6month = array_reverse($List_6month); // 反轉>>小~大
		 return $List_6month;
	 }
	 //ajax Git GitLab版控commit最近的5個版本
	 public function actionAjaxGitCommit()
	 {
		// get Git Commit
		$gitUrl = "https://gitlab.com/s15631102/mis07/-/commits/master?feed_token=GGwyNKtdMTbB1-9VSHkh&format=atom";
		$gitJson = simplexml_load_file($gitUrl);
		$gitCommit = $gitJson->entry;
		$gitArray = array();
		for($i=0;$i<5;$i++)
		{
				$commit = $gitCommit[$i]->title;
				$author = $gitCommit[$i]->author->name;
				$date = $gitCommit[$i] ->updated;
				$gitArray[$i]['commit'] = $commit;
				$gitArray[$i]['author'] = $author;
				$gitArray[$i]['date'] = explode("T",$date)[0];
		}

		print json_encode($gitArray);

	 }

	//** AJAX取得區域 (非AJAX用)*/
	public function actionAjaxArea()
	{
		$sql = "SELECT * FROM tbs_area WHERE opt1 = '1' and depart_id = '3' ORDER BY opt3";
		$areaAry = yii::app()->localEIP->createCommand($sql)->queryAll();
		$result = array();
		foreach($areaAry as $a)
		{
			$result[$a['id']] = $a;
		}

		return $result;
		// print json_encode($result);
	}
	public function actionAjaxArea2()
	{
		$sql = "SELECT * FROM tbs_area WHERE opt1 = '1' and depart_id = '3' ORDER BY opt3";
		// $sql = "SELECT * FROM tbs_store WHERE opt1 = '1'";

		$areaAry = yii::app()->localEIP->createCommand($sql)->queryAll();
		$result = array();
		foreach($areaAry as $a)
		{
			$result[$a['id']] = $a;
		}

		print json_encode($result);
	}
	//**AJAX 依據areaID取得門市資料 */
	public function actionAjaxGetStore()
	{
		$id = $_POST['area_id'];
		if(!isset($id))
			$result = '';
		else{
			$sql = "SELECT * FROM tbs_store WHERE area_id = '$id' and opt1 = '1'";
			$result = yii::app()->localEIP->createCommand($sql)->queryAll();
		}

		print json_encode($result);
	}
	// AJAX資料轉換JSON格式_門市資料(bs-table測試用)
	public function actionAjaxGetJsonData(){
		// 資料
		$sql = "SELECT * FROM tbs_store WHERE opt1 = '1'";
		$result = yii::app()->localEIP->createCommand($sql)->queryAll();
		$arrayList = $this->actionAjaxArea();
		$storeList_Json = array();
		$jsonAry = array();
		foreach($result as $r)
		{
			if($r['depart_id'] == '3')
			{
				$jsonAdd = array(
					"storecode"=> $r['storecode'],
					"storename"=> $r['storename'],
					"area_id"=> $arrayList[$r['area_id']]['areaname'],
					"地址"=>$r['address'],
					"電話"=>$r['tel1'],
					"電腦IP"=>$r['storeip1'],
					"售票機IP"=>$r['storeip2'],
					"監視器IP"=>$r['storeip3'],
					"刷卡機IP"=>$r['storeip4'],
					"memo"=>$r['memo'],
				);
				array_push($jsonAry,$jsonAdd);
			}
		}
		// JSON製作
		$storeList_Json = json_encode($jsonAry,JSON_UNESCAPED_UNICODE);
		//路
		$jPath = Yii::getPathOfAlias('webroot').'/images/';	//儲存路徑(外)
		$jsonDownload = file_put_contents($jPath."storeList.json",$storeList_Json);
		$downloadPath = $jPath."storeList.json";
		$herf = 'images/storeList.json';

		print json_encode($herf);

	}
	// 2022.5.12 AJAX 快篩試劑地圖資料JSON
	public function actionAjaxGetFstData(){
		//---快篩試劑---
		$fstAry = array();
		$fstData = fopen("https://data.nhi.gov.tw/resource/Nhi_Fst/Fstdata.csv", "r");
		$n = 0;
		while(!feof($fstData))
		{
			$aryAdd_part = fgetcsv($fstData);
			if($n != 0)
			{
				$address = mb_substr($aryAdd_part['2'],0,3,"utf-8");
				$jsonAdd = array(
					"0"=> $aryAdd_part['0'],
					"1"=> $aryAdd_part['1'],
					"2"=> $aryAdd_part['2'],
					"3"=> $aryAdd_part['3'],
					"4"=> $aryAdd_part['4'],
					"5"=> $aryAdd_part['5'],
					"6"=> $aryAdd_part['6'],
					"7"=> $aryAdd_part['7'],
					"8"=> $aryAdd_part['8'],
					"9"=> $aryAdd_part['9'],
					"10"=>$address, //縣市
				);
				array_push($fstAry,$jsonAdd);
			}
			$n++;
		}
		$fstJson = json_encode($fstAry,JSON_UNESCAPED_UNICODE);
		$jPath = Yii::getPathOfAlias('webroot').'/images/';	//儲存路徑(外)
		$jsonDownload = file_put_contents($jPath."fstMap.json",$fstJson);
		$downloadPath = $jPath."fstMap.json";
		$herf = 'images/fstMap.json';
		print json_encode($herf);

	} 
	// 2022.5.23 測試datatables巢狀陣列
	public function actionAreaDataAry()
	{
		$testGet = isset($_GET['testGet'])?$_GET['testGet']:'';
		$sql = "SELECT * FROM tbs_area WHERE opt1 = '1' and depart_id = '3' ORDER BY opt3";
		$areaAry = yii::app()->localEIP->createCommand($sql)->queryAll();
		$data = '';
		$result = array();
		foreach($areaAry as $a)
		{
			$data = array(
				'id'=>$a['id'],
				'areaCode'=>$a['areaCode'],
				'areaname'=>$a['areaname'],
				'empname1'=>$a['empname1'],
			);
			array_push($result,$data);
			// $result[$a['id']] = $a;
		}
		foreach($result as $k=>$r)
		{
			$result[$k]['info'] = $areaAry[$k];
		}

		// return $result;
		print json_encode($result);
	}
	// 取得門市資料 以門市店編對應
	public function getStoreInfo()
	{
		// 門市資料
		$storeListA = "SELECT * FROM tbs_store";
		$storeListARy = Yii::app()->localEIP->createCommand($storeListA)->queryAll();
		$storeListMatch = array();
		foreach($storeListARy as $aa)
		{
			$storeListMatch[$aa['storecode']] = $aa;
		}
		return $storeListMatch;
		
	}

	// menu_11 AJAX_STEP 檢查存在、確認新增
	public function actionMenu11AjaxStNew()
	{
		$serviceno = isset($_POST['serviceno'])?$_POST['serviceno']:'';
		$storeAry = isset($_POST['storeAry'])?$_POST['storeAry']:'';
		$st_serviceData = isset($_POST['st_serviceData'])?$_POST['st_serviceData']:'';
		$statusOpt = isset($_POST['statusOpt'])?$_POST['statusOpt']:'';
		$ctime = date("Y-m-d H:i:s");
		// 步驟類型 (1先檢查 2 確認新增)
		$step = isset($_POST['step'])?$_POST['step']:'';

		$result = '';

		
		// STEP 1 檢查 確認 2新增
		switch($step)
		{
			// 檢查 ->查出有該服務項目的3筆資料進行確認，後續才進行新增
			case 'n1':
				$checkStSql = "SELECT * FROM tbp_st_service WHERE serviceno = '$serviceno' limit 3";
				$checkSt = yii::app()->localEIP->createCommand($checkStSql)->queryAll();
				$result = $checkSt;
				break;
			// 沒有該服務項目的門市，進行新增
			case 'n2':
				// 先查出沒有該服務項目的門市
				$storeListSql = "SELECT storecode,storename FROM 
				( SELECT * FROM tbs_store WHERE opt1 = '1' and depart_id = '3') as a 
				WHERE storecode not in 
				( SELECT storecode FROM tbp_st_service WHERE serviceno = '$serviceno')";
				$storeList = yii::app()->localEIP->createCommand($storeListSql)->queryAll();
				// // 進行新增
				foreach($storeList as $s)
				{
					$instSql =  "INSERT INTO `tbp_st_service` ( `storecode`, `storename`, `serviceno`, `machineno`, `queue`, `memo`, `opt1`, `opt2`, `opt3`, `cemp`, `uemp`, `ctime`, `utime`, `ip`) 
					VALUES 
					('".$s['storecode']."', '".$s['storename']."', '".$st_serviceData[3]."', '".$st_serviceData[4]."', '".$st_serviceData[5]."', '".$st_serviceData[2]."', '0', 'N', 'N', 'sysInst', NULL, '".$ctime."', '0000-00-00 00:00:00', '60.249.143.208')";
					yii::app()->localEIP->createCommand($instSql)->execute();

				}


				$result = $storeList;

				break;
			//檢查 ->查出目前該服務項目的opt1狀態 
			case 'u1':
				$checkStSql = "SELECT * FROM tbp_st_service WHERE serviceno = '$serviceno' ";
				// 如果有輸入店編
				if($storeAry[0] != '')
				{
					// 處理店編列表
					$storecodeList = '';
					foreach($storeAry as $s)
					{
						$storecodeList .= "'".$s."',";
					}
					$storecodeList = substr($storecodeList,0,-1);

					$checkStSql .= " and storecode in ($storecodeList)";
				}
				$checkSt = yii::app()->localEIP->createCommand($checkStSql)->queryAll();
				$result = $checkSt;
				break; 
			//進行服務項目狀態更新 
			case 'u2':
				$updateStSql = "update tbp_st_service  SET opt1 = '$statusOpt' WHERE serviceno = '$serviceno' ";
				// 如果有輸入店編
				if($storeAry[0] != '')
				{
					// 處理店編列表
					$storecodeList = '';
					foreach($storeAry as $s)
					{
						$storecodeList .= "'".$s."',";
					}
					$storecodeList = substr($storecodeList,0,-1);

					$updateStSql .= " and storecode in ($storecodeList)";
				}
				$updateSt = yii::app()->localEIP->createCommand($updateStSql)->execute();
				$result = $updateSt;
				break; 

		}
	

		print json_encode($result);
	}

	// 自動產生行銷折價卷整理
	public function createCouponData()
	{
		
		$serviceMapSql = "SELECT serviceno,cname,showname FROM tbs_service";
		$serviceMapAry = yii::app()->localEIP->createCommand($serviceMapSql)->queryAll();
		$serviceMap = array();
		foreach($serviceMapAry as $s)
		{
			$serviceMap[$s['serviceno']] = $s;
		}
		$couponSql = "SELECT pdate,storecode,storename,serviceno,sum(num) as sum FROM `tbp_empperform_coupon` WHERE pdate BETWEEN '20220901' and '20220911' GROUP BY storecode,pdate ";
		$couponGiveSql = "SELECT pdate,storecode,storename,serviceno,sum(num) as sum FROM `tbp_empperform_coupon_give` WHERE pdate BETWEEN '20220901' and '20220911' GROUP BY storecode,pdate";
		$couponAry = yii::app()->localEIP->createCommand($couponSql)->queryAll();
		$couponGiveAry = yii::app()->localEIP->createCommand($couponGiveSql)->queryAll();

		$fpR = fopen('protected/tmp/couponReceive.csv','w');
		$fpG = fopen('protected/tmp/couponGive.csv','w');

		// 回收
		foreach($couponAry as $c)
		{
			$service = isset($serviceMap[$c['serviceno']])?$serviceMap[$c['serviceno']]['cname']:$c['serviceno'];
			// array_push($couponResult['receive'],[$c['pdate'],$c['storecode'],$c['storename'],$service,$c['sum']]);
			fputcsv($fpR,[$c['pdate'],$c['storecode'],$c['storename'],$service,$c['sum']]);
		}
		fclose($fpR);
		// 送出
		foreach($couponGiveAry as $c)
		{
			$service = isset($serviceMap[$c['serviceno']])?$serviceMap[$c['serviceno']]['cname']:$c['serviceno'];
			// array_push($couponResult['give'],[$c['pdate'],$c['storecode'],$c['storename'],$service,$c['sum']]);
			fputcsv($fpG,[$c['pdate'],$c['storecode'],$c['storename'],$service,$c['sum']]);
		}
		fclose($fpG);

		return "執行完畢";
	}


	/**
	 * linePay API
	 * 1.reserve 發送支付要求
	 * 2.confirm 確認支付、結果
	 */
	// reserve
	public function linepayReserve()
	{
		$alertMsg = '';
		$jsonMsg = '';
		$excepMsg = '';
		$confirmMsg = '';
		$returnMsg = [];

		$apiEndpoint   = $_POST['apiEndpoint'];
		$channelId     = $_POST['channelId'];
		$channelSecret = $_POST['channelSecret'];

		$params = [
			"productName"     => $_POST['productName'],
			"productImageUrl" => $_POST['productImageUrl'],
			"amount"          => $_POST['amount'],
			"currency"        => $_POST['currency'],
			// "confirmUrl"      => Yii::app()->basePath."/views/site/testPageX_Panes/linePay/confirm.php",
			"confirmUrl"      => 'confirm.php',
			"orderId"         => $_POST['orderId'],
			"confirmUrlType"  => $_POST['confirmUrlType'],
		];

		try {
			require_once(Yii::app()->basePath."/views/site/testPageX_Panes/linePay/Chinwei6_LinePay.php");
			// session_start();
			$LinePay = new Chinwei6\LinePay($apiEndpoint, $channelId, $channelSecret);
			
			// Save params in the _SESSION
			$_SESSION['cache'] = [
				"apiEndpoint"   => $_POST['apiEndpoint'],
				"channelId"     => $_POST['channelId'],
				"channelSecret" => $_POST['channelSecret'],
				"amount"        => $_POST['amount'],
				"currency"      => $_POST['currency'],
			];

			$result = $LinePay->reserve($params);
			// echo '<pre class="code">';
			$jsonMsg = $result;
			// $jsonMsg = json_encode($result, JSON_PRETTY_PRINT);
			array_push($returnMsg,$jsonMsg);
			// echo '</pre>';

			if(isset($result['info']['paymentUrl']['web'])){
				// echo '<a target="_blank" href="' . $result['info']['paymentUrl']['web'] . '">點此連至 Line 頁面登入帳戶</a>';
				$alertMsg = $result['info']['paymentUrl']['web'];
				array_push($returnMsg,$alertMsg);

			}
		}
		catch(Exception $e) {
			// echo '<pre class="code">';
			$excepMsg = $e->getMessage();
			// $confirmMsg = $this->linepayConfirm();
			// array_push($returnMsg,$confirmMsg);
			// echo '</pre>';
		}
		return $returnMsg;
	}
	// confirm
	public function linepayConfirm()
	{
		if(isset($_GET['transactionId']) && isset($_SESSION['cache'])) {
			$apiEndpoint   = $_SESSION['cache']['apiEndpoint'];
			$channelId     = $_SESSION['cache']['channelId'];
			$channelSecret = $_SESSION['cache']['channelSecret'];

			$params = [
				"amount"   => $_SESSION['cache']['amount'], 
				"currency" => $_SESSION['cache']['currency'],
			];

			try {
				$LinePay = new Chinwei6\LinePay($apiEndpoint, $channelId, $channelSecret);

				$result = $LinePay->confirm($_GET['transactionId'], $params);
				echo '<pre class="code">';
				echo json_encode($result, JSON_PRETTY_PRINT);
				echo '</pre>';
			}
			catch(Exception $e) {
				echo '<pre class="code">';
				echo $e->getMessage();
				echo '</pre>';
			}

			unset($_SESSION['cache']);
		}
	}
	// ------------------------------------
	//API 測試
	public function apiDataTest(){
		
	} 

	
	
}