<?php

class TbaAdjustController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','tbaadjust_create'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TbaAdjust;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TbaAdjust']))
		{
			$model->attributes=$_POST['TbaAdjust'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TbaAdjust']))
		{
			$model->attributes=$_POST['TbaAdjust'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TbaAdjust');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TbaAdjust('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TbaAdjust']))
			$model->attributes=$_GET['TbaAdjust'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TbaAdjust the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TbaAdjust::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	//新增稽核申請項目

	public function actiontbaadjust_Create()
	{
		$model=new TbaAdjust;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		//轉換日期格式

		$monthlist = $this->getMonth(); //月份資料
		$adjustItem = $_POST['adjustItem'];//申請表單類型(表頭)
		$applyName = $_POST['applyName']; //申請名稱
		$applyCode = $_POST['applyCode']; //申請代號
		$month = $_POST['month'];	//申請月份 getMonth
		$date_sn = date("yymd",strtotime($_POST['date_sn'])); //日期sn (始.新)
		$date_eo = date("yymd",strtotime($_POST['date_eo'])); //日期eo (訖.舊)
		$aft_days = (date(strtotime($_POST['date_eo'])) - date(strtotime($_POST['date_sn'])))/86400; //實際影響天數 =>eo-sn
		$daytime = $_POST['daytime']; //時間
		$store = $_POST['storeName']; //門市 (新)
		$storeCode = $_POST['storeCode']; //店編
		$o_store = $_POST['o_storeName']; //門市(舊)
		$o_storeCode = $_POST['o_storeCode']; //(舊)店編
		$empno = $_POST['empno']; //員編
		$empname = $_POST['empname']; //設計師
		$act_emps = $_POST['act_emps']; //實際員額 增、減
		$act_empn = $_POST['act_empn']; //實際員額數
		$user_barcode = $_POST['user_barcode']; //顧客條碼
		$user_mobile = $_POST['user_mobile']; //顧客手機
		$reason = $_POST['reason']; //原因
		$memo = $_POST['memo']; //備註
		$applyDateTime = date('yymd').date('AHi'); //填表日期時間

	
		
		//撈調整項目
		$adjusts = array();
		$adjustTable = array();
		$adjustTable = TbaAdjustItem::model()->findAll();
		foreach ($adjustTable as $adjT)
			{
				$adjusts[$adjT->applyCode] = $adjT->applyName;
			}
		$adjustItemAry = array();
		$adjustItemSql = "SELECT applyCode FROM tba_adjust_item";
		$adjustItemResult = Yii::app()->db->createCommand($adjustItemSql)->queryAll();
		for ($i = 0; $i < count($adjustItemResult); $i++) {
			$applyCode = $adjustItemResult[$i]['applyCode'];
			$adjustItemAry[$applyCode] = $adjusts[$applyCode];} 
					// CVarDumper::dump($adjustItemAry);

			
		if(isset($_POST['import']))
		{
			CVarDumper::dump($_POST,10,true);
			CVarDumper::dump($aft_days);
			//type1 sql
			if($adjustItem != null && $month !=0)
			{
				$ap1Sql="INSERT INTO tba_adjust(applyName,applyCode,month,date_sn,date_eo,aft_days,store,storeCode,reason,memo,applyDateTime)
				VALUE('$applyName','$applyCode','$month','$date_sn','$date_eo','$aft_days','$store','$storeCode','$reason','$memo','$applyDateTime')";
				Yii::app()->db->createCommand($ap1Sql)->execute();
				yii::app()->user->setFlash('success','申請送出成功，請待稽核組查核確認');
			}
			else
				yii::app()->user->setFlash('error','請確實填寫');
		}

		$this->render('tbaadjust_create',array(
			'model'=>$model,
			'adjustitem'=>$adjustitem,
			'date_sn'=>$date_sn,
			'adjustItemAry'=>$adjustItemAry,
			'monthlist'=>$monthlist,
		));
	}

	/**
	 * Performs the AJAX validation.
	 * @param TbaAdjust $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tba-adjust-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function getMonth() //月份array
	{
		$month = array(
			// 0 =>'id',
			0=>'請選擇月份',
			1=>'一月',
			2=>'二月',
			3=>'三月',
			4=>'四月',
			5=>'五月',
			6=>'六月',
			7=>'七月',
			8=>'八月',
			9=>'九月',
			10=>'十月',
			11=>'十一月',
			12=>'十二月',
		);
		return $month;
	}
}
