<?php
	error_reporting(0);

class ImpExcelController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','import','jsontest'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ImpExcel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ImpExcel']))
		{
			$model->attributes=$_POST['ImpExcel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ImpExcel']))
		{
			$model->attributes=$_POST['ImpExcel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ImpExcel');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ImpExcel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ImpExcel']))
			$model->attributes=$_GET['ImpExcel'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ImpExcel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ImpExcel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ImpExcel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='imp-excel-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
//--------------JSON
public function actionJsontest(){
	
	if(isset($_POST['storecode']))
		$storecode = $_POST['storecode'];
	if(isset($_POST['dates']))
		$dates = $_POST['dates'];
	if(isset($_POST['datee']))
		$datee = $_POST['datee'];
	if(isset($_POST['cash']))
		$cash = $_POST['cash'];
	if(isset($_POST['credit']))
		$credit = $_POST['credit'];
	if(isset($_POST['check']))
		$check = $_POST['check'];

	if(isset($_POST['qry']))
	{
		if($storecode !='' && $dates != '' && $datee !='')
		{
		$sql = "SELECT storecode,data,ctime FROM posdata WHERE storecode = $storecode and ctime between '$dates' and '$datee'";
		$jsonAry = Yii::app()->db->createCommand($sql)->queryAll();
			// CVarDumper::dump($jsonAry[0],10,true);
		}
		else
		{
			Yii::app()->user->setFlash('error', '請輸入資料');
		}
	}
	$this->render('jsontest',array(
		'storecode'=>$storecode,
		'dates'=>$dates,
		'datee'=>$datee,
		'cash'=>$cash,
		'credit'=>$credit,
		'check'=>$check,
		'jsonAry'=>$jsonAry,
		
		)); 
}


//-----------------import-------

public function actionImport() {
				//MB設定為64M.128M.256M.512M.1024M，用於配置內部記憶體容量
			ini_set('memory_limit', '256M');  
			XPHPExcel::init();
			$phpexcel = new PHPExcel;             
			$storeAry = array(); 
			$iresult = array();  
			//排序用  
			$areaSort = "'北一區','北二區','北三區','北四區','北五區','桃一區','桃二區','竹苗區','中一區','中二區','中三區','嘉南一區','嘉南二區','嘉南三區','高屏一區','高屏二區','高屏三區','高屏四區','高屏五區','高屏六區','全公司'";
			
			//抓年月欄位 date
			$dateAry = array();
			$dateSql = "SELECT date FROM impexcel GROUP BY date ORDER BY date DESC";
			$dateResult = Yii::app()->db->createCommand($dateSql)->queryAll();
			for ($i = 0; $i < count($dateResult); $i++) {
				$date = $dateResult[$i]['date'];
				$dateAry[$date] = $date;} 
			//抓區域欄位 area
			$areaAry = array();
			$areaSql = "SELECT area FROM impexcel GROUP BY area ORDER BY id ";
			$areaResult = Yii::app()->db->createCommand($areaSql)->queryAll();
			for ($i = 0; $i < count($areaResult); $i++) {
				$area = $areaResult[$i]['area'];
				$areaAry[$area] = $area;}
			// CVarDumper::dump($dateSql);
			// CVarDumper::dump($dateResult);

			//正常方式(紀錄於textbox)
			// if(isset($_POST['qrydate'])){
			// 	$qrydate = $_POST['qrydate'];
			// }else{
			// 	$qrydate = date('Ym');
			// }

			//簡短(紀錄於textbox)
			$qrydate = isset($_POST['qrydate']) ? $_POST['qrydate'] : date('');
			$qryarea = isset($_POST['qryarea']) ? $_POST['qryarea'] : ('');

			//查詢---------------------------------
			if(isset($_POST['submit']))
			{
				if($qrydate!="" && $qryarea!="")
				{
				$storeAry = ImpExcel::model()->findAllByAttributes(
					array('date'=>$qrydate ,'area'=>$qryarea));
					if(count($storeAry)>0)
					{
						//合計
						$totalsql = "SELECT area,sum(mincome) as tincome ,round(avg(mincome),2) as vincome FROM impexcel WHERE date='$qrydate' and area='$qryarea'";
						$iresult = Yii::app()->db->createCommand($totalsql)->queryRow();

						//圖表用---------------------------------
						$storeSqlAry = array();
						$storeSqlIncome = array();
						for($i=0;$i<count($storeAry);$i++)
						{
							$storeName = $storeAry[$i][store];
							$storeIncome = (int)$storeAry[$i][mincome];
							// $storeContinue .= $storeName;
							array_push($storeSqlAry,$storeName);
							array_push($storeSqlIncome,$storeIncome);  
						}
						//---------------------------------------
		
						yii::app()->user->setFlash('success','查詢成功，'.$qryarea.' 共'.count($storeAry).'筆資料');
						// CVarDumper::dump($totalsql);
						// CVarDumper::dump($iresult);

					}
					else
					{
						yii::app()->user->setFlash('error','查無資料');
					}
				}
				
				elseif($qrydate!="" && $qryarea=="")
				{
				// $storeAry = ImpExcel::model()->findAllByAttributes(
				// 	array('date'=>$qrydate));
				$storeSql = "SELECT * FROM impexcel WHERE date = $qrydate ORDER BY field(area,$areaSort)";
				$storeAry = Yii::app()->db->createCommand($storeSql)->queryAll();

					if(count($storeAry)>0)
					{
						//合計--
						$totalsql = "SELECT sum(mincome) as tincome ,round(avg(mincome),2) as vincome FROM impexcel WHERE date='$qrydate'";
						$iresult = Yii::app()->db->createCommand($totalsql)->queryRow();
						//區合計--
						$areaTotalsql = "SELECT area,sum(mincome) as aincome ,round(avg(mincome),2) as vincome FROM impexcel WHERE date='$qrydate' GROUP BY area ORDER BY 
						field(area,$areaSort)";
						$areaResult = Yii::app()->db->createCommand($areaTotalsql)->queryAll();
						//圖表用---------------------------------
						$storeSqlAry = array();
						$storeSqlIncome = array();
						$storeSqlAvgIncome = array();
						for($i=0;$i<count($areaResult);$i++)
						{
							$storeName = $areaResult[$i][area];
							$storeIncome = (int)$areaResult[$i][aincome];
							$storeAvgIncome = (int)$areaResult[$i][vincome];
							// $storeContinue .= $storeName;
							array_push($storeSqlAry,$storeName);
							array_push($storeSqlIncome,$storeIncome);  
							array_push($storeSqlAvgIncome,$storeAvgIncome);
						}
						//---------------------------------------

						yii::app()->user->setFlash('success','查詢成功，共'.count($storeAry).'筆資料');
					}
					else
					{
						yii::app()->user->setFlash('error','查無資料');
					}
				}
				else
				{
					yii::app()->user->setFlash('error','查無資料');

				}
				
				

		
			}
				// CVarDumper::dump($areaResult,10,true);
				// echo $areaResult;

		//匯出-------------------------------
			// 取得表格數
			//欄位名稱,0=>'ano'
			$col    = array();
			//欄位顯示的中文字,'ano'=>'調撥單號'
			$title  = array();
			//負責儲存輸出在畫面上的陣列
			$colAry = array();             
			$fileName = '';

			if(isset($_POST['export'])) 
			{  
				
				if($qrydate!="" && $qryarea!="")
					{
					$sql = " SELECT * FROM impexcel "
						." WHERE date = $qrydate and area = '$qryarea'"                
						// ." ORDER BY id "
						." UNION " //合計用，單筆資料結尾
						." SELECT '','合計','$qryarea','','','',SUM(mincome) FROM impexcel "   
						." WHERE date = $qrydate and area = '$qryarea' ";
						
						
						$allocAry = yii::app()->db->createcommand($sql)->queryAll();
						$col = $this->getcol();
						$title = $this->getTitle(TRUE);
						$countrow = count($allocAry)-1;

						if($countrow>0)
							{
							// $data = $allocAry[0]['id'];
							// $data = $allocAry[0]['date'];
							// $data = $allocAry[0]['area'];
							// $data = $allocAry[0]['store'];
							// $data = $allocAry[0]['type'];
							// $data = $allocAry[0]['storecode'];
							// $data = $allocAry[0]['mincome'];
						
							// yii::app()->user->setFlash('success','查詢成功，共'.count($allocAry).'筆資料');
							//建立
							$fileName = $this->exportEXCEL($qrydates,$col,$title,$allocAry);  
							$clickUrl =  "<a href='".Yii::app()->request->baseUrl. '/' . "tmp" . '/' .$fileName. "'>點我下載</a>";
							Yii::app()->user->setFlash('success','匯出成功，共'.$countrow.'筆資料'."<br>".$clickUrl);   
							}
					else
						yii::app()->user->setFlash('error','查無資料,匯出失敗');
					}
			
					
	
				elseif($qrydate!="" && $qryarea=="")
					{
						$sql = " SELECT * FROM impexcel "
						." WHERE date = $qrydate "                
						." UNION "	//合計用，單筆資料結尾
						." SELECT '','合計','全公司','','','',SUM(mincome) FROM impexcel "   
						." WHERE date = $qrydate ORDER BY field(area,$areaSort)";

						$allocAry = yii::app()->db->createcommand($sql)->queryAll();
						$col = $this->getcol();
						$col2 = $this->getCol2();
						$title = $this->getTitle(TRUE);
						$countrow = count($allocAry)-1;
						
						//總合計--
						$totalsql = "SELECT sum(mincome) as tincome FROM impexcel WHERE date='$qrydate'";
						$iresult = Yii::app()->db->createCommand($totalsql)->queryRow();
						//區合計--
						$areaTotalsql = "SELECT area,sum(mincome) as aincome ,round(avg(mincome),2) as vincome FROM impexcel WHERE date='$qrydate' GROUP BY area ORDER BY 
						field(area,$areaSort)";
						$areaResult = Yii::app()->db->createCommand($areaTotalsql)->queryAll();


						if($countrow>0)
							{
							// $data = $allocAry[0]['id'];
							// $data = $allocAry[0]['date'];
							// $data = $allocAry[0]['area'];
							// $data = $allocAry[0]['store'];
							// $data = $allocAry[0]['type'];
							// $data = $allocAry[0]['storecode'];
							// $data = $allocAry[0]['mincome'];

						
							// yii::app()->user->setFlash('success','查詢成功，共'.count($allocAry).'筆資料');
							//建立
							$fileName = $this->exportEXCEL($qrydates,$col,$title,$allocAry,$iresult,$areaResult,$col2);  
							$clickUrl =  "<a href='".Yii::app()->request->baseUrl. '/' . "tmp" . '/' .$fileName. "'>點我下載</a>";
							Yii::app()->user->setFlash('success','匯出成功，共'.$countrow.'筆資料'."<br>".$clickUrl);   
							}
						else
							yii::app()->user->setFlash('error','查無資料,匯出失敗');

						}
				else
					{
						yii::app()->user->setFlash('error','查無資料,匯出失敗');
	
					}
			}
				// CVarDumper::dump($col);
				// CVarDumper::dump($title);

		//匯入------------------------------------
		if(isset($_POST['import']))
		{     
				//檔案的名稱與暫存區名稱不等於空值,取得檔案連結
				if(isset($_FILES['filename']) && $_FILES['filename']['tmp_name']!='') {  
					//檔案路徑-置於暫存區
					$filepath = $_FILES['filename']['tmp_name'];                             
					//檔案名稱來源
					$filename = $_FILES['filename']['name'];             
					$phpreader = new PHPExcel_Reader_Excel2007();
						//不等於版本2007的時候往下讀取舊版本2003系列 
						if(!$phpreader->canRead($filepath)) {                                                              

							$phpreader = new PHPExcel_Reader_Excel5();                            
						}
					//讀取EXCEL    
					$phpexcel = $phpreader->load($filepath); 


					//讀取第一張工作表
					$currentsheet = $phpexcel->getSheet(0);  
					//取得最大的列數 *垂直的是列(ABCD欄位)
					$allcolumn = $currentsheet->getHighestColumn();   
					//取得總行數  *水平的是行(1.2.3.4欄位)
					$allrow = $currentsheet->getHighestRow();  



					$imperror=0;	//匯入失敗筆數
					$impsuccess=0;		//匯入成功筆數

					//匯入DB
					for($currentRow=2;$currentRow<=$allrow;$currentRow++)
					{
						$date = $phpexcel->getActiveSheet()->getCell("A".$currentRow)->getValue(); 
						$area = $phpexcel->getActiveSheet()->getCell("B".$currentRow)->getValue();
						$store = $phpexcel->getActiveSheet()->getCell("C".$currentRow)->getValue();
						$type = $phpexcel->getActiveSheet()->getCell("D".$currentRow)->getValue();
						$storecode = $phpexcel->getActiveSheet()->getCell("E".$currentRow)->getValue();
						$mincome = $phpexcel->getActiveSheet()->getCell("F".$currentRow)->getValue();
						
						//檢查重複
						$query0 = "SELECT * FROM impexcel WHERE date='$date' and storecode='$storecode'";
						$qresult = Yii::app()->db->createCommand($query0)->queryAll();
						
						if(count($qresult) > 0)
						{
							$imperror++;
							yii::app()->user->setFlash('error',"::::匯入失敗!".$imperror.'筆錯誤::::'.$msg);
							$msg = $msg.'<br>('.$qresult[0]['date'].$qresult[0]['store'].') 資料重複';
						}
						else
						{
							$impsuccess++;
							$inssql = "INSERT INTO impexcel (date,area,store,type,storecode,mincome) VALUES('$date','$area','$store','$type','$storecode','$mincome')";
							Yii::app()->db->createCommand($inssql)->execute();
							yii::app()->user->setFlash('success','匯入成功!'.$impsuccess.'筆資料');
						}
					}
																																	
	} }
				
	$this->render('import',array(
		'phpexcel'  =>$phpexcel,
		'storeAry' =>$storeAry,               
		'fileName'  =>$fileName,
		'allrow'    =>$allrow, 
		'dateAry' =>$dateAry,
		'areaAry' =>$areaAry, 
		'qryarea'=>$qryarea,
		'iresult'=>$iresult,
		'areaResult'=>$areaResult,
		'title'=>$title,
		'storeSqlAry'=>$storeSqlAry,
		'storeSqlIncome'=>$storeSqlIncome,
		'storeSqlAvgIncome'=>$storeSqlAvgIncome,

		)); 


	} //public最外圍
		      
        /**
         * 輸出EXCEL_Function---------
         * @param type $qrydate - 開始年-月
         * @param type $col - 欄位
         * @param type $title - 標題列
         * @param type $allocAry - 資料列
         */    
        private function exportEXCEL($qrydate,$col, $title, $allocAry,$iresult,$areaResult,$col2)
        {        
                // PHP EXCEL 初始化
                XPHPExcel::init();
                $fileTitle = "JIT $qrydate Export File";
                $objPHPExcel= XPHPExcel::createPHPExcel();
                $objPHPExcel->getProperties()->setCreator("JIT")
                                             ->setLastModifiedBy("JIT")
                                             ->setTitle($fileTitle)
                                             ->setSubject("")
                                             ->setDescription($fileTitle)
                                             ->setKeywords("office 2007 openxml php")
                                             ->setCategory("Export File");

                // 第一列 填入標題，由第0欄開始
                $column = 0;            
                for ($i = 0; $i < count($col); $i++) {

                    if(isset($title[$col[$i]])) {

                        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($column,1, (isset($title[$col[$i]]))?
                        $title[$col[$i]]:'',PHPExcel_Cell_DataType::TYPE_STRING);
                        $column++;
                    }
                }            
                // 由第2列開始
                $row = 2;
                for ($j = 0; $j < count($allocAry); $j++) {

                    if(isset($allocAry[$j][$col[0]])) {
                        // 第幾欄. 由第0欄開始
						$column = 0; 


                        for ($i = 0; $i < count($col); $i++) {
							// 若符合篩選欄位. 才進行
							$storageArea1 = $allocAry[$j][area];
							if($storageArea1 != '')
							{
								//合計欄位
								$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $row)->setValueExplicit((isset($areaResult[$j][$col2[$i]]))?
								$areaResult[$j][$col2[$i]]:'', PHPExcel_Cell_DataType::TYPE_STRING);

								//區域
								$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $row)->setValueExplicit((isset($allocAry[$j][$col[$i]]))?
                                $allocAry[$j][$col[$i]]:'', PHPExcel_Cell_DataType::TYPE_STRING);
								
							}
                            if(isset($title[$col[$i]])) {


                                $column++;
							}
							
                        }
					$row++;
					$storageArea2 = $storageArea1;
                    }
				}
				// $row = $allrow;




                //sheet 表名稱
                $objPHPExcel->getActiveSheet()->setTitle($qrydate.'門市業績');
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);
                // Redirect output to a web browser (Excel5)
                $webroot = Yii::getPathOfAlias('webroot');
                //$fileName =$excelname.'-'.time().'.xls';
                $fileName = $qrydate.time().'.xls';
                $filePath = $webroot . '/' . "tmp" . '/';
                $fileUrl = $filePath.$fileName;
                // If you're serving to IE over SSL, then the following may be needed
                // header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                // header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                // header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                // header ('Pragma: public'); // HTTP/1.0
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save($fileUrl);
                return $fileName;
        }               
    
        /**
        *   設定標題列
        */
        private function getcol() 
        {
                $col = array(
					// 0 =>'id',
                    0 =>'date',
                    1 =>'area',
                    2 =>'store',
                    3 =>'type',
					4 =>'storecode',
					5 =>'mincome',
                );
                return $col; 
        }

        /**
        *   設定標題列轉中文
        */
        private function getTitle() 
        {
                $title = array(
					// 'id'=>'id',
                    'date'=>'年月',
                    'area'=>'區域',
                    'store'=>'門市',
                    'type'=>'型態',
					'storecode'=>'店編', 
					'mincome'=>'合計',                        
                );
                return $title;
		}
		private function getCol2()
		{
			$col2 = array(
				0 => 'area',
				1 => 'aincome',
				2 => 'vincome',
			) ;
			return $col2;
		}
		
 
    

}  //最外圍
