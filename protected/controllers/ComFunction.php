<?php

/**
 * 公用元件
 */
class ComFunction {

    /**
     * 取得洗.染.燙業績
     * @param type $ary
     * @return int
     */
    public function getPerform($ary) {

        $sum = 0;
        $service = TbsService::model()->findAllByAttributes(array('type1' => '1'));
        if (count($service) > 0)
            foreach ($service as $row) {
                if (isset($ary[$row->serviceno]))
                    $sum = $sum + $ary[$row->serviceno] * $row->perform;
            }
        return $sum;
    }

  /**
    *  已經沒用到了
     * 取得洗助業績
     * @param type $ary
     * @return int
    *  @deprecated since version 20170206
     */
    public function getAssist($ary) {

        $sum = 0;
        $service = TbsService::model()->findAllByAttributes(array('type1' => '2'));
        if (count($service) > 0)
            foreach ($service as $row) {
                if (isset($ary[$row->serviceno])) {

                    $sum = $sum + $ary[$row->serviceno] * $row->draw;
                }
            }
        return $sum;
    }

    /**
     * 已經沒用到了
     * 取得銷售獎金
     * @param type $ary
     * @return int
     * @deprecated since version 20170206
     */
    public function getSaleBonus($ary) {

        $sum = 0;
        $service = TbsService::model()->findAllByAttributes(array('type1' => '3'));
        if (count($service) > 0)
            foreach ($service as $row) {
                if (isset($ary[$row->serviceno])) {

                    $sum = $sum + $ary[$row->serviceno] * $row->draw;
                }
            }
        return $sum;
    }

    /**
     * 取得銷售金額(剪.染.洗.燙.洗髮精
     * 與設計師有關.有抽成的商品
     * @param type $ary = array ('A01'=>1, 'D01'=>1);
     * @return int
     */
    public function getServiceArray() {

        $ary = array();
        $service = TbsService::model()->findAllByAttributes(
                array('opt1' => '1'), $condition = "type1 in ('1','2','3','4','5')", $param = array());
        if ($service != NULL)
            foreach ($service as $row) {
                $temp = array($row->serviceno => 0);
                $ary = array_merge($ary, $temp);
            }
        return $ary;
    }

    /**
     * 取得銷售金額(剪.染.洗.燙.洗髮精
     * 與設計師有關.有抽成的商品
     * @param type $ary = array ('A01'=>1, 'D01'=>1);
     * @return int
     */
    public function getMoneySale($ary) {

        $sum = 0;
        $service = TbsService::model()->findAllByAttributes(
                array('opt1' => '1'), $condition = "type1 in ('1','2','3')", $param = array());
        if (count($service) > 0)
            foreach ($service as $row) {
                if (isset($ary[$row->serviceno]))
                    $sum = $sum + $ary[$row->serviceno] * $row->price;
            }
        return $sum;
    }

    /**
     * 取得販賣金額(染膏燙劑)
     * 門市販賣.沒有設計師抽成的商品
     * @param type $ary = array ('N011'=>1, 'N012'=>1);
     * @return int
     */
    public function getMoneySell($ary) {

        $sum = 0;
        $service = TbsService::model()->findAllByAttributes(array('type1' => '4'));
        if (count($service) > 0)
            foreach ($service as $row) {
                if (isset($ary[$row->serviceno]))
                    $sum = $sum + $ary[$row->serviceno] * $row->price;
            }
        return $sum;
    }

    /**
     * 取得少收票卡金額
     * @param type $ary = array ('X01'=>1, 'X02'=>1);
     * @return int
     */
    public function getMoneyReduce($ary) {

        $sum = 0;
        $service = TbsService::model()->findAllByAttributes(array('type1' => '5'));
        if (count($service) > 0)
            foreach ($service as $row) {
                if (isset($ary[$row->serviceno])) {
//                    print_r ($row->serviceno.' '.$row->noreceive.'<br>');
                    $data = TbsService::model()->findByAttributes(array('serviceno' => $row->noreceive));
                    if ($data != NULL)
                        $sum = $sum + $ary[$row->serviceno] * $data->price;
                }
            }
        return $sum;
    }

    /**
     * 取得異匯異動金額
     * @param type $ary = array ('Z01'=>1, 'Z02'=>1);
     * @return int
     */
    public function getMoneyChange($ary) {

        $sum = 0;
        $service = TbsService::model()->findAllByAttributes(array('type1' => '6'));
        if (count($service) > 0)
            foreach ($service as $row) {
                if (isset($ary[$row->serviceno]))
                    $sum = $sum + $ary[$row->serviceno] * $row->chgprice;
            }
        return $sum;
    }

    /**
     * 取得區域陣列, array[0] 是 id=>區域名,  array[1]是 id=>區域權限
     * 回傳 array[0]=>array (
     *          '1'=>北一區,
     *          '2'=>北二區....)
     * 回傳 array[1]=>array (
     *          '1'=>AreaManager01,
     *          '2'=>AreaManager02....)
     * @param boolean $inUse - 是否使用
     * @return array
     */
    public function getAreaArray($inUse, $depart = '3') {

        $areas = array();
        $areaRight = array();

        $TbsArea = array();

        $depart_str = $depart;
        if ($depart == '2') {
            $depart_str = '2,3';
        }

        $depart_ary = explode(',', $depart_str);

        if ($inUse)
            $TbsArea = TbsArea::model()->findAllByAttributes(array('opt1' => 1, 'depart_id' => $depart_ary), array('order' => 'opt3 ASC'));
        else
            $TbsArea = TbsArea::model()->findAllByAttributes(array('depart_id' => $depart_ary), array('order' => 'opt3 ASC'));

        
        foreach ($TbsArea as $area) {
            $areas[$area->id] = $area->areaname;
            $areaRight[$area->id] = $area->opt2;
        }

        return array($areas, $areaRight);
    }

    /**
     * 取得門市陣列, array[0] 是 店編=>店名,  array[1]是 店編=>區id, array[2]是 店編=>區名
     * 回傳 array[0]=>array (
     *          '007001'=>高雄聯興,
     *          '007002'=>高雄復興....)
     * 回傳 array[1]=>array (
     *          '007001'=>8,
     *          '007002'=>8)
     * 回傳 array[2]=>array (
     *          '007001'=>高屏二區,
     *          '007002'=>高屏二區) 
     * @param boolean $inUse
     * @return array - array(門市名稱), array(門市區域id), array(門市區域名稱)
     */
    public function getStoreArray($inUse = FALSE, $depart = '3') {
        $stores = array();
        $storesArea = array();
        $storesAreaName = array();

        $TbsStore = array();


        $depart_str = $depart;
        if ($depart == '2') {
            $depart_str = '2,3';
            $area = $this->getAreaArray($inUse, $depart);

        }elseif($depart == '4'){
            $depart_str = '1,2,3';
            $area = $this->getAreaArray($inUse, $depart);
        }
        else {
            $area = $this->getAreaArray($inUse);
        }

        $depart_ary = explode(',', $depart_str);

        if ($inUse)
            $TbsStore = TbsStore::model()->findAllByAttributes(array('opt1' => 1, 'depart_id' => $depart_ary));
        else
            $TbsStore = TbsStore::model()->findAllByAttributes(array('depart_id' => $depart_ary));

        //CVarDumper::dump($TbsStore,10,true);


        foreach ($TbsStore as $store) {
            $stores[$store->storecode] = $store->storename;
            $storesArea[$store->storecode] = $store->area_id;
            if(isset($area[0][$store->area_id]))
                $storesAreaName[$store->storecode] = $area[0][$store->area_id];
            else
                $storesAreaName[$store->storecode] = "";
        }



        return array($stores, $storesArea, $storesAreaName);
    }

    
    /**
     * 依傳入資訊取得EMAIL
     * 1. 傳入員編陣列
     * 2. 傳入區域代碼陣列 array(3110,3120...) 3110為北1區,3120為北2區...  - 回傳區經理EMAIL
     * 3. 傳入店編陣列 - 回傳區經理EMAIL array
     * 4. 傳入職位以在職為主
     * @param  mixed $type
     * @param  array $ary - 寄給$ary
     *
     * @return array - ('傳入值'=>email)
     */
    public function getEmail($type = 1, array $ary, $daymonth) {

        $emails=array();
        $empsql="SELECT empno,email FROM `tbs_emp`";
        $result = Yii::app()->db->createCommand($empsql)->queryAll();
        //陣列 員編=>email
        $empEmailAry=array();
        $areaEmpnoAry=array();
        foreach($result as $row)
        {
            $empEmailAry[$row['empno']]=$row['email'];
        }
        
       
        switch($type) {
            case '1':
                foreach($ary as $row)
                {
                    array_push($emails,$empEmailAry[$row]);
                }
                break;
            case '2': // 
                $organizationSql="SELECT areaCode, areaLNo FROM `tbs_organization` WHERE daymonth = $daymonth AND opt1 = 1";
                $organization_result = Yii::app()->db->createCommand($organizationSql)->queryAll();
                foreach($organization_result as $row)
                {
                    $areaEmpnoAry[$row['areaCode']]=$row['areaLNo']; 
                }
                foreach($ary as $row)
                {    
                   $emails[$row]=$empEmailAry[$areaEmpnoAry[$row]];
                }
                break;
            case '3':
                $organizationSql="SELECT areaLNo, storeCode FROM `tbs_organization` WHERE daymonth = $daymonth AND opt1 = 1";
                $organization_result = Yii::app()->db->createCommand($organizationSql)->queryAll();
                //店編=>區經理員編
                $store = array();
                foreach($organization_result as $row ) {
                    $store[$row['storeCode']] = $row['areaLNo'];
                }
                //店編=>Email
                foreach($ary as $row) {
                    $emails[$row] = $empEmailAry[$store[$row]];
                }
                break;
            case '4':
                //最新一筆員工 => 職位代碼  (653人.109/2/13)
                $empsql = "SELECT a.empno, a.empname, a.position1
                            FROM(
                                SELECT empno, MAX(daymonth) as daymonth
                                FROM tbs_emp_month
                                GROUP BY empno  
                                )b
                            INNER JOIN tbs_emp_month a
                            ON a.empno = b.empno
                            AND a.daymonth = b.daymonth
                            WHERE a.opt3 =1
                            ORDER BY a.arrivedate DESC";
                $empPosition = Yii::app()->db->createCommand($empsql)->queryAll();
                //取得職位代碼
                $position = TbsPosition::model()->findAllByAttributes(array('opt1'=>1));
                //職位代碼 => 員編陣列 
                $result = array();
                foreach($position as $v1){
                    foreach($empPosition as $v2) {
                        if($v1['id'] == $v2['position1']) {
                            $result[$v1['id']][] = $v2['empno'];
                        }
                    }
                }
                //職位代碼 => Email陣列
                foreach($result as $k => $v3) {
                    foreach($ary as $row) {
                        if($row == $k) {
                            foreach($v3 as $row1){
                                $emails[$row][] = $empEmailAry[$row1];
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
        
        return $emails;
    }

    /**
     * 自動補0函數
     * @param type $num 需要補0的字串
     * @param type $digit 要補0的長度
     * @return type
     */
    function addZero($num, $digit) {

        if (strlen($num) > $digit)
            return $num;
        else
            return $this->addZero('0' . $num, $digit);
    }

    /**
     * 依員工查詢出缺勤, 回傳曰期陣列, 並內含每日差勤
     * form參數, 主要是自動將變數轉成中文
     * @param String $daymonth - 年月 ( 201405 )
     * @param String $empno - 員工編號 ( 02010001) 
     * @param Boolean $form - 是否將1=>全天, 0.5=>半天, 預設為FALSE
     * @return array - 巢狀陣列
     */
    function getAbsenceByEmp($daymonth, $empno, $form) {

        $format = is_bool($form) ? $form : FALSE;

        //切割出年份
        $tmp_year = substr($daymonth, 0, 4);
        //切割出月份
        $tmp_mon = substr($daymonth, 4, 2);

        $aStart = date("Ymd", mktime(24, 0, 0, $tmp_mon, 0, $tmp_year)); //月初
        $aEnd = date("Ymd", mktime(0, 0, 0, $tmp_mon + 1, 0, $tmp_year)); //月底

        $sql = "SELECT logtype,logitem,logday,logname, num,opt2 FROM tba_log
               WHERE  empno = '$empno' and logday BETWEEN '$aStart'  AND '$aEnd' ORDER BY logday DESC ";

        $result = Yii::app()->db->createCommand($sql)->queryAll();

        // output array
        $_array = array();

        if (count($result > 0)) {
            // 用來比較的日期        
            $day = "";
            if (isset($result[0]['logday'])) {
                $day = $result[0]['logday'];
            }
            // 每一天. 所有差勤獎懲的ARRAY
            $logAry = array();
            for ($i = 0; $i < count($result); $i++) {
                if ($result[$i]['logday'] == $day) {
                    if ($format)
                        $logAry[$result[$i]['logname']] = $this->absenceByEmpFormat($result[$i]);
                    else
                        $logAry[$result[$i]['logitem']] = (float) $result[$i]['num'];
                }else {
                    $_array[$day] = $logAry; //array日期 存logname=>num
                    $day = $result[$i]['logday']; //day設成進來的日期
                    $logAry = array(); //清空
                    if ($format)
                        $logAry[$result[$i]['logname']] = $this->absenceByEmpFormat($result[$i]);
                    else
                        $logAry[$result[$i]['logitem']] = (float) $result[$i]['num'];
                }
            }
            $_array[$day] = $logAry;  //把最後一筆資料加進去           
        }

        return $_array;
    }

    /**
     * 將數字轉成符合需求的
     * @param array $log
     * @return real
     */
    private function absenceByEmpFormat($log) {

        $num = 0;

        if ($log['logtype'] == 2 OR $log['opt2'] == 1) {
            $num = (float) $log['num'];
        } else {
            if ($log['num'] == 1)
                $num = '全天';
            elseif ($log['num'] == 0.5)
                $num = '半天';
            else
                $num = $log['num'];
        }

        return $num;
    }

    /**
     * 依員工查詢權重
     * @param String $daymonth - 年月 ( 2014-01-01 )
     * @param String $empno - 員工編號 ( 02010001) 
     * @return int
     */
    public function getWeightByEmp($daymonth, $empno) {

        $result = 0;
        // 員工出缺勤
        $absence = $this->getAbsenceByEmp($daymonth, $empno, FALSE);
        // 取得國定假日
        $dates = array();
        foreach ($absence as $key => $abs) {
            array_push($dates, $key);
        }
        $holidays = $this->getHolidayByDates($dates);
        // 取得有權重的差勤項目
        $logitems = TbaLogItem::model()->findAllByAttributes(array('weight' => 1, 'opt1' => 1));
        $itemdays = array(); // 以天計
        $itemmins = array(); // 以分計 ( 遲到早退開小差 )
        $itemeach = array(); // 以支計 ( 小過, 大過 )
        foreach ($logitems as $item) {
            if ($item->logtype == 1) {
                if ($item->opt2 == 0)
                    array_push($itemdays, $item->id);
                else
                    array_push($itemmins, $item->id);
            } else
                array_push($itemeach, $item->id);
        }
        // 取得差勤權重
        $logweights = TbaWeight::model()->findAllByAttributes(array('opt1' => 1));
        $nweight = array(); // 平日權重
        $hweight = array(); // 假日權重
        foreach ($logweights as $weight) {
            $nweight[$weight->logitem] = $weight->nweight;
            $hweight[$weight->logitem] = $weight->hweight;
        }
        // 計算權重
        // 以 員工出缺勤 $absence 來 loop
        // 每一天都丟進 $holidays 來判斷是否為假日
        // 再逐項去判斷是否有權重. 若有則再做計算.
        // 而權重要判斷成是以天計, 以分計, 還是以支計
        foreach ($absence as $date => $abs) {
            
        }

        return $hweight;
    }

    /**
     * 依員工查詢出缺勤, 回傳曰期陣列, 並內含每日差勤
     * 給期中期末用
     * @param String $daymonth - 年月 ( 201405 )
     * @param String $emp - 員工編號　02010001
     * @return array - 巢狀陣列
     */
    function getAbsenceSumByEmp($daymonth, $empno) {

        //切割出年份
        $tmp_year = substr($daymonth, 0, 4);
        //切割出月份
        $tmp_mon = substr($daymonth, 4, 2);

        $aStart = date("Ymd", mktime(24, 0, 0, $tmp_mon, 0, $tmp_year)); //月初
        $aEnd = date("Ymd", mktime(0, 0, 0, $tmp_mon + 1, 0, $tmp_year)); //月底

        $sql = "SELECT empno, logitem, sum(num) AS sum FROM tba_log
               WHERE  empno = '$empno' and logday BETWEEN '$aStart'  AND '$aEnd' GROUP BY logitem ";

        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $_array = array();


        for ($i = 0; $i < count($result); $i++) {


            $_array['weight'] = $this->getWeightByEmp($daymonth, $empno);
            $item = $result[$i]['logitem'];
            switch ($item) {
                case 1:
                    $_array['late5'] = (float) $result[$i]['sum'];  //婚
                    break;

                case 2:
                    $_array['late6'] = (float) $result[$i]['sum']; //喪
                    break;

                case 3:
                case 10:
                    $_array['late3'] = (float) $result[$i]['sum']; //病
                    break;

                case 4:
                case 9:
                    $_array['late2'] = (float) $result[$i]['sum']; //事
                    break;

                case 6:
                case 7:
                case 8:
                case 11:
                    $_array['late4'] = (float) $result[$i]['sum']; //公
                    break;

                case 19:
                case 20:
                case 21:
                    $_array['late1'] = (float) $result[$i]['sum']; //遲到
                    break;

                case 18:
                    $_array['late7'] = (float) $result[$i]['sum']; //曠職
                    break;
            }
        }

        return $_array;
    }

    /**
     * 查詢JIT假日, 回傳曰期是否存在,
     * @param String $date - 年月 ( 201405 )
     * @return boolean - 是否為日期
     */
    function getHolidayByDate($date) {

        //預設false
        $existholiday = false;
        //從DB判斷日期是否存在
        $judge = TbaHoliday::model()->findByAttributes(array(), $conditon = " holiday = '$date' "
        );

        //當$judge存在，則$existholiday為true
        if (isset($judge)) {
            $existholiday = true;
        }

        return $existholiday;
    }

    /**
     * 查詢JIT假日, 回傳曰期陳列是否存在,
     * @param Array $dates - 日期陣列 ( 2014-05-01,2014-05-09,.... )
     * @return Array - 日期陣列
     */
    function getHolidayByDates($dates) {

        $_arr = array(); //回傳日期是否為假日的陣列
        // '2014-01-01','2014-01-02'  array轉成字串  
        if (count($dates) > 0) {
            $_str = "'" . implode("','", $dates) . "'"; //每個日期加入單引號為了sql in查詢使用
            //從DB判斷日期是否存在            
            $sql = "SELECT holiday from tba_holiday where holiday  IN ($_str) ORDER BY holiday ASC ";

            $result = Yii::app()->db->createCommand($sql)->queryAll();

            //將雙層array轉成單層array()
            $_array = array();
            for ($i = 0; $i < count($result); $i++) {
                array_push($_array, $result[$i]['holiday']);
            }

            //假日true，平常日false
            for ($i = 0; $i < count($dates); $i++) {

                $_arr[$dates[$i]] = in_array($dates[$i], $_array);
            }
        }
        return $_arr;
    }

    /**
     * 依年月直接取得該年月內所有日期的假日
     * @param String $daymonth - 年月, 如: 201405
     * @return Array - array('20140501'=>true, '20140502'=>false ... )
     */
    public function getHolidayByYearMonth($daymonth) {

        $dates = array();

        $aStart = $this->getTheFirstDayByYearMonth($daymonth, "Ymd");
        $aEnd = $this->getTheLastDayByYearMonth($daymonth, "Ymd");

        for ($i = 0; ($aStart + $i) <= $aEnd; $i++) {
            array_push($dates, $aStart + $i);
        }

        return $this->getHolidayByDates($dates);
    }

    /**
     * 依年月取得該月第一天
     * @param type $daymonth
     * @param type $format
     * @return type
     */
    public function getTheFirstDayByYearMonth($daymonth, $format) {

        // 切割出年份
        $tmp_year = substr($daymonth, 0, 4);
        // 切割出月份
        $tmp_mon = substr($daymonth, 4, 2);

        return date($format, mktime(24, 0, 0, $tmp_mon, 0, $tmp_year));
    }

    /**
     * 依年月取得該月最後一天
     * @param type $daymonth
     * @param type $format
     * @return type
     */
    public function getTheLastDayByYearMonth($daymonth, $format) {

        // 切割出年份
        $tmp_year = substr($daymonth, 0, 4);
        // 切割出月份
        $tmp_mon = substr($daymonth, 4, 2);

        return date($format, mktime(0, 0, 0, $tmp_mon + 1, 0, $tmp_year));
    }

    /**
     * 依傳入年月計算年資
     * @param type $daymonth
     * @param type $arrivedate
     */
    public function getSeniority($daymonth, $arrivedate) {

        // 切割出年份
        $tmp_year = substr($daymonth, 0, 4);
        // 切割出月份
        $tmp_mon = substr($daymonth, 4, 2);

        // 年資計算日為該年月之月底
        $computeDay = mktime(0, 0, 0, $tmp_mon + 1, 0, $tmp_year);
        // 年數
        $year = (($computeDay - strtotime($arrivedate)) / 86400 - 1 ) / 365;

        // 年資為無條件捨去
//        return round($year, 0, PHP_ROUND_HALF_DOWN);
        return round(floor($year), 0);
    }

    public function sendEmail2($body, $title,$address)
    {
        $mail = new phpmailer();

        # code...
        
        try {
            $mail->SMTPDebug = 1;
            // 戰國策
            $mail->isSMTP();                  //設定使用SMTP方式寄信
            $mail->SMTPAuth = TRUE;           //設定SMTP需要驗證

            // $mail->Host = "mail.jithouse.com.tw";     //智邦的SMTP主機
            // $mail->Port = 25;                 //智邦的SMTP主機的SMTP埠位為465埠
            // $mail->CharSet = "utf-8";          //設定郵件編碼，預設UTF-8
            // $mail->Username = "sensor@jithouse.com.tw";  //智邦帳號
            // $mail->Password = "jit3944560";  //智邦密碼
            // $mail->From = "sensor@jithouse.com.tw"; //設定寄件者信箱
// =============================================================================			
            $mail->Host = "smtp.gmail.com";     //GCP的SMTP主機
            $mail->Port = 587;                 //智邦的SMTP主機的SMTP埠位為465埠
            $mail->SMTPSecure = "tls";        // 智邦的SMTP主機需要使用SSL連線
            $mail->CharSet = "utf-8";          //設定郵件編碼，預設UTF-8
            $mail->Username = "notify@jithouse.com";  //智邦帳號
            $mail->Password = "4rQ&3<jq";  //智邦密碼
            // $mail->From = "notify@jithouse.com"; //設定寄件者信箱
            $mail->setFrom('notify@jithouse.com', 'email');

// =========================================================================
            $mail->FromName = "JIT-EIP系統";   //設定寄件者姓名
            $mail->Subject =$title;    //設定郵件標題

            $mail->Body = $body;
            $mail->IsHTML(true); //設定郵件內容為HTML       
            $mail->AddAddress($address);   
                    
            if (!$mail->Send()) {
                CVarDumper::dump($mail,10,true);
                // 'Error: ' . $mail->ErrorInfo;
            }
            // } else {
            //     CVarDumper::dump($mail,10,true);
            // }
        } catch (phpmailerException $e) {
            $errors[] = $e->errorMessage(); //Pretty error messages from PHPMailer
            CVarDumper::dump($e,10,true);
        } catch (Exception $e) {
            $errors[] = $e->getMessage(); //Boring error messages from anything else!
            CVarDumper::dump($e,10,true);
        }            
            
    

    

    }

    /*
     * 寄信
     * 
     * 收件者 附件名稱 標題 內文    
     * 
     * @param array $addressAry - 收件者Array
     * @param str   $title - 信件標題
     * @param str   $content - 信件內文
     * @param array   $attachment - 附件路徑位置及檔名 ex:tmp/201215xxx.xls
     * 
     * 
     */

    public function sendEmail(array $addressAry, $title = '', $content = '', array $attachment) {
        if (!is_array($addressAry) && count($addressAry) == 0) {
            return array('result' => '200', 'msg' => 'addressAry error');
        }
        try {
            $mail = new PHPMailer();
            $mail->isSMTP();                 //設定使用SMTP方式寄信
            $mail->SMTPAuth = true;          //設定SMTP需要驗證
            $mail->SMTPSecure = "tls";       // Gmail的SMTP主機需要使用SSL連線
            $mail->Host = "smtp.gmail.com";  //Gamil的SMTP主機
            $mail->Port = 587;               //Gamil的SMTP主機的SMTP埠位為465埠
            $mail->CharSet = "utf-8";         //設定郵件編碼，預設UTF-8
            $mail->Username = "jittoptop@gmail.com";  //Gmail帳號
            $mail->Password = "abcd306083";  //Gmail密碼
            $mail->From = "jittoptop@gmail.com"; //設定寄件者信箱
            $mail->FromName = "jittop";           //設定寄件者姓名
            $mail->Subject = $title;    //設定郵件標題
            $mail->Body = $content;  //設定郵件內容
            $mail->IsHTML(true);                     //設定郵件內容為HTML
            if (is_array($attachment) && count($attachment) > 0) {
                foreach ($attachment as $v) {
                    $mail->AddAttachment($v); // 設定附件檔
                }
            }
            if (is_array($addressAry) && count($addressAry) > 0) {
                foreach ($addressAry as $v) {
                    $mail->AddAddress($v);
                }
                //$mail->AddAddress("ma18top@gmail.com", "top"); //設定收件者郵件及名稱
            }


            if (!$mail->Send()) {
                return array('result' => '200', 'msg' => 'send error');
                // 'Error: ' . $mail->ErrorInfo;
            } else {
                return array('result' => '000');
            }
        } catch (phpmailerException $e) {
            $errors[] = $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            $errors[] = $e->getMessage(); //Boring error messages from anything else!
        }
    }

    /*
     * 2016/01/05 Top
     * 
     * 處理績效計算時的店鋪
     * 因應績效系統所以須計算的店鋪需開店超過兩個月
     * 
     * 
     * 2016/05/27
     * 正式站多了精簡達人這種第二品牌
     * 所以須修改判斷條件新增Stype搜尋條件
     * 
     * @param str $lmonth = date("Ym");
     * 
     */

    public function tkpStore($lmonth) {

        $storeary = array();


        if (!isset($lmonth)) {
            $lmonth = date("Ym", strtotime('-1 month'));
        }

        $lmonth = $lmonth . '01';

        $beforelmonth = date("Y-m-d H:i:s", strtotime("$lmonth -1 month"));

        $TbsStore = array();

        $nqry = '301';

        $TbsStore = TbsStore::model()->findAllByAttributes(
                array(), $condition = " begin_day < :lmd AND stype NOT IN (:nqry)  AND opt1 = '1' order by storecode ", $params = array(':lmd' => $beforelmonth, ':nqry' => $nqry)
        );

//               $TbsStore = TbsStore::model()->findAllByAttributes(
//                array(), 
//                $condition = " begin_day < :lmd AND opt1 = '1' order by storecode ", 
//                $params = array(':lmd' => $beforelmonth)
//        );



        return $TbsStore;
    }

    /*
     * 2016/01/07 Top
     * 
     * 處理績效計算時的員額
     * 因應績效系統所需
     * 
     * 由此撈出的員額不包含 position1=17 的助理!!!
     *  
     * @param str $lmonth = date("Ym");
     * -------------------------------------------
     *
     * 2016/03/24 Top
     * 
     * 新增 $type 變數
     * array會在外迴圈包areacode 
     * 
     * 
     */

    public function tkpStoreEmp($lmonth, $type = 0) {

        $empAry = array();

        $empnumq = TkpEmpnum::model()->findAllByAttributes(array('opt1' => 1, 'daymonth' => $lmonth));
        if ($empnumq) {
            if ($type) {
                foreach ($empnumq as $v) {
                    $empAry["$v->area"]["$v->storecode"] = $v['empnum'];
                }
            } else {
                foreach ($empnumq as $v) {
                    $empAry[$v['storecode']] = $v['empnum'];
                }
            }
        }
        return $empAry;
    }
    
  /**
     * 建立員工薪資項目&備註
     * @param type $daymonth
     * @param type $empno
     * @param type $empname
     * @param type $itemno
     * @param type $value
     * @param type $memo
     * @return TbmEmpItem
     */
    public function createEmpItemMemo($daymonth,$empno,$empname,$itemno,$value,$memo){
        
        $empitem = new TbmEmpItem;
        
        $empitem->daymonth   = $daymonth;
        $empitem->empno       = $empno;
        $empitem->empname   = $empname;
        $empitem->itemno       = $itemno;
        $empitem->value         = $value;
        $empitem->opt1 = '1';
        $empitem->memo = $memo;
        $this->createData($empitem);
        
        return $empitem;
    }        

    /**
     * 取得員工該年度國定假日可休月份<br><br>
     * 比如員工2017/1/1進來, 會有12個值, <br>
     * 但如果員工 2017/12/1進來, 回傳為空陣列, 因沒有國假可以排<br>
國假編號	國假日期	國假名稱	國假可休天數	應放月份<br>
國1	1月1日	元日	12	1月<br>
國2	1月27日	除夕	11	2月<br>
國3	1月28日	春節初一	10	3月<br>
國4	1月29日	春節初二	9	4月<br>
國5	1月30日	春節初三	8	5月<br>
國6	2月28日	和平紀念	7	6月<br>
國7	4月3日	兒童節	6	7月<br>
國8	4月4日	清明節	5	8月<br>
國9	5月1日	51勞工節	4	9月<br>
國10	5月30日	端午節	3	10月<br>
國11	10月4日	中秋節	2	11月<br>
國12	10月10日	國慶日	1	12月<br>
     * 回傳 array(1=>'201701',...12=>'201712')
     * @param type $year - 西元年
     * @param type $empno - 員編
     * @return array - 國1-12,可休月份
     */
    public function getNational($year, $empno, $month=null) {

        $ary = array();
        if(strlen($year)>4)
            $year = substr($year,0,4);

//        CVarDumper::dump($year);
        if($year=='2019')
            $ary = array(
                '20190101'=>'1',
                '20190204'=>'2',
                '20190205'=>'3',
                '20190206'=>'4',
                '20190207'=>'5',
                '20190228'=>'6',
                '20190404'=>'7',
                '20190405'=>'8',
                '20190501'=>'9',
                '20190607'=>'10',
                '20190913'=>'11',
                '20191010'=>'12'
            );
        else if($year=='2018')
            // 先查出該年月, 國假日期(TABLE未建)
            $ary = array(
              '20180101'=>'1',
              '20180215'=>'2',
              '20180216'=>'3',
              '20180217'=>'4',
              '20180218'=>'5',
              '20180228'=>'6',
              '20180404'=>'7',
              '20180405'=>'8',
              '20180501'=>'9',
              '20180618'=>'10',
              '20180924'=>'11',
              '20181010'=>'12'
            );
        else if($year == '2021')
            $ary = array(
                '20210101'=>'1',
                '20210211'=>'2',
                '20210212'=>'3',
                '20210213'=>'4',
                '20210214'=>'5',
                '20210228'=>'6',
                '20210403'=>'7',
                '20210404'=>'8',
                '20210501'=>'9',
                '20210614'=>'10',
                '20210921'=>'11',
                '20211010'=>'12'
            );
        else
            $ary = array(
                '20200101'=>'1',
                '20200124'=>'2',
                '20200125'=>'3',
                '20200126'=>'4',
                '20200127'=>'5',
                '20200228'=>'6',
                '20200403'=>'7',
                '20200404'=>'8',
                '20200501'=>'9',
                '20200625'=>'10',
                '20201001'=>'11',
                '20201010'=>'12'
                );
        // 依傳入員編. 及西元年. 判斷可休之國定假日. 應放在幾月        
        // 取得員工到職日. 依到職日先判斷應有幾個國假
        
        //到職日期
        $arrivedate = '';
        
        if(isset($month) && $month !=null ) {
            
            $Ym = $year.$month;
            
            $empSql = "SELECT a.* FROM (tbs_emp_month a INNER JOIN ( SELECT `empno`, MAX( `daymonth` ) AS daymonth"
                    ." FROM tbs_emp_month WHERE `daymonth` <= '$Ym' AND empno = '$empno')b"
                    ." ON a.`empno` = b.`empno` AND a.`daymonth` = b.`daymonth` )";
        }else{

            $empSql = "SELECT a.* FROM (tbs_emp_month a INNER JOIN ( SELECT `empno`, MAX( `daymonth` ) AS daymonth"
                    ." FROM tbs_emp_month WHERE mid(`daymonth`,1,4) <= '$year' AND empno = '$empno')b"
                    ." ON a.`empno` = b.`empno` AND a.`daymonth` = b.`daymonth` )";
        }
        $tbsempmonth_Ary = TbsEmpMonth::model()->findAllBySql($empSql);
        
        foreach ($tbsempmonth_Ary as $item) {
            
            $arrivedate = $item['arrivedate'];
        }
        //到職日期去除"-"
        $date = explode('-', $arrivedate);
        //到職日期
        $arrivedates = date("Ymd", mktime(0, 0, 0,$date[1],$date[2],$date[0]));
        //到職-年月
        $arrivedate_Ym = date("Ym", mktime(0, 0, 0,$date[1],$date[2],$date[0]));
        //到職-天
        $arrivedate_day = date("d", mktime(0, 0, 0,0,$date[2],0));
         
        //留職停薪資料
        // 2017-7-18 新增需判斷留停日期是否包含在國假內
        $tmp = "SELECT * FROM `tbs_emp_month_log` WHERE mid(`datee`,1,4) = '$year' AND `empno` = '$empno'";
        
        $tbs_log = Yii::app()->db->createCommand($tmp)->queryAll();
        
        $dates_emplog = array();
        $datee_emplog = array();
        
        if(isset($tbs_log) && $tbs_log !=null) {
        
            foreach ($tbs_log as $value) {
                
                $dates = $value['dates']; 
                
                $datee = $value['datee'];                
            }
            //留停日期去除"-"
            $dates_log = explode('-', $dates);
            //留停日期-開始日期
            $dates_emplog = date("Ymd", mktime(0, 0, 0,$dates_log[1],$dates_log[2],$dates_log[0]));
            //留停日期去除"-"
            $datee_log = explode('-', $datee);
            //留停日期-結束日期
            $datee_emplog = date("Ymd", mktime(0, 0, 0,$datee_log[1],$datee_log[2],$datee_log[0]));
        }
        
        
        //回傳可放年月
        $national = array();
        $national_Ary = array();
            
       //array('20170101'=>'1',...
        foreach ($ary as $days =>$value) {
            
            //到職日 <= 年假日期則
            //20170523 <= 20170530            
            if($arrivedates <= $days) {
                
                if(isset($date_emplog) && $date_emplog !=NULL) {
                    
                    // 國假日 <= 留停第一天 && 留停最後一天 <= 國假日
                    if($days <= $dates_emplog && $datee_emplog <= $days) {
                        //國定假日->201705
                        $national_Ym = (int)substr($days, 0,6);
                        
                        //$national_count = $national_count + 1 ;
                        //如果到職年月(201701) = 國假年月(201701)  1
                        if($arrivedate_Ym == $national_Ym) {

                            //如果到職年月相等，而且到職日-天又是當月1號，則當月可休；否則隔月休
                            if($arrivedate_day == '1') {

                                $national = $this->getNationalArray($national, $value, $national_Ym);   
                            }else{

                                $national_Ym = $national_Ym + 1;
                                $national = $this->getNationalArray($national, $value, $national_Ym);   
                            }
                        }else{

                            $national = $this->getNationalArray($national, $value, $national_Ym);   
                        }
                    }
                }else{

                    //國定假日->201705
                    $national_Ym = (int)substr($days, 0,6);
                    
                    //$national_count = $national_count + 1 ;
                    //如果到職年月(201701) = 國假年月(201701)  1
                    if($arrivedate_Ym == $national_Ym) {
                       
                        //如果到職年月相等，而且到職日-天又是當月1號，則當月可休；否則隔月休
                        if($arrivedate_day == '1') {

                            $national = $this->getNationalArray($national, $value, $national_Ym);   
                        }else{

                            $national_Ym = $national_Ym + 1;
                            $national = $this->getNationalArray($national, $value, $national_Ym);   
                        }
                    }else{
                        
                        $national = $this->getNationalArray($national, $value, $national_Ym);   
                    }
                }
            }
        }
        
        
        $national_Ary['national_leave'] = $national;
        
        //到職日
        $national_Ary['arrivedates'] = $arrivedates;
        
        //國定假日休假紀錄，而且>=到職日
        $log_Ary = "SELECT `logday`,`memo` FROM `tba_log` WHERE mid(`logday`,1,4) = '$year' AND `logitem` = '30' AND `empno` = '$empno' AND `logday` >= '$arrivedates'";
            
        //$tba_log = Yii::app()->db->createCommand($log_Ary)->queryRow();
        $tba_log = Yii::app()->db->createCommand($log_Ary)->queryAll();
        
        if(isset($tba_log) && $tba_log !=NULL) {

                $national_Ary['tbalog'] = $tba_log;                
        }
       
        return array(
            
            $national_Ary
        );
    }
    
    /**
     * 取得員工該年度特休假可休月份<br><br>
     * 特休假，設計師最多可以放10天，放在3-12月。每月1天。<br>
     * 特休不足10天著，依淡月優先排。 11.12.9.3.4.5.6.7.8.10。<br>
     * 助理不發放不休假獎金，故凡年資符合有特休假時，可於1-12月平均排休。若超過12天。1個月可排2天以上。<br>
     * 滿半年時，未休之特休可往後延，但不得延至次年。<br>     * 
     * 回傳 array(1=>'201701',...14=>'201712')
     * @param type $year - 西元年
     * @param type $empno - 員編
     * @return array - 特1-N,可休月份
     */
    public function getAnnual($year, $empno, $month=null) {
        
        // 依傳入員編. 及西元年. 判斷可休之特休假
        $ary = array();


        if(strlen($year)>4)
            $year = substr($year,0,4);

        //特休陣列
        $Annual_leave_Ary = array();
        // 取得員工到職日. 依到職日先判斷應有幾個特休假
        
        //到職日期
        $arrivedate = '';
        
        if(isset($month) && $month !=null ) {
            
            $Ym = $year.$month;
            
            $empSql = "SELECT a.* FROM (tbs_emp_month a INNER JOIN ( SELECT `empno`, MAX( `daymonth` ) AS daymonth"
                    ." FROM tbs_emp_month WHERE `daymonth` <= '$Ym' AND empno = '$empno')b"
                    ." ON a.`empno` = b.`empno` AND a.`daymonth` = b.`daymonth` )";
        }else{

            $empSql = "SELECT a.* FROM (tbs_emp_month a INNER JOIN ( SELECT `empno`, MAX( `daymonth` ) AS daymonth"
                    ." FROM tbs_emp_month WHERE mid(`daymonth`,1,4) <= '$year' AND empno = '$empno')b"
                    ." ON a.`empno` = b.`empno` AND a.`daymonth` = b.`daymonth` )";
        }
        $tbsempmonth_Ary = TbsEmpMonth::model()->findAllBySql($empSql);
        
        foreach ($tbsempmonth_Ary as $item) {
            
            $arrivedate = $item['arrivedate'];
            
            $position = $item['position1'];
        }
        //到職日期去除"-"
        $date = explode('-', $arrivedate);
        //到職日期
        $arrivedates = date("Ymd", mktime(0, 0, 0,$date[1],$date[2],$date[0]));
        //到職-年月
        $arrivedate_Ym = date("Ym", mktime(0, 0, 0,$date[1],$date[2],$date[0]));
        //到職-年
        $arrivedate_Year = date("Y", mktime(0, 0, 0, $date[1],$date[2],$date[0]));        
        //到職-天
        $arrivedate_day = date("d", mktime(0, 0, 0,0,$date[2],0));
        //基準年年初
        //$End_year = date("Ymd", mktime(0, 0, 0,12,31,$year));
        //基準年年底
        $year_EndOfYear = date("Ymd", mktime(0, 0, 0,12,31,$year));
        
        // if (基準年 - 到職年) = 0 ，則可能未滿1年 EX. 基準年2017 到職年2017 { 
        //      if(
        //        到職年月日+(半年) > 12/ 31 ，未滿半年沒特休   
        //      )else{
        //        [ 12/ 31 - 到職年月日+半年(半年基準日) ] / 半年 * 3(半年特休)
        //      }
        // }else 滿一年，先算前半年 ([基準年+到職月日] - 1/1 ) ；在取得年資換算天數比例 ，再算後半年 (基準年 - 12/31 ) ；在取得年資換算天數比例
        // 兩個相加天數，在判斷職務，進去特休假建議表回傳
        
        //留職停薪資料
        // 2017-7-18 修改判斷為留停日期須大於到職日
        $tmp = "SELECT sum(`days`) as days FROM `tbs_emp_month_log` WHERE `datee` > '$arrivedate' AND `empno` = '$empno' AND `opt2` = '1'";
        
        $tbs_log = Yii::app()->db->createCommand($tmp)->queryAll();
        
        $Annual_tbslog = 0;
        
        if(isset($tbs_log) && $tbs_log !=NULL) {
            
            foreach ($tbs_log as $key => $value) {
                
                $days = $value['days'];
            }
                //留停扣除年資
                $Annual_tbslog = round(($days / 365),2);
                //留停扣除天數
                $Annual_tbslog_day = $days; 
        }
        //扣除年資
         $Annual_leave_Ary['tbslog'] = $Annual_tbslog;    
         
        //特休假
        $Annual_leave_num = 0;        
        //年資換算特休天數    
        $Annual_leave = $this->getAnnualLeave();
        
        //基準年-到職年 = 0，進迴圈判斷是否有半年特休
        if(($year - $arrivedate_Year) == 0) {
            //到職年月日+半年
            $arrivedate_AfterHalfYear = date("Ymd", mktime(0, 0, 0,$date[1]+6,$date[2],$date[0]));
            $arrivedate_AfterHalfYear_Month = date("m", mktime(0, 0, 0,$date[1]+6,$date[2],$date[0]));
            
            //到職年月日+半年 會在 基準年年底前達成，給3天特(按剩餘月份比例)
            if(strtotime($arrivedate_AfterHalfYear) < strtotime($year_EndOfYear)) {
                
                //以結算年份的年底 - 到職日，換算到職天數
                $arrive_day_total = (strtotime($year_EndOfYear) - strtotime($arrivedate)) / (60*60*24);
                //年資 = 總天數/365,取小數第2位
                $arrive_year = round(($arrive_day_total / 365),2);
                //年資陣列
                $Annual_leave_Ary['arrive_year'] = $arrive_year;
                //留停扣除天數>0
                if($Annual_tbslog_day > 0) {

                    //年資 = (總天數-扣除天數) / 365 ,取小數第2位
                    $arrive_year = round((($arrive_day_total-$Annual_tbslog_day) / 365),2);
                    
                    if($arrive_year >=0.5) {
                        // last_month = 12月- 滿半年的月份
                        $last_month = 12 - $arrivedate_AfterHalfYear_Month;
                        //無條件捨去
                        $Annual_leave_num = floor(($last_month / 6 ) * 3);
                    }else{
                        //無條件捨去
                        $Annual_leave_num = 0;
                    }
                }else{
                    // last_month = 12月- 滿半年的月份
                    $last_month = 12 - $arrivedate_AfterHalfYear_Month;
                    //無條件捨去
                    $Annual_leave_num = floor(($last_month / 6 ) * 3);
                }
            }else{
                //以結算年份的年底 - 到職日，換算到職天數
                $arrive_day_total = (strtotime($year_EndOfYear) - strtotime($arrivedate)) / (60*60*24);
                //年資 = 總天數/365,取小數第2位
                $arrive_year = round(($arrive_day_total / 365),2);
                //年資陣列
                $Annual_leave_Ary['arrive_year'] = $arrive_year;
            }
        }elseif(($year - $arrivedate_Year) > 0) {
            //以結算年份的年底 - 到職日，換算到職天數
            $arrive_day_total = (strtotime($year_EndOfYear) - strtotime($arrivedate)) / (60*60*24);
            //留停扣除天數>0
            if($Annual_tbslog_day > 0) {
                //年資 = (總天數-扣除天數)/365,取小數2位
                $arrive_year = round((($arrive_day_total - $Annual_tbslog_day) / 365),2) ;
                //年資-無條件捨去小數點
                $arriveyear = floor($arrive_year);
                
            }else{
                
                //年資 = 總天數/365,取小數2位
                $arrive_year = round(($arrive_day_total / 365),2) ;
                //年資-無條件捨去小數點
                $arriveyear = floor($arrive_year);
                
            }
            //年資 = 總天數/365,取小數2位
             $arrive_year = round(($arrive_day_total / 365),2) ;
            //年資陣列
            $Annual_leave_Ary['arrive_year'] = $arrive_year;
            
            if($arriveyear == 1) {
                // 如果留停>0 && 工作年資 - 實際年資 > 0 
                // 新到職日 = 到職日 + 留停天數
                if($Annual_tbslog_day > 0 && ($arrive_year-($arrive_year-$Annual_tbslog))>0) {
                    //新到職日期
                    $arrivedate = date("Y-m-d", mktime(0, 0, 0,$date[1],$date[2]+$Annual_tbslog_day,$date[0]));
                    //到職日期去除"-"
                    $date = explode('-', $arrivedate);
                    //到職滿一年
                    $arrivedate_AfterYear = date("Ymd", mktime(0, 0, 0,$date[1],$date[2],$date[0]+1));
                    
                    $arrivedate_AfterYear_Month = date("m", mktime(0, 0, 0,$date[1],$date[2],$date[0]+1));
                    //上半年
                    $First_half_year = $arrivedate_AfterYear_Month;
                }else{
                
                    //到職滿一年
                    $arrivedate_AfterYear = date("Ymd", mktime(0, 0, 0,$date[1],$date[2],$date[0]+1));
                    $arrivedate_AfterYear_Month = date("m", mktime(0, 0, 0,$date[1],$date[2],$date[0]+1));
                    //上半年
                    $First_half_year = $arrivedate_AfterYear_Month;
                }
                //上半年超過6個月，直接給3天特休
                if($First_half_year >= 6) {
                    
                    $Annual_leave_first_half_num = 3;
                }else{
                    //有滿半年的條件，給3天特(按剩餘月份比例)
                    $Annual_leave_first_half_num = ceil(($First_half_year / 6 ) * 3);
                }
                                
                //下半年，如果到職日是當月1號，則+1
                if($arrivedate_day == '1') {
                    $Second_half_year = (12 - $arrivedate_AfterYear_Month) + 1;
                }else{
                    $Second_half_year = 12 - $arrivedate_AfterYear_Month;
                }
                
                if($Second_half_year > 0) {
                    
                    //下半年特休-無條件捨去
                    $Annual_leave_second_half_num = floor(($Second_half_year / 12 ) * 7);
                }else{
                    //如果12月到職，則下半年無特休
                    $Annual_leave_second_half_num = 0;
                }
                
                //特休假天數 = 上半年3天特 + 下半年滿一年7天特(按比例)
                $Annual_leave_num = $Annual_leave_first_half_num + $Annual_leave_second_half_num;
                
            }elseif($arriveyear >= 2) {
                
                //年資滿2年(含以上)，要算上年度的比例(按比例)
                $arrive_years = $arriveyear - 1;
                //上年度特休天數，如果有扣除年資要先扣除
                $before_year_Annual_leave_num = $Annual_leave[$arrive_years];
                //今年度特休天數，如果有扣除年資要先扣除
                $After_year_Annual_leave_num = $Annual_leave[$arriveyear];
                
                // 如果留停>0 && 工作年資 - 實際年資 > 0 
                // 新到職日 = 到職日 + 留停天數
                if($Annual_tbslog_day > 0 && ($arrive_year-($arrive_year-$Annual_tbslog))>0) {
                    //新到職日期
                    $arrivedate = date("Y-m-d", mktime(0, 0, 0,$date[1],$date[2]+$Annual_tbslog_day,$date[0]));
                    //到職日期去除"-"
                    $date = explode('-', $arrivedate);
                    //到職滿一年
                    $arrivedate_AfterYear = date("Ymd", mktime(0, 0, 0,$date[1],$date[2],$date[0]+1));
                    
                    $arrivedate_AfterYear_Month = date("m", mktime(0, 0, 0,$date[1],$date[2],$date[0]+1));
                    //上半年
                    $First_half_year = $arrivedate_AfterYear_Month -1;
                }else{
                
                    //到職 >= 2年
                    $arrivedate_AfterYear = date("Ymd", mktime(0, 0, 0,$date[1],$date[2],$date[0]+1));
                    $arrivedate_AfterYear_Month = date("m", mktime(0, 0, 0,$date[1],$date[2],$date[0]+1));
                    //上半年
                    $First_half_year = $arrivedate_AfterYear_Month - 1;
                }
                //上半年特休-無條件進位
                $Annual_leave_first_half_num = ceil(($First_half_year / 12 ) * $before_year_Annual_leave_num);
                //下半年
                $Second_half_year = (12 - $arrivedate_AfterYear_Month) + 1;
                //下半年特休-無條件捨去
                $Annual_leave_second_half_num = floor(($Second_half_year / 12 ) * $After_year_Annual_leave_num);
                //特休假
                $Annual_leave_num = $Annual_leave_first_half_num + $Annual_leave_second_half_num;
                
            }else{
                //扣完年資低於1年，滿半年3天特 or 沒有
                if($arriveyear >= 0.5) {
                    
                    // last_month = 12月- 滿半年的月份
                    $last_month = 12 - $arrivedate_AfterHalfYear_Month;
                    //無條件捨去
                    $Annual_leave_num = floor(($last_month / 6 ) * 3);
                }else{
                    
                    $Annual_leave_num = 0;
                }
            }
        }
        
        // 再依特休天數去判斷特休可放年月
        // 設計師1,2月不能放
        // 助理1年可以放到14天...30天.
        
        if($Annual_leave_num > 0 ) {
            //職位 = 助理(17)  
            if($position == 17){

                //特休天數換對應月份-助理特休    
                $Annual_leave_Ary = $this->getAN_Annual_leave($Annual_leave_Ary,$Annual_leave_num);
            }else{
                
                //特休天數換對應月份-設計師特休    
                $Annual_leave_Ary = $this->getDN_Annual_leave($Annual_leave_Ary,$Annual_leave_num);
            }
        }
        
        //年資天數
        $Annual_leave_Ary['Annual_leave_num'] = $Annual_leave_num;
        
        //特休假休假紀錄，而且>=到職日
        $log_Ary = "SELECT `logday`,`memo` FROM `tba_log` WHERE mid(`logday`,1,4) = '$year' AND `logitem` = '8' AND `empno` = '$empno' AND `logday` >= '$arrivedates'";

        $tba_log = Yii::app()->db->createCommand($log_Ary)->queryAll();
        
        if(isset($tba_log) && $tba_log !=NULL) {

                $Annual_leave_Ary['tbalog'] = $tba_log;                
        }
        
        return array(
          
            $Annual_leave_Ary
        );
    }
    
    /*
     * @param type $national_Ary - 國定假日放假ARRYY
     * @param type $value - 國定假日次數
     * @param type $national_Ym - 國定假日的月份
     *  // '10' => '201705'
     *  // '11' => '201710'
     *  // '12' => '201710'
     */
    private function getNationalArray($national,$value,$national_Ym) {
        // Ary[10] = 201710
        $Ary = array();
        
        if($national == NULL) {
            
            //$national_Ary[1] => '201701'
            //$national_Ary[2] => '201701'
            $national[$value] = $national_Ym;
        }else{
            
            $Ary[$value] = $national_Ym;
        } 

        //    '20170403'=>'7'  ->201704
        //    '20170404'=>'8'  ->201705
        //    '20170501'=>'9'  ->201706
        //    '20170530'=>'10' ->201707
        //    '20171004'=>'11' ->201710
        //    '20171010'=>'12' ->201711
 
        if(isset($Ary) && $Ary !=NULL) {
            // 7 => 201704
            // 8 => 201704
            foreach ($Ary as $key => $value) {
                
                //如果丟進來的年月在$national_Ary 存在，則陣列的年.月+1
                if(in_array($value, $national)) {
                    
                    foreach ($national as $count => $date) {
            
                        $national[$key] = $date + 1;
                    }
                    
                }else{
                    
                    $national[$key] = $value;
                }
            }
        }
        
        return $national;
    }
    
    /**
     *  年資換算特休天數
     *  年資 => 特休天
     */
    private function getAnnualLeave() {
        
        $Annual_leave = array(
            
                 '0.5'  =>3, 
                   '1'  =>7,
                   '2'  =>10,
                   '3'  =>14,
                   '4'  =>14,
                   '5'  =>15,
                   '6'  =>15,
                   '7'  =>15,
                   '8'  =>15,
                   '9'  =>15,
                   '10' =>16,
                   '11' =>17,
                   '12' =>18,
                   '13' =>19,
                   '14' =>20,
                   '15' =>21,
                   '16' =>22,
                   '17' =>23,
                   '18' =>24,
                   '19' =>25,
                   '20' =>26,
                   '21' =>27,
                   '22' =>28,
                   '23' =>29,
                   '24' =>30,
                   '25' =>30,
                );
        
        return $Annual_leave;    
    }
    
    /*
     * 取得助理特休假休假順序
     * 助理不發放不休假獎金，故凡年資符合有特休假時，可於1-12月平均排休。
     * 助理特休依淡月優先排。 11.12.9.3.4.5.6.7.8.10.1.2。
     * 回傳 array(1=>'201701',...14=>'201712')
     */
    private function getAN_Annual_leave($Annual_leave_Ary,$Annual_leave_num) {
        //天數 => 月份
        $Ary = array(
                   1  =>11,
                   2  =>12,
                   3  =>9,
                   4  =>3,
                   5  =>4,
                   6  =>5,
                   7  =>6,
                   8  =>7,
                   9  =>8,
                   10 =>10,
                   11 =>1,
                   12 =>2,
                   13 =>11,
                   14 =>12,
                   15 =>9,
                   16 =>3,
                   17 =>4,
                   18 =>5,
                   19 =>6,
                   20 =>7,
                   21 =>8,
                   22 =>10,
                   23 =>1,
                   24 =>2,
                   25 =>11,
                   26 =>12,
                   27 =>9,
                   28 =>3,
                   29 =>4,
                   30 =>5,
        );
        
        foreach ($Ary as $key => $value) {
            // 特休天數 <= 陣列次數
            if($Annual_leave_num >= $key) {
                
                $Annual_leave_Ary['Annual_leave'][$key] = $value;
            }
        }
        return $Annual_leave_Ary;
    }
    
     /*
      * 特休假，最多可以放10天，放在3-12月。每月1天。(設計師區經理講師)
      * 特休不足10天著，依淡月優先排。 11.12.9.3.4.5.6.7.8.10。
      * 未休之特休假部份(超過10天者)，隔年計算不休假獎金。
      * 取得設計師.講師.區經理特休假休假順序
      * 故凡年資符合有特休假時，可於3-12月平均排休。
      * 助理特休依淡月優先排。 11.12.9.3.4.5.6.7.8.10
      * 回傳 array(1=>'201701',...10=>'201712')
     */
    private function getDN_Annual_leave($Annual_leave_Ary,$Annual_leave_num) {
        //天數 => 月份
        $Ary = array(
                   1  =>11,
                   2  =>12,
                   3  =>9,
                   4  =>3,
                   5  =>4,
                   6  =>5,
                   7  =>6,
                   8  =>7,
                   9  =>8,
                   10 =>10,
        );
        
        foreach ($Ary as $key => $value) {
            // 特休天數 <= 陣列次數
            if($Annual_leave_num >= $key) {
                
                $Annual_leave_Ary['Annual_leave'][$key] = $value;
            }
        }
        return $Annual_leave_Ary;
    }

    /**
     * 取得國字星期幾
     * @param datetime : 日期
     * @return string : 星期幾
     */
    public function getChineseWeek($datetime) {
        $weekday  = date('w', strtotime($datetime));
        $weeklist = array('日', '一', '二', '三', '四', '五', '六');
        return $weeklist[$weekday];
    }


    /**
     * 用以設定建立資料的人員及時間
     * @param type $model
     */
    public function createData($model){

        $model->ctime =  date("Y-m-d H:i:s", time());
        $model->cemp = Yii::app()->user->getId();                        
        $model->ip = $this->getMyip();
    }

    /**
     * 取得IP
     * @return type
     */
    public function getMyip(){
        
        $myip = '';
        if (empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
            $myip = $_SERVER['REMOTE_ADDR'];  
        } else {  
            $myip = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);  
            $myip = $myip[0];  
        }
        return $myip;
    }

    /**
     * 取得員工對應年月資料
     * getEmpData
     * tbsMonth
     * storename - 門市名稱 
     * areaname - 區域名稱
     * @param  mixed $daymonth - 不傳則預設查詢時的年月
     * @param  mixed $depart - 預設營業部
     * @param  mixed $onJob - 是否在職(目前在用)
     *
     * @return array - 員工資料, 
     */
    public function getEmpData($daymonth=NULL, $depart = 3, $onJob = FALSE) {

        // 
        $empAry = array();
        $storeAry = array();
        $areaAry = array();

        $YM = isset($daymonth)?$daymonth:date('Ym');
        $sql = "SELECT a.* FROM ( 
                    tbs_emp_month a RIGHT JOIN ( 
                        SELECT empno, MAX( daymonth ) AS daymonth, storecode 
                        FROM tbs_emp_month WHERE daymonth <= '$YM' GROUP BY empno ) b 
                        ON a.empno = b.empno AND a.daymonth = b.daymonth 
                    )";
        
        $tmpAry = Yii::app()->db->createCommand($sql)->queryAll();

        $result = $this->getStoreArray();
        $storeAry = $result[0];
        $areaAry = $result[2];

        if($depart != 3) {
            $storeAry['A07001'] = '管理部';
            $storeAry['C00700'] = '髮學院';            
            $areaAry['A07001'] = '總公司';
            $areaAry['C00700'] = '髮學院';
        }

        foreach ($tmpAry as $emp) {

            $empno = $emp['empno'];

            if(isset($emp['storecode'])) {

                $empAry[$empno]['empno'] = $emp['empno'];
                $empAry[$empno]['empname'] = $emp['empname'];
                // 補０
                $storecode = $this->addZero($emp['storecode'],5);

                $empAry[$empno]['storecode'] = $storecode;
                $empAry[$empno]['storename'] = isset($storeAry[$storecode])?$storeAry[$storecode]:'';
                $empAry[$empno]['areaname'] = isset($areaAry[$storecode])?$areaAry[$storecode]:''; 
            }
        }

        return $empAry;
    }    
}
