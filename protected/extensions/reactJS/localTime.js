function tick(){
	const element = (           //將要渲染的內容放在變數裡
      <div>
         <h5>{new Date().toLocaleTimeString()}</h5>
      </div>
    )
      ReactDOM.render(
        element,               //呼叫存有渲染內容的變數
        document.getElementById('clockTime')
      );
    }
	// tick();
	setInterval(tick, 1000);
  