/*
 * Metro 4 Components Library 4.4.3  (https://metroui.org.ua)
 * Copyright 2012-2022 by Serhii Pimenov (https://pimenov.com.ua). All rights reserved.
 * Built at 22/04/2022 05:02:44
 * Licensed under MIT
 */



/*
 * m4q v1.0.10, (https://github.com/olton/m4q.git)
 * Copyright 2018 - 2020 by Sergey Pimenov
 * Helper for DOM manipulation, animation, and ajax routines.
 * Licensed under MIT
 */

 (function (global, undefined) {

// Source: src/mode.js

/* jshint -W097 */
'use strict';

// Source: src/func.js

/* global dataSet */
/* exported isTouch, isSimple, isHidden, isPlainObject, isEmptyObject, isArrayLike, str2arr, parseUnit, getUnit, setStyleProp, acceptData, dataAttr, normName, strip, dashedName, isLocalhost */

var numProps = ['opacity', 'zIndex'];

function isSimple(v){
    return typeof v === "string" || typeof v === "boolean" || typeof v === "number";
}

function isVisible(elem) {
    return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
}

function isHidden(elem) {
    var s = getComputedStyle(elem);
    return !isVisible(elem) || +s.opacity === 0 || elem.hidden || s.visibility === "hidden";
}

function not(value){
    return value === undefined || value === null;
}

function camelCase(string){
    return string.replace( /-([a-z])/g, function(all, letter){
        return letter.toUpperCase();
    });
}

function dashedName(str){
    return str.replace(/([A-Z])/g, function(u) { return "-" + u.toLowerCase(); });
}

function isPlainObject( obj ) {
    var proto;
    if ( !obj || Object.prototype.toString.call( obj ) !== "[object Object]" ) {
        return false;
    }
    proto = obj.prototype !== undefined;
    if ( !proto ) {
        return true;
    }
    return proto.constructor && typeof proto.constructor === "function";
}

function isEmptyObject( obj ) {
    for (var name in obj ) {
        if (hasProp(obj, name)) return false;
    }
    return true;
}

function isArrayLike (o){
    return o instanceof Object && 'length' in o;
}

function str2arr (str, sep) {
    sep = sep || " ";
    return str.split(sep).map(function(el){
        return  (""+el).trim();
    }).filter(function(el){
        return el !== "";
    });
}

function parseUnit(str, out) {
    if (!out) out = [ 0, '' ];
    str = String(str);
    out[0] = parseFloat(str);
    out[1] = str.match(/[\d.\-+]*\s*(.*)/)[1] || '';
    return out;
}

function getUnit(val, und){
    var split = /[+-]?\d*\.?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?(%|px|pt|em|rem|in|cm|mm|ex|ch|pc|vw|vh|vmin|vmax|deg|rad|turn)?$/.exec(val);
    return typeof split[1] !== "undefined" ? split[1] : und;
}

function setStyleProp(el, key, val){
    key = camelCase(key);

    if (["scrollLeft", "scrollTop"].indexOf(key) > -1) {
        el[key] = (parseInt(val));
    } else {
        el.style[key] = isNaN(val) || numProps.indexOf(""+key) > -1 ? val : val + 'px';
    }
}

function acceptData(owner){
    return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
}

function getData(data){
    try {
        return JSON.parse(data);
    } catch (e) {
        return data;
    }
}

function dataAttr(elem, key, data){
    var name;

    if ( not(data) && elem.nodeType === 1 ) {
        name = "data-" + key.replace( /[A-Z]/g, "-$&" ).toLowerCase();
        data = elem.getAttribute( name );

        if ( typeof data === "string" ) {
            data = getData( data );
            dataSet.set( elem, key, data );
        } else {
            data = undefined;
        }
    }
    return data;
}

function normName(name) {
    return typeof name !== "string" ? undefined : name.replace(/-/g, "").toLowerCase();
}

function strip(name, what) {
    return typeof name !== "string" ? undefined : name.replace(what, "");
}

function hasProp(obj, prop){
    return Object.prototype.hasOwnProperty.call(obj, prop);
}

function isLocalhost(host){
    var hostname = host || window.location.hostname;
    return (
        hostname === "localhost" ||
        hostname === "127.0.0.1" ||
        hostname === "[::1]" ||
        hostname === "" ||
        hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/) !== null
    );
}

function isTouch() {
    return (('ontouchstart' in window)
        || (navigator.maxTouchPoints > 0)
        || (navigator.msMaxTouchPoints > 0));
}

// Source: src/setimmediate.js

/* global global */
/*
 * setImmediate polyfill
 * Version 1.0.5
 * Url: https://github.com/YuzuJS/setImmediate
 * Copyright (c) 2016 Yuzu (https://github.com/YuzuJS)
 * Licensed under MIT
 */
(function (global) {

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1;
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var registerImmediate;

    function setImmediate(callback) {
        if (typeof callback !== "function") {
            /* jshint -W054 */
            callback = new Function("" + callback);
        }
        var args = new Array(arguments.length - 1);
        for (var i = 0; i < args.length; i++) {
            args[i] = arguments[i + 1];
        }
        tasksByHandle[nextHandle] = { callback: callback, args: args };
        registerImmediate(nextHandle);
        return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
            case 0:
                callback();
                break;
            case 1:
                callback(args[0]);
                break;
            case 2:
                callback(args[0], args[1]);
                break;
            case 3:
                callback(args[0], args[1], args[2]);
                break;
            default:
                callback.apply(undefined, args);
                break;
        }
    }

    function runIfPresent(handle) {
        if (currentlyRunningATask) {
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    // global.process
    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            global.process.nextTick(function () { runIfPresent(handle); });
        };
    }

    // web workers
    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    // Browsers
    function installPostMessageImplementation() {
        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        global.addEventListener("message", onGlobalMessage, false);

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    if ({}.toString.call(global.process) === "[object process]") {

        installNextTickImplementation();

    } else if (global.MessageChannel) {

        installMessageChannelImplementation();

    } else {

        installPostMessageImplementation();

    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;

}(typeof self === "undefined" ? typeof global === "undefined" ? window : global : self));

// Source: src/promise.js

/* global setImmediate */

/*
 * Promise polyfill
 * Version 1.2.0
 * Url: https://github.com/lahmatiy/es6-promise-polyfill
 * Copyright (c) 2014 Roman Dvornov
 * Licensed under MIT
 */
(function (global) {

    if (global.Promise) {
        return;
    }

    // console.log("Promise polyfill v1.2.0");

    var PENDING = 'pending';
    var SEALED = 'sealed';
    var FULFILLED = 'fulfilled';
    var REJECTED = 'rejected';
    var NOOP = function(){};

    function isArray(value) {
        return Object.prototype.toString.call(value) === '[object Array]';
    }

    // async calls
    var asyncSetTimer = typeof setImmediate !== 'undefined' ? setImmediate : setTimeout;
    var asyncQueue = [];
    var asyncTimer;

    function asyncFlush(){
        // run promise callbacks
        for (var i = 0; i < asyncQueue.length; i++)
            asyncQueue[i][0](asyncQueue[i][1]);

        // reset async asyncQueue
        asyncQueue = [];
        asyncTimer = false;
    }

    function asyncCall(callback, arg){
        asyncQueue.push([callback, arg]);

        if (!asyncTimer)
        {
            asyncTimer = true;
            asyncSetTimer(asyncFlush, 0);
        }
    }

    function invokeResolver(resolver, promise) {
        function resolvePromise(value) {
            resolve(promise, value);
        }

        function rejectPromise(reason) {
            reject(promise, reason);
        }

        try {
            resolver(resolvePromise, rejectPromise);
        } catch(e) {
            rejectPromise(e);
        }
    }

    function invokeCallback(subscriber){
        var owner = subscriber.owner;
        var settled = owner.state_;
        var value = owner.data_;
        var callback = subscriber[settled];
        var promise = subscriber.then;

        if (typeof callback === 'function')
        {
            settled = FULFILLED;
            try {
                value = callback(value);
            } catch(e) {
                reject(promise, e);
            }
        }

        if (!handleThenable(promise, value))
        {
            if (settled === FULFILLED)
                resolve(promise, value);

            if (settled === REJECTED)
                reject(promise, value);
        }
    }

    function handleThenable(promise, value) {
        var resolved;

        try {
            if (promise === value)
                throw new TypeError('A promises callback cannot return that same promise.');

            if (value && (typeof value === 'function' || typeof value === 'object'))
            {
                var then = value.then;  // then should be retrived only once

                if (typeof then === 'function')
                {
                    then.call(value, function(val){
                        if (!resolved)
                        {
                            resolved = true;

                            if (value !== val)
                                resolve(promise, val);
                            else
                                fulfill(promise, val);
                        }
                    }, function(reason){
                        if (!resolved)
                        {
                            resolved = true;

                            reject(promise, reason);
                        }
                    });

                    return true;
                }
            }
        } catch (e) {
            if (!resolved)
                reject(promise, e);

            return true;
        }

        return false;
    }

    function resolve(promise, value){
        if (promise === value || !handleThenable(promise, value))
            fulfill(promise, value);
    }

    function fulfill(promise, value){
        if (promise.state_ === PENDING)
        {
            promise.state_ = SEALED;
            promise.data_ = value;

            asyncCall(publishFulfillment, promise);
        }
    }

    function reject(promise, reason){
        if (promise.state_ === PENDING)
        {
            promise.state_ = SEALED;
            promise.data_ = reason;

            asyncCall(publishRejection, promise);
        }
    }

    function publish(promise) {
        var callbacks = promise.then_;
        promise.then_ = undefined;

        for (var i = 0; i < callbacks.length; i++) {
            invokeCallback(callbacks[i]);
        }
    }

    function publishFulfillment(promise){
        promise.state_ = FULFILLED;
        publish(promise);
    }

    function publishRejection(promise){
        promise.state_ = REJECTED;
        publish(promise);
    }

    /**
     * @class
     */
    function Promise(resolver){
        if (typeof resolver !== 'function')
            throw new TypeError('Promise constructor takes a function argument');

        if (!(this instanceof Promise))
            throw new TypeError('Failed to construct \'Promise\': Please use the \'new\' operator, this object constructor cannot be called as a function.');

        this.then_ = [];

        invokeResolver(resolver, this);
    }

    Promise.prototype = {
        constructor: Promise,

        state_: PENDING,
        then_: null,
        data_: undefined,

        then: function(onFulfillment, onRejection){
            var subscriber = {
                owner: this,
                then: new this.constructor(NOOP),
                fulfilled: onFulfillment,
                rejected: onRejection
            };

            if (this.state_ === FULFILLED || this.state_ === REJECTED)
            {
                // already resolved, call callback async
                asyncCall(invokeCallback, subscriber);
            }
            else
            {
                // subscribe
                this.then_.push(subscriber);
            }

            return subscriber.then;
        },

        done: function(onFulfillment){
            return this.then(onFulfillment, null);
        },

        always: function(onAlways){
            return this.then(onAlways, onAlways);
        },

        'catch': function(onRejection) {
            return this.then(null, onRejection);
        }
    };

    Promise.all = function(promises){
        var Class = this;

        if (!isArray(promises))
            throw new TypeError('You must pass an array to Promise.all().');

        return new Class(function(resolve, reject){
            var results = [];
            var remaining = 0;

            function resolver(index){
                remaining++;
                return function(value){
                    results[index] = value;
                    if (!--remaining)
                        resolve(results);
                };
            }

            for (var i = 0, promise; i < promises.length; i++)
            {
                promise = promises[i];

                if (promise && typeof promise.then === 'function')
                    promise.then(resolver(i), reject);
                else
                    results[i] = promise;
            }

            if (!remaining)
                resolve(results);
        });
    };

    Promise.race = function(promises){
        var Class = this;

        if (!isArray(promises))
            throw new TypeError('You must pass an array to Promise.race().');

        return new Class(function(resolve, reject) {
            for (var i = 0, promise; i < promises.length; i++)
            {
                promise = promises[i];

                if (promise && typeof promise.then === 'function')
                    promise.then(resolve, reject);
                else
                    resolve(promise);
            }
        });
    };

    Promise.resolve = function(value){
        var Class = this;

        if (value && typeof value === 'object' && value.constructor === Class)
            return value;

        return new Class(function(resolve){
            resolve(value);
        });
    };

    Promise.reject = function(reason){
        var Class = this;

        return new Class(function(resolve, reject){
            reject(reason);
        });
    };

    if (typeof  global.Promise === "undefined") {
        global.Promise = Promise;
    }
}(window));

// Source: src/core.js

/* global hasProp */

var m4qVersion = "v1.0.10. Built at 08/12/2020 00:01:48";

/* eslint-disable-next-line */
var matches = Element.prototype.matches
    || Element.prototype.matchesSelector
    || Element.prototype.webkitMatchesSelector
    || Element.prototype.mozMatchesSelector
    || Element.prototype.msMatchesSelector
    || Element.prototype.oMatchesSelector;

var $ = function(selector, context){
    return new $.init(selector, context);
};

$.version = m4qVersion;

$.fn = $.prototype = {
    version: m4qVersion,
    constructor: $,
    length: 0,
    uid: "",

    push: [].push,
    sort: [].sort,
    splice: [].splice,
    indexOf: [].indexOf,
    reverse: [].reverse
};

$.extend = $.fn.extend = function(){
    var options, name,
        target = arguments[ 0 ] || {},
        i = 1,
        length = arguments.length;

    if ( typeof target !== "object" && typeof target !== "function" ) {
        target = {};
    }

    if ( i === length ) {
        target = this;
        i--;
    }

    for ( ; i < length; i++ ) {
        if ( ( options = arguments[ i ] ) != null ) {
            for ( name in options ) {
                if (hasProp(options, name))
                    target[ name ] = options[ name ];
            }
        }
    }

    return target;
};

$.assign = function(){
    var options, name,
        target = arguments[ 0 ] || {},
        i = 1,
        length = arguments.length;

    if ( typeof target !== "object" && typeof target !== "function" ) {
        target = {};
    }

    if ( i === length ) {
        target = this;
        i--;
    }

    for ( ; i < length; i++ ) {
        if ( ( options = arguments[ i ] ) != null ) {
            for ( name in options ) {
                if (hasProp(options, name) && options[name] !== undefined)
                    target[ name ] = options[ name ];
            }
        }
    }

    return target;
};

// if (typeof window["hideM4QVersion"] === "undefined") console.info("m4q " + $.version);

// Source: src/interval.js

/* global $ */

var now = function(){
    return Date.now();
};

$.extend({

    intervalId: -1,
    intervalQueue: [],
    intervalTicking: false,
    intervalTickId: null,

    setInterval: function(fn, int){
        var that = this;

        this.intervalId++;

        this.intervalQueue.push({
            id: this.intervalId,
            fn: fn,
            interval: int,
            lastTime: now()
        });

        if (!this.intervalTicking) {
            var tick = function(){
                that.intervalTickId = requestAnimationFrame(tick);
                $.each(that.intervalQueue, function(){
                    var item = this;
                    if (item.interval < 17 || now() - item.lastTime >= item.interval) {
                        item.fn();
                        item.lastTime = now();
                    }
                });
            };
            this.intervalTicking = true;
            tick();
        }

        return this.intervalId;
    },

    clearInterval: function(id){
        for(var i = 0; i < this.intervalQueue.length; i++){
            if (id === this.intervalQueue[i].id) {
                this.intervalQueue.splice(i, 1);
                break;
            }
        }
        if (this.intervalQueue.length === 0) {
            cancelAnimationFrame(this.intervalTickId);
            this.intervalTicking = false;
        }
    },

    setTimeout: function(fn, interval){
        var that = this, id = this.setInterval(function(){
            that.clearInterval(id);
            fn();
        }, interval);

        return id;
    },

    clearTimeout: function(id){
        return this.clearInterval(id);
    }
});

// Source: src/contains.js

/* global $, not, matches, isArrayLike, isVisible */

$.fn.extend({
    index: function(sel){
        var el, _index = -1;

        if (this.length === 0) {
            return _index;
        }

        if (not(sel)) {
            el = this[0];
        } else if (sel instanceof $ && sel.length > 0) {
            el = sel[0];
        } else if (typeof sel === "string") {
            el = $(sel)[0];
        } else {
            el = undefined;
        }

        if (not(el)) {
            return _index;
        }

        if (el && el.parentNode) $.each(el.parentNode.children, function(i){
            if (this === el) {
                _index = i;
            }
        });
        return _index;
    },

    get: function(i){
        if (i === undefined) {
            return this.items();
        }
        return i < 0 ? this[ i + this.length ] : this[ i ];
    },

    eq: function(i){
        return !not(i) && this.length > 0 ? $.extend($(this.get(i)), {_prevObj: this}) : this;
    },

    is: function(s){
        var result = false;

        if (this.length === 0) {
            return false;
        }

        if (s instanceof $) {
            return this.same(s);
        }

        if (s === ":selected") {
            this.each(function(){
                if (this.selected) result = true;
            });
        } else

        if (s === ":checked") {
            this.each(function(){
                if (this.checked) result = true;
            });
        } else

        if (s === ":visible") {
            this.each(function(){
                if (isVisible(this)) result = true;
            });
        } else

        if (s === ":hidden") {
            this.each(function(){
                var styles = getComputedStyle(this);
                if (
                    this.getAttribute('type') === 'hidden'
                        || this.hidden
                        || styles.display === 'none'
                        || styles.visibility === 'hidden'
                        || parseInt(styles.opacity) === 0
                ) result = true;
            });
        } else

        if (typeof  s === "string" && [':selected'].indexOf(s) === -1) {
            this.each(function(){
                if (matches.call(this, s)) {
                    result = true;
                }
            });
        } else

        if (isArrayLike(s)) {
            this.each(function(){
                var el = this;
                $.each(s, function(){
                    var sel = this;
                    if (el === sel) {
                        result = true;
                    }
                });
            });
        } else

        if (typeof s === "object" && s.nodeType === 1) {
            this.each(function(){
                if  (this === s) {
                    result = true;
                }
            });
        }

        return result;
    },

    same: function(o){
        var result = true;

        if (!(o instanceof $)) {
            o = $(o);
        }

        if (this.length !== o.length) return false;

        this.each(function(){
            if (o.items().indexOf(this) === -1) {
                result = false;
            }
        });

        return result;
    },

    last: function(){
        return this.eq(this.length - 1);
    },

    first: function(){
        return this.eq(0);
    },

    odd: function(){
        var result = this.filter(function(el, i){
            return i % 2 === 0;
        });
        return $.extend(result, {_prevObj: this});
    },

    even: function(){
        var result = this.filter(function(el, i){
            return i % 2 !== 0;
        });
        return $.extend(result, {_prevObj: this});
    },

    filter: function(fn){
        if (typeof fn === "string") {
            var sel = fn;
            fn = function(el){
                return matches.call(el, sel);
            };
        }

        return $.extend($.merge($(), [].filter.call(this, fn)), {_prevObj: this});
    },

    find: function(s){
        var res = [], result;

        if (s instanceof $) return s;

        if (this.length === 0) {
            result = this;
        } else {
            this.each(function () {
                var el = this;
                if (typeof el.querySelectorAll === "undefined") {
                    return ;
                }
                res = res.concat([].slice.call(el.querySelectorAll(s)));
            });
            result = $.merge($(), res);
        }

        return $.extend(result, {_prevObj: this});
    },

    contains: function(s){
        return this.find(s).length > 0;
    },

    children: function(s){
        var i, res = [];

        if (s instanceof $) return s;

        this.each(function(){
            var el = this;
            for(i = 0; i < el.children.length; i++) {
                if (el.children[i].nodeType === 1)
                    res.push(el.children[i]);
            }
        });
        res = s ? res.filter(function(el){
            return matches.call(el, s);
        }) : res;

        return $.extend($.merge($(), res), {_prevObj: this});
    },

    parent: function(s){
        var res = [];
        if (this.length === 0) {
            return ;
        }

        if (s instanceof $) return s;

        this.each(function(){
            if (this.parentNode) {
                if (res.indexOf(this.parentNode) === -1) res.push(this.parentNode);
            }
        });
        res = s ? res.filter(function(el){
            return matches.call(el, s);
        }) : res;

        return $.extend($.merge($(), res), {_prevObj: this});
    },

    parents: function(s){
        var res = [];

        if (this.length === 0) {
            return ;
        }

        if (s instanceof $) return s;

        this.each(function(){
            var par = this.parentNode;
            while (par) {
                if (par.nodeType === 1 && res.indexOf(par) === -1) {
                    if (!not(s)) {
                        if (matches.call(par, s)) {
                            res.push(par);
                        }
                    } else {
                        res.push(par);
                    }
                }
                par = par.parentNode;
            }
        });

        return $.extend($.merge($(), res), {_prevObj: this});
    },

    siblings: function(s){
        var res = [];

        if (this.length === 0) {
            return ;
        }

        if (s instanceof $) return s;

        this.each(function(){
            var el = this;
            if (el.parentNode) {
                $.each(el.parentNode.children, function(){
                    if (el !== this) res.push(this);
                });
            }
        });

        if (s) {
            res = res.filter(function(el){
                return matches.call(el, s);
            });
        }

        return $.extend($.merge($(), res), {_prevObj: this});
    },

    _siblingAll: function(dir, s){
        var res = [];

        if (this.length === 0) {
            return ;
        }

        if (s instanceof $) return s;

        this.each(function(){
            var el = this;
            while (el) {
                el = el[dir];
                if (!el) break;
                res.push(el);
            }
        });

        if (s) {
            res = res.filter(function(el){
                return matches.call(el, s);
            });
        }

        return $.extend($.merge($(), res), {_prevObj: this});
    },

    _sibling: function(dir, s){
        var res = [];

        if (this.length === 0) {
            return ;
        }

        if (s instanceof $) return s;

        this.each(function(){
            var el = this[dir];
            if (el && el.nodeType === 1) {
                res.push(el);
            }
        });

        if (s) {
            res = res.filter(function(el){
                return matches.call(el, s);
            });
        }

        return $.extend($.merge($(), res), {_prevObj: this});
    },

    prev: function(s){
        return this._sibling('previousElementSibling', s);
    },

    next: function(s){
        return this._sibling('nextElementSibling', s);
    },

    prevAll: function(s){
        return this._siblingAll('previousElementSibling', s);
    },

    nextAll: function(s){
        return this._siblingAll('nextElementSibling', s);
    },

    closest: function(s){
        var res = [];

        if (this.length === 0) {
            return ;
        }

        if (s instanceof $) return s;

        if (!s) {
            return this.parent(s);
        }

        this.each(function(){
            var el = this;
            while (el) {
                if (!el) break;
                if (matches.call(el, s)) {
                    res.push(el);
                    return ;
                }
                el = el.parentElement;
            }
        });

        return $.extend($.merge($(), res.reverse()), {_prevObj: this});
    },

    has: function(selector){
        var res = [];

        if (this.length === 0) {
            return ;
        }

        this.each(function(){
            var el = $(this);
            var child = el.children(selector);
            if (child.length > 0) {
                res.push(this);
            }
        });

        return $.extend($.merge($(), res), {_prevObj: this});
    },

    back: function(to_start){
        var ret;
        if (to_start === true) {
            ret = this._prevObj;
            while (ret) {
                if (!ret._prevObj) break;
                ret = ret._prevObj;
            }
        } else {
            ret = this._prevObj ? this._prevObj : this;
        }
        return ret;
    }
});

// Source: src/script.js

/* global $, not */

function createScript(script){
    var s = document.createElement('script');
    s.type = 'text/javascript';

    if (not(script)) return $(s);

    var _script = $(script)[0];

    if (_script.src) {
        s.src = _script.src;
    } else {
        s.textContent = _script.innerText;
    }

    document.body.appendChild(s);

    if (_script.parentNode) _script.parentNode.removeChild(_script);

    return s;
}

$.extend({
    script: function(el){

        if (not(el)) {
            return createScript();
        }

        var _el = $(el)[0];

        if (_el.tagName && _el.tagName === "SCRIPT") {
            createScript(_el);
        } else $.each($(_el).find("script"), function(){
            createScript(this);
        });
    }
});

$.fn.extend({
    script: function(){
        return this.each(function(){
            $.script(this);
        });
    }
});

// Source: src/prop.js

/* global $, not */

$.fn.extend({
    _prop: function(prop, value){
        if (arguments.length === 1) {
            return this.length === 0 ? undefined : this[0][prop];
        }

        if (not(value)) {
            value = '';
        }

        return this.each(function(){
            var el = this;

            el[prop] = value;

            if (prop === "innerHTML") {
                $.script(el);
            }
        });
    },

    prop: function(prop, value){
        return arguments.length === 1 ? this._prop(prop) : this._prop(prop, typeof value === "undefined" ? "" : value);
    },

    val: function(value){
        if (not(value)) {
            return this.length === 0 ? undefined : this[0].value;
        }

        return this.each(function(){
            var el = $(this);
            if (typeof this.value !== "undefined") {
                this.value = value;
            } else {
                el.html(value);
            }
        });
    },

    html: function(value){
        var that = this, v = [];

        if (arguments.length === 0) {
            return this._prop('innerHTML');
        }

        if (value instanceof $) {
            value.each(function(){
                v.push($(this).outerHTML());
            });
        } else {
            v.push(value);
        }

        that._prop('innerHTML', v.length === 1 && not(v[0]) ? "" : v.join("\n"));

        return this;
    },

    outerHTML: function(){
        return this._prop('outerHTML');
    },

    text: function(value){
        return arguments.length === 0 ? this._prop('textContent') : this._prop('textContent', typeof value === "undefined" ? "" : value);
    },

    innerText: function(value){
        return arguments.length === 0 ? this._prop('innerText') : this._prop('innerText', typeof value === "undefined" ? "" : value);
    },

    empty: function(){
        return this.each(function(){
            if (typeof this.innerHTML !== "undefined") this.innerHTML = "";
        });
    },

    clear: function(){
        return this.empty();
    }
});

// Source: src/each.js

/* global $, isArrayLike, hasProp */

$.each = function(ctx, cb){
    var index = 0;
    if (isArrayLike(ctx)) {
        [].forEach.call(ctx, function(val, key) {
            cb.apply(val, [key, val]);
        });
    } else {
        for(var key in ctx) {
            if (hasProp(ctx, key))
                cb.apply(ctx[key], [key, ctx[key],  index++]);
        }
    }

    return ctx;
};

$.fn.extend({
    each: function(cb){
        return $.each(this, cb);
    }
});


// Source: src/data.js

/* global acceptData, camelCase, $, not, dataAttr, isEmptyObject, hasProp */

/*
 * Data routines
 * Url: https://jquery.com
 * Copyright (c) Copyright JS Foundation and other contributors, https://js.foundation/
 * Licensed under MIT
 */
var Data = function(ns){
    this.expando = "DATASET:UID:" + ns.toUpperCase();
    Data.uid++;
};

Data.uid = -1;

Data.prototype = {
    cache: function(owner){
        var value = owner[this.expando];
        if (!value) {
            value = {};
            if (acceptData(owner)) {
                if (owner.nodeType) {
                    owner[this.expando] = value;
                } else {
                    Object.defineProperty(owner, this.expando, {
                        value: value,
                        configurable: true
                    });
                }
            }
        }
        return value;
    },

    set: function(owner, data, value){
        var prop, cache = this.cache(owner);

        if (typeof data === "string") {
            cache[camelCase(data)] = value;
        } else {
            for (prop in data) {
                if (hasProp(data, prop))
                    cache[camelCase(prop)] = data[prop];
            }
        }
        return cache;
    },

    get: function(owner, key){
        return key === undefined ? this.cache(owner) : owner[ this.expando ] && owner[ this.expando ][ camelCase( key ) ];
    },

    access: function(owner, key, value){
        if (key === undefined || ((key && typeof key === "string") && value === undefined) ) {
            return this.get(owner, key);
        }
        this.set(owner, key, value);
        return value !== undefined ? value : key;
    },

    remove: function(owner, key){
        var i, cache = owner[this.expando];
        if (cache === undefined) {
            return ;
        }
        if (key !== undefined) {
            if ( Array.isArray( key ) ) {
                key = key.map( camelCase );
            } else {
                key = camelCase( key );

                key = key in cache ? [ key ] : ( key.match( /[^\x20\t\r\n\f]+/g ) || [] ); // ???
            }

            i = key.length;

            while ( i-- ) {
                delete cache[ key[ i ] ];
            }
        }
        if ( key === undefined || isEmptyObject( cache ) ) {
            if ( owner.nodeType ) {
                owner[ this.expando ] = undefined;
            } else {
                delete owner[ this.expando ];
            }
        }
        return true;
    },

    hasData: function(owner){
        var cache = owner[ this.expando ];
        return cache !== undefined && !isEmptyObject( cache );
    }
};

var dataSet = new Data('m4q');

$.extend({
    hasData: function(elem){
        return dataSet.hasData(elem);
    },

    data: function(elem, key, val){
        return dataSet.access(elem, key, val);
    },

    removeData: function(elem, key){
        return dataSet.remove(elem, key);
    },

    dataSet: function(ns){
        if (not(ns)) return dataSet;
        if (['INTERNAL', 'M4Q'].indexOf(ns.toUpperCase()) > -1) {
            throw Error("You can not use reserved name for your dataset");
        }
        return new Data(ns);
    }
});

$.fn.extend({
    data: function(key, val){
        var res, elem, data, attrs, name, i;

        if (this.length === 0) {
            return ;
        }

        elem = this[0];

        if ( arguments.length === 0 ) {
            if ( this.length ) {
                data = dataSet.get( elem );

                if ( elem.nodeType === 1) {
                    attrs = elem.attributes;
                    i = attrs.length;
                    while ( i-- ) {
                        if ( attrs[ i ] ) {
                            name = attrs[ i ].name;
                            if ( name.indexOf( "data-" ) === 0 ) {
                                name = camelCase( name.slice( 5 ) );
                                dataAttr( elem, name, data[ name ] );
                            }
                        }
                    }
                }
            }

            return data;
        }

        if ( arguments.length === 1 ) {
            res = dataSet.get(elem, key);
            if (res === undefined) {
                if ( elem.nodeType === 1) {
                    if (elem.hasAttribute("data-"+key)) {
                        res = elem.getAttribute("data-"+key);
                    }
                }
            }
            return res;
        }

        return this.each( function() {
            dataSet.set( this, key, val );
        } );
    },

    removeData: function( key ) {
        return this.each( function() {
            dataSet.remove( this, key );
        } );
    },

    origin: function(name, value, def){

        if (this.length === 0) {
            return this;
        }

        if (not(name) && not(value)) {
            return $.data(this[0]);
        }

        if (not(value)) {
            var res = $.data(this[0], "origin-"+name);
            return !not(res) ? res : def;
        }

        this.data("origin-"+name, value);

        return this;
    }
});

// Source: src/utils.js

/* global $, not, camelCase, dashedName, isPlainObject, isEmptyObject, isArrayLike, acceptData, parseUnit, getUnit, isVisible, isHidden, matches, strip, normName, hasProp, isLocalhost, isTouch */

$.extend({

    device: (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase())),
    localhost: isLocalhost(),
    isLocalhost: isLocalhost,
    touchable: isTouch(),

    uniqueId: function (prefix) {
        var d = new Date().getTime();
        if (not(prefix)) {
            prefix = 'm4q';
        }
        return (prefix !== '' ? prefix + '-' : '') + 'xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    },

    toArray: function(n){
        var i, out = [];

        for (i = 0 ; i < n.length; i++ ) {
            out.push(n[i]);
        }

        return out;
    },

    import: function(ctx){
        var res = [];
        this.each(ctx, function(){
            res.push(this);
        });
        return this.merge($(), res);
    },

    merge: function( first, second ) {
        var len = +second.length,
            j = 0,
            i = first.length;

        for ( ; j < len; j++ ) {
            first[ i++ ] = second[ j ];
        }

        first.length = i;

        return first;
    },

    type: function(obj){
        return Object.prototype.toString.call(obj).replace(/^\[object (.+)]$/, '$1').toLowerCase();
    },

    sleep: function(ms) {
        ms += new Date().getTime();
        /* eslint-disable-next-line */
        while (new Date() < ms){}
    },

    isSelector: function(selector){
        if (typeof selector !== 'string') {
            return false;
        }
        try {
            document.querySelector(selector);
        } catch(error) {
            return false;
        }
        return true;
    },

    remove: function(s){
        return $(s).remove();
    },

    camelCase: camelCase,
    dashedName: dashedName,
    isPlainObject: isPlainObject,
    isEmptyObject: isEmptyObject,
    isArrayLike: isArrayLike,
    acceptData: acceptData,
    not: not,
    parseUnit: parseUnit,
    getUnit: getUnit,
    unit: parseUnit,
    isVisible: isVisible,
    isHidden: isHidden,
    matches: function(el, s) {return matches.call(el, s);},
    random: function(from, to) {
        if (arguments.length === 1 && isArrayLike(from)) {
            return from[Math.floor(Math.random()*(from.length))];
        }
        return Math.floor(Math.random()*(to-from+1)+from);
    },
    strip: strip,
    normName: normName,
    hasProp: hasProp,

    serializeToArray: function(form){
        var _form = $(form)[0];
        if (!_form || _form.nodeName !== "FORM") {
            console.warn("Element is not a HTMLFromElement");
            return;
        }
        var i, j, q = [];
        for (i = _form.elements.length - 1; i >= 0; i = i - 1) {
            if (_form.elements[i].name === "") {
                continue;
            }
            switch (_form.elements[i].nodeName) {
                case 'INPUT':
                    switch (_form.elements[i].type) {
                        case 'checkbox':
                        case 'radio':
                            if (_form.elements[i].checked) {
                                q.push(_form.elements[i].name + "=" + encodeURIComponent(_form.elements[i].value));
                            }
                            break;
                        case 'file':
                            break;
                        default: q.push(_form.elements[i].name + "=" + encodeURIComponent(_form.elements[i].value));
                    }
                    break;
                case 'TEXTAREA':
                    q.push(_form.elements[i].name + "=" + encodeURIComponent(_form.elements[i].value));
                    break;
                case 'SELECT':
                    switch (_form.elements[i].type) {
                        case 'select-one':
                            q.push(_form.elements[i].name + "=" + encodeURIComponent(_form.elements[i].value));
                            break;
                        case 'select-multiple':
                            for (j = _form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                                if (_form.elements[i].options[j].selected) {
                                    q.push(_form.elements[i].name + "=" + encodeURIComponent(_form.elements[i].options[j].value));
                                }
                            }
                            break;
                    }
                    break;
                case 'BUTTON':
                    switch (_form.elements[i].type) {
                        case 'reset':
                        case 'submit':
                        case 'button':
                            q.push(_form.elements[i].name + "=" + encodeURIComponent(_form.elements[i].value));
                            break;
                    }
                    break;
            }
        }
        return q;
    },
    serialize: function(form){
        return $.serializeToArray(form).join("&");
    }
});

$.fn.extend({
    items: function(){
        return $.toArray(this);
    }
});

// Source: src/events.js

/* global $, not, camelCase, str2arr, normName, matches, isEmptyObject, isPlainObject */

(function () {
    if ( typeof window.CustomEvent === "function" ) return false;

    function CustomEvent ( event, params ) {
        params = params || { bubbles: false, cancelable: false, detail: null };
        var evt = document.createEvent( 'CustomEvent' );
        evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();

var overriddenStop =  Event.prototype.stopPropagation;
var overriddenPrevent =  Event.prototype.preventDefault;

Event.prototype.stopPropagation = function(){
    this.isPropagationStopped = true;
    overriddenStop.apply(this, arguments);
};
Event.prototype.preventDefault = function(){
    this.isPreventedDefault = true;
    overriddenPrevent.apply(this, arguments);
};

Event.prototype.stop = function(immediate){
    return immediate ? this.stopImmediatePropagation() : this.stopPropagation();
};

$.extend({
    events: [],
    eventHooks: {},

    eventUID: -1,

    /*
    * el, eventName, handler, selector, ns, id, options
    * */
    setEventHandler: function(obj){
        var i, freeIndex = -1, eventObj, resultIndex;
        if (this.events.length > 0) {
            for(i = 0; i < this.events.length; i++) {
                if (this.events[i].handler === null) {
                    freeIndex = i;
                    break;
                }
            }
        }

        eventObj = {
            element: obj.el,
            event: obj.event,
            handler: obj.handler,
            selector: obj.selector,
            ns: obj.ns,
            id: obj.id,
            options: obj.options
        };

        if (freeIndex === -1) {
            this.events.push(eventObj);
            resultIndex = this.events.length - 1;
        } else {
            this.events[freeIndex] = eventObj;
            resultIndex = freeIndex;
        }

        return resultIndex;
    },

    getEventHandler: function(index){
        if (this.events[index] !== undefined && this.events[index] !== null) {
            this.events[index] = null;
            return this.events[index].handler;
        }
        return undefined;
    },

    off: function(){
        $.each(this.events, function(){
            this.element.removeEventListener(this.event, this.handler, true);
        });
        this.events = [];
        return this;
    },

    getEvents: function(){
        return this.events;
    },

    getEventHooks: function(){
        return this.eventHooks;
    },

    addEventHook: function(event, handler, type){
        if (not(type)) {
            type = "before";
        }
        $.each(str2arr(event), function(){
            this.eventHooks[camelCase(type+"-"+this)] = handler;
        });
        return this;
    },

    removeEventHook: function(event, type){
        if (not(type)) {
            type = "before";
        }
        $.each(str2arr(event), function(){
            delete this.eventHooks[camelCase(type+"-"+this)];
        });
        return this;
    },

    removeEventHooks: function(event){
        var that = this;
        if (not(event)) {
            this.eventHooks = {};
        } else {
            $.each(str2arr(event), function(){
                delete that.eventHooks[camelCase("before-"+this)];
                delete that.eventHooks[camelCase("after-"+this)];
            });
        }
        return this;
    }
});

$.fn.extend({
    on: function(eventsList, sel, handler, options){
        if (this.length === 0) {
            return ;
        }

        if (typeof sel === 'function') {
            options = handler;
            handler = sel;
            sel = undefined;
        }

        if (!isPlainObject(options)) {
            options = {};
        }

        return this.each(function(){
            var el = this;
            $.each(str2arr(eventsList), function(){
                var h, ev = this,
                    event = ev.split("."),
                    name = normName(event[0]),
                    ns = options.ns ? options.ns : event[1],
                    index, originEvent;

                $.eventUID++;

                h = function(e){
                    var target = e.target;
                    var beforeHook = $.eventHooks[camelCase("before-"+name)];
                    var afterHook = $.eventHooks[camelCase("after-"+name)];

                    if (typeof beforeHook === "function") {
                        beforeHook.call(target, e);
                    }

                    if (!sel) {
                        handler.call(el, e);
                    } else {
                        while (target && target !== el) {
                            if (matches.call(target, sel)) {
                                handler.call(target, e);
                                if (e.isPropagationStopped) {
                                    e.stopImmediatePropagation();
                                    break;
                                }
                            }
                            target = target.parentNode;
                        }
                    }

                    if (typeof afterHook === "function") {
                        afterHook.call(target, e);
                    }

                    if (options.once) {
                        index = +$(el).origin( "event-"+e.type+(sel ? ":"+sel:"")+(ns ? ":"+ns:"") );
                        if (!isNaN(index)) $.events.splice(index, 1);
                    }
                };

                Object.defineProperty(h, "name", {
                    value: handler.name && handler.name !== "" ? handler.name : "func_event_"+name+"_"+$.eventUID
                });

                originEvent = name+(sel ? ":"+sel:"")+(ns ? ":"+ns:"");

                el.addEventListener(name, h, !isEmptyObject(options) ? options : false);

                index = $.setEventHandler({
                    el: el,
                    event: name,
                    handler: h,
                    selector: sel,
                    ns: ns,
                    id: $.eventUID,
                    options: !isEmptyObject(options) ? options : false
                });
                $(el).origin('event-'+originEvent, index);
            });
        });
    },

    one: function(events, sel, handler, options){
        if (!isPlainObject(options)) {
            options = {};
        }

        options.once = true;

        return this.on.apply(this, [events, sel, handler, options]);
    },

    off: function(eventsList, sel, options){

        if (isPlainObject(sel)) {
            options = sel;
            sel = null;
        }

        if (!isPlainObject(options)) {
            options = {};
        }

        if (not(eventsList) || eventsList.toLowerCase() === 'all') {
            return this.each(function(){
                var el = this;
                $.each($.events, function(){
                    var e = this;
                    if (e.element === el) {
                        el.removeEventListener(e.event, e.handler, e.options);
                        e.handler = null;
                        $(el).origin("event-"+name+(e.selector ? ":"+e.selector:"")+(e.ns ? ":"+e.ns:""), null);
                    }
                });
            });
        }

        return this.each(function(){
            var el = this;
            $.each(str2arr(eventsList), function(){
                var evMap = this.split("."),
                    name = normName(evMap[0]),
                    ns = options.ns ? options.ns : evMap[1],
                    originEvent, index;

                originEvent = "event-"+name+(sel ? ":"+sel:"")+(ns ? ":"+ns:"");
                index = $(el).origin(originEvent);

                if (index !== undefined && $.events[index].handler) {
                    el.removeEventListener(name, $.events[index].handler, $.events[index].options);
                    $.events[index].handler = null;
                }

                $(el).origin(originEvent, null);
            });
        });
    },

    trigger: function(name, data){
        return this.fire(name, data);
    },

    fire: function(name, data){
        var _name, e;

        if (this.length === 0) {
            return ;
        }

        _name = normName(name);

        if (['focus', 'blur'].indexOf(_name) > -1) {
            this[0][_name]();
            return this;
        }

        if (typeof CustomEvent !== "undefined") {
            e = new CustomEvent(_name, {
                bubbles: true,
                cancelable: true,
                detail: data
            });
        } else {
            e = document.createEvent('Events');
            e.detail = data;
            e.initEvent(_name, true, true);
        }

        return this.each(function(){
            this.dispatchEvent(e);
        });
    }
});

( "blur focus resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu touchstart touchend touchmove touchcancel" )
    .split( " " )
    .forEach(
    function( name ) {
        $.fn[ name ] = function( sel, fn, opt ) {
            return arguments.length > 0 ?
                this.on( name, sel, fn, opt ) :
                this.fire( name );
        };
});

$.fn.extend( {
    hover: function( fnOver, fnOut ) {
        return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
    }
});

$.ready = function(fn, options){
    document.addEventListener('DOMContentLoaded', fn, (options || false));
};

$.load = function(fn){
    return $(window).on("load", fn);
};

$.unload = function(fn){
    return $(window).on("unload", fn);
};

$.fn.extend({
    unload: function(fn){
        return (this.length === 0 || this[0].self !== window) ? undefined : $.unload(fn);
    }
});

$.beforeunload = function(fn){
    if (typeof fn === "string") {
        return $(window).on("beforeunload", function(e){
            e.returnValue = fn;
            return fn;
        });
    } else {
        return $(window).on("beforeunload", fn);
    }
};

$.fn.extend({
    beforeunload: function(fn){
        return (this.length === 0 || this[0].self !== window) ? undefined : $.beforeunload(fn);
    }
});

$.fn.extend({
    ready: function(fn){
        if (this.length && this[0] === document && typeof fn === 'function') {
            return $.ready(fn);
        }
    }
});

// Source: src/ajax.js

/* global $, Promise, not, isSimple, isPlainObject, isEmptyObject, camelCase */

$.ajax = function(p){
    return new Promise(function(resolve, reject){
        var xhr = new XMLHttpRequest(), data;
        var method = (p.method || "GET").toUpperCase();
        var headers = [];
        var async = not(p.async) ? true : p.async;
        var url = p.url;

        var exec = function(fn, params){
            if (typeof fn === "function") {
                fn.apply(null, params);
            }
        };

        var isGet = function(method){
            return ["GET", "JSON"].indexOf(method) !== -1;
        };

        var plainObjectToData = function(obj){
            var _data = [];
            $.each(obj, function(k, v){
                var _v = isSimple(v) ? v : JSON.stringify(v);
                _data.push(k+"=" + _v);
            });
            return _data.join("&");
        };

        if (p.data instanceof HTMLFormElement) {
            var _action = p.data.getAttribute("action");
            var _method = p.data.getAttribute("method");

            if (not(url) && _action && _action.trim() !== "") {url = _action;}
            if (_method && _method.trim() !== "") {method = _method.toUpperCase();}
        }


        if (p.timeout) {
            xhr.timeout = p.timeout;
        }

        if (p.withCredentials) {
            xhr.withCredentials = p.withCredentials;
        }

        if (p.data instanceof HTMLFormElement) {
            data = $.serialize(p.data);
        } else if (p.data instanceof HTMLElement && p.data.getAttribute("type") && p.data.getAttribute("type").toLowerCase() === "file") {
            var _name = p.data.getAttribute("name");
            data = new FormData();
            for (var i = 0; i < p.data.files.length; i++) {
                data.append(_name, p.data.files[i]);
            }
        } else if (isPlainObject(p.data)) {
            data = plainObjectToData(p.data);
        } else if (p.data instanceof FormData) {
            data = p.data;
        } else if (typeof p.data === "string") {
            data = p.data;
        } else {
            data = new FormData();
            data.append("_data", JSON.stringify(p.data));
        }

        if (isGet(method)) {
            url += (typeof data === "string" ? "?"+data : isEmptyObject(data) ? "" : "?"+JSON.stringify(data));
        }

        xhr.open(method, url, async, p.user, p.password);
        if (p.headers) {
            $.each(p.headers, function(k, v){
                xhr.setRequestHeader(k, v);
                headers.push(k);
            });
        }
        if (!isGet(method)) {
            if (headers.indexOf("Content-type") === -1 && p.contentType !== false) {
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            }
        }
        xhr.send(data);

        xhr.addEventListener("load", function(e){
            if (xhr.readyState === 4 && xhr.status < 300) {
                var _return = p.returnValue && p.returnValue === 'xhr' ? xhr : xhr.response;
                if (p.parseJson) {
                    try {
                        _return = JSON.parse(_return);
                    } catch (ex) {
                        _return = {};
                    }
                }
                exec(resolve, [_return]);
                exec(p.onSuccess, [e, xhr]);
            } else {
                exec(reject, [xhr]);
                exec(p.onFail, [e, xhr]);
            }
            exec(p.onLoad, [e, xhr]);
        });

        $.each(["readystatechange", "error", "timeout", "progress", "loadstart", "loadend", "abort"], function(){
            var ev = camelCase("on-"+(this === 'readystatechange' ? 'state' : this));
            xhr.addEventListener(ev, function(e){
                exec(p[ev], [e, xhr]);
            });
        });
    });
};

['get', 'post', 'put', 'patch', 'delete', 'json'].forEach(function(method){
    $[method] = function(url, data, options){
        var _method = method.toUpperCase();
        var _options = {
            method: _method === 'JSON' ? 'GET' : _method,
            url: url,
            data: data,
            parseJson: _method === 'JSON'
        };
        return $.ajax($.extend({}, _options, options));
    };
});

$.fn.extend({
    load: function(url, data, options){
        var that = this;

        if (this.length && this[0].self === window ) {
            return $.load(url);
        }

        return $.get(url, data, options).then(function(data){
            that.each(function(){
                this.innerHTML = data;
            });
        });
    }
});

// Source: src/css.js

/* global $, not, setStyleProp */

$.fn.extend({

    style: function(name, pseudo){
        var el;

        function _getStyle(el, prop, pseudo){
            return ["scrollLeft", "scrollTop"].indexOf(prop) > -1 ? $(el)[prop]() : getComputedStyle(el, pseudo)[prop];
        }

        if (typeof name === 'string' && this.length === 0) {
            return undefined;
        }

        if (this.length === 0) {
            return this;
        }

        el = this[0];

        if (not(name) || name === "all") {
            return getComputedStyle(el, pseudo);
        } else {
            var result = {}, names = name.split(", ").map(function(el){
                return (""+el).trim();
            });
            if (names.length === 1)  {
                return _getStyle(el, names[0], pseudo);
            } else {
                $.each(names, function () {
                    var prop = this;
                    result[this] = _getStyle(el, prop, pseudo);
                });
                return result;
            }
        }
    },

    removeStyleProperty: function(name){
        if (not(name) || this.length === 0) return this;
        var names = name.split(", ").map(function(el){
            return (""+el).trim();
        });

        return this.each(function(){
            var el = this;
            $.each(names, function(){
                el.style.removeProperty(this);
            });
        });
    },

    css: function(key, val){
        key = key || 'all';

        if (typeof key === "string" && not(val)) {
            return  this.style(key);
        }

        return this.each(function(){
            var el = this;
            if (typeof key === "object") {
                $.each(key, function(key, val){
                    setStyleProp(el, key, val);
                });
            } else if (typeof key === "string") {
                setStyleProp(el, key, val);
            }
        });
    },

    scrollTop: function(val){
        if (not(val)) {
            return this.length === 0 ? undefined : this[0] === window ? pageYOffset : this[0].scrollTop;
        }
        return this.each(function(){
            this.scrollTop = val;
        });
    },

    scrollLeft: function(val){
        if (not(val)) {
            return this.length === 0 ? undefined : this[0] === window ? pageXOffset : this[0].scrollLeft;
        }
        return this.each(function(){
            this.scrollLeft = val;
        });
    }
});



// Source: src/classes.js

/* global $, not */

$.fn.extend({
    addClass: function(){},
    removeClass: function(){},
    toggleClass: function(){},

    containsClass: function(cls){
        return this.hasClass(cls);
    },

    hasClass: function(cls){
        var result = false;
        var classes = cls.split(" ").filter(function(v){
            return (""+v).trim() !== "";
        });

        if (not(cls)) {
            return false;
        }

        this.each(function(){
            var el = this;

            $.each(classes, function(){
                if (!result && el.classList && el.classList.contains(this)) {
                    result = true;
                }
            });
        });

        return result;
    },

    clearClasses: function(){
        return this.each(function(){
            this.className = "";
        });
    },

    cls: function(array){
        return this.length === 0 ? undefined : array ? this[0].className.split(" ") : this[0].className;
    },

    removeClassBy: function(mask){
        return this.each(function(){
            var el = $(this);
            var classes = el.cls(true);
            $.each(classes, function(){
                var elClass = this;
                if (elClass.indexOf(mask) > -1) {
                    el.removeClass(elClass);
                }
            });
        });
    }
});

['add', 'remove', 'toggle'].forEach(function (method) {
    $.fn[method + "Class"] = function(cls){
        if (not(cls) || (""+cls).trim() === "") return this;
        return this.each(function(){
            var el = this;
            var hasClassList = typeof el.classList !== "undefined";
            $.each(cls.split(" ").filter(function(v){
                return (""+v).trim() !== "";
            }), function(){
                if (hasClassList) el.classList[method](this);
            });
        });
    };
});


// Source: src/parser.js

/* global $ */

$.parseHTML = function(data){
    var base, singleTag, result = [], ctx, _context;
    var regexpSingleTag = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i; // eslint-disable-line

    if (typeof data !== "string") {
        return [];
    }

    data = data.trim();

    ctx = document.implementation.createHTMLDocument("");
    base = ctx.createElement( "base" );
    base.href = document.location.href;
    ctx.head.appendChild( base );
    _context = ctx.body;

    singleTag = regexpSingleTag.exec(data);

    if (singleTag) {
        result.push(document.createElement(singleTag[1]));
    } else {
        _context.innerHTML = data;
        for(var i = 0; i < _context.childNodes.length; i++) {
            result.push(_context.childNodes[i]);
        }
    }

    return result;
};


// Source: src/size.js

/* global $, not */

$.fn.extend({
    _size: function(prop, val){
        if (this.length === 0) return ;

        if (not(val)) {

            var el = this[0];

            if (prop === 'height') {
                return el === window ? window.innerHeight : el === document ? el.body.clientHeight : parseInt(getComputedStyle(el).height);
            }
            if (prop === 'width') {
                return el === window ? window.innerWidth : el === document ? el.body.clientWidth : parseInt(getComputedStyle(el).width);
            }
        }

        return this.each(function(){
            var el = this;
            if (el === window || el === document) {return ;}
            el.style[prop] = isNaN(val) ? val : val + 'px';
        });
    },

    height: function(val){
        return this._size('height', val);
    },

    width: function(val){
        return this._size('width', val);
    },

    _sizeOut: function(prop, val){
        var el, size, style, result;

        if (this.length === 0) {
            return ;
        }

        if (!not(val) && typeof val !== "boolean") {
            return this.each(function(){
                var el = this;
                if (el === window || el === document) {return ;}
                var h, style = getComputedStyle(el),
                    bs = prop === 'width' ? parseInt(style['border-left-width']) + parseInt(style['border-right-width']) : parseInt(style['border-top-width']) + parseInt(style['border-bottom-width']),
                    pa = prop === 'width' ? parseInt(style['padding-left']) + parseInt(style['padding-right']) : parseInt(style['padding-top']) + parseInt(style['padding-bottom']);

                h = $(this)[prop](val)[prop]() - bs - pa;
                el.style[prop] = h + 'px';
            });
        }

        el = this[0];
        size = el[prop === 'width' ? 'offsetWidth' : 'offsetHeight'];
        style = getComputedStyle(el);
        result = size + parseInt(style[prop === 'width' ? 'margin-left' : 'margin-top']) + parseInt(style[prop === 'width' ? 'margin-right' : 'margin-bottom']);
        return val === true ? result : size;
    },

    outerWidth: function(val){
        return this._sizeOut('width', val);
    },

    outerHeight: function(val){
        return this._sizeOut('height', val);
    },

    padding: function(p){
        if (this.length === 0) return;
        var s = getComputedStyle(this[0], p);

        return {
            top: parseInt(s["padding-top"]),
            right: parseInt(s["padding-right"]),
            bottom: parseInt(s["padding-bottom"]),
            left: parseInt(s["padding-left"])
        };
    },

    margin: function(p){
        if (this.length === 0) return;
        var s = getComputedStyle(this[0], p);

        return {
            top: parseInt(s["margin-top"]),
            right: parseInt(s["margin-right"]),
            bottom: parseInt(s["margin-bottom"]),
            left: parseInt(s["margin-left"])
        };
    },

    border: function(p){
        if (this.length === 0) return;
        var s = getComputedStyle(this[0], p);

        return {
            top: parseInt(s["border-top-width"]),
            right: parseInt(s["border-right-width"]),
            bottom: parseInt(s["border-bottom-width"]),
            left: parseInt(s["border-left-width"])
        };
    }
});

// Source: src/position.js

/* global $, not */

$.fn.extend({
    offset: function(val){
        var rect;

        if (not(val)) {
            if (this.length === 0) return undefined;
            rect = this[0].getBoundingClientRect();
            return {
                top: rect.top + pageYOffset,
                left: rect.left + pageXOffset
            };
        }

        return this.each(function(){ //?
            var el = $(this),
                top = val.top,
                left = val.left,
                position = getComputedStyle(this).position,
                offset = el.offset();

            if (position === "static") {
                el.css("position", "relative");
            }

            if (["absolute", "fixed"].indexOf(position) === -1) {
                top = top - offset.top;
                left = left - offset.left;
            }

            el.css({
                top: top,
                left: left
            });
        });
    },

    position: function(margin){
        var ml = 0, mt = 0, el, style;

        if (not(margin) || typeof margin !== "boolean") {
            margin = false;
        }

        if (this.length === 0) {
            return undefined;
        }

        el = this[0];
        style = getComputedStyle(el);

        if (margin) {
            ml = parseInt(style['margin-left']);
            mt = parseInt(style['margin-top']);
        }

        return {
            left: el.offsetLeft - ml,
            top: el.offsetTop - mt
        };
    },

    left: function(val, margin){
        if (this.length === 0) return ;
        if (not(val)) {
            return this.position(margin).left;
        }
        if (typeof val === "boolean") {
            margin = val;
            return this.position(margin).left;
        }
        return this.each(function(){
            $(this).css({
                left: val
            });
        });
    },

    top: function(val, margin){
        if (this.length === 0) return ;
        if (not(val)) {
            return this.position(margin).top;
        }
        if (typeof val === "boolean") {
            margin = val;
            return this.position(margin).top;
        }
        return this.each(function(){
            $(this).css({
                top: val
            });
        });
    },

    coord: function(){
        return this.length === 0 ? undefined : this[0].getBoundingClientRect();
    },

    pos: function(){
        if (this.length === 0) return ;
        return {
            top: parseInt($(this[0]).style("top")),
            left: parseInt($(this[0]).style("left"))
        };
    }
});

// Source: src/attr.js

/* global $, not, isPlainObject */

$.fn.extend({
    attr: function(name, val){
        var attributes = {};

        if (this.length === 0 && arguments.length === 0) {
            return undefined;
        }

        if (this.length && arguments.length === 0) {
            $.each(this[0].attributes, function(){
                attributes[this.nodeName] = this.nodeValue;
            });
            return attributes;
        }

        if (arguments.length === 1 && typeof name === "string") {
            return this.length && this[0].nodeType === 1 && this[0].hasAttribute(name) ? this[0].getAttribute(name) : undefined;
        }

        return this.each(function(){
            var el = this;
            if (isPlainObject(name)) {
                $.each(name, function(k, v){
                    el.setAttribute(k, v);
                });
            } else {
                el.setAttribute(name, val);
                // console.log(name, val);
            }
        });
    },

    removeAttr: function(name){
        var attributes;

        if (not(name)) {
            return this.each(function(){
                var el = this;
                $.each(this.attributes, function(){
                    el.removeAttribute(this);
                });
            });
        }

        attributes = typeof name === "string" ? name.split(",").map(function(el){
            return el.trim();
        }) : name;

        return this.each(function(){
            var el = this;
            $.each(attributes, function(){
                if (el.hasAttribute(this)) el.removeAttribute(this);
            });
        });
    },

    toggleAttr: function(name, val){
        return this.each(function(){
            var el = this;

            if (not(val)) {
                el.removeAttribute(name);
            } else {
                el.setAttribute(name, val);
            }

        });
    },

    id: function(val){
        return this.length ? $(this[0]).attr("id", val) : undefined;
    }
});

$.extend({
    meta: function(name){
        return not(name) ? $("meta") : $("meta[name='$name']".replace("$name", name));
    },

    metaBy: function(name){
        return not(name) ? $("meta") : $("meta[$name]".replace("$name", name));
    },

    doctype: function(){
        return $("doctype");
    },

    html: function(){
        return $("html");
    },

    head: function(){
        return $("html").find("head");
    },

    body: function(){
        return $("body");
    },

    document: function(){
        return $(document);
    },

    window: function(){
        return $(window);
    },

    charset: function(val){
        var meta = $("meta[charset]");
        if (val) {
            meta.attr("charset", val);
        }
        return meta.attr("charset");
    }
});

// Source: src/proxy.js

/* global $ */

$.extend({
    proxy: function(fn, ctx){
        return typeof fn !== "function" ? undefined : fn.bind(ctx);
    },

    bind: function(fn, ctx){
        return this.proxy(fn, ctx);
    }
});


// Source: src/manipulation.js

/* global $, isArrayLike, not, matches, hasProp */

(function (arr) {
    arr.forEach(function (item) {
        ['append', 'prepend'].forEach(function(where){
            if (hasProp(item, where)) {
                return;
            }

            Object.defineProperty(item, where, {
                configurable: true,
                enumerable: true,
                writable: true,
                value: function () {
                    var argArr = Array.prototype.slice.call(arguments),
                        docFrag = document.createDocumentFragment();

                    argArr.forEach(function (argItem) {
                        var isNode = argItem instanceof Node;
                        docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
                    });

                    if (where === 'prepend')
                        this.insertBefore(docFrag, this.firstChild);
                    else
                        this.appendChild(docFrag);
                }
            });
        });
    });
})([Element.prototype, Document.prototype, DocumentFragment.prototype]);

var normalizeElements = function(s){
    var result;

    if (typeof s === "string") result = $.isSelector(s) ? $(s) : $.parseHTML(s);
    else if (s instanceof HTMLElement) result = [s];
    else if (isArrayLike(s)) result = s;
    return result;
};

$.fn.extend({

    appendText: function(text){
        return this.each(function(elIndex, el){
            el.innerHTML += text;
        });
    },

    prependText: function(text){
        return this.each(function(elIndex, el){
            el.innerHTML = text + el.innerHTML;
        });
    },

    append: function(elements){
        var _elements = normalizeElements(elements);

        return this.each(function(elIndex, el){
            $.each(_elements, function(){
                if (el === this) return ;
                var child = elIndex === 0 ? this : this.cloneNode(true);
                $.script(child);
                if (child.tagName && child.tagName !== "SCRIPT") el.append(child);
            });
        });
    },

    appendTo: function(elements){
        var _elements = normalizeElements(elements);

        return this.each(function(){
            var el = this;
            $.each(_elements, function(parIndex, parent){
                if (el === this) return ;
                parent.append(parIndex === 0 ? el : el.cloneNode(true));
            });
        });
    },

    prepend: function(elements){
        var _elements = normalizeElements(elements);

        return this.each(function (elIndex, el) {
            $.each(_elements, function(){
                if (el === this) return ;
                var child = elIndex === 0 ? this : this.cloneNode(true);
                $.script(child);
                if (child.tagName && child.tagName !== "SCRIPT") el.prepend(child);
            });
        });
    },

    prependTo: function(elements){
        var _elements = normalizeElements(elements);

        return this.each(function(){
            var el = this;
            $.each(_elements, function(parIndex, parent){
                if (el === this) return ;
                $(parent).prepend(parIndex === 0 ? el : el.cloneNode(true));
            });
        });
    },

    insertBefore: function(elements){
        var _elements = normalizeElements(elements);

        return this.each(function(){
            var el = this;
            $.each(_elements, function(elIndex){
                if (el === this) return ;
                var parent = this.parentNode;
                if (parent) {
                    parent.insertBefore(elIndex === 0 ? el : el.cloneNode(true), this);
                }
            });
        });
    },

    insertAfter: function(elements){
        var _elements = normalizeElements(elements);

        return this.each(function(){
            var el = this;
            $.each(_elements, function(elIndex, element){
                if (el === this) return ;
                var parent = this.parentNode;
                if (parent) {
                    parent.insertBefore(elIndex === 0 ? el : el.cloneNode(true), element.nextSibling);
                }
            });
        });
    },

    after: function(html){
        return this.each(function(){
            var el = this;
            if (typeof html === "string") {
                el.insertAdjacentHTML('afterend', html);
            } else {
                $(html).insertAfter(el);
            }
        });
    },

    before: function(html){
        return this.each(function(){
            var el = this;
            if (typeof html === "string") {
                el.insertAdjacentHTML('beforebegin', html);
            } else {
                $(html).insertBefore(el);
            }
        });
    },

    clone: function(deep, withData){
        var res = [];
        if (not(deep)) {
            deep = false;
        }
        if (not(withData)) {
            withData = false;
        }
        this.each(function(){
            var el = this.cloneNode(deep);
            var $el = $(el);
            var data;
            if (withData && $.hasData(this)) {
                data = $(this).data();
                $.each(data, function(k, v){
                    $el.data(k, v);
                });
            }
            res.push(el);
        });
        return $.merge($(), res);
    },

    import: function(deep){
        var res = [];
        if (not(deep)) {
            deep = false;
        }
        this.each(function(){
            res.push(document.importNode(this, deep));
        });
        return $.merge($(), res);
    },

    adopt: function(){
        var res = [];
        this.each(function(){
            res.push(document.adoptNode(this));
        });
        return $.merge($(), res);
    },

    remove: function(selector){
        var i = 0, node, out, res = [];

        if (this.length === 0) {
            return ;
        }

        out = selector ? this.filter(function(el){
            return matches.call(el, selector);
        }) : this.items();

        for ( ; ( node = out[ i ] ) != null; i++ ) {
            if (node.parentNode) {
                res.push(node.parentNode.removeChild(node));
                $.removeData(node);
            }
        }

        return $.merge($(), res);
    },

    wrap: function( el ){
        if (this.length === 0) {
            return ;
        }

        var wrapper = $(normalizeElements(el));

        if (!wrapper.length) {
            return ;
        }

        var res = [];

        this.each(function(){
            var _target, _wrapper;

            _wrapper = wrapper.clone(true, true);
            _wrapper.insertBefore(this);

            _target = _wrapper;
            while (_target.children().length) {
                _target = _target.children().eq(0);
            }
            _target.append(this);

            res.push(_wrapper);
        });

        return $(res);
    },

    wrapAll: function( el ){
        var wrapper, _wrapper, _target;

        if (this.length === 0) {
            return ;
        }

        wrapper = $(normalizeElements(el));

        if (!wrapper.length) {
            return ;
        }

        _wrapper = wrapper.clone(true, true);
        _wrapper.insertBefore(this[0]);

        _target = _wrapper;
        while (_target.children().length) {
            _target = _target.children().eq(0);
        }

        this.each(function(){
            _target.append(this);
        })

        return _wrapper;
    },

    wrapInner: function( el ){
        if (this.length === 0) {
            return ;
        }

        var wrapper = $(normalizeElements(el));

        if (!wrapper.length) {
            return ;
        }

        var res = [];

        this.each(function(){
            var elem = $(this);
            var html = elem.html();
            var wrp = wrapper.clone(true, true);
            elem.html(wrp.html(html));
            res.push(wrp);
        });

        return $(res);
    }
});

// Source: src/animation.js

/* global $, not, camelCase, parseUnit, Promise, getUnit, matches */

$.extend({
    animation: {
        duration: 1000,
        ease: "linear",
        elements: {}
    }
});

if (typeof window["setupAnimation"] === 'object') {
    $.each(window["setupAnimation"], function(key, val){
        if (typeof $.animation[key] !== "undefined" && !not(val))
            $.animation[key] = val;
    });
}

var transformProps = ['translateX', 'translateY', 'translateZ', 'rotate', 'rotateX', 'rotateY', 'rotateZ', 'scale', 'scaleX', 'scaleY', 'scaleZ', 'skew', 'skewX', 'skewY'];
var numberProps = ['opacity', 'zIndex'];
var floatProps = ['opacity', 'volume'];
var scrollProps = ["scrollLeft", "scrollTop"];
var reverseProps = ["opacity", "volume"];

function _validElement(el) {
    return el instanceof HTMLElement || el instanceof SVGElement;
}

/**
 *
 * @param to
 * @param from
 * @returns {*}
 * @private
 */
function _getRelativeValue (to, from) {
    var operator = /^(\*=|\+=|-=)/.exec(to);
    if (!operator) return to;
    var u = getUnit(to) || 0;
    var x = parseFloat(from);
    var y = parseFloat(to.replace(operator[0], ''));
    switch (operator[0][0]) {
        case '+':
            return x + y + u;
        case '-':
            return x - y + u;
        case '*':
            return x * y + u;
        case '/':
            return x / y + u;
    }
}

/**
 *
 * @param el
 * @param prop
 * @param pseudo
 * @returns {*|number|string}
 * @private
 */
function _getStyle (el, prop, pseudo){
    if (typeof el[prop] !== "undefined") {
        if (scrollProps.indexOf(prop) > -1) {
            return prop === "scrollLeft" ? el === window ? pageXOffset : el.scrollLeft : el === window ? pageYOffset : el.scrollTop;
        } else {
            return el[prop] || 0;
        }
    }

    return el.style[prop] || getComputedStyle(el, pseudo)[prop];
}

/**
 *
 * @param el
 * @param key
 * @param val
 * @param unit
 * @param toInt
 * @private
 */
function _setStyle (el, key, val, unit, toInt) {

    if (not(toInt)) {
        toInt = false;
    }

    key = camelCase(key);

    if (toInt) {
        val  = parseInt(val);
    }

    if (_validElement(el)) {
        if (typeof el[key] !== "undefined") {
            el[key] = val;
        } else {
            el.style[key] = key === "transform" || key.toLowerCase().indexOf('color') > -1 ? val : val + unit;
        }
    } else {
        el[key] = val;
    }
}

/**
 *
 * @param el
 * @param mapProps
 * @param p
 * @private
 */
function _applyStyles (el, mapProps, p) {
    $.each(mapProps, function (key, val) {
        _setStyle(el, key, val[0] + (val[2] * p), val[3], val[4]);
    });
}

/**
 *
 * @param el
 * @returns {{}}
 * @private
 */
function _getElementTransforms (el) {
    if (!_validElement(el)) return {};
    var str = el.style.transform || '';
    var reg = /(\w+)\(([^)]*)\)/g;
    var transforms = {};
    var m;

    /* jshint ignore:start */
    // eslint-disable-next-line
    while (m = reg.exec(str))
        transforms[m[1]] = m[2];
    /* jshint ignore:end */

    return transforms;
}

/**
 *
 * @param val
 * @returns {number[]}
 * @private
 */
function _getColorArrayFromHex (val){
    var a = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(val ? val : "#000000");
    return a.slice(1).map(function(v) {
            return parseInt(v, 16);
    });
}

/**
 *
 * @param el
 * @param key
 * @returns {number[]}
 * @private
 */
function _getColorArrayFromElement (el, key) {
    return getComputedStyle(el)[key].replace(/[^\d.,]/g, '').split(',').map(function(v) {
        return parseInt(v);
    });
}

/**
 *
 * @param el
 * @param mapProps
 * @param p
 * @private
 */
function _applyTransform (el, mapProps, p) {
    var t = [];
    var elTransforms = _getElementTransforms(el);

    $.each(mapProps, function(key, val) {
        var from = val[0], to = val[1], delta = val[2], unit = val[3];
        key = "" + key;

        if ( key.indexOf("rotate") > -1 || key.indexOf("skew") > -1) {
            if (unit === "") unit = "deg";
        }

        if (key.indexOf('scale') > -1) {
            unit = '';
        }

        if (key.indexOf('translate') > -1 && unit === '') {
            unit = 'px';
        }

        if (unit === "turn") {
            t.push(key+"(" + (to * p) + unit + ")");
        } else {
            t.push(key +"(" + (from + (delta * p)) + unit+")");
        }
    });

    $.each(elTransforms, function(key, val) {
        if (mapProps[key] === undefined) {
            t.push(key+"("+val+")");
        }
    });

    el.style.transform = t.join(" ");
}

/**
 *
 * @param el
 * @param mapProps
 * @param p
 * @private
 */
function _applyColors (el, mapProps, p) {
    $.each(mapProps, function (key, val) {
        var i, result = [0, 0, 0], v;
        for (i = 0; i < 3; i++) {
            result[i] = Math.floor(val[0][i] + (val[2][i] * p));
        }
        v = "rgb("+(result.join(","))+")";
        el.style[key] = v;
    });
}

/**
 *
 * @param val
 * @returns {string|*}
 * @private
 */
function _expandColorValue (val) {
    var regExp = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    if (val[0] === "#" && val.length === 4) {
        return "#" + val.replace(regExp, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });
    }
    return val[0] === "#" ? val : "#"+val;
}

/**
 *
 * @param el
 * @param map
 * @param p
 */
function applyProps (el, map, p) {
    _applyStyles(el, map.props, p);
    _applyTransform(el, map.transform, p);
    _applyColors(el, map.color, p);
}

/**
 *
 * @param el
 * @param draw
 * @param dir
 * @returns {{transform: {}, color: {}, props: {}}}
 */
function createAnimationMap (el, draw, dir) {
    var map = {
        props: {},
        transform: {},
        color: {}
    };
    var i, from, to, delta, unit, temp;
    var elTransforms = _getElementTransforms(el);

    if (not(dir)) {
        dir = "normal";
    }

    $.each(draw, function(key, val) {

        var isTransformProp = transformProps.indexOf(""+key) > -1;
        var isNumProp = numberProps.indexOf(""+key) > -1;
        var isColorProp = (""+key).toLowerCase().indexOf("color") > -1;

        if (Array.isArray(val) && val.length === 1) {
            val = val[0];
        }

        if (!Array.isArray(val)) {
            if (isTransformProp) {
                from = elTransforms[key] || 0;
            } else if (isColorProp) {
                from = _getColorArrayFromElement(el, key);
            } else {
                from = _getStyle(el, key);
            }
            from = !isColorProp ? parseUnit(from) : from;
            to = !isColorProp ? parseUnit(_getRelativeValue(val, Array.isArray(from) ? from[0] : from)) : _getColorArrayFromHex(val);
        } else {
            from = !isColorProp ? parseUnit(val[0]) : _getColorArrayFromHex(_expandColorValue(val[0]));
            to = !isColorProp ? parseUnit(val[1]) : _getColorArrayFromHex(_expandColorValue(val[1]));
        }

        if (reverseProps.indexOf(""+key) > -1 && from[0] === to[0]) {
            from[0] = to[0] > 0 ? 0 : 1;
        }

        if (dir === "reverse") {
            temp = from;
            from = to;
            to = temp;
        }

        unit = el instanceof HTMLElement && to[1] === '' && !isNumProp && !isTransformProp ? 'px' : to[1];

        if (isColorProp) {
            delta = [0, 0, 0];
            for (i = 0; i < 3; i++) {
                delta[i] = to[i] - from[i];
            }
        } else {
            delta = to[0] - from[0];
        }

        if (isTransformProp) {
            map.transform[key] = [from[0], to[0], delta, unit];
        } else if (isColorProp) {
            map.color[key] = [from, to, delta, unit];
        } else {
            map.props[key] = [from[0], to[0], delta, unit, floatProps.indexOf(""+key) === -1];
        }
    });

    return map;
}

function minMax(val, min, max) {
    return Math.min(Math.max(val, min), max);
}

var Easing = {
    linear: function(){return function(t) {return t;};}
};

Easing.default = Easing.linear;

var eases = {
    Sine: function(){
        return function(t){
            return 1 - Math.cos(t * Math.PI / 2);
        };
    },
    Circ: function(){
        return function(t){
            return 1 - Math.sqrt(1 - t * t);
        };
    },
    Back: function(){
        return function(t){
            return t * t * (3 * t - 2);
        };
    },
    Bounce: function(){
        return function(t){
            var pow2, b = 4;
            // eslint-disable-next-line
            while (t < (( pow2 = Math.pow(2, --b)) - 1) / 11) {}
            return 1 / Math.pow(4, 3 - b) - 7.5625 * Math.pow(( pow2 * 3 - 2 ) / 22 - t, 2);
        };
    },
    Elastic: function(amplitude, period){
        if (not(amplitude)) {
            amplitude = 1;
        }

        if (not(period)) {
            period = 0.5;
        }
        var a = minMax(amplitude, 1, 10);
        var p = minMax(period, 0.1, 2);
        return function(t){
            return (t === 0 || t === 1) ? t :
                -a * Math.pow(2, 10 * (t - 1)) * Math.sin((((t - 1) - (p / (Math.PI * 2) * Math.asin(1 / a))) * (Math.PI * 2)) / p);
        };
    }
};

['Quad', 'Cubic', 'Quart', 'Quint', 'Expo'].forEach(function(name, i) {
    eases[name] = function(){
        return function(t){
            return Math.pow(t, i + 2);
        };
    };
});

Object.keys(eases).forEach(function(name) {
    var easeIn = eases[name];
    Easing['easeIn' + name] = easeIn;
    Easing['easeOut' + name] = function(a, b){
        return function(t){
            return 1 - easeIn(a, b)(1 - t);
        };
    };
    Easing['easeInOut' + name] = function(a, b){
        return function(t){
            return t < 0.5 ? easeIn(a, b)(t * 2) / 2 : 1 - easeIn(a, b)(t * -2 + 2) / 2;
        };
    };
});

var defaultAnimationProps = {
    id: null,
    el: null,
    draw: {},
    dur: $.animation.duration,
    ease: $.animation.ease,
    loop: 0,
    pause: 0,
    dir: "normal",
    defer: 0,
    onStart: function(){},
    onStop: function(){},
    onStopAll: function(){},
    onPause: function(){},
    onPauseAll: function(){},
    onResume: function(){},
    onResumeAll: function(){},
    onFrame: function(){},
    onDone: function(){}
};

function animate(args){
    return new Promise(function(resolve){
        var that = this;
        var props = $.assign({}, defaultAnimationProps, {dur: $.animation.duration, ease: $.animation.ease}, args);
        var id = props.id, el = props.el, draw = props.draw, dur = props.dur, ease = props.ease, loop = props.loop,
            onStart = props.onStart, onFrame = props.onFrame, onDone = props.onDone,
            pauseStart = props.pause, dir = props.dir, defer = props.defer;
        var map = {};
        var easeName = "linear", easeArgs = [], easeFn = Easing.linear, matchArgs;
        var direction = dir === "alternate" ? "normal" : dir;
        var replay = false;
        var animationID = id ? id : +(performance.now() * Math.pow(10, 14));

        if (not(el)) {
            throw new Error("Unknown element!");
        }

        if (typeof el === "string") {
            el = document.querySelector(el);
        }

        if (typeof draw !== "function" && typeof draw !== "object") {
            throw new Error("Unknown draw object. Must be a function or object!");
        }

        if (dur === 0) {
            dur = 1;
        }

        if (dir === "alternate" && typeof loop === "number") {
            loop *= 2;
        }

        if (typeof ease === "string") {
            matchArgs = /\(([^)]+)\)/.exec(ease);
            easeName = ease.split("(")[0];
            easeArgs = matchArgs ? matchArgs[1].split(',').map(function(p){return parseFloat(p);}) : [];
            easeFn = Easing[easeName] || Easing.linear;
        } else if (typeof ease === "function") {
            easeFn = ease;
        } else {
            easeFn = Easing.linear;
        }

        $.animation.elements[animationID] = {
            element: el,
            id: null,
            stop: 0,
            pause: 0,
            loop: 0,
            t: -1,
            started: 0,
            paused: 0
        };

        var play = function() {
            if (typeof draw === "object") {
                map = createAnimationMap(el, draw, direction);
            }

            if (typeof onStart === "function") {
                onStart.apply(el);
            }

            // start = performance.now();
            $.animation.elements[animationID].loop += 1;
            $.animation.elements[animationID].started = performance.now();
            $.animation.elements[animationID].duration = dur;
            $.animation.elements[animationID].id = requestAnimationFrame(animate);
        };

        var done = function() {
            cancelAnimationFrame($.animation.elements[animationID].id);
            delete $.animation.elements[id];

            if (typeof onDone === "function") {
                onDone.apply(el);
            }

            resolve(that);
        };

        var animate = function(time) {
            var p, t;
            var stop = $.animation.elements[animationID].stop;
            var pause = $.animation.elements[animationID].pause;
            var start = $.animation.elements[animationID].started;

            if ($.animation.elements[animationID].paused) {
                start = time - $.animation.elements[animationID].t * dur;
                $.animation.elements[animationID].started = start;
            }

            t = ((time - start) / dur).toFixed(4);

            if (t > 1) t = 1;
            if (t < 0) t = 0;

            p = easeFn.apply(null, easeArgs)(t);

            $.animation.elements[animationID].t = t;
            $.animation.elements[animationID].p = p;

            if (pause) {
                $.animation.elements[animationID].id = requestAnimationFrame(animate);
                // $.animation.elements[animationID].started = performance.now();
                return;
            }

            if ( stop > 0) {
                if (stop === 2) {
                    if (typeof draw === "function") {
                        draw.bind(el)(1, 1);
                    } else {
                        applyProps(el, map, 1);
                    }
                }
                done();
                return;
            }

            if (typeof draw === "function") {
                draw.bind(el)(t, p);
            } else {
                applyProps(el, map, p);
            }

            if (typeof onFrame === 'function') {
                onFrame.apply(el, [t, p]);
            }

            if (t < 1) {
                $.animation.elements[animationID].id = requestAnimationFrame(animate);
            }

            if (parseInt(t) === 1) {
                if (loop) {
                    if (dir === "alternate") {
                        direction = direction === "normal" ? "reverse" : "normal";
                    }

                    if (typeof loop === "boolean") {
                        setTimeout(function () {
                            play();
                        }, pauseStart);
                    } else {
                        if (loop > $.animation.elements[animationID].loop) {
                            setTimeout(function () {
                                play();
                            }, pauseStart);
                        } else {
                            done();
                        }
                    }
                } else {
                    if (dir === "alternate" && !replay) {
                        direction = direction === "normal" ? "reverse" : "normal";
                        replay = true;
                        play();
                    } else {
                        done();
                    }
                }
            }
        };
        if (defer > 0) {
            setTimeout(function() {
                play();
            }, defer);
        } else {
            play();
        }
    });
}

// Stop animation
function stopAnimation(id, done){
    var an = $.animation.elements[id];

    if (typeof an === "undefined") {
        return ;
    }

    if (not(done)) {
        done = true;
    }

    an.stop = done === true ? 2 : 1;

    if (typeof an.onStop === "function") {
        an.onStop.apply(an.element);
    }
}

function stopAnimationAll(done, filter){
    $.each($.animation.elements, function(k, v){
        if (filter) {
            if (typeof filter === "string") {
                if (matches.call(v.element, filter)) stopAnimation(k, done);
            } else if (filter.length) {
                $.each(filter, function(){
                    if (v.element === this) stopAnimation(k, done);
                });
            } else if (filter instanceof Element) {
                if (v.element === filter) stopAnimation(k, done);
            }
        } else {
            stopAnimation(k, done);
        }
    });
}
// end of stop

// Pause and resume animation
function pauseAnimation(id){
    var an = $.animation.elements[id];

    if (typeof an === "undefined") {
        return ;
    }

    an.pause = 1;
    an.paused = performance.now();

    if (typeof an.onPause === "function") {
        an.onPause.apply(an.element);
    }
}

function pauseAnimationAll(filter){
    $.each($.animation.elements, function(k, v){
        if (filter) {
            if (typeof filter === "string") {
                if (matches.call(v.element, filter)) pauseAnimation(k);
            } else if (filter.length) {
                $.each(filter, function(){
                    if (v.element === this) pauseAnimation(k);
                });
            } else if (filter instanceof Element) {
                if (v.element === filter) pauseAnimation(k);
            }
        } else {
            pauseAnimation(k);
        }
    });
}
// end of pause

function resumeAnimation(id){
    var an = $.animation.elements[id];

    if (typeof an === "undefined") {
        return ;
    }

    an.pause = 0;
    an.paused = 0;

    if (typeof an.onResume === "function") {
        an.onResume.apply(an.element);
    }
}

function resumeAnimationAll(filter){
    $.each($.animation.elements, function(k, v){
        if (filter) {
            if (typeof filter === "string") {
                if (matches.call(v.element, filter)) resumeAnimation(k);
            } else if (filter.length) {
                $.each(filter, function(){
                    if (v.element === this) resumeAnimation(k);
                });
            } else if (filter instanceof Element) {
                if (v.element === filter) resumeAnimation(k);
            }
        } else {
            resumeAnimation(k);
        }
    });
}

/* eslint-enable */

var defaultChainOptions = {
    loop: false,
    onChainItem: null,
    onChainItemComplete: null,
    onChainComplete: null
}

function chain(arr, opt){
    var o = $.extend({}, defaultChainOptions, opt);

    if (typeof o.loop !== "boolean") {
        o.loop--;
    }

    if (!Array.isArray(arr)) {
        console.warn("Chain array is not defined!");
        return false;
    }

    var reducer = function(acc, item){
        return acc.then(function(){
            if (typeof o["onChainItem"] === "function") {
                o["onChainItem"](item);
            }
            return animate(item).then(function(){
                if (typeof o["onChainItemComplete"] === "function") {
                    o["onChainItemComplete"](item);
                }
            });
        });
    };

    arr.reduce(reducer, Promise.resolve()).then(function(){
        if (typeof o["onChainComplete"] === "function") {
            o["onChainComplete"]();
        }

        if (o.loop) {
            chain(arr, o);
        }
    });
}

$.easing = {};

$.extend($.easing, Easing);

$.extend({
    animate: function(args){
        var el, draw, dur, ease, cb;

        if (arguments.length > 1) {
            el = $(arguments[0])[0];
            draw = arguments[1];
            dur = arguments[2] || $.animation.duration;
            ease = arguments[3] || $.animation.ease;
            cb = arguments[4];

            if (typeof dur === 'function') {
                cb = dur;
                ease = $.animation.ease;
                dur = $.animation.duration;
            }

            if (typeof ease === 'function') {
                cb = ease;
                ease = $.animation.ease;
            }

            return animate({
                el: el,
                draw: draw,
                dur: dur,
                ease: ease,
                onDone: cb
            });
        }

        return animate(args);
    },
    chain: chain,
    stop: stopAnimation,
    stopAll: stopAnimationAll,
    resume: resumeAnimation,
    resumeAll: resumeAnimationAll,
    pause: pauseAnimation,
    pauseAll: pauseAnimationAll
});

$.fn.extend({
    /**
     *

     args = {
         draw: {} | function,
         dur: 1000,
         ease: "linear",
         loop: 0,
         pause: 0,
         dir: "normal",
         defer: 0,
         onFrame: function,
         onDone: function
     }

     * @returns {this}
     */
    animate: function(args){
        var that = this;
        var draw, dur, easing, cb;
        var a = args;
        var compatibilityMode;

        compatibilityMode = !Array.isArray(args) && (arguments.length > 1 || (arguments.length === 1 && typeof arguments[0].draw === 'undefined'));

        if ( compatibilityMode ) {
            draw = arguments[0];
            dur = arguments[1] || $.animation.duration;
            easing = arguments[2] || $.animation.ease;
            cb = arguments[3];

            if (typeof dur === 'function') {
                cb = dur;
                dur = $.animation.duration;
                easing = $.animation.ease;
            }

            if (typeof easing === 'function') {
                cb = easing;
                easing = $.animation.ease;
            }

            return this.each(function(){
                return $.animate({
                    el: this,
                    draw: draw,
                    dur: dur,
                    ease: easing,
                    onDone: cb
                });
            });
        }

        if (Array.isArray(args)) {
            $.each(args, function(){
                var a = this;
                that.each(function(){
                    a.el = this;
                    $.animate(a);
                });
            });
            return this;
        }

        return this.each(function(){
            a.el = this;
            $.animate(a);
        });
    },

    chain: function(arr, loop){
        return this.each(function(){
            var el = this;
            $.each(arr, function(){
                this.el = el;
            });
            $.chain(arr, loop);
        });
    },

    /**
     *
     * @param done
     * @returns {this}
     */
    stop: function(done){
        return this.each(function(){
            var el = this;
            $.each($.animation.elements, function(k, o){
                if (o.element === el) {
                    stopAnimation(k, done);
                }
            });
        });
    },

    pause: function(){
        return this.each(function(){
            var el = this;
            $.each($.animation.elements, function(k, o){
                if (o.element === el) {
                    pauseAnimation(k);
                }
            });
        });
    },

    resume: function(){
        return this.each(function(){
            var el = this;
            $.each($.animation.elements, function(k, o){
                if (o.element === el) {
                    resumeAnimation(k);
                }
            });
        });
    }
});


// Source: src/visibility.js

/* global $ */

$.extend({
    hidden: function(el, val, cb){
        el = $(el)[0];

        if (typeof val === "string") {
            val = val.toLowerCase() === "true";
        }

        if (typeof val === "function") {
            cb = val;
            val = !el.hidden;
        }

        el.hidden = val;

        if (typeof cb === "function") {
            $.bind(cb, el);
            cb.call(el, arguments);
        }

        return this;
    },

    hide: function(el, cb){
        var $el = $(el);

        $el.origin('display', (el.style.display ? el.style.display : getComputedStyle(el, null).display));
        el.style.display = 'none';

        if (typeof cb === "function") {
            $.bind(cb, el);
            cb.call(el, arguments);
        }

        return this;
    },

    show: function(el, cb){
        var display = $(el).origin('display', undefined, "block");
        el.style.display = display ? display === 'none' ? 'block' : display : '';
        if (parseInt(el.style.opacity) === 0) {
            el.style.opacity = "1";
        }
        if (typeof cb === "function") {
            $.bind(cb, el);
            cb.call(el, arguments);
        }
        return this;
    },

    visible: function(el, mode, cb){
        if (mode === undefined) {
            mode = true;
        }
        el.style.visibility = mode ? 'visible' : 'hidden';
        if (typeof cb === "function") {
            $.bind(cb, el);
            cb.call(el, arguments);
        }
        return this;
    },

    toggle: function(el, cb){
        var func = getComputedStyle(el, null).display !== 'none' ? 'hide' : 'show';
        return $[func](el, cb);
    }
});

$.fn.extend({
    hide: function(){
        var callback;

        $.each(arguments, function(){
            if (typeof this === 'function') {
                callback = this;
            }
        });

        return this.each(function(){
            $.hide(this, callback);
        });
    },

    show: function(){
        var callback;

        $.each(arguments, function(){
            if (typeof this === 'function') {
                callback = this;
            }
        });

        return this.each(function(){
            $.show(this, callback);
        });
    },

    visible: function(mode, cb){
        return this.each(function(){
            $.visible(this, mode, cb);
        });
    },

    toggle: function(cb){
        return this.each(function(){
            $.toggle(this, cb);
        });
    },

    hidden: function(val, cb){
        return this.each(function(){
            $.hidden(this, val, cb);
        });
    }
});



// Source: src/effects.js

/* global $, not, isVisible */

$.extend({
    fx: {
        off: false
    }
});

$.fn.extend({
    fadeIn: function(dur, easing, cb){
        return this.each(function(){
            var el = this;
            var $el = $(el);
            var visible = !(!isVisible(el) || (isVisible(el) && +($el.style('opacity')) === 0));

            if (visible) {
                return this;
            }

            if (not(dur) && not(easing) && not(cb)) {
                cb = null;
                dur = $.animation.duration;
            } else if (typeof dur === "function") {
                cb = dur;
                dur = $.animation.duration;
            }

            if (typeof easing === "function") {
                cb = easing;
                easing = $.animation.ease;
            }

            if ($.fx.off) {
                dur = 0;
            }

            var originDisplay = $el.origin("display", undefined, 'block');

            el.style.opacity = "0";
            el.style.display = originDisplay;

            return $.animate({
                el: el,
                draw: {
                    opacity: 1
                },
                dur: dur,
                ease: easing,
                onDone: function(){
                    if (typeof cb === 'function') {
                        $.proxy(cb, this)();
                    }
                }
            });
        });
    },

    fadeOut: function(dur, easing, cb){
        return this.each(function(){
            var el = this;
            var $el = $(el);

            if ( !isVisible(el) ) return ;

            if (not(dur) && not(easing) && not(cb)) {
                cb = null;
                dur = $.animation.duration;
            } else
            if (typeof dur === "function") {
                cb = dur;
                dur = $.animation.duration;
            }
            if (typeof easing === "function") {
                cb = easing;
                easing = $.animation.ease;
            }

            $el.origin("display", $el.style('display'));

            return $.animate({
                el: el,
                draw: {
                    opacity: 0
                },
                dur: dur,
                ease: easing,
                onDone: function(){
                    this.style.display = 'none';

                    if (typeof cb === 'function') {
                        $.proxy(cb, this)();
                    }
                }
            });
        });
    },

    slideUp: function(dur, easing, cb){
        return this.each(function(){
            var el = this;
            var $el = $(el);
            var currHeight;

            if ($el.height() === 0) return ;

            if (not(dur) && not(easing) && not(cb)) {
                cb = null;
                dur = $.animation.duration;
            } else
            if (typeof dur === "function") {
                cb = dur;
                dur = $.animation.duration;
            }
            if (typeof easing === "function") {
                cb = easing;
                easing = $.animation.ease;
            }

            currHeight = $el.height();
            $el.origin("height", currHeight);
            $el.origin("display", $(el).style('display'));

            $el.css({
                overflow: "hidden"
            });

            return $.animate({
                el: el,
                draw: {
                    height: 0
                },
                dur: dur,
                ease: easing,
                onDone: function(){
                    $el.hide().removeStyleProperty("overflow, height");
                    if (typeof cb === 'function') {
                        $.proxy(cb, this)();
                    }
                }
            });
        });
    },

    slideDown: function(dur, easing, cb){
        return this.each(function(){
            var el = this;
            var $el = $(el);
            var targetHeight, originDisplay;

            if (not(dur) && not(easing) && not(cb)) {
                cb = null;
                dur = $.animation.duration;
            } else
            if (typeof dur === "function") {
                cb = dur;
                dur = $.animation.duration;
            }
            if (typeof easing === "function") {
                cb = easing;
                easing = $.animation.ease;
            }

            $el.show().visible(false);
            targetHeight = +$el.origin("height", undefined, $el.height());
            if (parseInt(targetHeight) === 0) {
                targetHeight = el.scrollHeight;
            }
            originDisplay = $el.origin("display", $el.style('display'), "block");
            $el.height(0).visible(true);

            $el.css({
                overflow: "hidden",
                display: originDisplay === "none" ? "block" : originDisplay
            });

            return $.animate({
                el: el,
                draw: {
                    height: targetHeight
                },
                dur: dur,
                ease: easing,
                onDone: function(){
                    $(el).removeStyleProperty("overflow, height, visibility");
                    if (typeof cb === 'function') {
                        $.proxy(cb, this)();
                    }
                }
            });
        });
    },

    moveTo: function(x, y, dur, easing, cb){
        var draw = {
            top: y,
            left: x
        };

        if (typeof dur === "function") {
            cb = dur;
            dur = $.animation.duration;
            easing = $.animation.ease;
        }

        if (typeof easing === "function") {
            cb = easing;
            easing = $.animation.ease;
        }

        return this.each(function(){
            $.animate({
                el: this,
                draw: draw,
                dur: dur,
                ease: easing,
                onDone: cb
            });
        });
    },

    centerTo: function(x, y, dur, easing, cb){
        if (typeof dur === "function") {
            cb = dur;
            dur = $.animation.duration;
            easing = $.animation.ease;
        }

        if (typeof easing === "function") {
            cb = easing;
            easing = $.animation.ease;
        }

        return this.each(function(){
            var draw = {
                left: x - this.clientWidth / 2,
                top: y - this.clientHeight / 2
            };
            $.animate({
                el: this,
                draw: draw,
                dur: dur,
                ease: easing,
                onDone: cb
            });
        });
    },

    colorTo: function(color, dur, easing, cb){
        var draw = {
            color: color
        };

        if (typeof dur === "function") {
            cb = dur;
            dur = $.animation.duration;
            easing = $.animation.ease;
        }

        if (typeof easing === "function") {
            cb = easing;
            easing = $.animation.ease;
        }

        return this.each(function(){
            $.animate({
                el: this,
                draw: draw,
                dur: dur,
                ease: easing,
                onDone: cb
            });
        });
    },

    backgroundTo: function(color, dur, easing, cb){
        var draw = {
            backgroundColor: color
        };

        if (typeof dur === "function") {
            cb = dur;
            dur = $.animation.duration;
            easing = $.animation.ease;
        }

        if (typeof easing === "function") {
            cb = easing;
            easing = $.animation.ease;
        }

        return this.each(function(){
            $.animate({
                el: this,
                draw: draw,
                dur: dur,
                ease: easing,
                onDone: cb
            });
        });
    }
});

// Source: src/init.js

/* global $, isArrayLike, isPlainObject, hasProp, str2arr */

$.init = function(sel, ctx){
    var parsed;
    var that = this;

    if (typeof sel === "string") {
        sel = sel.trim();
    }

    this.uid = $.uniqueId();

    if (!sel) {
        return this;
    }

    if (typeof sel === "function") {
        return $.ready(sel);
    }

    if (sel instanceof Element) {
        this.push(sel);
        return this;
    }

    if (sel instanceof $) {
        $.each(sel, function(){
            that.push(this);
        });
        return this;
    }

    if (sel === "window") sel = window;
    if (sel === "document") sel = document;
    if (sel === "body") sel = document.body;
    if (sel === "html") sel = document.documentElement;
    if (sel === "doctype") sel = document.doctype;
    if (sel && (sel.nodeType || sel.self === window)) {
        this.push(sel);
        return this;
    }

    if (isArrayLike(sel)) {
        $.each(sel, function(){
            $(this).each(function(){
                that.push(this);
            });
        });
        return this;
    }

    if (typeof sel !== "string" && (sel.self && sel.self !== window)) {
        return this;
    }

    if (sel === "#" || sel === ".") {
        console.error("Selector can't be # or .") ;
        return this;
    }

    if (sel[0] === "@") {

        $("[data-role]").each(function(){
            var roles = str2arr($(this).attr("data-role"), ",");
            if (roles.indexOf(sel.slice(1)) > -1) {
                that.push(this);
            }
        });

    } else {

        parsed = $.parseHTML(sel);

        if (parsed.length === 1 && parsed[0].nodeType === 3) { // Must be a text node -> css sel
            try {
                [].push.apply(this, document.querySelectorAll(sel));
            } catch (e) {
                //console.error(sel + " is not a valid selector");
            }
        } else {
            $.merge(this, parsed);
        }
    }

    if (ctx !== undefined) {
        if (ctx instanceof $) {
            this.each(function () {
                $(ctx).append(that);
            });
        } else if (ctx instanceof HTMLElement) {
            $(ctx).append(that);
        } else {
            if (isPlainObject(ctx)) {
                $.each(this,function(){
                    for(var name in ctx) {
                        if (hasProp(ctx, name))
                            this.setAttribute(name, ctx[name]);
                    }
                });
            }
        }
    }

    return this;
};

$.init.prototype = $.fn;


// Source: src/populate.js

/* global Promise, $ */

var _$ = window.$;

$.Promise = Promise;

window.m4q = $;

if (typeof window.$ === "undefined") {
    window.$ = $;
}

$.global = function(){
    _$ = window.$;
    window.$ = $;
};

$.noConflict = function() {
    if ( window.$ === $ ) {
        window.$ = _$;
    }

    return $;
};

}(window));



(function($) {
    'use strict';

    var meta_init = $.meta('metro4:init').attr("content");
    var meta_locale = $.meta('metro4:locale').attr("content");
    var meta_week_start = $.meta('metro4:week_start').attr("content");
    var meta_date_format = $.meta('metro4:date_format').attr("content");
    var meta_date_format_input = $.meta('metro4:date_format_input').attr("content");
    var meta_animation_duration = $.meta('metro4:animation_duration').attr("content");
    var meta_callback_timeout = $.meta('metro4:callback_timeout').attr("content");
    var meta_timeout = $.meta('metro4:timeout').attr("content");
    var meta_scroll_multiple = $.meta('metro4:scroll_multiple').attr("content");
    var meta_cloak = $.meta('metro4:cloak').attr("content");
    var meta_cloak_duration = $.meta('metro4:cloak_duration').attr("content");
    var meta_global_common = $.meta('metro4:global_common').attr("content");
    var meta_blur_image = $.meta('metro4:blur_image').attr("content");

    if (window.METRO_BLUR_IMAGE === undefined) {
        window.METRO_BLUR_IMAGE = meta_blur_image !== undefined ? JSON.parse(meta_global_common) : false;
    }

    if (window.METRO_GLOBAL_COMMON === undefined) {
        window.METRO_GLOBAL_COMMON = meta_global_common !== undefined ? JSON.parse(meta_global_common) : false;
    }

    var meta_jquery = $.meta('metro4:jquery').attr("content"); //undefined
    window.jquery_present = typeof jQuery !== "undefined";
    if (window.METRO_JQUERY === undefined) {
        window.METRO_JQUERY = meta_jquery !== undefined ? JSON.parse(meta_jquery) : true;
    }
    window.useJQuery = window.jquery_present && window.METRO_JQUERY;


    /* Added by Ken Kitay https://github.com/kens-code*/
    var meta_about = $.meta('metro4:about').attr("content");
    if (window.METRO_SHOW_ABOUT === undefined) {
        window.METRO_SHOW_ABOUT = meta_about !== undefined ? JSON.parse(meta_about) : true;
    }
    /* --- end ---*/

    var meta_compile = $.meta('metro4:compile').attr("content");
    if (window.METRO_SHOW_COMPILE_TIME === undefined) {
        window.METRO_SHOW_COMPILE_TIME = meta_compile !== undefined ? JSON.parse(meta_compile) : true;
    }

    if (window.METRO_INIT === undefined) {
        window.METRO_INIT = meta_init !== undefined ? JSON.parse(meta_init) : true;
    }

    if (window.METRO_DEBUG === undefined) {window.METRO_DEBUG = true;}

    if (window.METRO_WEEK_START === undefined) {
        window.METRO_WEEK_START = meta_week_start !== undefined ? parseInt(meta_week_start) : 0;
    }
    if (window.METRO_DATE_FORMAT === undefined) {
        window.METRO_DATE_FORMAT = meta_date_format !== undefined ? meta_date_format : "%Y-%m-%d";
    }
    if (window.METRO_DATE_FORMAT_INPUT === undefined) {
        window.METRO_DATE_FORMAT_INPUT = meta_date_format_input !== undefined ? meta_date_format_input : "%Y-%m-%d";
    }
    if (window.METRO_LOCALE === undefined) {
        window.METRO_LOCALE = meta_locale !== undefined ? meta_locale : 'en-US';
    }
    if (window.METRO_ANIMATION_DURATION === undefined) {
        window.METRO_ANIMATION_DURATION = meta_animation_duration !== undefined ? parseInt(meta_animation_duration) : 100;
    }
    if (window.METRO_CALLBACK_TIMEOUT === undefined) {
        window.METRO_CALLBACK_TIMEOUT = meta_callback_timeout !== undefined ? parseInt(meta_callback_timeout) : 500;
    }
    if (window.METRO_TIMEOUT === undefined) {
        window.METRO_TIMEOUT = meta_timeout !== undefined ? parseInt(meta_timeout) : 2000;
    }
    if (window.METRO_SCROLL_MULTIPLE === undefined) {
        window.METRO_SCROLL_MULTIPLE = meta_scroll_multiple !== undefined ? parseInt(meta_scroll_multiple) : 20;
    }
    if (window.METRO_CLOAK_REMOVE === undefined) {
        window.METRO_CLOAK_REMOVE = meta_cloak !== undefined ? (""+meta_cloak).toLowerCase() : "fade";
    }
    if (window.METRO_CLOAK_DURATION === undefined) {
        window.METRO_CLOAK_DURATION = meta_cloak_duration !== undefined ? parseInt(meta_cloak_duration) : 300;
    }

    if (window.METRO_HOTKEYS_FILTER_CONTENT_EDITABLE === undefined) {window.METRO_HOTKEYS_FILTER_CONTENT_EDITABLE = true;}
    if (window.METRO_HOTKEYS_FILTER_INPUT_ACCEPTING_ELEMENTS === undefined) {window.METRO_HOTKEYS_FILTER_INPUT_ACCEPTING_ELEMENTS = true;}
    if (window.METRO_HOTKEYS_FILTER_TEXT_INPUTS === undefined) {window.METRO_HOTKEYS_FILTER_TEXT_INPUTS = true;}
    if (window.METRO_HOTKEYS_BUBBLE_UP === undefined) {window.METRO_HOTKEYS_BUBBLE_UP = false;}
    if (window.METRO_THROWS === undefined) {window.METRO_THROWS = true;}

    window.METRO_MEDIA = [];

}(m4q));


/* global jQuery, define */
/* Metro 4 Core */
(function( factory ) {
    if ( typeof define === 'function' && define.amd ) {
        define('metro4', factory );
    } else {
        factory( );
    }
}(function( ) {
    'use strict';

    var $ = m4q; // eslint-disable-line

    if (typeof m4q === 'undefined') {
        throw new Error('Metro 4 requires m4q helper!');
    }

    if (!('MutationObserver' in window)) {
        throw new Error('Metro 4 requires MutationObserver!');
    }

    var isTouch = (('ontouchstart' in window) || (navigator["MaxTouchPoints"] > 0) || (navigator["msMaxTouchPoints"] > 0));

    var normalizeComponentName = function(name){
        return typeof name !== "string" ? undefined : name.replace(/-/g, "").toLowerCase();
    };

    var Metro = {

        version: "4.4.3",
        compileTime: "22/04/2022 05:02:47",
        buildNumber: "",
        isTouchable: isTouch,
        fullScreenEnabled: document.fullscreenEnabled,
        sheet: null,


        controlsPosition: {
            INSIDE: "inside",
            OUTSIDE: "outside"
        },

        groupMode: {
            ONE: "one",
            MULTI: "multi"
        },

        aspectRatio: {
            HD: "hd",
            SD: "sd",
            CINEMA: "cinema"
        },

        fullScreenMode: {
            WINDOW: "window",
            DESKTOP: "desktop"
        },

        position: {
            TOP: "top",
            BOTTOM: "bottom",
            LEFT: "left",
            RIGHT: "right",
            TOP_RIGHT: "top-right",
            TOP_LEFT: "top-left",
            BOTTOM_LEFT: "bottom-left",
            BOTTOM_RIGHT: "bottom-right",
            LEFT_BOTTOM: "left-bottom",
            LEFT_TOP: "left-top",
            RIGHT_TOP: "right-top",
            RIGHT_BOTTOM: "right-bottom"
        },

        popoverEvents: {
            CLICK: "click",
            HOVER: "hover",
            FOCUS: "focus"
        },

        stepperView: {
            SQUARE: "square",
            CYCLE: "cycle",
            DIAMOND: "diamond"
        },

        listView: {
            LIST: "list",
            CONTENT: "content",
            ICONS: "icons",
            ICONS_MEDIUM: "icons-medium",
            ICONS_LARGE: "icons-large",
            TILES: "tiles",
            TABLE: "table"
        },

        events: {
            click: 'click',
            start: isTouch ? 'touchstart' : 'mousedown',
            stop: isTouch ? 'touchend' : 'mouseup',
            move: isTouch ? 'touchmove' : 'mousemove',
            enter: isTouch ? 'touchstart' : 'mouseenter',

            startAll: 'mousedown touchstart',
            stopAll: 'mouseup touchend',
            moveAll: 'mousemove touchmove',

            leave: 'mouseleave',
            focus: 'focus',
            blur: 'blur',
            resize: 'resize',
            keyup: 'keyup',
            keydown: 'keydown',
            keypress: 'keypress',
            dblclick: 'dblclick',
            input: 'input',
            change: 'change',
            cut: 'cut',
            paste: 'paste',
            scroll: 'scroll',
            mousewheel: 'mousewheel',
            inputchange: "change input propertychange cut paste copy drop",
            dragstart: "dragstart",
            dragend: "dragend",
            dragenter: "dragenter",
            dragover: "dragover",
            dragleave: "dragleave",
            drop: 'drop',
            drag: 'drag'
        },

        keyCode: {
            BACKSPACE: 8,
            TAB: 9,
            ENTER: 13,
            SHIFT: 16,
            CTRL: 17,
            ALT: 18,
            BREAK: 19,
            CAPS: 20,
            ESCAPE: 27,
            SPACE: 32,
            PAGEUP: 33,
            PAGEDOWN: 34,
            END: 35,
            HOME: 36,
            LEFT_ARROW: 37,
            UP_ARROW: 38,
            RIGHT_ARROW: 39,
            DOWN_ARROW: 40,
            COMMA: 188
        },

        media_queries: {
            FS: "(min-width: 0px)",
            XS: "(min-width: 360px)",
            SM: "(min-width: 576px)",
            MD: "(min-width: 768px)",
            LG: "(min-width: 992px)",
            XL: "(min-width: 1200px)",
            XXL: "(min-width: 1452px)"
        },

        media_sizes: {
            FS: 0,
            XS: 360,
            SM: 576,
            LD: 640,
            MD: 768,
            LG: 992,
            XL: 1200,
            XXL: 1452
        },

        media_mode: {
            FS: "fs",
            XS: "xs",
            SM: "sm",
            MD: "md",
            LG: "lg",
            XL: "xl",
            XXL: "xxl"
        },

        media_modes: ["fs","xs","sm","md","lg","xl","xxl"],

        actions: {
            REMOVE: 1,
            HIDE: 2
        },

        hotkeys: {},
        locales: {},
        utils: {},
        colors: {},
        dialog: null,
        pagination: null,
        md5: null,
        storage: null,
        export: null,
        animations: null,
        cookie: null,
        template: null,
        defaults: {},

        about: function(){
            var content =
                "<h3>About</h3>" +
                "<hr>" +
                "<div><b>Metro 4</b> - v" + Metro.version +". "+ Metro.showCompileTime() + "</div>" +
                "<div><b>M4Q</b> - " + m4q.version + "</div>";
            Metro.infobox.create(content)
        },

        info: function(){
            console.info("Metro 4 - v" + Metro.version +". "+ Metro.showCompileTime());
            console.info("m4q - " + m4q.version);
        },

        showCompileTime: function(){
            return "Built at: " + Metro.compileTime;
        },

        aboutDlg: function(){
            alert("Metro 4 - v" + Metro.version +". "+ Metro.showCompileTime());
        },

        ver: function(){
            return Metro.version;
        },

        build: function(){
            return Metro.build;
        },

        compile: function(){
            return Metro.compileTime;
        },

        observe: function(){
            var observer, observerCallback;
            var observerConfig = {
                childList: true,
                attributes: true,
                subtree: true
            };
            observerCallback = function(mutations){
                mutations.map(function(mutation){
                    if (mutation.type === 'attributes' && mutation.attributeName !== "data-role") {
                        if (mutation.attributeName === 'data-hotkey') {
                            Metro.initHotkeys([mutation.target], true);
                        } else {
                            var element = $(mutation.target);
                            var mc = element.data('metroComponent');
                            var attr = mutation.attributeName, newValue = element.attr(attr), oldValue = mutation.oldValue;

                            if (mc !== undefined) {
                                element.fire("attr-change", {
                                    attr: attr,
                                    newValue: newValue,
                                    oldValue: oldValue,
                                    __this: element[0]
                                });

                                $.each(mc, function(){
                                    var plug = Metro.getPlugin(element, this);
                                    if (plug && typeof plug.changeAttribute === "function") {
                                        plug.changeAttribute(attr, newValue, oldValue);
                                    }
                                });
                            }
                        }
                    } else

                    if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
                        var i, widgets = [];
                        var $node, node, nodes = mutation.addedNodes;

                        if (nodes.length) {
                            for(i = 0; i < nodes.length; i++) {
                                node = nodes[i];
                                $node = $(node);

                                if ($node.attr("data-role") !== undefined) {
                                    widgets.push(node);
                                }

                                $.each($node.find("[data-role]"), function(){
                                    var o = this;
                                    if (widgets.indexOf(o) !== -1) {
                                        return;
                                    }
                                    widgets.push(o);
                                });
                            }

                            if (widgets.length) Metro.initWidgets(widgets, "observe");
                        }

                    } else  {
                        //console.log(mutation);
                    }
                });
            };
            observer = new MutationObserver(observerCallback);
            observer.observe($("html")[0], observerConfig);
        },

        init: function(){
            var widgets = $("[data-role]");
            var hotkeys = $("[data-hotkey]");
            var html = $("html");
            var that = this;

            if (window.METRO_BLUR_IMAGE) {
                html.addClass("use-blur-image");
            }

            if (window.METRO_SHOW_ABOUT) Metro.info(true);

            if (isTouch === true) {
                html.addClass("metro-touch-device");
            } else {
                html.addClass("metro-no-touch-device");
            }

            Metro.sheet = this.utils.newCssSheet();

            this.utils.addCssRule(Metro.sheet, "*, *::before, *::after", "box-sizing: border-box;");

            window.METRO_MEDIA = [];
            $.each(Metro.media_queries, function(key, query){
                if (that.utils.media(query)) {
                    window.METRO_MEDIA.push(Metro.media_mode[key]);
                }
            });

            Metro.observe();

            Metro.initHotkeys(hotkeys);
            Metro.initWidgets(widgets, "init");

            if (window.METRO_CLOAK_REMOVE !== "fade") {
                $(".m4-cloak").removeClass("m4-cloak");
                $(window).fire("metro-initiated");
            } else {
                $(".m4-cloak").animate({
                    draw: {
                        opacity: 1
                    },
                    dur: 300,
                    onDone: function(){
                        $(".m4-cloak").removeClass("m4-cloak");
                        $(window).fire("metro-initiated");
                    }
                });
            }
        },

        initHotkeys: function(hotkeys, redefine){
            $.each(hotkeys, function(){
                var element = $(this);
                var hotkey = element.attr('data-hotkey') ? element.attr('data-hotkey').toLowerCase() : false;
                var fn = element.attr('data-hotkey-func') ? element.attr('data-hotkey-func') : false;

                if (hotkey === false) {
                    return;
                }

                if (element.data('hotKeyBonded') === true && redefine !== true) {
                    return;
                }

                Metro.hotkeys[hotkey] = [this, fn];
                element.data('hotKeyBonded', true);
                element.fire("hot-key-bonded", {
                    __this: element[0],
                    hotkey: hotkey,
                    fn: fn
                });
            });
        },

        initWidgets: function(widgets) {
            var that = this;

            $.each(widgets, function () {
                var $this = $(this), roles;

                if (!this.hasAttribute("data-role")) {
                    return ;
                }

                roles = $this.attr('data-role').split(/\s*,\s*/);

                roles.map(function (func) {

                    var $$ = that.utils.$();
                    var _func = normalizeComponentName(func);

                    if ($$.fn[_func] !== undefined && $this.attr("data-role-"+_func) === undefined) {
                        try {
                            $$.fn[_func].call($this);
                            $this.attr("data-role-"+_func, true);

                            var mc = $this.data('metroComponent');

                            if (mc === undefined) {
                                mc = [_func];
                            } else {
                                mc.push(_func);
                            }
                            $this.data('metroComponent', mc);

                            $this.fire("create", {
                                __this: $this[0],
                                name: _func
                            });
                            $(document).fire("component-create", {
                                element: $this[0],
                                name: _func
                            });
                        } catch (e) {
                            console.error("Error creating component " + func + " for ", $this[0]);
                            throw e;
                        }
                    }
                });
            });
        },

        plugin: function(name, object){
            var _name = normalizeComponentName(name);

            var register = function($){
                $.fn[_name] = function( options ) {
                    return this.each(function() {
                        $.data( this, _name, Object.create(object).init(options, this ));
                    });
                };
            }

            register(m4q);

            if (window.useJQuery) {
                register(jQuery);
            }
        },

        pluginExists: function(name){
            var $ = window.useJQuery ? jQuery : m4q;
            return typeof $.fn[normalizeComponentName(name)] === "function";
        },

        destroyPlugin: function(element, name){
            var p, mc;
            var el = $(element);
            var _name = normalizeComponentName(name);

            p = Metro.getPlugin(el, _name);

            if (typeof p === 'undefined') {
                console.warn("Component "+name+" can not be destroyed: the element is not a Metro 4 component.");
                return ;
            }

            if (typeof p['destroy'] !== 'function') {
                console.warn("Component "+name+" can not be destroyed: method destroy not found.");
                return ;
            }

            p['destroy']();
            mc = el.data("metroComponent");
            this.utils.arrayDelete(mc, _name);
            el.data("metroComponent", mc);
            $.removeData(el[0], _name);
            el.removeAttr("data-role-"+_name);
        },

        destroyPluginAll: function(element){
            var el = $(element);
            var mc = el.data("metroComponent");

            if (mc !== undefined && mc.length > 0) $.each(mc, function(){
                Metro.destroyPlugin(el[0], this);
            });
        },

        noop: function(){},
        noop_true: function(){return true;},
        noop_false: function(){return false;},

        requestFullScreen: function(element){
            if (element["mozRequestFullScreen"]) {
                element["mozRequestFullScreen"]();
            } else if (element["webkitRequestFullScreen"]) {
                element["webkitRequestFullScreen"]();
            } else if (element["msRequestFullscreen"]) {
                element["msRequestFullscreen"]();
            } else {
                element.requestFullscreen().catch( function(err){
                    console.warn("Error attempting to enable full-screen mode: "+err.message+" "+err.name);
                });
            }
        },

        exitFullScreen: function(){
            if (document["mozCancelFullScreen"]) {
                document["mozCancelFullScreen"]();
            }
            else if (document["webkitCancelFullScreen"]) {
                document["webkitCancelFullScreen"]();
            }
            else if (document["msExitFullscreen"]) {
                document["msExitFullscreen"]();
            } else {
                document.exitFullscreen().catch( function(err){
                    console.warn("Error attempting to disable full-screen mode: "+err.message+" "+err.name);
                });
            }
        },

        inFullScreen: function(){
            var fsm = (document.fullscreenElement || document["webkitFullscreenElement"] || document["mozFullScreenElement"] || document["msFullscreenElement"]);
            return fsm !== undefined;
        },

        $: function(){
            return window.useJQuery ? jQuery : m4q;
        },

        get$el: function(el){
            return Metro.$()($(el)[0]);
        },

        get$elements: function(el){
            return Metro.$()($(el));
        },

        getPlugin: function(el, name){
            var _name = normalizeComponentName(name);
            var $el = Metro.get$el(el);
            return $el.length ? $el.data(_name) : undefined;
        },

        makePlugin: function(el, name, options){
            var _name = normalizeComponentName(name);
            var $el = Metro.get$elements(el);
            return $el.length && typeof $el[_name] === "function" ? $el[_name](options) : undefined;
        },

        Component: function(nameName, compObj){
            var name = normalizeComponentName(nameName);
            var Utils = Metro.utils;
            var component = $.extend({name: name}, {
                _super: function(el, options, defaults, setup){
                    var self = this;

                    this.elem = el;
                    this.element = $(el);
                    this.options = $.extend( {}, defaults, options );
                    this.component = this.elem;

                    this._setOptionsFromDOM();
                    this._runtime();

                    if (setup && typeof setup === 'object') {
                        $.each(setup, function(key, val){
                            self[key] = val;
                        })
                    }

                    this._createExec();
                },

                _setOptionsFromDOM: function(){
                    var element = this.element, o = this.options;

                    $.each(element.data(), function(key, value){
                        if (key in o) {
                            try {
                                o[key] = JSON.parse(value);
                            } catch (e) {
                                o[key] = value;
                            }
                        }
                    });
                },

                _runtime: function(){
                    var element = this.element, mc;
                    var roles = (element.attr("data-role") || "").toArray(",").map(function(v){
                        return normalizeComponentName(v);
                    }).filter(function(v){
                        return v.trim() !== "";
                    });

                    if (!element.attr('data-role-'+this.name)) {
                        element.attr("data-role-"+this.name, true);
                        if (roles.indexOf(this.name) === -1) {
                            roles.push(this.name);
                            element.attr("data-role", roles.join(","));
                        }

                        mc = element.data('metroComponent');
                        if (mc === undefined) {
                            mc = [this.name];
                        } else {
                            mc.push(this.name);
                        }
                        element.data('metroComponent', mc);
                    }
                },

                _createExec: function(){
                    var that = this, timeout = this.options[this.name+'Deferred'];

                    if (timeout) {
                        setTimeout(function(){
                            that._create();
                        }, timeout)
                    } else {
                        that._create();
                    }
                },

                _fireEvent: function(eventName, data, log, noFire){
                    var element = this.element, o = this.options;
                    var _data;
                    var event = eventName.camelCase().capitalize();

                    data = $.extend({}, data, {__this: element[0]});

                    _data = data ? Object.values(data) : {};

                    if (log) {
                        console.warn(log);
                        console.warn("Event: " + "on"+eventName.camelCase().capitalize());
                        console.warn("Data: ", data);
                        console.warn("Element: ", element[0]);
                    }

                    if (noFire !== true)
                        element.fire(event.toLowerCase(), data);

                    return Utils.exec(o["on"+event], _data, element[0]);
                },

                _fireEvents: function(events, data, log, noFire){
                    var that = this, _events;

                    if (arguments.length === 0) {
                        return ;
                    }

                    if (arguments.length === 1) {

                        $.each(events, function () {
                            var ev = this;
                            that._fireEvent(ev.name, ev.data, ev.log, ev.noFire);
                        });

                        return Utils.objectLength(events);
                    }

                    if (!Array.isArray(events) && typeof events !== "string") {
                        return ;
                    }

                    _events = Array.isArray(events) ? events : events.toArray(",");

                    $.each(_events, function(){
                        that._fireEvent(this, data, log, noFire);
                    });
                },

                getComponent: function(){
                    return this.component;
                },

                getComponentName: function(){
                    return this.name;
                }
            }, compObj);

            Metro.plugin(name, component);

            return component;
        }
    };

    $(window).on(Metro.events.resize, function(){
        window.METRO_MEDIA = [];
        $.each(Metro.media_queries, function(key, query){
            if (Metro.utils.media(query)) {
                window.METRO_MEDIA.push(Metro.media_mode[key]);
            }
        });
    });

    window.Metro = Metro;

    if (window.METRO_INIT ===  true) {
        $(function(){
            Metro.init()
        });
    }

    return Metro;

}));



(function() {
    'use strict';

    if (typeof Array.shuffle !== "function") {
        Array.prototype.shuffle = function () {
            var currentIndex = this.length, temporaryValue, randomIndex;

            while (0 !== currentIndex) {

                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                temporaryValue = this[currentIndex];
                this[currentIndex] = this[randomIndex];
                this[randomIndex] = temporaryValue;
            }

            return this;
        };
    }

    if (typeof Array.clone !== "function") {
        Array.prototype.clone = function () {
            return this.slice(0);
        };
    }

    if (typeof Array.unique !== "function") {
        Array.prototype.unique = function () {
            var a = this.concat();
            for (var i = 0; i < a.length; ++i) {
                for (var j = i + 1; j < a.length; ++j) {
                    if (a[i] === a[j])
                        a.splice(j--, 1);
                }
            }

            return a;
        };
    }

    if (typeof Array.from !== "function") {
        Array.prototype.from = function(val) {
            var i, a = [];

            if (val.length === undefined && typeof val === "object") {
                return Object.values(val);
            }

            if (val.length !== undefined) {
                for(i = 0; i < val.length; i++) {
                    a.push(val[i]);
                }
                return a;
            }

            throw new Error("Value can not be converted to array");
        };
    }

    if (typeof Array.contains !== "function") {
        Array.prototype.contains = function(val, from){
            return this.indexOf(val, from) > -1;
        }
    }

    if (typeof Array.includes !== "function") {
        Array.prototype.includes = function(val, from){
            return this.indexOf(val, from) > -1;
        }
    }
}());


/* global Metro, METRO_WEEK_START */
(function(Metro) {
    'use strict';
    Date.prototype.getWeek = function (dowOffset) {
        var nYear, nday, newYear, day, daynum, weeknum;

        dowOffset = typeof dowOffset === "undefined" || isNaN(dowOffset) ? METRO_WEEK_START : typeof dowOffset === 'number' ? parseInt(dowOffset) : 0;
        newYear = new Date(this.getFullYear(),0,1);
        day = newYear.getDay() - dowOffset;
        day = (day >= 0 ? day : day + 7);
        daynum = Math.floor((this.getTime() - newYear.getTime() -
            (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;

        if(day < 4) {
            weeknum = Math.floor((daynum+day-1)/7) + 1;
            if(weeknum > 52) {
                nYear = new Date(this.getFullYear() + 1,0,1);
                nday = nYear.getDay() - dowOffset;
                nday = nday >= 0 ? nday : nday + 7;
                weeknum = nday < 4 ? 1 : 53;
            }
        }
        else {
            weeknum = Math.floor((daynum+day-1)/7);
        }
        return weeknum;
    };

    Date.prototype.getYear = function(){
        return this.getFullYear().toString().substr(-2);
    };

    Date.prototype.format = function(format, locale){

        if (locale === undefined) {
            locale = "en-US";
        }

        var cal = (Metro.locales !== undefined && Metro.locales[locale] !== undefined ? Metro.locales[locale] : Metro.locales["en-US"])['calendar'];

        var date = this;
        var nDay = date.getDay(),
            nDate = date.getDate(),
            nMonth = date.getMonth(),
            nYear = date.getFullYear(),
            nHour = date.getHours(),
            aDays = cal['days'],
            aMonths = cal['months'],
            aDayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334],
            isLeapYear = function() {
                return (nYear%4===0 && nYear%100!==0) || nYear%400===0;
            },
            getThursday = function() {
                var target = new Date(date);
                target.setDate(nDate - ((nDay+6)%7) + 3);
                return target;
            },
            zeroPad = function(nNum, nPad) {
                return ('' + (Math.pow(10, nPad) + nNum)).slice(1);
            };
        return format.replace(/(%[a-z])/gi, function(sMatch) {
            return {
                '%a': aDays[nDay].slice(0,3),
                '%A': aDays[nDay],
                '%b': aMonths[nMonth].slice(0,3),
                '%B': aMonths[nMonth],
                '%c': date.toUTCString(),
                '%C': Math.floor(nYear/100),
                '%d': zeroPad(nDate, 2),
                'dd': zeroPad(nDate, 2),
                '%e': nDate,
                '%F': date.toISOString().slice(0,10),
                '%G': getThursday().getFullYear(),
                '%g': ('' + getThursday().getFullYear()).slice(2),
                '%H': zeroPad(nHour, 2),
                // 'HH': zeroPad(nHour, 2),
                '%I': zeroPad((nHour+11)%12 + 1, 2),
                '%j': zeroPad(aDayCount[nMonth] + nDate + ((nMonth>1 && isLeapYear()) ? 1 : 0), 3),
                '%k': '' + nHour,
                '%l': (nHour+11)%12 + 1,
                '%m': zeroPad(nMonth + 1, 2),
                // 'mm': zeroPad(nMonth + 1, 2),
                '%M': zeroPad(date.getMinutes(), 2),
                // 'MM': zeroPad(date.getMinutes(), 2),
                '%p': (nHour<12) ? 'AM' : 'PM',
                '%P': (nHour<12) ? 'am' : 'pm',
                '%s': Math.round(date.getTime()/1000),
                // 'ss': Math.round(date.getTime()/1000),
                '%S': zeroPad(date.getSeconds(), 2),
                // 'SS': zeroPad(date.getSeconds(), 2),
                '%u': nDay || 7,
                '%V': (function() {
                    var target = getThursday(),
                        n1stThu = target.valueOf();
                    target.setMonth(0, 1);
                    var nJan1 = target.getDay();
                    if (nJan1!==4) target.setMonth(0, 1 + ((4-nJan1)+7)%7);
                    return zeroPad(1 + Math.ceil((n1stThu-target)/604800000), 2);
                })(),
                '%w': '' + nDay,
                '%x': date.toLocaleDateString(),
                '%X': date.toLocaleTimeString(),
                '%y': ('' + nYear).slice(2),
                // 'yy': ('' + nYear).slice(2),
                '%Y': nYear,
                // 'YYYY': nYear,
                '%z': date.toTimeString().replace(/.+GMT([+-]\d+).+/, '$1'),
                '%Z': date.toTimeString().replace(/.+\((.+?)\)$/, '$1')
            }[sMatch] || sMatch;
        });
    };

    Date.prototype.addHours = function(n) {
        this.setTime(this.getTime() + (n*60*60*1000));
        return this;
    };

    Date.prototype.addDays = function(n) {
        this.setDate(this.getDate() + (n));
        return this;
    };

    Date.prototype.addMonths = function(n) {
        this.setMonth(this.getMonth() + (n));
        return this;
    };

    Date.prototype.addYears = function(n) {
        this.setFullYear(this.getFullYear() + (n));
        return this;
    };
}(Metro));


(function() {
    'use strict';

    /**
     * Number.prototype.format(n, x, s, c)
     *
     * @param  n: length of decimal
     * @param  x: length of whole part
     * @param  s: sections delimiter
     * @param  c: decimal delimiter
     */
    Number.prototype.format = function(n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };
}());


(function() {
    'use strict';

    if ( typeof Object.create !== 'function' ) {
        Object.create = function (o) {
            function F() {}
            F.prototype = o;
            return new F();
        };
    }

    if (typeof Object.values !== 'function') {
        Object.values = function(obj) {
            return Object.keys(obj).map(function(e) {
                return obj[e]
            });
        }
    }
}());


/* global Metro */
(function(Metro, $) {
    'use strict';

    String.prototype.camelCase = function(){
        return $.camelCase(this);
    };

    String.prototype.dashedName = function(){
        return $.dashedName(this);
    };

    String.prototype.shuffle = function(){
        var _shuffle = function (a) {
            var currentIndex = a.length, temporaryValue, randomIndex;

            while (0 !== currentIndex) {

                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                temporaryValue = a[currentIndex];
                a[currentIndex] = a[randomIndex];
                a[randomIndex] = temporaryValue;
            }

            return a;
        };

        return _shuffle(this.split("")).join("");
    }

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };

    String.prototype.contains = function() {
        return !!~String.prototype.indexOf.apply(this, arguments);
    };

    if (typeof String.includes !== "function") {
        String.prototype.includes = function(){
            return !!~String.prototype.indexOf.apply(this, arguments);
        }
    }

    String.prototype.toDate = function(format, locale) {
        var result;
        var normalized, normalizedFormat, formatItems, dateItems, checkValue;
        var monthIndex, dayIndex, yearIndex, hourIndex, minutesIndex, secondsIndex;
        var year, month, day, hour, minute, second;
        var parsedMonth;

        locale = locale || "en-US";

        var monthNameToNumber = function(month){
            var d, months, index, i;
            var Locales = Metro.locales;

            if (typeof month === "undefined" || month === null) {
                return -1;
            }

            month = month.substr(0, 3);

            if (
                locale !== undefined
                && locale !== "en-US"
                && Locales !== undefined
                && Locales[locale] !== undefined
                && Locales[locale]['calendar'] !== undefined
                && Locales[locale]['calendar']['months'] !== undefined
            ) {
                months = Locales[locale]['calendar']['months'];
                for(i = 12; i < months.length; i++) {
                    if (months[i].toLowerCase() === month.toLowerCase()) {
                        index = i - 12;
                        break;
                    }
                }
                month = Locales["en-US"]['calendar']['months'][index];
            }

            d = Date.parse(month + " 1, 1972");
            if(!isNaN(d)){
                return new Date(d).getMonth() + 1;
            }
            return -1;
        };

        if (format === undefined || format === null || format === "") {
            return new Date(this);
        }

        /* eslint-disable-next-line */
        normalized      = this.replace(/[\/,.:\s]/g, '-');
        /* eslint-disable-next-line */
        normalizedFormat= format.toLowerCase().replace(/[^a-zA-Z0-9%]/g, '-');
        formatItems     = normalizedFormat.split('-');
        dateItems       = normalized.split('-');
        checkValue      = normalized.replace(/-/g,"");

        if (checkValue.trim() === "") {
            return "Invalid Date";
        }

        monthIndex  = formatItems.indexOf("mm") > -1 ? formatItems.indexOf("mm") : formatItems.indexOf("%m");
        dayIndex    = formatItems.indexOf("dd") > -1 ? formatItems.indexOf("dd") : formatItems.indexOf("%d");
        yearIndex   = formatItems.indexOf("yyyy") > -1 ? formatItems.indexOf("yyyy") : formatItems.indexOf("yy") > -1 ? formatItems.indexOf("yy") : formatItems.indexOf("%y");
        hourIndex     = formatItems.indexOf("hh") > -1 ? formatItems.indexOf("hh") : formatItems.indexOf("%h");
        minutesIndex  = formatItems.indexOf("ii") > -1 ? formatItems.indexOf("ii") : formatItems.indexOf("mi") > -1 ? formatItems.indexOf("mi") : formatItems.indexOf("%i");
        secondsIndex  = formatItems.indexOf("ss") > -1 ? formatItems.indexOf("ss") : formatItems.indexOf("%s");

        if (monthIndex > -1 && dateItems[monthIndex] !== "") {
            if (isNaN(parseInt(dateItems[monthIndex]))) {
                dateItems[monthIndex] = monthNameToNumber(dateItems[monthIndex]);
                if (dateItems[monthIndex] === -1) {
                    return "Invalid Date";
                }
            } else {
                parsedMonth = parseInt(dateItems[monthIndex]);
                if (parsedMonth < 1 || parsedMonth > 12) {
                    return "Invalid Date";
                }
            }
        } else {
            return "Invalid Date";
        }

        year  = yearIndex >-1 && dateItems[yearIndex] !== "" ? dateItems[yearIndex] : null;
        month = monthIndex >-1 && dateItems[monthIndex] !== "" ? dateItems[monthIndex] : null;
        day   = dayIndex >-1 && dateItems[dayIndex] !== "" ? dateItems[dayIndex] : null;

        hour    = hourIndex >-1 && dateItems[hourIndex] !== "" ? dateItems[hourIndex] : null;
        minute  = minutesIndex>-1 && dateItems[minutesIndex] !== "" ? dateItems[minutesIndex] : null;
        second  = secondsIndex>-1 && dateItems[secondsIndex] !== "" ? dateItems[secondsIndex] : null;

        result = new Date(year,month-1,day,hour,minute,second);

        return result;
    };

    String.prototype.toArray = function(delimiter, type, format){
        var str = this;
        var a;

        type = type || "string";
        delimiter = delimiter || ",";
        format = format === undefined || format === null ? false : format;

        a = (""+str).split(delimiter);

        return a.map(function(s){
            var result;

            switch (type) {
                case "int":
                case "integer": result = isNaN(s) ? s.trim() : parseInt(s); break;
                case "number":
                case "float": result = isNaN(s) ? s : parseFloat(s); break;
                case "date": result = !format ? new Date(s) : s.toDate(format); break;
                default: result = s.trim();
            }

            return result;
        });
    };
}(Metro, m4q));


/* global jQuery, Metro */
(function(Metro, $) {
    'use strict';
    Metro.utils = {
        isVisible: function(element){
            var el = $(element)[0];
            return this.getStyleOne(el, "display") !== "none"
                && this.getStyleOne(el, "visibility") !== "hidden"
                && el.offsetParent !== null;
        },

        isUrl: function (val) {
            /* eslint-disable-next-line */
            return /^(\.\/|\.\.\/|ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@\-\/]))?/.test(val);
        },

        isTag: function(val){
            /* eslint-disable-next-line */
            return /^<\/?[\w\s="/.':;#-\/\?]+>/gi.test(val);
        },

        isEmbedObject: function(val){
            var embed = ["iframe", "object", "embed", "video"];
            var result = false;
            $.each(embed, function(){
                if (typeof val === "string" && val.toLowerCase() === this) {
                    result = true;
                } else if (val.nodeType !== undefined && val.tagName.toLowerCase() === this) {
                    result = true;
                }
            });
            return result;
        },

        isVideoUrl: function(val){
            return /youtu\.be|youtube|twitch|vimeo/gi.test(val);
        },

        isDate: function(val, format){
            var result;

            if (this.isDateObject(val)) {
                return true;
            }

            if (this.isValue(format)) {
                result = String(val).toDate(format);
            } else {
                result = String(new Date(val));
            }

            return result !== "Invalid Date";
        },

        isDateObject: function(v){
            return typeof v === 'object' && v.getMonth !== undefined;
        },

        isInt: function(n){
            return !isNaN(n) && +n % 1 === 0;
        },

        isFloat: function(n){
            return (!isNaN(n) && +n % 1 !== 0) || /^\d*\.\d+$/.test(n);
        },

        isFunc: function(f){
            return this.isType(f, 'function');
        },

        isObject: function(o){
            return this.isType(o, 'object');
        },

        isType: function(o, t){
            if (!this.isValue(o)) {
                return false;
            }

            if (typeof o === t) {
                return o;
            }

            if ((""+t).toLowerCase() === 'tag' && this.isTag(o)) {
                return o;
            }

            if ((""+t).toLowerCase() === 'url' && this.isUrl(o)) {
                return o;
            }

            if ((""+t).toLowerCase() === 'array' && Array.isArray(o)) {
                return o;
            }

            if (this.isTag(o) || this.isUrl(o)) {
                return false;
            }

            if (typeof window[o] === t) {
                return window[o];
            }

            if (typeof o === 'string' && o.indexOf(".") === -1) {
                return false;
            }

            if (typeof o === 'string' && /[/\s([]+/gm.test(o)) {
                return false;
            }

            if (typeof o === "number" && t.toLowerCase() !== "number") {
                return false;
            }

            var ns = o.split(".");
            var i, context = window;

            for(i = 0; i < ns.length; i++) {
                context = context[ns[i]];
            }

            return typeof context === t ? context : false;
        },

        $: function(){
            return window.useJQuery ? jQuery : m4q;
        },

        isMetroObject: function(el, type){
            var $el = $(el), el_obj = Metro.getPlugin(el, type);

            if ($el.length === 0) {
                console.warn(type + ' ' + el + ' not found!');
                return false;
            }

            if (el_obj === undefined) {
                console.warn('Element not contain role '+ type +'! Please add attribute data-role="'+type+'" to element ' + el);
                return false;
            }

            return true;
        },

        isJQuery: function(el){
            return (typeof jQuery !== "undefined" && el instanceof jQuery);
        },

        isM4Q: function(el){
            return (typeof m4q !== "undefined" && el instanceof m4q);
        },

        isQ: function(el){
            return this.isJQuery(el) || this.isM4Q(el);
        },

        isIE11: function(){
            return !!window.MSInputMethodContext && !!document["documentMode"];
        },

        embedUrl: function(val){
            if (val.indexOf("youtu.be") !== -1) {
                val = "https://www.youtube.com/embed/" + val.split("/").pop();
            }
            return "<div class='embed-container'><iframe src='"+val+"'></iframe></div>";
        },

        elementId: function(prefix){
            return prefix+"-"+(new Date()).getTime()+$.random(1, 1000);
        },

        secondsToTime: function(secs) {
            var hours = Math.floor(secs / (60 * 60));

            var divisor_for_minutes = secs % (60 * 60);
            var minutes = Math.floor(divisor_for_minutes / 60);

            var divisor_for_seconds = divisor_for_minutes % 60;
            var seconds = Math.ceil(divisor_for_seconds);

            return {
                "h": hours,
                "m": minutes,
                "s": seconds
            };
        },

        secondsToFormattedString: function(time){
            var sec_num = parseInt(time, 10);
            var hours   = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {minutes = "0"+minutes;}
            if (seconds < 10) {seconds = "0"+seconds;}

            return [hours, minutes, seconds].join(":");
        },

        func: function(f){
            /* jshint -W054 */
            return new Function("a", f);
        },

        exec: function(f, args, context){
            var result;
            if (f === undefined || f === null) {return false;}
            var func = this.isFunc(f);

            if (func === false) {
                func = this.func(f);
            }

            try {
                result = func.apply(context, args);
            } catch (err) {
                result = null;
                if (window.METRO_THROWS === true) {
                    throw err;
                }
            }
            return result;
        },

        isOutsider: function(element) {
            var el = $(element);
            var inViewport;
            var clone = el.clone();

            clone.removeAttr("data-role").css({
                visibility: "hidden",
                position: "absolute",
                display: "block"
            });
            el.parent().append(clone);

            inViewport = this.inViewport(clone[0]);

            clone.remove();

            return !inViewport;
        },

        inViewport: function(el){
            var rect = this.rect(el);

            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        },

        rect: function(el){
            return el.getBoundingClientRect();
        },

        getCursorPosition: function(el, e){
            var a = this.rect(el);
            return {
                x: this.pageXY(e).x - a.left - window.pageXOffset,
                y: this.pageXY(e).y - a.top - window.pageYOffset
            };
        },

        getCursorPositionX: function(el, e){
            return this.getCursorPosition(el, e).x;
        },

        getCursorPositionY: function(el, e){
            return this.getCursorPosition(el, e).y;
        },

        objectLength: function(obj){
            return Object.keys(obj).length;
        },

        percent: function(total, part, round_value){
            if (total === 0) {
                return 0;
            }
            var result = part * 100 / total;
            return round_value === true ? Math.round(result) : Math.round(result * 100) / 100;
        },

        objectShift: function(obj){
            var min = 0;
            $.each(obj, function(i){
                if (min === 0) {
                    min = i;
                } else {
                    if (min > i) {
                        min = i;
                    }
                }
            });
            delete obj[min];

            return obj;
        },

        objectDelete: function(obj, key){
            if (obj[key] !== undefined) delete obj[key];
        },

        arrayDeleteByMultipleKeys: function(arr, keys){
            keys.forEach(function(ind){
                delete arr[ind];
            });
            return arr.filter(function(item){
                return item !== undefined;
            });
        },

        arrayDelete: function(arr, val){
            if (arr.indexOf(val) > -1) arr.splice(arr.indexOf(val), 1);
        },

        arrayDeleteByKey: function(arr, key){
            arr.splice(key, 1);
        },

        nvl: function(data, other){
            return data === undefined || data === null ? other : data;
        },

        objectClone: function(obj){
            var copy = {};
            for(var key in obj) {
                if ($.hasProp(obj, key)) {
                    copy[key] = obj[key];
                }
            }
            return copy;
        },

        github: function(repo, callback){
            var that = this;
            $.json('https://api.github.com/repos/' + repo).then(function(data){
                that.exec(callback, [data]);
            });
        },

        detectIE: function() {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                // IE 10 or older => return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }

            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                // IE 11 => return version number
                var rv = ua.indexOf('rv:');
                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }

            var edge = ua.indexOf('Edge/');
            if (edge > 0) {
                // Edge (IE 12+) => return version number
                return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            }

            // other browser
            return false;
        },

        detectChrome: function(){
            return /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        },

        encodeURI: function(str){
            return encodeURI(str).replace(/%5B/g, '[').replace(/%5D/g, ']');
        },

        pageHeight: function(){
            var body = document.body,
                html = document.documentElement;

            return Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
        },

        cleanPreCode: function(selector){
            var els = Array.prototype.slice.call(document.querySelectorAll(selector), 0);

            els.forEach(function(el){
                var txt = el.textContent
                    .replace(/^[\r\n]+/, "")	// strip leading newline
                    .replace(/\s+$/g, "");

                if (/^\S/gm.test(txt)) {
                    el.textContent = txt;
                    return;
                }

                var mat, str, re = /^[\t ]+/gm, len, min = 1e3;

                /* jshint -W084 */
                /* eslint-disable-next-line */
                while (mat = re.exec(txt)) {
                    len = mat[0].length;

                    if (len < min) {
                        min = len;
                        str = mat[0];
                    }
                }

                if (min === 1e3)
                    return;

                el.textContent = txt.replace(new RegExp("^" + str, 'gm'), "").trim();
            });
        },

        coords: function(element){
            var el = $(element)[0];
            var box = el.getBoundingClientRect();

            return {
                top: box.top + window.pageYOffset,
                left: box.left + window.pageXOffset
            };
        },

        positionXY: function(e, t){
            switch (t) {
                case 'client': return this.clientXY(e);
                case 'screen': return this.screenXY(e);
                case 'page': return this.pageXY(e);
                default: return {x: 0, y: 0};
            }
        },

        /**
         *
         * @param {TouchEvent|Event|MouseEvent} e
         * @returns {{x: (*), y: (*)}}
         */
        clientXY: function(e){
            return {
                x: e.changedTouches ? e.changedTouches[0].clientX : e.clientX,
                y: e.changedTouches ? e.changedTouches[0].clientY : e.clientY
            };
        },

        /**
         *
         * @param {TouchEvent|Event|MouseEvent} e
         * @returns {{x: (*), y: (*)}}
         */
        screenXY: function(e){
            return {
                x: e.changedTouches ? e.changedTouches[0].screenX : e.screenX,
                y: e.changedTouches ? e.changedTouches[0].screenY : e.screenY
            };
        },

        /**
         *
         * @param {TouchEvent|Event|MouseEvent} e
         * @returns {{x: (*), y: (*)}}
         */
        pageXY: function(e){
            return {
                x: e.changedTouches ? e.changedTouches[0].pageX : e.pageX,
                y: e.changedTouches ? e.changedTouches[0].pageY : e.pageY
            };
        },

        isRightMouse: function(e){
            return "which" in e ? e.which === 3 : "button" in e ? e.button === 2 : undefined;
        },

        hiddenElementSize: function(el, includeMargin){
            var width, height, clone = $(el).clone(true);

            clone.removeAttr("data-role").css({
                visibility: "hidden",
                position: "absolute",
                display: "block"
            });
            $("body").append(clone);

            if (!this.isValue(includeMargin)) {
                includeMargin = false;
            }

            width = clone.outerWidth(includeMargin);
            height = clone.outerHeight(includeMargin);
            clone.remove();
            return {
                width: width,
                height: height
            };
        },

        getStyle: function(element, pseudo){
            var el = $(element)[0];
            return window.getComputedStyle(el, pseudo);
        },

        getStyleOne: function(el, property){
            return this.getStyle(el).getPropertyValue(property);
        },

        getInlineStyles: function(element){
            var i, l, styles = {}, el = $(element)[0];
            for (i = 0, l = el.style.length; i < l; i++) {
                var s = el.style[i];
                styles[s] = el.style[s];
            }

            return styles;
        },

        updateURIParameter: function(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        },

        getURIParameter: function(url, name){
            if (!url) url = window.location.href;
            /* eslint-disable-next-line */
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },

        getLocales: function(){
            return Object.keys(Metro.locales);
        },

        addLocale: function(locale){
            Metro.locales = $.extend( {}, Metro.locales, locale );
        },

        aspectRatioH: function(width, a){
            if (a === "16/9") return width * 9 / 16;
            if (a === "21/9") return width * 9 / 21;
            if (a === "4/3") return width * 3 / 4;
        },

        aspectRatioW: function(height, a){
            if (a === "16/9") return height * 16 / 9;
            if (a === "21/9") return height * 21 / 9;
            if (a === "4/3") return height * 4 / 3;
        },

        valueInObject: function(obj, value){
            return Object.values(obj).indexOf(value) > -1;
        },

        keyInObject: function(obj, key){
            return Object.keys(obj).indexOf(key) > -1;
        },

        inObject: function(obj, key, val){
            return obj[key] !== undefined && obj[key] === val;
        },

        newCssSheet: function(media){
            var style = document.createElement("style");

            if (media !== undefined) {
                style.setAttribute("media", media);
            }

            style.appendChild(document.createTextNode(""));

            document.head.appendChild(style);

            return style.sheet;
        },

        addCssRule: function(sheet, selector, rules, index){
            if("insertRule" in sheet) {
                sheet.insertRule(selector + "{" + rules + "}", index);
            }
            else if("addRule" in sheet) {
                sheet.addRule(selector, rules, index);
            }
        },

        media: function(query){
            return window.matchMedia(query).matches;
        },

        mediaModes: function(){
            return window.METRO_MEDIA;
        },

        mediaExist: function(media){
            return window.METRO_MEDIA.indexOf(media) > -1;
        },

        inMedia: function(media){
            return window.METRO_MEDIA.indexOf(media) > -1 && window.METRO_MEDIA.indexOf(media) === window.METRO_MEDIA.length - 1;
        },

        isValue: function(val){
            return val !== undefined && val !== null && val !== "";
        },

        isNull: function(val){
            return val === undefined || val === null;
        },

        isNegative: function(val){
            return parseFloat(val) < 0;
        },

        isPositive: function(val){
            return parseFloat(val) > 0;
        },

        isZero: function(val){
            return (parseFloat(val.toFixed(2))) === 0.00;
        },

        between: function(val, bottom, top, equals){
            return equals === true ? val >= bottom && val <= top : val > bottom && val < top;
        },

        parseMoney: function(val){
            return Number(parseFloat(val.replace(/[^0-9-.]/g, '')));
        },

        parseCard: function(val){
            return val.replace(/[^0-9]/g, '');
        },

        parsePhone: function(val){
            return this.parseCard(val);
        },

        parseNumber: function(val, thousand, decimal){
            return val.replace(new RegExp('\\'+thousand, "g"), "").replace(new RegExp('\\'+decimal, 'g'), ".");
        },

        nearest: function(val, precision, down){
            val /= precision;
            val = Math[down === true ? 'floor' : 'ceil'](val) * precision;
            return val;
        },

        bool: function(value){
            switch(value){
                case true:
                case "true":
                case 1:
                case "1":
                case "on":
                case "yes":
                    return true;
                default:
                    return false;
            }
        },

        copy: function(element){
            var body = document.body, range, sel;
            var el = $(element)[0];

            if (document.createRange && window.getSelection) {
                range = document.createRange();
                sel = window.getSelection();
                sel.removeAllRanges();
                try {
                    range.selectNodeContents(el);
                    sel.addRange(range);
                } catch (e) {
                    range.selectNode(el);
                    sel.addRange(range);
                }
            } else if (body["createTextRange"]) {
                range = body["createTextRange"]();
                range["moveToElementText"](el);
                range.select();
            }

            document.execCommand("Copy");

            if (window.getSelection) {
                if (window.getSelection().empty) {  // Chrome
                    window.getSelection().empty();
                } else if (window.getSelection().removeAllRanges) {  // Firefox
                    window.getSelection().removeAllRanges();
                }
            } else if (document["selection"]) {  // IE?
                document["selection"].empty();
            }
        },

        decCount: function(v){
            return v % 1 === 0 ? 0 : v.toString().split(".")[1].length;
        },

        /**
         * Add symbols to string on the left side
         * @param str Where
         * @param pad what
         * @param length to length
         */
        lpad: function(str, pad, length){
            var _str = ""+str;
            if (length && _str.length >= length) {
                return _str;
            }
            return Array((length + 1) - _str.length).join(pad) + _str;
        },

        rpad: function(str, pad, length){
            var _str = ""+str;
            if (length && _str.length >= length) {
                return _str;
            }
            return _str + Array((length + 1) - _str.length).join(pad);
        }
    };

    if (window.METRO_GLOBAL_COMMON === true) {
        window.Utils = Metro.utils;
    }
}(Metro, m4q));


/* global Metro */
(function(Metro, $) {
    $.extend(Metro.locales, {
        'en-US': {
            "calendar": {
                "months": [
                    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",
                    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                ],
                "days": [
                    "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",
                    "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa",
                    "Sun", "Mon", "Tus", "Wen", "Thu", "Fri", "Sat"
                ],
                "time": {
                    "days": "DAYS",
                    "hours": "HOURS",
                    "minutes": "MINS",
                    "seconds": "SECS",
                    "month": "MON",
                    "day": "DAY",
                    "year": "YEAR"
                }
            },
            "buttons": {
                "ok": "OK",
                "cancel": "Cancel",
                "done": "Done",
                "today": "Today",
                "now": "Now",
                "clear": "Clear",
                "help": "Help",
                "yes": "Yes",
                "no": "No",
                "random": "Random",
                "save": "Save",
                "reset": "Reset"
            },
            "table": {
                "rowsCount": "Show entries:",
                "search": "Search:",
                "info": "Showing $1 to $2 of $3 entries",
                "prev": "Prev",
                "next": "Next",
                "all": "All",
                "inspector": "Inspector",
                "skip": "Goto page",
                "empty": "Nothing to show"
            },
            "colorSelector": {
                addUserColorButton: "ADD TO SWATCHES",
                userColorsTitle: "USER COLORS"
            }
        }
    });
}(Metro, m4q));


/* global Metro, setImmediate, METRO_LOCALE */
(function(Metro, $) {
    'use strict';

    var Utils = Metro.utils;
    var TableDefaultConfig = {
        showInspectorButton: false,
        inspectorButtonIcon: "<span class='default-icon-equalizer'>",
        tableDeferred: 0,
        templateBeginToken: "<%",
        templateEndToken: "%>",
        paginationDistance: 5,

        locale: METRO_LOCALE,

        horizontalScroll: false,
        horizontalScrollStop: null,
        check: false,
        checkType: "checkbox",
        checkStyle: 1,
        checkColIndex: 0,
        checkName: null,
        checkStoreKey: "TABLE:$1:KEYS",
        rownum: false,
        rownumTitle: "#",

        filters: null,
        filtersOperator: "and",

        head: null,
        body: null,
        static: false,
        source: null,

        searchMinLength: 1,
        searchThreshold: 500,
        searchFields: null,

        showRowsSteps: true,
        showSearch: true,
        showTableInfo: true,
        showPagination: true,
        paginationShortMode: true,
        showActivity: true,
        muteTable: true,
        showSkip: false,

        rows: 10,
        rowsSteps: "10,25,50,100",

        staticView: false,
        viewSaveMode: "client",
        viewSavePath: "TABLE:$1:OPTIONS",

        sortDir: "asc",
        decimalSeparator: ".",
        thousandSeparator: ",",

        tableRowsCountTitle: null,
        tableSearchTitle: null,
        tableInfoTitle: null,
        paginationPrevTitle: null,
        paginationNextTitle: null,
        allRecordsTitle: null,
        inspectorTitle: null,
        tableSkipTitle: null,
        emptyTableTitle: null,

        activityType: "atom",
        activityStyle: "color",
        activityTimeout: 100,

        searchWrapper: null,
        rowsWrapper: null,
        infoWrapper: null,
        paginationWrapper: null,
        skipWrapper: null,

        cellWrapper: true,

        clsComponent: "",
        clsTableContainer: "",
        clsTable: "",

        clsHead: "",
        clsHeadRow: "",
        clsHeadCell: "",

        clsBody: "",
        clsBodyRow: "",
        clsBodyCell: "",
        clsCellWrapper: "",

        clsFooter: "",
        clsFooterRow: "",
        clsFooterCell: "",

        clsTableTop: "",
        clsRowsCount: "",
        clsSearch: "",

        clsTableBottom: "",
        clsTableInfo: "",
        clsTablePagination: "",

        clsPagination: "",
        clsTableSkip: "",
        clsTableSkipInput: "",
        clsTableSkipButton: "",

        clsEvenRow: "",
        clsOddRow: "",
        clsRow: "",

        clsEmptyTableTitle: "",

        onDraw: Metro.noop,
        onDrawRow: Metro.noop,
        onDrawCell: Metro.noop,
        onAppendRow: Metro.noop,
        onAppendCell: Metro.noop,
        onSortStart: Metro.noop,
        onSortStop: Metro.noop,
        onSortItemSwitch: Metro.noop,
        onSearch: Metro.noop,
        onRowsCountChange: Metro.noop,
        onDataLoad: Metro.noop,
        onDataLoadError: Metro.noop,
        onDataLoaded: Metro.noop,
        onDataSaveError: Metro.noop,
        onFilterRowAccepted: Metro.noop,
        onFilterRowDeclined: Metro.noop,
        onCheckClick: Metro.noop,
        onCheckClickAll: Metro.noop,
        onCheckDraw: Metro.noop,
        onViewSave: Metro.noop,
        onViewGet: Metro.noop,
        onViewCreated: Metro.noop,
        onTableCreate: Metro.noop,
        onSkip: Metro.noop
    };

    Metro.tableSetup = function(options){
        TableDefaultConfig = $.extend({}, TableDefaultConfig, options);
    };

    if (typeof window["metroTableSetup"] !== undefined) {
        Metro.tableSetup(window["metroTableSetup"]);
    }

    Metro.Component('table', {
        init: function( options, elem ) {
            this._super(elem, options, TableDefaultConfig, {
                currentPage: 1,
                pagesCount: 1,
                searchString: "",
                data: null,
                activity: null,
                loadActivity: null,
                busy: false,
                filters: [],
                wrapperInfo: null,
                wrapperSearch: null,
                wrapperRows: null,
                wrapperPagination: null,
                wrapperSkip: null,
                filterIndex: null,
                filtersIndexes: [],
                component: null,
                inspector: null,
                view: {},
                viewDefault: {},
                locale: Metro.locales["en-US"],
                input_interval: null,
                searchFields: [],
                id: Utils.elementId('table'),
                sort: {
                    dir: "asc",
                    colIndex: 0
                },
                service: [],
                heads: [],
                items: [],
                foots: [],
                filteredItems: [],
                index: {}
            });

            return this;
        },

        _create: function(){
            var that = this, element = this.element, o = this.options;
            var id = Utils.elementId("table");
            var table_component, table_container, activity;

            if (!Utils.isValue(element.attr("id"))) {
                element.attr("id", id);
            }

            if (Utils.isValue(Metro.locales[o.locale])) {
                this.locale = Metro.locales[o.locale];
            }

            if (Utils.isValue(o.searchFields)) {
                this.searchFields = o.searchFields.toArray();
            }

            if (Utils.isValue(o.head)) {
                var _head = o.head;
                o.head = Utils.isObject(o.head);
                if (!o.head) {
                    console.warn("Head "+_head+" defined but not exists!");
                    o.head = null;
                }
            }

            if (Utils.isValue(o.body)) {
                var _body = o.body;
                o.body = Utils.isObject(o.body);
                if (!o.body) {
                    console.warn("Body "+_body+" defined but not exists!");
                    o.body = null;
                }
            }

            if (o.static === true) {
                o.showPagination = false;
                o.showRowsSteps = false;
                o.showSearch = false;
                o.showTableInfo = false;
                o.showSkip = false;
                o.rows = -1;
            }

            table_component = $("<div>").addClass("table-component");
            table_component.insertBefore(element);

            table_container = $("<div>").addClass("table-container").addClass(o.clsTableContainer).appendTo(table_component);
            element.appendTo(table_container);

            if (o.horizontalScroll === true) {
                table_container.addClass("horizontal-scroll");
            }
            if (!Utils.isNull(o.horizontalScrollStop) && Utils.mediaExist(o.horizontalScrollStop)) {
                table_container.removeClass("horizontal-scroll");
            }

            table_component.addClass(o.clsComponent);

            this.activity =  $("<div>").addClass("table-progress").appendTo(table_component);

            activity = $("<div>").appendTo(this.activity);
            Metro.makePlugin(activity, "activity", {
                type: o.activityType,
                style: o.activityStyle
            });

            if (o.showActivity !== true) {
                this.activity.css({
                    visibility: "hidden"
                })
            }

            this.component = table_component[0];

            if (o.source !== null) {

                this._fireEvent("data-load", {
                    source: o.source
                });

                var objSource = Utils.isObject(o.source);

                if (objSource !== false && $.isPlainObject(objSource)) {
                    that._build(objSource);
                } else {
                    this.activity.show(function () {
                        $.json(o.source).then(function (data) {
                            that.activity.hide();
                            if (typeof data !== "object") {
                                throw new Error("Data for table is not a object");
                            }

                            that._fireEvent("data-loaded", {
                                source: o.source,
                                data: data
                            });

                            that._build(data);
                        }, function (xhr) {
                            that.activity.hide();

                            that._fireEvent("data-load-error", {
                                source: o.source,
                                xhr: xhr
                            });
                        });
                    });
                }
            } else {
                that._build();
            }
        },

        _createIndex: function(){
            var that = this, colIndex = this.options.checkColIndex;
            setImmediate(function(){
                that.items.forEach(function(v, i){
                    that.index[v[colIndex]] = i;
                });
            });
        },

        _build: function(data){
            var that = this, element = this.element, o = this.options;
            var view, id = element.attr("id"), viewPath;

            o.rows = parseInt(o.rows);

            this.items = [];
            this.heads = [];
            this.foots = [];

            if (Array.isArray(o.head)) {
                this.heads = o.head;
            }

            if (Array.isArray(o.body)) {
                this.items = o.body;
            }

            if (Utils.isValue(data)) {
                this._createItemsFromJSON(data);
            } else {
                this._createItemsFromHTML()
            }

            // Create index
            this._createIndex();

            this.view = this._createView();
            this.viewDefault = Utils.objectClone(this.view);

            viewPath = o.viewSavePath.replace("$1", id);

            if (o.viewSaveMode.toLowerCase() === "client") {

                view = Metro.storage.getItem(viewPath);
                if (Utils.isValue(view) && Utils.objectLength(view) === Utils.objectLength(this.view)) {
                    this.view = view;

                    this._fireEvent("view-get", {
                        source: "client",
                        view: view
                    });
                }
                this._final();

            } else {

                $.json(viewPath, (viewPath !== o.viewSavePath ? null : {id: id}))
                .then(function(view){
                    if (Utils.isValue(view) && Utils.objectLength(view) === Utils.objectLength(that.view)) {
                        that.view = view;
                        that._fireEvent("view-get", {
                            source: "server",
                            view: view
                        });
                    }
                    that._final();
                }, function(){
                    that._final();
                    console.warn("Warning! Error loading view for table " + element.attr('id') + " ");
                });

            }
        },

        _final: function(){
            var element = this.element, o = this.options;
            var id = element.attr("id");

            Metro.storage.delItem(o.checkStoreKey.replace("$1", id));

            this._service();
            this._createStructure();
            this._createInspector();
            this._createEvents();

            this._fireEvent("table-create", {
                element: element
            });
        },

        _service: function(){
            var o = this.options;

            this.service = [
                {
                    // Rownum
                    title: o.rownumTitle,
                    format: undefined,
                    name: undefined,
                    sortable: false,
                    sortDir: undefined,
                    clsColumn: "rownum-cell " + (o.rownum !== true ? "d-none" : ""),
                    cls: "rownum-cell " + (o.rownum !== true ? "d-none" : ""),
                    colspan: undefined,
                    type: "rownum"
                },
                {
                    // Check
                    title: o.checkType === "checkbox" ? "<input type='checkbox' data-role='checkbox' class='table-service-check-all' data-style='"+o.checkStyle+"'>" : "",
                    format: undefined,
                    name: undefined,
                    sortable: false,
                    sortDir: undefined,
                    clsColumn: "check-cell " + (o.check !== true ? "d-none" : ""),
                    cls: "check-cell "+(o.check !== true ? "d-none" : ""),
                    colspan: undefined,
                    type: "rowcheck"
                }
            ];
        },

        _createView: function(){
            var view;

            view = {};

            $.each(this.heads, function(i){

                if (Utils.isValue(this.cls)) {this.cls = this.cls.replace("hidden", "");}
                if (Utils.isValue(this.clsColumn)) {this.clsColumn = this.clsColumn.replace("hidden", "");}

                view[i] = {
                    "index": i,
                    "index-view": i,
                    "show": !Utils.isValue(this.show) ? true : this.show,
                    "size": Utils.isValue(this.size) ? this.size : ""
                }
            });

            this._fireEvent("view-created", {
                view: view
            });

            return view;
        },

        _createInspectorItems: function(table){
            var that = this, o = this.options;
            var j, tds = [], row;
            var cells = this.heads;

            table.html("");

            for (j = 0; j < cells.length; j++){
                tds[j] = null;
            }

            $.each(cells, function(i){
                row = $("<tr>");
                row.data('index', i);
                row.data('index-view', i);
                $("<td>").html("<input type='checkbox' data-style='"+o.checkStyle+"' data-role='checkbox' name='column_show_check[]' value='"+i+"' "+(Utils.bool(that.view[i]['show']) ? "checked" : "")+">").appendTo(row);
                $("<td>").html(this.title).appendTo(row);
                $("<td>").html("<input type='number' data-role='spinner' name='column_size' value='"+that.view[i]['size']+"' data-index='"+i+"'>").appendTo(row);
                $("<td>").html("" +
                    "<button class='button square js-table-inspector-field-up' type='button'><span class='mif-arrow-up'></span></button>" +
                    "<button class='button square js-table-inspector-field-down' type='button'><span class='mif-arrow-down'></span></button>" +
                    "").appendTo(row);
                tds[that.view[i]['index-view']] = row;
            });

            //
            for (j = 0; j < cells.length; j++){
                tds[j].appendTo(table);
            }
        },

        _createInspector: function(){
            var o = this.options;
            var inspector, table_wrap, table, tbody, actions;

            inspector = $("<div data-role='draggable' data-drag-element='.table-inspector-header' data-drag-area='body'>").addClass("table-inspector");
            inspector.attr("for", this.element.attr("id"));

            $("<div class='table-inspector-header'>"+(o.inspectorTitle || this.locale.table["inspector"])+"</div>").appendTo(inspector);

            table_wrap = $("<div>").addClass("table-wrap").appendTo(inspector);

            table = $("<table>").addClass("table subcompact");
            tbody = $("<tbody>").appendTo(table);

            table.appendTo(table_wrap);

            this._createInspectorItems(tbody);

            actions = $("<div class='table-inspector-actions'>").appendTo(inspector);
            $("<button class='button primary js-table-inspector-save' type='button'>").html(this.locale.buttons.save).appendTo(actions);
            $("<button class='button secondary js-table-inspector-reset ml-2 mr-2' type='button'>").html(this.locale.buttons.reset).appendTo(actions);
            $("<button class='button link js-table-inspector-cancel place-right' type='button'>").html(this.locale.buttons.cancel).appendTo(actions);

            inspector.data("open", false);
            this.inspector = inspector;

            $("body").append(inspector);

            this._createInspectorEvents();
        },

        _resetInspector: function(){
            var inspector = this.inspector;
            var table = inspector.find("table tbody");
            this._createInspectorItems(table);
            this._createInspectorEvents();
        },

        _createHeadsFromHTML: function(){
            var that = this, element = this.element;
            var head = element.find("thead");

            if (head.length > 0) $.each(head.find("tr > *"), function(){
                var item = $(this);
                var dir, head_item, item_class;

                if (Utils.isValue(item.data('sort-dir'))) {
                    dir = item.data('sort-dir');
                } else {
                    if (item.hasClass("sort-asc")) {
                        dir = "asc";
                    } else if (item.hasClass("sort-desc")) {
                        dir = "desc"
                    } else {
                        dir = undefined;
                    }
                }

                item_class = item[0].className.replace("sortable-column", "");
                item_class = item_class.replace("sort-asc", "");
                item_class = item_class.replace("sort-desc", "");
                item_class = item_class.replace("hidden", "");

                head_item = {
                    type: "data",
                    title: item.html(),
                    name: Utils.isValue(item.data("name")) ? item.data("name") : item.text().replace(" ", "_"),
                    sortable: item.hasClass("sortable-column") || (Utils.isValue(item.data('sortable')) && JSON.parse(item.data('sortable')) === true),
                    sortDir: dir,
                    format: Utils.isValue(item.data("format")) ? item.data("format") : "string",
                    formatMask: Utils.isValue(item.data("format-mask")) ? item.data("format-mask") : null,
                    clsColumn: Utils.isValue(item.data("cls-column")) ? item.data("cls-column") : "",
                    cls: item_class,
                    colspan: item.attr("colspan"),
                    size: Utils.isValue(item.data("size")) ? item.data("size") : "",
                    show: !(item.hasClass("hidden") || (Utils.isValue(item.data('show')) && JSON.parse(item.data('show')) === false)),

                    required: Utils.isValue(item.data("required")) ? JSON.parse(item.data("required")) === true  : false,
                    field: Utils.isValue(item.data("field")) ? item.data("field") : "input",
                    fieldType: Utils.isValue(item.data("field-type")) ? item.data("field-type") : "text",
                    validator: Utils.isValue(item.data("validator")) ? item.data("validator") : null,

                    template: Utils.isValue(item.data("template")) ? item.data("template") : null
                };
                that.heads.push(head_item);
            });
        },

        _createFootsFromHTML: function(){
            var that = this, element = this.element;
            var foot = element.find("tfoot");

            if (foot.length > 0) $.each(foot.find("tr > *"), function(){
                var item = $(this);
                var foot_item;

                foot_item = {
                    title: item.html(),
                    name: Utils.isValue(item.data("name")) ? item.data("name") : false,
                    cls: item[0].className,
                    colspan: item.attr("colspan")
                };

                that.foots.push(foot_item);
            });
        },

        _createItemsFromHTML: function(){
            var that = this, element = this.element;
            var body = element.find("tbody");

            if (body.length > 0) $.each(body.find("tr"), function(){
                var row = $(this);
                var tr = [];
                $.each(row.children("td"), function(){
                    var td = $(this);
                    tr.push(td.html());
                });
                that.items.push(tr);
            });

            this._createHeadsFromHTML();
            this._createFootsFromHTML();
        },

        _createItemsFromJSON: function(source){
            var that = this;

            if (typeof source === "string") {
                source = JSON.parse(source);
            }

            if (source.header !== undefined) {
                that.heads = source.header;
            } else {
                this._createHeadsFromHTML();
            }

            if (source.data !== undefined) {
                $.each(source.data, function(){
                    var row = this;
                    var tr = [];
                    $.each(row, function(){
                        var td = this;
                        tr.push(td);
                    });
                    that.items.push(tr);
                });
            }

            if (source.footer !== undefined) {
                this.foots = source.footer;
            } else {
                this._createFootsFromHTML();
            }
        },

        _createTableHeader: function(){
            var element = this.element, o = this.options;
            var head = element.find("thead");
            var tr, th, tds = [], j, cells;
            var view = o.staticView ? this._createView() : this.view;

            if (head.length === 0) {
                head = $("<thead>");
                element.prepend(head);
            }

            head.clear().addClass(o.clsHead);

            if (this.heads.length === 0) {
                return head;
            }

            tr = $("<tr>").addClass(o.clsHeadRow).appendTo(head);

            $.each(this.service, function(){
                var item = this, classes = [];
                th = $("<th>").appendTo(tr);
                if (Utils.isValue(item.title)) {th.html(item.title);}
                if (Utils.isValue(item.size)) {th.css({width: item.size});}
                if (Utils.isValue(item.cls)) {classes.push(item.cls);}
                classes.push(o.clsHeadCell);
                th.addClass(classes.join(" "));
            });

            cells = this.heads;

            for (j = 0; j < cells.length; j++){
                tds[j] = null;
            }

            $.each(cells, function(cell_index){
                var item = this;
                var classes = [];

                th = $("<th>");
                th.data("index", cell_index);

                if (Utils.isValue(item.title)) {th.html(item.title);}
                if (Utils.isValue(item.format)) {th.attr("data-format", item.format);}
                if (Utils.isValue(item.name)) {th.attr("data-name", item.name);}
                if (Utils.isValue(item.colspan)) {th.attr("colspan", item.colspan);}
                if (Utils.isValue(view[cell_index]['size'])) {th.css({width: view[cell_index]['size']});}
                if (item.sortable === true) {
                    classes.push("sortable-column");

                    if (Utils.isValue(item.sortDir)) {
                        classes.push("sort-" + item.sortDir);
                    }
                }
                if (Utils.isValue(item.cls)) {
                    $.each(item.cls.toArray(), function () {
                        classes.push(this);
                    });
                }
                if (Utils.bool(view[cell_index]['show']) === false) {
                    if (classes.indexOf('hidden') === -1) classes.push("hidden");
                }

                classes.push(o.clsHeadCell);

                if (Utils.bool(view[cell_index]['show'])) {
                    Utils.arrayDelete(classes, "hidden");
                }

                th.addClass(classes.join(" "));

                tds[view[cell_index]['index-view']] = th;
            });

            for (j = 0; j < cells.length; j++){
                tds[j].appendTo(tr);
            }
        },

        _createTableBody: function(){
            var body, head, element = this.element;

            head  = element.find("thead");
            body  = element.find("tbody");

            if (body.length === 0) {
                body = $("<tbody>").addClass(this.options.clsBody);
                if (head.length !== 0) {
                    body.insertAfter(head);
                } else {
                    element.append(body);
                }
            }

            body.clear();
        },

        _createTableFooter: function(){
            var element = this.element, o = this.options;
            var foot = element.find("tfoot");
            var tr, th;

            if (foot.length === 0) {
                foot = $("<tfoot>").appendTo(element);
            }

            foot.clear().addClass(o.clsFooter);

            if (this.foots.length === 0) {
                return;
            }

            tr = $("<tr>").addClass(o.clsHeadRow).appendTo(foot);
            $.each(this.foots, function(){
                var item = this;
                th = $("<th>").appendTo(tr);

                if (item.title !== undefined) {
                    th.html(item.title);
                }

                if (item.name !== undefined) {
                    th.addClass("foot-column-name-" + item.name);
                }

                if (item.cls !== undefined) {
                    th.addClass(item.cls);
                }

                if (Utils.isValue(item.colspan)) {
                    th.attr("colspan", item.colspan);
                }

                th.appendTo(tr);
            });
        },

        _createTopBlock: function (){
            var that = this, element = this.element, o = this.options;
            var top_block = $("<div>").addClass("table-top").addClass(o.clsTableTop).insertBefore(element.parent());
            var search_block, search_input, rows_block, rows_select;

            search_block = Utils.isValue(this.wrapperSearch) ? this.wrapperSearch : $("<div>").addClass("table-search-block").addClass(o.clsSearch).appendTo(top_block);
            search_block.addClass(o.clsSearch);

            search_input = $("<input>").attr("type", "text").appendTo(search_block);
            Metro.makePlugin(search_input, "input", {
                prepend: o.tableSearchTitle || that.locale.table["search"]
            });

            if (o.showSearch !== true) {
                search_block.hide();
            }

            rows_block = Utils.isValue(this.wrapperRows) ? this.wrapperRows : $("<div>").addClass("table-rows-block").appendTo(top_block);
            rows_block.addClass(o.clsRowsCount);

            rows_select = $("<select>").appendTo(rows_block);
            $.each(o.rowsSteps.toArray(), function () {
                var val = parseInt(this);
                var option = $("<option>").attr("value", val).text(val === -1 ? (o.allRecordsTitle || that.locale.table["all"]) : val).appendTo(rows_select);
                if (val === parseInt(o.rows)) {
                    option.attr("selected", "selected");
                }
            });
            Metro.makePlugin(rows_select, "select",{
                filter: false,
                prepend: o.tableRowsCountTitle || that.locale.table["rowsCount"],
                onChange: function (val) {
                    val = parseInt(val);
                    if (val === parseInt(o.rows)) {
                        return;
                    }
                    o.rows = val;
                    that.currentPage = 1;
                    that._draw();

                    that._fireEvent("rows-count-change", {
                        val: val
                    });
                }
            });

            if (o.showInspectorButton) {
                $("<button>").addClass("button inspector-button").attr("type", "button").html(o.inspectorButtonIcon).insertAfter(rows_block);
            }

            if (o.showRowsSteps !== true) {
                rows_block.hide();
            }

            return top_block;
        },

        _createBottomBlock: function (){
            var element = this.element, o = this.options;
            var bottom_block = $("<div>").addClass("table-bottom").addClass(o.clsTableBottom).insertAfter(element.parent());
            var info, pagination, skip;

            info = Utils.isValue(this.wrapperInfo) ? this.wrapperInfo : $("<div>").addClass("table-info").appendTo(bottom_block);
            info.addClass(o.clsTableInfo);
            if (o.showTableInfo !== true) {
                info.hide();
            }

            pagination = Utils.isValue(this.wrapperPagination) ? this.wrapperPagination : $("<div>").addClass("table-pagination").appendTo(bottom_block);
            pagination.addClass(o.clsTablePagination);
            if (o.showPagination !== true) {
                pagination.hide();
            }

            skip = Utils.isValue(this.wrapperSkip) ? this.wrapperSkip : $("<div>").addClass("table-skip").appendTo(bottom_block);
            skip.addClass(o.clsTableSkip);

            $("<input type='text'>").addClass("input table-skip-input").addClass(o.clsTableSkipInput).appendTo(skip);
            $("<button>").addClass("button table-skip-button").addClass(o.clsTableSkipButton).html(o.tableSkipTitle || this.locale.table["skip"]).appendTo(skip);

            if (o.showSkip !== true) {
                skip.hide();
            }

            return bottom_block;
        },

        _createStructure: function(){
            var that = this, element = this.element, o = this.options;
            var columns;
            var w_search = $(o.searchWrapper),
                w_info = $(o.infoWrapper),
                w_rows = $(o.rowsWrapper),
                w_paging = $(o.paginationWrapper),
                w_skip = $(o.skipWrapper);

            if (w_search.length > 0) {this.wrapperSearch = w_search;}
            if (w_info.length > 0) {this.wrapperInfo = w_info;}
            if (w_rows.length > 0) {this.wrapperRows = w_rows;}
            if (w_paging.length > 0) {this.wrapperPagination = w_paging;}
            if (w_skip.length > 0) {this.wrapperSkip = w_skip;}

            element.addClass(o.clsTable);

            this._createTableHeader();
            this._createTableBody();
            this._createTableFooter();

            this._createTopBlock();
            this._createBottomBlock();

            var need_sort = false;
            if (this.heads.length > 0) $.each(this.heads, function(i){
                var item = this;
                if (!need_sort && ["asc", "desc"].indexOf(item.sortDir) > -1) {
                    need_sort = true;
                    that.sort.colIndex = i;
                    that.sort.dir = item.sortDir;
                }
            });

            if (need_sort) {
                columns = element.find("thead th");
                this._resetSortClass(columns);
                $(columns.get(this.sort.colIndex + that.service.length)).addClass("sort-"+this.sort.dir);
                this.sorting();
            }

            var filter_func;

            if (Utils.isValue(o.filters) && typeof o.filters === 'string') {
                $.each(o.filters.toArray(), function(){
                    filter_func = Utils.isFunc(this);
                    if (filter_func !== false) {
                        that.filtersIndexes.push(that.addFilter(filter_func));
                    }
                });
            }

            this.currentPage = 1;

            this._draw();
        },

        _resetSortClass: function(el){
            $(el).removeClass("sort-asc sort-desc");
        },

        _createEvents: function(){
            var that = this, element = this.element, o = this.options;
            var component = element.closest(".table-component");
            var table_container = component.find(".table-container");
            var search = component.find(".table-search-block input");
            var skip_button = o.skipWrapper ? $(o.skipWrapper).find('.table-skip-button') : component.find(".table-skip-button");
            var skip_input = o.skipWrapper ? $(o.skipWrapper).find('.table-skip-input') : component.find(".table-skip-input");
            var customSearch;
            var id = element.attr("id");
            var inspectorButton = component.find(".inspector-button");

            inspectorButton.on(Metro.events.click, function(){
                that.toggleInspector();
            });

            skip_button.on(Metro.events.click, function(){
                var skipTo = parseInt(skip_input.val().trim());

                if (isNaN(skipTo) || skipTo <=0 || skipTo > that.pagesCount) {
                    skip_input.val('');
                    return false;
                }

                skip_input.val('');

                that._fireEvent("skip", {
                    skipTo: skipTo,
                    skipFrom: that.currentPage
                });

                that.page(skipTo);
            });

            $(window).on(Metro.events.resize, function(){
                if (o.horizontalScroll === true) {
                    if (!Utils.isNull(o.horizontalScrollStop) && Utils.mediaExist(o.horizontalScrollStop)) {
                        table_container.removeClass("horizontal-scroll");
                    } else {
                        table_container.addClass("horizontal-scroll");
                    }
                }
            }, {ns: this.id});

            element.on(Metro.events.click, ".sortable-column", function(){

                if (o.muteTable === true) element.addClass("disabled");

                if (that.busy) {
                    return false;
                }
                that.busy = true;

                var col = $(this);

                that.activity.show(function(){
                    setImmediate(function(){
                        that.currentPage = 1;
                        that.sort.colIndex = col.data("index");
                        if (!col.hasClass("sort-asc") && !col.hasClass("sort-desc")) {
                            that.sort.dir = o.sortDir;
                        } else {
                            if (col.hasClass("sort-asc")) {
                                that.sort.dir = "desc";
                            } else {
                                that.sort.dir = "asc";
                            }
                        }
                        that._resetSortClass(element.find(".sortable-column"));
                        col.addClass("sort-"+that.sort.dir);
                        that.sorting();
                        that._draw(function(){
                            that.busy = false;
                            if (o.muteTable === true) element.removeClass("disabled");
                        });
                    });
                });
            });

            element.on(Metro.events.click, ".table-service-check input", function(){
                var check = $(this);
                var status = check.is(":checked");
                var val = ""+check.val();
                var store_key = o.checkStoreKey.replace("$1", id);
                var storage = Metro.storage;
                var data = storage.getItem(store_key);
                var is_radio = check.attr('type') === 'radio';

                if (is_radio) {
                    data = [];
                }

                if (status) {
                    if (!Utils.isValue(data)) {
                        data = [val];
                    } else {
                        if (Array(data).indexOf(val) === -1) {
                            data.push(val);
                        }
                    }
                } else {
                    if (Utils.isValue(data)) {
                        Utils.arrayDelete(data, val);
                    } else {
                        data = [];
                    }
                }

                storage.setItem(store_key, data);

                that._fireEvent("check-click", {
                    check: this,
                    status: status,
                    data: data
                });
            });

            element.on(Metro.events.click, ".table-service-check-all input", function(){
                var status = $(this).is(":checked");
                var store_key = o.checkStoreKey.replace("$1", id);
                var data = [];
                var storage = Metro.storage;

                if (status) {
                    $.each(that.filteredItems, function(){
                        if (data.indexOf(this[o.checkColIndex]) !== -1) return ;
                        data.push(""+this[o.checkColIndex]);
                    });
                } else {
                    data = [];
                }

                storage.setItem(store_key, data);

                that._draw();

                that._fireEvent("check-click-all", {
                    check: this,
                    status: status,
                    data: data
                });
            });

            var _search = function(){
                that.searchString = this.value.trim().toLowerCase();

                clearInterval(that.input_interval); that.input_interval = false;
                if (!that.input_interval) that.input_interval = setTimeout(function(){
                    that.currentPage = 1;
                    that._draw();
                    clearInterval(that.input_interval); that.input_interval = false;
                }, o.searchThreshold);
            };

            search.on(Metro.events.inputchange, _search);

            if (Utils.isValue(this.wrapperSearch)) {
                customSearch = this.wrapperSearch.find("input");
                if (customSearch.length > 0) {
                    customSearch.on(Metro.events.inputchange, _search);
                }
            }

            function pageLinkClick(l){
                var link = $(l);
                var item = link.parent();
                if (that.filteredItems.length === 0) {
                    return ;
                }

                if (item.hasClass("active")) {
                    return ;
                }

                if (item.hasClass("service")) {
                    if (link.data("page") === "prev") {
                        that.currentPage--;
                        if (that.currentPage === 0) {
                            that.currentPage = 1;
                        }
                    } else {
                        that.currentPage++;
                        if (that.currentPage > that.pagesCount) {
                            that.currentPage = that.pagesCount;
                        }
                    }
                } else {
                    that.currentPage = link.data("page");
                }

                that._draw();
            }

            component.on(Metro.events.click, ".pagination .page-link", function(){
                pageLinkClick(this)
            });

            if (Utils.isValue(this.wrapperPagination)) {
                this.wrapperPagination.on(Metro.events.click, ".pagination .page-link", function(){
                    pageLinkClick(this)
                });
            }

            this._createInspectorEvents();

            element.on(Metro.events.click, ".js-table-crud-button", function(){

            });
        },

        _createInspectorEvents: function(){
            var that = this, inspector = this.inspector;
            // Inspector event

            this._removeInspectorEvents();

            inspector.on(Metro.events.click, ".js-table-inspector-field-up", function(){
                var button = $(this), tr = button.closest("tr");
                var tr_prev = tr.prev("tr");
                var index = tr.data("index");
                var index_view;
                if (tr_prev.length === 0) {
                    return ;
                }
                tr.insertBefore(tr_prev);
                tr.addClass("flash");
                setTimeout(function(){
                    tr.removeClass("flash");
                }, 1000);
                index_view = tr.index();

                tr.data("index-view", index_view);
                that.view[index]['index-view'] = index_view;

                $.each(tr.nextAll(), function(){
                    var t = $(this);
                    index_view++;
                    t.data("index-view", index_view);
                    that.view[t.data("index")]['index-view'] = index_view;
                });

                that._createTableHeader();
                that._draw();
            });

            inspector.on(Metro.events.click, ".js-table-inspector-field-down", function(){
                var button = $(this), tr = button.closest("tr");
                var tr_next = tr.next("tr");
                var index = tr.data("index");
                var index_view;
                if (tr_next.length === 0) {
                    return ;
                }
                tr.insertAfter(tr_next);
                tr.addClass("flash");
                setTimeout(function(){
                    tr.removeClass("flash");
                }, 1000);
                index_view = tr.index();

                tr.data("index-view", index_view);
                that.view[index]['index-view'] = index_view;

                $.each(tr.prevAll(), function(){
                    var t = $(this);
                    index_view--;
                    t.data("index-view", index_view);
                    that.view[t.data("index")]['index-view'] = index_view;
                });

                that._createTableHeader();
                that._draw();
            });

            inspector.on(Metro.events.click, "input[type=checkbox]", function(){
                var check = $(this);
                var status = check.is(":checked");
                var index = check.val();
                var op = ['cls', 'clsColumn'];

                if (status) {
                    $.each(op, function(){
                        var a;
                        a = Utils.isValue(that.heads[index][this]) ? (that.heads[index][this]).toArray(" ") : [];
                        Utils.arrayDelete(a, "hidden");
                        that.heads[index][this] = a.join(" ");
                        that.view[index]['show'] = true;
                    });
                } else {
                    $.each(op, function(){
                        var a;

                        a = Utils.isValue(that.heads[index][this]) ? (that.heads[index][this]).toArray(" ") : [];
                        if (a.indexOf("hidden") === -1) {
                            a.push("hidden");
                        }
                        that.heads[index][this] = a.join(" ");
                        that.view[index]['show'] = false;
                    });
                }

                that._createTableHeader();
                that._draw();
            });

            inspector.find("input[type=number]").on(Metro.events.inputchange, function(){
                var input = $(this);
                var index = input.attr("data-index");
                var val = parseInt(input.val());

                that.view[index]['size'] = val === 0 ? "" : val;

                that._createTableHeader();
            });

            inspector.on(Metro.events.click, ".js-table-inspector-save", function(){
                that._saveTableView();
                that.openInspector(false);
            });

            inspector.on(Metro.events.click, ".js-table-inspector-cancel", function(){
                that.openInspector(false);
            });

            inspector.on(Metro.events.click, ".js-table-inspector-reset", function(){
                that.resetView();
            });
        },

        _removeInspectorEvents: function(){
            var inspector = this.inspector;
            inspector.off(Metro.events.click, ".js-table-inspector-field-up");
            inspector.off(Metro.events.click, ".js-table-inspector-field-down");
            inspector.off(Metro.events.click, "input[type=checkbox]");
            inspector.off(Metro.events.click, ".js-table-inspector-save");
            inspector.off(Metro.events.click, ".js-table-inspector-cancel");
            inspector.off(Metro.events.click, ".js-table-inspector-reset");
            inspector.find("input[type=number]").off(Metro.events.inputchange);
        },

        _saveTableView: function(){
            var that = this, element = this.element, o = this.options;
            var view = this.view;
            var id = element.attr("id");
            var viewPath = o.viewSavePath.replace("$1", id);
            var storage = Metro.storage;

            if (o.viewSaveMode.toLowerCase() === "client") {
                storage.setItem(viewPath, view);

                this._fireEvent("view-save", {
                    target: "client",
                    path: o.viewSavePath,
                    view: view
                });
            } else {
                var post_data = {
                    id : element.attr("id"),
                    view : view
                };
                $.post(viewPath, post_data)
                    .then(function(data){

                        that._fireEvent("view-save", {
                            target: "server",
                            path: o.viewSavePath,
                            view: view,
                            post_data: post_data,
                            response: data
                        });

                    }, function(xhr){

                        that._fireEvent("data-save-error", {
                            source: o.viewSavePath,
                            xhr: xhr,
                            post_data: post_data
                        });

                    });
            }
        },

        _info: function(start, stop, length){
            var element = this.element, o = this.options;
            var component = element.closest(".table-component");
            var info = Utils.isValue(this.wrapperInfo) ? this.wrapperInfo : component.find(".table-info");
            var text;

            if (info.length === 0) {
                return ;
            }

            if (stop > length) {
                stop = length;
            }

            if (this.items.length === 0) {
                start = stop = length = 0;
            }

            text = o.tableInfoTitle || this.locale.table["info"];
            text = text.replace("$1", start);
            text = text.replace("$2", stop);
            text = text.replace("$3", length);
            info.html(text);
        },

        _paging: function(length){
            var element = this.element, o = this.options;
            var component = element.closest(".table-component");
            this.pagesCount = Math.ceil(length / o.rows); // Костыль
            Metro.pagination({
                length: length,
                rows: o.rows,
                current: this.currentPage,
                target: Utils.isValue(this.wrapperPagination) ? this.wrapperPagination : component.find(".table-pagination"),
                claPagination: o.clsPagination,
                prevTitle: o.paginationPrevTitle || this.locale.table["prev"],
                nextTitle: o.paginationNextTitle || this.locale.table["next"],
                distance: o.paginationShortMode === true ? o.paginationDistance : 0
            });
        },

        _filter: function(){
            var that = this, o = this.options;
            var items;
            if ((Utils.isValue(this.searchString) && that.searchString.length >= o.searchMinLength) || this.filters.length > 0) {
                items = this.items.filter(function(row){

                    var row_data = "", result, search_result, i, j = 0;

                    if (that.filters.length > 0) {

                        result = o.filtersOperator.toLowerCase() === "and";
                        for (i = 0; i < that.filters.length; i++) {
                            if (Utils.isNull(that.filters[i])) continue;
                            j++;
                            result = o.filtersOperator.toLowerCase() === "and" ?
                                result && Utils.exec(that.filters[i], [row, that.heads]) :
                                result || Utils.exec(that.filters[i], [row, that.heads])
                            ;
                        }

                        if (j === 0) result = true;
                    } else {
                        result = true;
                    }

                    if (that.searchFields.length > 0) {
                        $.each(that.heads, function(i, v){
                            if (that.searchFields.indexOf(v.name) > -1) {
                                row_data += "•"+row[i];
                            }
                        })
                    } else {
                        row_data = row.join("•");
                    }

                    row_data = row_data.replace(/[\n\r]+|[\s]{2,}/g, ' ').trim().toLowerCase();
                    search_result = Utils.isValue(that.searchString) && that.searchString.length >= o.searchMinLength ? ~row_data.indexOf(that.searchString) : true;

                    result = result && search_result;

                    if (result) {
                        that._fireEvent("filter-row-accepted", {
                            row: row
                        });
                    } else {
                        that._fireEvent("filter-row-declined", {
                            row: row
                        });
                    }

                    return result;
                });

            } else {
                items = this.items;
            }

            this._fireEvent("search", {
                search: that.searchString,
                items: items
            });

            this.filteredItems = items;

            return items;
        },

        _draw: function(cb){
            var that = this, element = this.element, o = this.options;
            var body = element.find("tbody");
            var i, j, tr, td, check, cells, tds, is_even_row;
            var start = parseInt(o.rows) === -1 ? 0 : o.rows * (this.currentPage - 1),
                stop = parseInt(o.rows) === -1 ? this.items.length - 1 : start + o.rows - 1;
            var items;
            var stored_keys = Metro.storage.getItem(o.checkStoreKey.replace("$1", element.attr('id')));

            var view = o.staticView ? this.viewDefault : this.view;

            body.html("");

            if (!this.heads.length) {
                console.warn("Heads is not defined for table ID " + element.attr("id"));
                return ;
            }

            items = this._filter();

            if (items.length > 0) {
                for (i = start; i <= stop; i++) {
                    cells = items[i];
                    tds = [];
                    if (!Utils.isValue(cells)) {continue;}
                    tr = $("<tr>").addClass(o.clsBodyRow);
                    tr.data('original', cells);

                    // Rownum

                    is_even_row = i % 2 === 0;

                    td = $("<td>").html(i + 1);
                    if (that.service[0].clsColumn !== undefined) {
                        td.addClass(that.service[0].clsColumn);
                    }
                    td.appendTo(tr);

                    // Checkbox
                    td = $("<td>");
                    if (o.checkType === "checkbox") {
                        check = $("<input type='checkbox' data-style='"+o.checkStyle+"' data-role='checkbox' name='" + (Utils.isValue(o.checkName) ? o.checkName : 'table_row_check') + "[]' value='" + items[i][o.checkColIndex] + "'>");
                    } else {
                        check = $("<input type='radio' data-style='"+o.checkStyle+"' data-role='radio' name='" + (Utils.isValue(o.checkName) ? o.checkName : 'table_row_check') + "' value='" + items[i][o.checkColIndex] + "'>");
                    }

                    if (Utils.isValue(stored_keys) && Array.isArray(stored_keys) && stored_keys.indexOf(""+items[i][o.checkColIndex]) > -1) {
                        check.prop("checked", true);
                    }

                    check.addClass("table-service-check");

                    this._fireEvent("check-draw", {
                        check: check
                    });

                    check.appendTo(td);
                    if (that.service[1].clsColumn !== undefined) {
                        td.addClass(that.service[1].clsColumn);
                    }
                    td.appendTo(tr);

                    for (j = 0; j < cells.length; j++){
                        tds[j] = null;
                    }

                    $.each(cells, function(cell_index){
                        var val = this;
                        var td = $("<td>");

                        if (Utils.isValue(that.heads[cell_index].template)) {
                            val = that.heads[cell_index].template.replace(/%VAL%/g, val);
                        }

                        td.html(val);

                        td.addClass(o.clsBodyCell);
                        if (Utils.isValue(that.heads[cell_index].clsColumn)) {
                            td.addClass(that.heads[cell_index].clsColumn);
                        }

                        if (Utils.bool(view[cell_index].show) === false) {
                            td.addClass("hidden");
                        }

                        if (Utils.bool(view[cell_index].show)) {
                            td.removeClass("hidden");
                        }

                        td.data('original',this);

                        tds[view[cell_index]['index-view']] = td;

                        that._fireEvent("draw-cell", {
                            td: td,
                            val: val,
                            cellIndex: cell_index,
                            head: that.heads[cell_index],
                            items: cells
                        });

                        if (o.cellWrapper === true) {
                            val = $("<div>").addClass("data-wrapper").addClass(o.clsCellWrapper).html(td.html());
                            td.html('').append(val);
                        }
                    });

                    for (j = 0; j < cells.length; j++){
                        tds[j].appendTo(tr);

                        that._fireEvent("append-cell", {
                            td: tds[j],
                            tr: tr,
                            index: j
                        });
                    }

                    that._fireEvent("draw-row", {
                        tr: tr,
                        view: that.view,
                        heads: that.heads,
                        items: cells
                    });

                    tr.addClass(o.clsRow).addClass(is_even_row ? o.clsEvenRow : o.clsOddRow).appendTo(body);

                    that._fireEvent("append-row", {
                        tr: tr
                    });
                }

            } else {
                j = 0;
                $.each(view, function(){
                    if (this.show) j++;
                });
                if (o.check === true) {
                    j++;
                }
                if (o.rownum === true) {
                    j++;
                }
                tr = $("<tr>").addClass(o.clsBodyRow).appendTo(body);
                td = $("<td>").attr("colspan", j).addClass("text-center").html($("<span>").addClass(o.clsEmptyTableTitle).html(o.emptyTableTitle || that.locale.table["empty"]));
                td.appendTo(tr);
            }

            this._info(start + 1, stop + 1, items.length);
            this._paging(items.length);

            if (this.activity) this.activity.hide();

            this._fireEvent("draw");

            if (cb !== undefined) {
                Utils.exec(cb, null, element[0])
            }
        },

        _getItemContent: function(row){
            var o = this.options;
            var result, col = row[this.sort.colIndex];
            var format = this.heads[this.sort.colIndex].format;
            var formatMask = !Utils.isNull(this.heads) && !Utils.isNull(this.heads[this.sort.colIndex]) && Utils.isValue(this.heads[this.sort.colIndex]['formatMask']) ? this.heads[this.sort.colIndex]['formatMask'] : "%Y-%m-%d";
            var thousandSeparator = this.heads && this.heads[this.sort.colIndex] && this.heads[this.sort.colIndex]["thousandSeparator"] ? this.heads[this.sort.colIndex]["thousandSeparator"] : o.thousandSeparator;
            var decimalSeparator  = this.heads && this.heads[this.sort.colIndex] && this.heads[this.sort.colIndex]["decimalSeparator"] ? this.heads[this.sort.colIndex]["decimalSeparator"] : o.decimalSeparator;

            result = (""+col).toLowerCase().replace(/[\n\r]+|[\s]{2,}/g, ' ').trim();

            if (Utils.isValue(result) && Utils.isValue(format)) {

                if (['number', 'int', 'float', 'money'].indexOf(format) !== -1) {
                    result = Utils.parseNumber(result, thousandSeparator, decimalSeparator);
                }

                switch (format) {
                    case "date": result = Utils.isValue(formatMask) ? result.toDate(formatMask) : new Date(result); break;
                    case "number": result = Number(result); break;
                    case "int": result = parseInt(result); break;
                    case "float": result = parseFloat(result); break;
                    case "money": result = Utils.parseMoney(result); break;
                    case "card": result = Utils.parseCard(result); break;
                    case "phone": result = Utils.parsePhone(result); break;
                }
            }

            return result;
        },

        addItem: function(item, redraw){
            if (!Array.isArray(item)) {
                console.warn("Item is not an array and can't be added");
                return this;
            }
            this.items.push(item);
            if (redraw !== false) this.draw();
        },

        addItems: function(items, redraw){
            if (!Array.isArray(items)) {
                console.warn("Items is not an array and can't be added");
                return this;
            }
            items.forEach(function(item){
                if (Array.isArray(item))
                    this.items.push(item, false);
            });
            this.draw();
            if (redraw !== false) this.draw();
        },

        updateItem: function(key, field, value){
            var item = this.items[this.index[key]];
            var fieldIndex = null;
            if (Utils.isNull(item)) {
                console.warn('Item is undefined for update');
                return this;
            }
            if (isNaN(field)) {
                this.heads.forEach(function(v, i){
                    if (v['name'] === field) {
                        fieldIndex = i;
                    }
                });
            }
            if (Utils.isNull(fieldIndex)) {
                console.warn('Item is undefined for update. Field ' + field + ' not found in data structure');
                return this;
            }

            item[fieldIndex] = value;
            this.items[this.index[key]] = item;
            return this;
        },

        getItem: function(key){
            return this.items[this.index[key]];
        },

        deleteItem: function(fieldIndex, value){
            var i, deleteIndexes = [];
            var is_func = Utils.isFunc(value);
            for(i = 0; i < this.items.length; i++) {
                if (is_func) {
                    if (Utils.exec(value, [this.items[i][fieldIndex]])) {
                        deleteIndexes.push(i);
                    }
                } else {
                    if (this.items[i][fieldIndex] === value) {
                        deleteIndexes.push(i);
                    }
                }
            }

            this.items = Utils.arrayDeleteByMultipleKeys(this.items, deleteIndexes);

            return this;
        },

        deleteItemByName: function(fieldName, value){
            var i, fieldIndex, deleteIndexes = [];
            var is_func = Utils.isFunc(value);

            for(i = 0; i < this.heads.length; i++) {
                if (this.heads[i]['name'] === fieldName) {
                    fieldIndex = i;
                    break;
                }
            }

            for(i = 0; i < this.items.length; i++) {
                if (is_func) {
                    if (Utils.exec(value, [this.items[i][fieldIndex]])) {
                        deleteIndexes.push(i);
                    }
                } else {
                    if (this.items[i][fieldIndex] === value) {
                        deleteIndexes.push(i);
                    }
                }
            }

            this.items = Utils.arrayDeleteByMultipleKeys(this.items, deleteIndexes);

            return this;
        },

        draw: function(){
            this._draw();
            return this;
        },

        sorting: function(dir){
            var that = this;

            if (Utils.isValue(dir)) {
                this.sort.dir = dir;
            }

            this._fireEvent("sort-start", {
                items: this.items
            });

            this.items.sort(function(a, b){
                var c1 = that._getItemContent(a);
                var c2 = that._getItemContent(b);
                var result = 0;

                if (c1 < c2) {
                    result = that.sort.dir === "asc" ? -1 : 1;
                }
                if (c1 > c2) {
                    result = that.sort.dir === "asc" ? 1 : -1;
                }

                if (result !== 0) {

                    that._fireEvent("sort-item-switch", {
                        a: a,
                        b: b,
                        result: result
                    });
                }

                return result;
            });

            this._fireEvent("sort-stop", {
                items: this.items
            });

            return this;
        },

        search: function(val){
            this.searchString = val.trim().toLowerCase();
            this.currentPage = 1;
            this._draw();
            return this;
        },

        _rebuild: function(review){
            var that = this, element = this.element;
            var need_sort = false, sortable_columns;

            this._createIndex();

            if (review === true) {
                this.view = this._createView();
            }

            this._createTableHeader();
            this._createTableBody();
            this._createTableFooter();

            if (this.heads.length > 0) $.each(this.heads, function(i){
                var item = this;
                if (!need_sort && ["asc", "desc"].indexOf(item.sortDir) > -1) {
                    need_sort = true;
                    that.sort.colIndex = i;
                    that.sort.dir = item.sortDir;
                }
            });

            if (need_sort) {
                sortable_columns = element.find(".sortable-column");
                this._resetSortClass(sortable_columns);
                $(sortable_columns.get(that.sort.colIndex)).addClass("sort-"+that.sort.dir);
                this.sorting();
            }

            that.currentPage = 1;

            that._draw();
        },

        setHeads: function(data){
            this.heads = data;
            return this;
        },

        setHeadItem: function(name, data){
            var i, index;
            for(i = 0; i < this.heads.length; i++) {
                if (this.heads[i].name === name) {
                    index = i;
                    break;
                }
            }
            this.heads[index] = data;
            return this;
        },

        setItems: function(data){
            this.items = data;
            return this;
        },

        setData: function(/*obj*/ data){
            var o = this.options;

            this.items = [];
            this.heads = [];
            this.foots = [];

            if (Array.isArray(o.head)) {
                this.heads = o.head;
            }

            if (Array.isArray(o.body)) {
                this.items = o.body;
            }

            this._createItemsFromJSON(data);

            this._rebuild(true);

            return this;
        },

        loadData: function(source, review){
            var that = this, element = this.element, o = this.options;

            if (!Utils.isValue(review)) {
                review = true;
            }

            element.html("");

            if (!Utils.isValue(source)) {

                this._rebuild(review);

            } else {
                o.source = source;

                this._fireEvent("data-load", {
                    source: o.source
                });

                that.activity.show(function(){
                    $.json(o.source).then(function(data){
                        that.activity.hide();
                        that.items = [];
                        that.heads = [];
                        that.foots = [];

                        that._fireEvent("data-loaded", {
                            source: o.source,
                            data: data
                        });

                        if (Array.isArray(o.head)) {
                            that.heads = o.head;
                        }

                        if (Array.isArray(o.body)) {
                            that.items = o.body;
                        }

                        that._createItemsFromJSON(data);
                        that._rebuild(review);
                    }, function(xhr){
                        that.activity.hide();
                        that._fireEvent("data-load-error", {
                            source: o.source,
                            xhr: xhr
                        });
                    });
                });

            }
        },

        reload: function(review){
            this.loadData(this.options.source, review);
        },

        clear: function(){
            this.items = [];
            return this.draw();
        },

        next: function(){
            if (this.items.length === 0) return ;
            this.currentPage++;
            if (this.currentPage > this.pagesCount) {
                this.currentPage = this.pagesCount;
                return ;
            }
            this._draw();
            return this;
        },

        prev: function(){
            if (this.items.length === 0) return ;
            this.currentPage--;
            if (this.currentPage === 0) {
                this.currentPage = 1;
                return ;
            }
            this._draw();
            return this;
        },

        first: function(){
            if (this.items.length === 0) return ;
            this.currentPage = 1;
            this._draw();
            return this;
        },

        last: function(){
            if (this.items.length === 0) return ;
            this.currentPage = this.pagesCount;
            this._draw();
            return this;
        },

        page: function(num){
            if (num <= 0) {
                num = 1;
            }

            if (num > this.pagesCount) {
                num = this.pagesCount;
            }

            this.currentPage = num;
            this._draw();
            return this;
        },

        addFilter: function(f, redraw){
            var filterIndex = null, i, func = Utils.isFunc(f);
            if (func === false) {
                return ;
            }

            for(i = 0; i < this.filters.length; i++) {
                if (Utils.isNull(this.filters[i])) {
                    filterIndex = i;
                    this.filters[i] = func;
                    break;
                }
            }

            if (Utils.isNull(filterIndex)) {
                this.filters.push(func);
                filterIndex = this.filters.length - 1;
            }

            if (redraw === true) {
                this.currentPage = 1;
                this.draw();
            }

            return filterIndex
        },

        removeFilter: function(key, redraw){
            this.filters[key] = null;
            if (redraw === true) {
                this.currentPage = 1;
                this.draw();
            }
            return this;
        },

        removeFilters: function(redraw){
            this.filters = [];
            if (redraw === true) {
                this.currentPage = 1;
                this.draw();
            }
            return this;
        },

        getItems: function(){
            return this.items;
        },

        getHeads: function(){
            return this.heads;
        },

        getView: function(){
            return this.view;
        },

        getFilteredItems: function(){
            return this.filteredItems.length > 0 ? this.filteredItems : this.items;
        },

        getSelectedItems: function(){
            var element = this.element, o = this.options;
            var stored_keys = Metro.storage.getItem(o.checkStoreKey.replace("$1", element.attr("id")));
            var selected = [];

            if (!Utils.isValue(stored_keys)) {
                return [];
            }

            $.each(this.items, function(){
                if (stored_keys.indexOf(""+this[o.checkColIndex]) !== -1) {
                    selected.push(this);
                }
            });
            return selected;
        },

        getStoredKeys: function(){
            var element = this.element, o = this.options;
            return Metro.storage.getItem(o.checkStoreKey.replace("$1", element.attr("id")), []);
        },

        clearSelected: function(redraw){
            var element = this.element, o = this.options;
            Metro.storage.setItem(o.checkStoreKey.replace("$1", element.attr("id")), []);
            element.find("table-service-check-all input").prop("checked", false);
            if (redraw === true) this._draw();
        },

        getFilters: function(){
            return this.filters;
        },

        getFiltersIndexes: function(){
            return this.filtersIndexes;
        },

        openInspector: function(mode){
            var ins = this.inspector;
            if (mode) {
                ins.show(0, function(){
                    ins.css({
                        top: ($(window).height()  - ins.outerHeight(true)) / 2 + pageYOffset,
                        left: ($(window).width() - ins.outerWidth(true)) / 2 + pageXOffset
                    }).data("open", true);
                });
            } else {
                ins.hide().data("open", false);
            }
        },

        closeInspector: function(){
            this.openInspector(false);
        },

        toggleInspector: function(){
            this.openInspector(!this.inspector.data("open"));
        },

        resetView: function(){

            this.view = this._createView();

            this._createTableHeader();
            this._createTableFooter();
            this._draw();

            this._resetInspector();
            this._saveTableView();
        },

        rebuildIndex: function(){
            this._createIndex();
        },

        getIndex: function(){
            return this.index;
        },

        export: function(to, mode, filename, options){
            var Export = Metro.export;
            var that = this, o = this.options;
            var table = document.createElement("table");
            var head = $("<thead>").appendTo(table);
            var body = $("<tbody>").appendTo(table);
            var i, j, cells, tds = [], items, tr, td;
            var start, stop;

            if (typeof Export.tableToCSV !== 'function') {
                return ;
            }

            mode = Utils.isValue(mode) ? mode.toLowerCase() : "all-filtered";
            filename = Utils.isValue(filename) ? filename : Utils.elementId("table")+"-export.csv";

            // Create table header
            tr = $("<tr>");
            cells = this.heads;

            for (j = 0; j < cells.length; j++){
                tds[j] = null;
            }

            $.each(cells, function(cell_index){
                var item = this;
                if (Utils.bool(that.view[cell_index]['show']) === false) {
                    return ;
                }
                td = $("<th>");
                if (Utils.isValue(item.title)) {
                    td.html(item.title);
                }
                tds[that.view[cell_index]['index-view']] = td;
            });

            for (j = 0; j < cells.length; j++){
                if (Utils.isValue(tds[j])) tds[j].appendTo(tr);
            }
            tr.appendTo(head);

            // Create table data
            if (mode === "checked") {
                items = this.getSelectedItems();
                start = 0; stop = items.length - 1;
            } else if (mode === "view") {
                items = this._filter();
                start = parseInt(o.rows) === -1 ? 0 : o.rows * (this.currentPage - 1);
                stop = parseInt(o.rows) === -1 ? items.length - 1 : start + o.rows - 1;
            } else if (mode === "all") {
                items = this.items;
                start = 0; stop = items.length - 1;
            } else {
                items = this._filter();
                start = 0; stop = items.length - 1;
            }

            for (i = start; i <= stop; i++) {
                if (Utils.isValue(items[i])) {
                    tr = $("<tr>");

                    cells = items[i];

                    for (j = 0; j < cells.length; j++){
                        tds[j] = null;
                    }

                    $.each(cells, function(cell_index){
                        if (Utils.bool(that.view[cell_index].show) === false) {
                            return ;
                        }
                        td = $("<td>").html(this);
                        tds[that.view[cell_index]['index-view']] = td;
                    });

                    for (j = 0; j < cells.length; j++){
                        if (Utils.isValue(tds[j])) tds[j].appendTo(tr);
                    }

                    tr.appendTo(body);
                }
            }

            // switch (to) {
            //     default: Export.tableToCSV(table, filename, options);
            // }
            Export.tableToCSV(table, filename, options);
            table.remove();
        },

        changeAttribute: function(attributeName){
            var that = this, element = this.element, o = this.options;

            function dataCheck(){
                o.check = Utils.bool(element.attr("data-check"));
                that._service();
                that._createTableHeader();
                that._draw();
            }

            function dataRownum(){
                o.rownum = Utils.bool(element.attr("data-rownum"));
                that._service();
                that._createTableHeader();
                that._draw();
            }

            switch (attributeName) {
                case "data-check": dataCheck(); break;
                case "data-rownum": dataRownum(); break;
            }
        },

        destroy: function(){
            var element = this.element;
            var component = element.closest(".table-component");
            var search_input = component.find("input");
            var rows_select = component.find("select");

            search_input.data("input").destroy();
            rows_select.data("select").destroy();

            $(window).off(Metro.events.resize, {ns: this.id});

            element.off(Metro.events.click, ".sortable-column");

            element.off(Metro.events.click, ".table-service-check input");

            element.off(Metro.events.click, ".table-service-check-all input");

            search_input.off(Metro.events.inputchange);

            if (Utils.isValue(this.wrapperSearch)) {
                var customSearch = this.wrapperSearch.find("input");
                if (customSearch.length > 0) {
                    customSearch.off(Metro.events.inputchange);
                }
            }

            component.off(Metro.events.click, ".pagination .page-link");
            if (Utils.isValue(this.wrapperPagination)) {
                this.wrapperPagination.off(Metro.events.click, ".pagination .page-link");
            }
            element.off(Metro.events.click, ".js-table-crud-button");

            this._removeInspectorEvents();

            return element;
        }
    });
}(Metro, m4q));


/* global Metro*/
(function(Metro, $) {
    'use strict';
    var Utils = Metro.utils;
    var SelectDefaultConfig = {
        label: "",
        size: "normal",
        selectDeferred: 0,
        clearButton: false,
        clearButtonIcon: "<span class='default-icon-cross'></span>",
        usePlaceholder: false,
        placeholder: "",
        addEmptyValue: false,
        emptyValue: "",
        duration: 0,
        prepend: "",
        append: "",
        filterPlaceholder: "Search...",
        filter: true,
        copyInlineStyles: false,
        dropHeight: 200,
        checkDropUp: true,
        dropUp: false,
        showGroupName: false,
        shortTag: true,

        clsSelect: "",
        clsSelectInput: "",
        clsPrepend: "",
        clsAppend: "",
        clsOption: "",
        clsOptionActive: "",
        clsOptionGroup: "",
        clsDropList: "",
        clsDropContainer: "",
        clsSelectedItem: "",
        clsSelectedItemRemover: "",
        clsLabel: "",
        clsGroupName: "",

        onChange: Metro.noop,
        onUp: Metro.noop,
        onDrop: Metro.noop,
        onItemSelect: Metro.noop,
        onItemDeselect: Metro.noop,
        onSelectCreate: Metro.noop
    };

    Metro.selectSetup = function (options) {
        SelectDefaultConfig = $.extend({}, SelectDefaultConfig, options);
    };

    if (typeof window["metroSelectSetup"] !== undefined) {
        Metro.selectSetup(window["metroSelectSetup"]);
    }

    Metro.Component('select', {
        init: function( options, elem ) {
            this._super(elem, options, SelectDefaultConfig, {
                list: null,
                placeholder: null
            });

            return this;
        },

        _create: function(){
            var element = this.element;

            this._createSelect();
            this._createEvents();

            this._fireEvent("select-create", {
                element: element
            });
        },

        _setPlaceholder: function(){
            var element = this.element, o = this.options;
            var input = element.siblings(".select-input");
            if (o.usePlaceholder === true && (!Utils.isValue(element.val()) || element.val() == o.emptyValue)) {
                input.html(this.placeholder);
            }
        },

        _addTag: function(val, data){
            var element = this.element, o = this.options;
            var tag, tagSize, container = element.closest(".select");
            tag = $("<div>").addClass("tag").addClass(o.shortTag ? "short-tag" : "").addClass(o.clsSelectedItem).html("<span class='title'>"+val+"</span>").data("option", data);
            $("<span>").addClass("remover").addClass(o.clsSelectedItemRemover).html("&times;").appendTo(tag);

            if (container.hasClass("input-large")) {
                tagSize = "large";
            } else if (container.hasClass("input-small")) {
                tagSize = "small"
            }

            tag.addClass(tagSize);

            return tag;
        },

        _addOption: function(item, parent, input, multiple, group){
            var option = $(item);
            var l, a;
            var element = this.element, o = this.options;
            var html = Utils.isValue(option.attr('data-template')) ? option.attr('data-template').replace("$1", item.text):item.text;
            var displayValue = option.attr("data-display");

            l = $("<li>").addClass(o.clsOption).data("option", item).attr("data-text", item.text).attr('data-value', item.value ? item.value : "");
            a = $("<a>").html(html);

            if (displayValue) {
                l.attr("data-display", displayValue);
                html = displayValue;
            }

            l.addClass(item.className);

            l.data("group", group);

            if (option.is(":disabled")) {
                l.addClass("disabled");
            }

            if (option.is(":selected")) {

                if (o.showGroupName && group) {
                    html += "&nbsp;<span class='selected-item__group-name "+o.clsGroupName+"'>" + group + "</span>";
                }

                if (multiple) {
                    l.addClass("d-none");
                    input.append(this._addTag(html, l));
                } else {
                    element.val(item.value);
                    input.html(html);
                    element.fire("change", {
                        val: item.value
                    });
                    l.addClass("active");
                }
            }

            l.append(a).appendTo(parent);
        },

        _addOptionGroup: function(item, parent, input, multiple){
            var that = this, o = this.options;
            var group = $(item);

            $("<li>").html(item.label).addClass("group-title").addClass(o.clsOptionGroup).appendTo(parent);

            $.each(group.children(), function(){
                that._addOption(this, parent, input, multiple, item.label);
            })
        },

        _createOptions: function(){
            var that = this, element = this.element, o = this.options, select = element.parent();
            var list = select.find("ul").empty();
            var selected = element.find("option[selected]").length > 0;
            var multiple = element[0].multiple;
            var input = element.siblings(".select-input");

            element.siblings(".select-input").empty();

            if (o.addEmptyValue === true) {
                element.prepend($("<option "+(!selected ? 'selected' : '')+" value='"+o.emptyValue+"' class='d-none'></option>"));
            }

            $.each(element.children(), function(){
                if (this.tagName === "OPTION") {
                    that._addOption(this, list, input, multiple, null);
                } else if (this.tagName === "OPTGROUP") {
                    that._addOptionGroup(this, list, input, multiple);
                }
            });
        },

        _createSelect: function(){
            var that = this, element = this.element, o = this.options;

            var container = $("<label>").addClass("select " + element[0].className).addClass(o.clsSelect);
            var multiple = element[0].multiple;
            var select_id = Utils.elementId("select");
            var buttons = $("<div>").addClass("button-group");
            var input, drop_container, drop_container_input, list, filter_input, dropdown_toggle;
            var checkboxID = Utils.elementId("select-focus-trigger");
            var checkbox = $("<input type='checkbox'>").addClass("select-focus-trigger").attr("id", checkboxID);

            this.placeholder = $("<span>").addClass("placeholder").html(o.placeholder);

            container.attr("id", select_id).attr("for", checkboxID);
            container.addClass("input-" + o.size);

            dropdown_toggle = $("<span>").addClass("dropdown-toggle");
            dropdown_toggle.appendTo(container);

            if (multiple) {
                container.addClass("multiple");
            }

            container.insertBefore(element);
            element.appendTo(container);
            buttons.appendTo(container);
            checkbox.appendTo(container);

            input = $("<div>").addClass("select-input").addClass(o.clsSelectInput).attr("name", "__" + select_id + "__");
            drop_container = $("<div>").addClass("drop-container").addClass(o.clsDropContainer);
            drop_container_input = $("<div>").appendTo(drop_container);
            list = $("<ul>").addClass("option-list").addClass(o.clsDropList).css({
                "max-height": o.dropHeight
            });
            filter_input = $("<input type='text' data-role='input'>").attr("placeholder", o.filterPlaceholder).appendTo(drop_container_input);

            container.append(input);
            container.append(drop_container);

            drop_container.append(drop_container_input);

            if (o.filter !== true) {
                drop_container_input.hide();
            }

            drop_container.append(list);

            this._createOptions();

            this._setPlaceholder();

            Metro.makePlugin(drop_container, "dropdown", {
                dropFilter: ".select",
                duration: o.duration,
                toggleElement: [container],
                checkDropUp: o.checkDropUp,
                dropUp: o.dropUp,
                onDrop: function(){
                    var dropped, target;
                    dropdown_toggle.addClass("active-toggle");
                    dropped = $(".select .drop-container");
                    $.each(dropped, function(){
                        var drop = $(this);
                        if (drop.is(drop_container)) {
                            return ;
                        }
                        var dataDrop = Metro.getPlugin(drop, 'dropdown');
                        if (dataDrop && dataDrop.close) {
                            dataDrop.close();
                        }
                    });

                    filter_input.val("").trigger(Metro.events.keyup);//.focus();

                    target = list.find("li.active").length > 0 ? $(list.find("li.active")[0]) : undefined;
                    if (target !== undefined) {
                        list[0].scrollTop = target.position().top - ( (list.height() - target.height() )/ 2);
                    }

                    that._fireEvent("drop", {
                        list: list[0]
                    });
                },
                onUp: function(){
                    dropdown_toggle.removeClass("active-toggle");

                    that._fireEvent("up", {
                        list: list[0]
                    });
                }
            });

            this.list = list;

            if (o.clearButton === true && !element[0].readOnly) {
                var clearButton = $("<button>").addClass("button input-clear-button").addClass(o.clsClearButton).attr("tabindex", -1).attr("type", "button").html(o.clearButtonIcon);
                clearButton.appendTo(buttons);
            } else {
                buttons.addClass("d-none");
            }

            if (o.prepend !== "" && !multiple) {
                var prepend = $("<div>").html(o.prepend);
                prepend.addClass("prepend").addClass(o.clsPrepend).appendTo(container);
            }

            if (o.append !== "" && !multiple) {
                var append = $("<div>").html(o.append);
                append.addClass("append").addClass(o.clsAppend).appendTo(container);
            }

            if (o.copyInlineStyles === true) {
                for (var i = 0, l = element[0].style.length; i < l; i++) {
                    container.css(element[0].style[i], element.css(element[0].style[i]));
                }
            }

            if (element.attr('dir') === 'rtl' ) {
                container.addClass("rtl").attr("dir", "rtl");
            }

            if (o.label) {
                var label = $("<label>").addClass("label-for-input").addClass(o.clsLabel).html(o.label).insertBefore(container);
                if (element.attr("id")) {
                    label.attr("for", element.attr("id"));
                }
                if (element.attr("dir") === "rtl") {
                    label.addClass("rtl");
                }
            }

            if (element.is(':disabled')) {
                this.disable();
            } else {
                this.enable();
            }

        },

        _createEvents: function(){
            var that = this, element = this.element, o = this.options;
            var container = element.closest(".select");
            var drop_container = container.find(".drop-container");
            var input = element.siblings(".select-input");
            var filter_input = drop_container.find("input");
            var list = drop_container.find("ul");
            var clearButton = container.find(".input-clear-button");
            var checkbox = container.find(".select-focus-trigger");

            checkbox.on("focus", function(){
                container.addClass("focused");
            });

            checkbox.on("blur", function(){
                container.removeClass("focused");
            });

            clearButton.on(Metro.events.click, function(e){
                element.val(o.emptyValue);
                if (element[0].multiple) {
                    list.find("li").removeClass("d-none");
                    input.clear();
                }
                that._setPlaceholder();
                e.preventDefault();
                e.stopPropagation();
            });

            element.on(Metro.events.change, function(){
                that._setPlaceholder();
            });

            container.on(Metro.events.click, function(){
                $(".focused").removeClass("focused");
                container.addClass("focused");
            });

            input.on(Metro.events.click, function(){
                $(".focused").removeClass("focused");
                container.addClass("focused");
            });

            list.on(Metro.events.click, "li", function(e){
                if ($(this).hasClass("group-title")) {
                    e.preventDefault();
                    e.stopPropagation();
                    return ;
                }
                var leaf = $(this);
                var displayValue = leaf.attr("data-display");
                var val = leaf.data('value');
                var group = leaf.data('group');
                var html = displayValue ? displayValue : leaf.children('a').html();
                var selected;
                var option = leaf.data("option");
                var options = element.find("option");

                if (o.showGroupName && group) {
                    html += "&nbsp;<span class='selected-item__group-name "+o.clsGroupName+"'>" + group + "</span>";
                }

                if (element[0].multiple) {
                    leaf.addClass("d-none");
                    input.append(that._addTag(html, leaf));
                } else {
                    list.find("li.active").removeClass("active").removeClass(o.clsOptionActive);
                    leaf.addClass("active").addClass(o.clsOptionActive);
                    input.html(html);
                    Metro.getPlugin(drop_container, "dropdown").close();
                }

                $.each(options, function(){
                    if (this === option) {
                        this.selected = true;
                    }
                });

                that._fireEvent("item-select", {
                    val: val,
                    option: option,
                    leaf: leaf[0]
                });

                selected = that.getSelected();

                that._fireEvent("change", {
                    selected: selected
                });
            });

            input.on("click", ".tag .remover", function(e){
                var item = $(this).closest(".tag");
                var leaf = item.data("option");
                var option = leaf.data('option');
                var selected;

                leaf.removeClass("d-none");
                $.each(element.find("option"), function(){
                    if (this === option) {
                        this.selected = false;
                    }
                });
                item.remove();

                that._fireEvent("item-deselect", {
                    option: option
                });

                selected = that.getSelected();

                that._fireEvent("change", {
                    selected: selected
                });

                e.preventDefault();
                e.stopPropagation();
            });

            filter_input.on(Metro.events.keyup, function(){
                var filter = this.value.toUpperCase();
                var li = list.find("li");
                var i, a;
                for (i = 0; i < li.length; i++) {
                    if ($(li[i]).hasClass("group-title")) continue;
                    a = li[i].getElementsByTagName("a")[0];
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            });

            filter_input.on(Metro.events.click, function(e){
                e.preventDefault();
                e.stopPropagation();
            });

            drop_container.on(Metro.events.click, function(e){
                e.preventDefault();
                e.stopPropagation();
            });
        },

        disable: function(){
            this.element.data("disabled", true);
            this.element.closest(".select").addClass("disabled");
        },

        enable: function(){
            this.element.data("disabled", false);
            this.element.closest(".select").removeClass("disabled");
        },

        toggleState: function(){
            if (this.elem.disabled) {
                this.disable();
            } else {
                this.enable();
            }
        },

        reset: function(to_default){
            var element = this.element;
            var options = element.find("option");
            var select = element.closest('.select');
            var selected;

            $.each(options, function(){
                this.selected = !Utils.isNull(to_default) ? this.defaultSelected : false;
            });

            this.list.find("li").remove();
            select.find(".select-input").html('');

            this._createOptions();

            selected = this.getSelected();

            this._fireEvent("change", {
                selected: selected
            });
        },

        getSelected: function(){
            var element = this.element;
            var result = [];

            element.find("option").each(function(){
                if (this.selected) result.push(this.value);
            });

            return result;
        },

        val: function(val){
            var that = this, element = this.element, o = this.options;
            var input = element.siblings(".select-input");
            var options = element.find("option");
            var list_items = this.list.find("li");
            var result = [];
            var multiple = element.attr("multiple") !== undefined;
            var option;
            var i, html, list_item, option_value, selected, group;

            if (Utils.isNull(val)) {
                $.each(options, function(){
                    if (this.selected) result.push(this.value);
                });
                return multiple ? result : result[0];
            }

            $.each(options, function(){
                this.selected = false;
            });
            list_items.removeClass("active").removeClass(o.clsOptionActive);
            input.html('');

            if (Array.isArray(val) === false) {
                val  = [val];
            }

            $.each(val, function(){
                for (i = 0; i < options.length; i++) {
                    option = options[i];
                    html = Utils.isValue(option.getAttribute('data-template')) ? option.getAttribute('data-template').replace("$1", option.text) : option.text;
                    if (""+option.value === ""+this) {
                        option.selected = true;
                        break;
                    }
                }

                for(i = 0; i < list_items.length; i++) {
                    list_item = $(list_items[i]);
                    group = list_item.data("group");
                    option_value = list_item.attr("data-value");
                    if (""+option_value === ""+this) {

                        if (o.showGroupName && group) {
                            html += "&nbsp;<span class='selected-item__group-name'>" + group + "</span>";
                        }

                        if (multiple) {
                            list_item.addClass("d-none");
                            input.append(that._addTag(html, list_item));

                            // tag = $("<div>").addClass("tag").addClass(o.clsSelectedItem).html("<span class='title'>"+html+"</span>").appendTo(input);
                            // tag.data("option", list_item);
                            // $("<span>").addClass("remover").addClass(o.clsSelectedItemRemover).html("&times;").appendTo(tag);
                        } else {
                            list_item.addClass("active").addClass(o.clsOptionActive);
                            input.html(html);
                        }
                        break;
                    }
                }
            });

            selected = this.getSelected();

            this._fireEvent("change", {
                selected: selected
            });
        },

        options: function(op, selected, delimiter){
            return this.data(op, selected, delimiter);
        },

        data: function(op, selected, delimiter){
            var element = this.element;
            var option_group, _selected;
            var _delimiter = delimiter || ",";

            if (typeof selected === "string") {
                _selected = selected.toArray(_delimiter).map(function(v){
                    return +v;
                });
            } else if (Array.isArray(selected)) {
                _selected = selected.slice().map(function(v){
                    return +v;
                });
            } else {
                _selected = [];
            }

            element.empty();

            if (typeof op === 'string') {
                element.html(op);
            } else if (Utils.isObject(op)) {
                $.each(op, function(key, val){
                    if (Utils.isObject(val)) {
                        option_group = $("<optgroup label=''>").attr("label", key).appendTo(element);
                        $.each(val, function(key2, val2){
                            var op = $("<option>").attr("value", key2).text(val2).appendTo(option_group);
                            if (_selected.indexOf(+key2) > -1) {
                                op.prop("selected", true);
                            }
                        });
                    } else {
                        var op = $("<option>").attr("value", key).text(val).appendTo(element);
                        if (_selected.indexOf(+key) > -1) {
                            op.prop("selected", true);
                        }
                    }
                });
            }

            this._createOptions();
        },

        changeAttribute: function(attributeName){
            if (attributeName === 'disabled') {
                this.toggleState();
            }
        },

        destroy: function(){
            var element = this.element;
            var container = element.closest(".select");
            var drop_container = container.find(".drop-container");
            var input = element.siblings(".select-input");
            var filter_input = drop_container.find("input");
            var list = drop_container.find("ul");
            var clearButton = container.find(".input-clear-button");

            container.off(Metro.events.click);
            container.off(Metro.events.click, ".input-clear-button");
            input.off(Metro.events.click);
            filter_input.off(Metro.events.blur);
            filter_input.off(Metro.events.focus);
            list.off(Metro.events.click, "li");
            filter_input.off(Metro.events.keyup);
            drop_container.off(Metro.events.click);
            clearButton.off(Metro.events.click);

            drop_container.data("dropdown").destroy();

            return element;
        }
    });

    $(document).on(Metro.events.click, function(){
        $(".select").removeClass("focused");
    }, {ns: "blur-select-elements"});
}(Metro, m4q));


/* global Metro */
(function(Metro, $) {
    'use strict';
    var Utils = Metro.utils;
    var RadioDefaultConfig = {
        radioDeferred: 0,
        transition: true,
        style: 1,
        caption: "",
        captionPosition: "right",
        clsRadio: "",
        clsCheck: "",
        clsCaption: "",
        onRadioCreate: Metro.noop
    };

    Metro.radioSetup = function (options) {
        RadioDefaultConfig = $.extend({}, RadioDefaultConfig, options);
    };

    if (typeof window["metroRadioSetup"] !== undefined) {
        Metro.radioSetup(window["metroRadioSetup"]);
    }

    Metro.Component('radio', {
        init: function( options, elem ) {
            this._super(elem, options, RadioDefaultConfig, {
                origin: {
                    className: ""
                }
            });

            return this;
        },

        _create: function(){
            var element = this.element;

            this._createStructure();
            this._createEvents();

            this._fireEvent("radio-create", {
                element: element
            });
        },

        _createStructure: function(){
            var element = this.element, o = this.options;
            var radio = $("<label>").addClass("radio " + element[0].className).addClass(o.style === 2 ? "style2" : "");
            var check = $("<span>").addClass("check");
            var caption = $("<span>").addClass("caption").html(o.caption);

            element.attr("type", "radio");

            radio.insertBefore(element);
            element.appendTo(radio);
            check.appendTo(radio);
            caption.appendTo(radio);

            if (o.transition === true) {
                radio.addClass("transition-on");
            }

            if (o.captionPosition === 'left') {
                radio.addClass("caption-left");
            }

            this.origin.className = element[0].className;
            element[0].className = '';

            radio.addClass(o.clsRadio);
            caption.addClass(o.clsCaption);
            check.addClass(o.clsCheck);

            if (element.is(':disabled')) {
                this.disable();
            } else {
                this.enable();
            }
        },

        _createEvents: function(){
            var element = this.element, check = element.siblings(".check");

            element.on("focus", function(){
                check.addClass("focused");
            });

            element.on("blur", function(){
                check.removeClass("focused");
            });
        },

        disable: function(){
            this.element.data("disabled", true);
            this.element.parent().addClass("disabled");
        },

        enable: function(){
            this.element.data("disabled", false);
            this.element.parent().removeClass("disabled");
        },

        toggleState: function(){
            if (this.elem.disabled) {
                this.disable();
            } else {
                this.enable();
            }
        },

        changeAttribute: function(attributeName){
            var element = this.element, o = this.options;
            var parent = element.parent();

            var changeStyle = function(){
                var new_style = parseInt(element.attr("data-style"));

                if (!Utils.isInt(new_style)) return;

                o.style = new_style;
                parent.removeClass("style1 style2").addClass("style"+new_style);
            };

            switch (attributeName) {
                case 'disabled': this.toggleState(); break;
                case 'data-style': changeStyle(); break;
            }
        },

        destroy: function(){
            return this.element;
        }
    });
}(Metro, m4q));


/* global Metro */
(function(Metro, $) {
    'use strict';
    var Utils = Metro.utils;
    var InputDefaultConfig = {
        inputDeferred: 0,

        label: "",

        autocomplete: null,
        autocompleteUrl: null,
        autocompleteUrlMethod: "GET",
        autocompleteUrlKey: null,
        autocompleteDivider: ",",
        autocompleteListHeight: 200,

        history: false,
        historyPreset: "",
        historyDivider: "|",
        preventSubmit: false,
        defaultValue: "",
        size: "default",
        prepend: "",
        append: "",
        copyInlineStyles: false,
        searchButton: false,
        clearButton: true,
        revealButton: true,
        clearButtonIcon: "<span class='default-icon-cross'></span>",
        revealButtonIcon: "<span class='default-icon-eye'></span>",
        searchButtonIcon: "<span class='default-icon-search'></span>",
        customButtons: [],
        searchButtonClick: 'submit',

        clsComponent: "",
        clsInput: "",
        clsPrepend: "",
        clsAppend: "",
        clsClearButton: "",
        clsRevealButton: "",
        clsCustomButton: "",
        clsSearchButton: "",
        clsLabel: "",

        onAutocompleteSelect: Metro.noop,
        onHistoryChange: Metro.noop,
        onHistoryUp: Metro.noop,
        onHistoryDown: Metro.noop,
        onClearClick: Metro.noop,
        onRevealClick: Metro.noop,
        onSearchButtonClick: Metro.noop,
        onEnterClick: Metro.noop,
        onInputCreate: Metro.noop
    };

    Metro.inputSetup = function (options) {
        InputDefaultConfig = $.extend({}, InputDefaultConfig, options);
    };

    if (typeof window["metroInputSetup"] !== undefined) {
        Metro.inputSetup(window["metroInputSetup"]);
    }

    Metro.Component('input', {
        init: function( options, elem ) {
            this._super(elem, options, InputDefaultConfig, {
                history: [],
                historyIndex: -1,
                autocomplete: []
            });

            return this;
        },

        _create: function(){
            var element = this.element;

            this._createStructure();
            this._createEvents();

            this._fireEvent("input-create", {
                element: element
            });
        },

        _createStructure: function(){
            var that = this, element = this.element, o = this.options;
            var container = $("<div>").addClass("input " + element[0].className);
            var buttons = $("<div>").addClass("button-group");
            var clearButton, revealButton, searchButton;

            if (Utils.isValue(o.historyPreset)) {
                $.each(o.historyPreset.toArray(o.historyDivider), function(){
                    that.history.push(this);
                });
                that.historyIndex = that.history.length - 1;
            }

            if (element.attr("type") === undefined) {
                element.attr("type", "text");
            }

            container.insertBefore(element);
            element.appendTo(container);
            buttons.appendTo(container);

            if (!Utils.isValue(element.val().trim())) {
                element.val(o.defaultValue);
            }

            if (o.clearButton === true && !element[0].readOnly) {
                clearButton = $("<button>").addClass("button input-clear-button").addClass(o.clsClearButton).attr("tabindex", -1).attr("type", "button").html(o.clearButtonIcon);
                clearButton.appendTo(buttons);
            }
            if (element.attr('type') === 'password' && o.revealButton === true) {
                revealButton = $("<button>").addClass("button input-reveal-button").addClass(o.clsRevealButton).attr("tabindex", -1).attr("type", "button").html(o.revealButtonIcon);
                revealButton.appendTo(buttons);
            }
            if (o.searchButton === true) {
                searchButton = $("<button>").addClass("button input-search-button").addClass(o.clsSearchButton).attr("tabindex", -1).attr("type", o.searchButtonClick === 'submit' ? "submit" : "button").html(o.searchButtonIcon);
                searchButton.appendTo(buttons);
            }

            if (Utils.isValue(o.prepend)) {
                var prepend = $("<div>").html(o.prepend);
                prepend.addClass("prepend").addClass(o.clsPrepend).appendTo(container);
            }

            if (Utils.isValue(o.append)) {
                var append = $("<div>").html(o.append);
                append.addClass("append").addClass(o.clsAppend).appendTo(container);
            }

            if (typeof o.customButtons === "string") {
                o.customButtons = Utils.isObject(o.customButtons);
            }

            if (typeof o.customButtons === "object" && Utils.objectLength(o.customButtons) > 0) {
                $.each(o.customButtons, function(){
                    var item = this;
                    var customButton = $("<button>");

                    customButton
                        .addClass("button input-custom-button")
                        .addClass(o.clsCustomButton)
                        .addClass(item.cls)
                        .attr("tabindex", -1)
                        .attr("type", "button")
                        .html(item.html);

                    if (item.attr && typeof item.attr === 'object') {
                        $.each(item.attr, function(k, v){
                            customButton.attr($.dashedName(k), v);
                        });
                    }

                    customButton.data("action", item.onclick);

                    customButton.appendTo(buttons);
                });
            }

            if (Utils.isValue(element.attr('data-exclaim'))) {
                container.attr('data-exclaim', element.attr('data-exclaim'));
            }

            if (element.attr('dir') === 'rtl' ) {
                container.addClass("rtl").attr("dir", "rtl");
            }

            element[0].className = '';
            if (o.copyInlineStyles === true) {
                for (var i = 0, l = element[0].style.length; i < l; i++) {
                    container.css(element[0].style[i], element.css(element[0].style[i]));
                }
            }

            container.addClass(o.clsComponent);
            element.addClass(o.clsInput);

            if (o.size !== "default") {
                container.css({
                    width: o.size
                });
            }

            if (!Utils.isNull(o.autocomplete) || !Utils.isNull(o.autocompleteUrl)) {
                $("<div>").addClass("autocomplete-list").css({
                    maxHeight: o.autocompleteListHeight,
                    display: "none"
                }).appendTo(container);
            }

            if (Utils.isValue(o.autocomplete)) {
                var autocomplete_obj = Utils.isObject(o.autocomplete);

                if (autocomplete_obj !== false) {
                    this.autocomplete = autocomplete_obj;
                } else {
                    this.autocomplete = o.autocomplete.toArray(o.autocompleteDivider);
                }
            }

            if (Utils.isValue(o.autocompleteUrl)) {
                $.ajax({
                    url: o.autocompleteUrl,
                    method: o.autocompleteUrlMethod
                }).then(function(response){
                    var newData = [];

                    try {
                        newData = JSON.parse(response);
                        if (o.autocompleteUrlKey) {
                            newData = newData[o.autocompleteUrlKey];
                        }
                    } catch (e) {
                        newData = response.split("\n");
                    }

                    that.autocomplete = that.autocomplete.concat(newData);
                });
            }

            if (o.label) {
                var label = $("<label>").addClass("label-for-input").addClass(o.clsLabel).html(o.label).insertBefore(container);
                if (element.attr("id")) {
                    label.attr("for", element.attr("id"));
                }
                if (element.attr("dir") === "rtl") {
                    label.addClass("rtl");
                }
            }

            if (element.is(":disabled")) {
                this.disable();
            } else {
                this.enable();
            }
        },

        _createEvents: function(){
            var that = this, element = this.element, o = this.options;
            var container = element.closest(".input");
            var autocompleteList = container.find(".autocomplete-list");

            container.on(Metro.events.click, ".input-clear-button", function(){
                var curr = element.val();
                element.val(Utils.isValue(o.defaultValue) ? o.defaultValue : "").fire('clear').fire('change').fire('keyup').focus();
                if (autocompleteList.length > 0) {
                    autocompleteList.css({
                        display: "none"
                    })
                }

                that._fireEvent("clear-click", {
                    prev: curr,
                    val: element.val()
                });

            });

            container.on(Metro.events.click, ".input-reveal-button", function(){
                if (element.attr('type') === 'password') {
                    element.attr('type', 'text');
                } else {
                    element.attr('type', 'password');
                }

                that._fireEvent("reveal-click", {
                    val: element.val()
                });

            });

            container.on(Metro.events.click, ".input-search-button", function(){
                if (o.searchButtonClick !== 'submit') {

                    that._fireEvent("search-button-click", {
                        val: element.val(),
                        button: this
                    });

                } else {
                    this.form.submit();
                }
            });

            // container.on(Metro.events.stop, ".input-reveal-button", function(){
            //     element.attr('type', 'password').focus();
            // });

            container.on(Metro.events.click, ".input-custom-button", function(){
                var button = $(this);
                var action = button.data("action");
                Utils.exec(action, [element.val(), button], this);
            });

            element.on(Metro.events.keyup, function(e){
                var val = element.val().trim();

                if (o.history && e.keyCode === Metro.keyCode.ENTER && val !== "") {
                    element.val("");
                    that.history.push(val);
                    that.historyIndex = that.history.length - 1;

                    that._fireEvent("history-change", {
                        val: val,
                        history: that.history,
                        historyIndex: that.historyIndex
                    })

                    if (o.preventSubmit === true) {
                        e.preventDefault();
                    }
                }

                if (o.history && e.keyCode === Metro.keyCode.UP_ARROW) {
                    that.historyIndex--;
                    if (that.historyIndex >= 0) {
                        element.val("");
                        element.val(that.history[that.historyIndex]);

                        that._fireEvent("history-down", {
                            val: element.val(),
                            history: that.history,
                            historyIndex: that.historyIndex
                        })
                    } else {
                        that.historyIndex = 0;
                    }
                    e.preventDefault();
                }

                if (o.history && e.keyCode === Metro.keyCode.DOWN_ARROW) {
                    that.historyIndex++;
                    if (that.historyIndex < that.history.length) {
                        element.val("");
                        element.val(that.history[that.historyIndex]);

                        that._fireEvent("history-up", {
                            val: element.val(),
                            history: that.history,
                            historyIndex: that.historyIndex
                        })
                    } else {
                        that.historyIndex = that.history.length - 1;
                    }
                    e.preventDefault();
                }
            });

            element.on(Metro.events.keydown, function(e){
                if (e.keyCode === Metro.keyCode.ENTER) {
                    that._fireEvent("enter-click", {
                        val: element.val()
                    });
                }
            });

            element.on(Metro.events.blur, function(){
                container.removeClass("focused");
            });

            element.on(Metro.events.focus, function(){
                container.addClass("focused");
            });

            element.on(Metro.events.input, function(){
                var val = this.value.toLowerCase();
                that._drawAutocompleteList(val);
            });

            container.on(Metro.events.click, ".autocomplete-list .item", function(){
                var val = $(this).attr("data-autocomplete-value");
                element.val(val);
                autocompleteList.css({
                    display: "none"
                });
                element.trigger("change");
                that._fireEvent("autocomplete-select", {
                    value: val
                });
            });
        },

        _drawAutocompleteList: function(val){
            var that = this, element = this.element;
            var container = element.closest(".input");
            var autocompleteList = container.find(".autocomplete-list");
            var items;

            if (autocompleteList.length === 0) {
                return;
            }

            autocompleteList.html("");

            items = this.autocomplete.filter(function(item){
                return item.toLowerCase().indexOf(val) > -1;
            });

            autocompleteList.css({
                display: items.length > 0 ? "block" : "none"
            });

            $.each(items, function(){
                var v = this;
                var index = v.toLowerCase().indexOf(val), content;
                var item = $("<div>").addClass("item").attr("data-autocomplete-value", v);

                if (index === 0) {
                    content = "<strong>"+v.substr(0, val.length)+"</strong>"+v.substr(val.length);
                } else {
                    content = v.substr(0, index) + "<strong>"+v.substr(index, val.length)+"</strong>"+v.substr(index + val.length);
                }

                item.html(content).appendTo(autocompleteList);

                that._fireEvent("draw-autocomplete-item", {
                    item: item
                })
            });
        },

        getHistory: function(){
            return this.history;
        },

        getHistoryIndex: function(){
            return this.historyIndex;
        },

        setHistoryIndex: function(val){
            this.historyIndex = val >= this.history.length ? this.history.length - 1 : val;
        },

        setHistory: function(history, append) {
            var that = this, o = this.options;
            if (Utils.isNull(history)) return;
            if (!Array.isArray(history) && typeof history === 'string') {
                history = history.toArray(o.historyDivider);
            }
            if (append === true) {
                $.each(history, function () {
                    that.history.push(this);
                })
            } else{
                this.history = history;
            }
            this.historyIndex = this.history.length - 1;
        },

        clear: function(){
            this.element.val('');
        },

        toDefault: function(){
            this.element.val(Utils.isValue(this.options.defaultValue) ? this.options.defaultValue : "");
        },

        disable: function(){
            this.element.data("disabled", true);
            this.element.parent().addClass("disabled");
        },

        enable: function(){
            this.element.data("disabled", false);
            this.element.parent().removeClass("disabled");
        },

        toggleState: function(){
            if (this.elem.disabled) {
                this.disable();
            } else {
                this.enable();
            }
        },

        setAutocompleteList: function(l){
            var autocomplete_list = Utils.isObject(l);
            if (autocomplete_list !== false) {
                this.autocomplete = autocomplete_list;
            } else if (typeof l === "string") {
                this.autocomplete = l.toArray(this.options.autocompleteDivider);
            }
        },

        changeAttribute: function(attributeName){
            switch (attributeName) {
                case 'disabled': this.toggleState(); break;
            }
        },

        destroy: function(){
            var element = this.element;
            var parent = element.parent();
            var clearBtn = parent.find(".input-clear-button");
            var revealBtn = parent.find(".input-reveal-button");
            var customBtn = parent.find(".input-custom-button");

            if (clearBtn.length > 0) {
                clearBtn.off(Metro.events.click);
            }
            if (revealBtn.length > 0) {
                revealBtn.off(Metro.events.start);
                revealBtn.off(Metro.events.stop);
            }
            if (customBtn.length > 0) {
                clearBtn.off(Metro.events.click);
            }

            element.off(Metro.events.blur);
            element.off(Metro.events.focus);

            return element;
        }
    });

    $(document).on(Metro.events.click, function(){
        $('.input .autocomplete-list').hide();
    });
}(Metro, m4q));


/* global Metro */
(function (Metro, $) {
    'use strict';
    var Utils = Metro.utils;
    var Export = {

        init: function () {
            return this;
        },

        options: {
            csvDelimiter: "\t",
            csvNewLine: "\r\n",
            includeHeader: true
        },

        setup: function (options) {
            this.options = $.extend({}, this.options, options);
            return this;
        },

        base64: function (data) {
            return window.btoa(unescape(encodeURIComponent(data)));
        },

        b64toBlob: function (b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = window.atob(b64Data);
            var byteArrays = [];

            var offset;
            for (offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                var i;
                for (i = 0; i < slice.length; i = i + 1) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new window.Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            return new Blob(byteArrays, {
                type: contentType
            });
        },

        tableToCSV: function (table, filename, options) {
            var o;
            var body, head, data = "";
            var i, j, row, cell;

            o = $.extend({}, this.options, options);

            table = $(table)[0];

            if (Utils.bool(o.includeHeader)) {

                head = table.querySelectorAll("thead")[0];

                for (i = 0; i < head.rows.length; i++) {
                    row = head.rows[i];
                    for (j = 0; j < row.cells.length; j++) {
                        cell = row.cells[j];
                        data += (j ? o.csvDelimiter : '') + cell.textContent.trim();
                    }
                    data += o.csvNewLine;
                }
            }

            body = table.querySelectorAll("tbody")[0];

            for (i = 0; i < body.rows.length; i++) {
                row = body.rows[i];
                for (j = 0; j < row.cells.length; j++) {
                    cell = row.cells[j];
                    data += (j ? o.csvDelimiter : '') + cell.textContent.trim();
                }
                data += o.csvNewLine;
            }

            if (Utils.isValue(filename)) {
                return this.createDownload(this.base64("\uFEFF" + data), 'application/csv', filename);
            }

            return data;
        },

        createDownload: function (data, contentType, filename) {
            var blob, anchor, url;

            anchor = document.createElement('a');
            anchor.style.display = "none";
            document.body.appendChild(anchor);

            blob = this.b64toBlob(data, contentType);

            url = window.URL.createObjectURL(blob);
            anchor.href = url;
            anchor.download = filename || Utils.elementId("download");
            anchor.click();
            window.URL.revokeObjectURL(url);
            document.body.removeChild(anchor);
            return true;
        },

        arrayToCsv: function(array, filename, options){
            var o, data = "", i, row;

            o = $.extend({}, this.options, options);

            for (i = 0; i < array.length; i++) {
                row = array[i];

                if (typeof row !== "object") {
                    data += row + o.csvNewLine;
                } else {
                    $.each(row, function(key, val){
                        data += (key ? o.csvDelimiter : '') + val.toString();
                    });
                    data += o.csvNewLine;
                }
            }

            if (Utils.isValue(filename)) {
                return this.createDownload(this.base64("\uFEFF" + data), 'application/csv', filename);
            }

            return data;
        }
    };

    Metro.export = Export.init();

    if (window.METRO_GLOBAL_COMMON === true) {
        window.Export = Metro.export;
    }
}(Metro, m4q));


/* global Metro */
(function(Metro, $) {
    'use strict';
    var Utils = Metro.utils;
    var DropdownDefaultConfig = {
        dropdownDeferred: 0,
        dropFilter: null,
        toggleElement: null,
        noClose: false,
        duration: 50,
        checkDropUp: false,
        dropUp: false,
        onDrop: Metro.noop,
        onUp: Metro.noop,
        onDropdownCreate: Metro.noop
    };

    Metro.dropdownSetup = function (options) {
        DropdownDefaultConfig = $.extend({}, DropdownDefaultConfig, options);
    };

    if (typeof window["metroDropdownSetup"] !== undefined) {
        Metro.dropdownSetup(window["metroDropdownSetup"]);
    }

    Metro.Component('dropdown', {
        init: function( options, elem ) {
            this._super(elem, options, DropdownDefaultConfig, {
                _toggle: null,
                displayOrigin: null,
                isOpen: false
            });

            return this;
        },

        _create: function(){
            var that = this, element = this.element;

            this._createStructure();
            this._createEvents();

            this._fireEvent("dropdown-create", {
                element: element
            });

            if (element.hasClass("open")) {
                element.removeClass("open");
                setTimeout(function(){
                    that.open(true);
                },0);
            }
        },

        _createStructure: function(){
            var element = this.element, o = this.options;
            var toggle;

            if (o.dropUp) {
                element.addClass("drop-up");
            }

            toggle = o.toggleElement !== null ? $(o.toggleElement) : element.siblings('.dropdown-toggle').length > 0 ? element.siblings('.dropdown-toggle') : element.prev();

            this.displayOrigin = Utils.getStyleOne(element, "display");

            if (element.hasClass("v-menu")) {
                element.addClass("for-dropdown");
            }

            element.css("display", "none");

            this._toggle = toggle;
        },

        _createEvents: function(){
            var that = this, element = this.element, o = this.options;
            var toggle = this._toggle, parent = element.parent();

            toggle.on(Metro.events.click, function(e){
                parent.siblings(parent[0].tagName).removeClass("active-container");
                $(".active-container").removeClass("active-container");

                if (element.css('display') !== 'none' && !element.hasClass('keep-open')) {
                    that._close(element);
                } else {
                    $('[data-role*=dropdown]').each(function(i, el){
                        if (!element.parents('[data-role*=dropdown]').is(el) && !$(el).hasClass('keep-open') && $(el).css('display') !== 'none') {
                            if (!Utils.isValue(o.dropFilter)) {
                                that._close(el);
                            } else {
                                if ($(el).closest(o.dropFilter).length > 0) {
                                    that._close(el);
                                }
                            }
                        }
                    });
                    if (element.hasClass('horizontal')) {
                        element.css({
                            'visibility': 'hidden',
                            'display': 'block'
                        });
                        var children_width = 0;
                        $.each(element.children('li'), function(){
                            children_width += $(this).outerWidth(true);
                        });

                        element.css({
                            'visibility': 'visible',
                            'display': 'none'
                        });
                        element.css('width', children_width);
                    }
                    that._open(element);
                    parent.addClass("active-container");
                }
                e.preventDefault();
                e.stopPropagation();
            });

            if (o.noClose === true) {
                element.addClass("keep-open").on(Metro.events.click, function (e) {
                    //e.preventDefault();
                    e.stopPropagation();
                });
            }

            $(element).find('li.disabled a').on(Metro.events.click, function(e){
                e.preventDefault();
            });
        },

        _close: function(el, immediate){
            el = $(el);

            var dropdown  = Metro.getPlugin(el, "dropdown");
            var toggle = dropdown._toggle;
            var options = dropdown.options;
            var func = "slideUp";

            toggle.removeClass('active-toggle').removeClass("active-control");
            dropdown.element.parent().removeClass("active-container");

            if (immediate) {
                func = 'hide'
            }

            el[func](immediate ? 0 : options.duration, function(){
                dropdown._fireEvent("close");
                dropdown._fireEvent("up");

                if (!options.dropUp && options.checkDropUp) {
                    dropdown.element.removeClass("drop-up");
                }
            });

            this.isOpen = false;
        },

        _open: function(el, immediate){
            el = $(el);

            var dropdown  = Metro.getPlugin(el, "dropdown");
            var toggle = dropdown._toggle;
            var options = dropdown.options;
            var func = "slideDown";

            toggle.addClass('active-toggle').addClass("active-control");

            el[func](immediate ? 0 : options.duration, function(){

                if (!options.dropUp && options.checkDropUp) {
                    // dropdown.element.removeClass("drop-up");
                    if (!Utils.inViewport(dropdown.element[0])) {
                        dropdown.element.addClass("drop-up");
                    }
                }

                dropdown._fireEvent("open");
                dropdown._fireEvent("drop");
            });

            // this._fireEvent("drop");

            this.isOpen = true;
        },

        close: function(immediate){
            this._close(this.element, immediate);
        },

        open: function(immediate){
            this._open(this.element, immediate);
        },

        toggle: function(){
            if (this.isOpen)
                this.close();
            else
                this.open();
        },

        /* eslint-disable-next-line */
        changeAttribute: function(){
        },

        destroy: function(){
            this._toggle.off(Metro.events.click);
        }
    });

    $(document).on(Metro.events.click, function(){
        $('[data-role*=dropdown]').each(function(){
            var el = $(this);

            if (el.css('display')!=='none' && !el.hasClass('keep-open') && !el.hasClass('stay-open') && !el.hasClass('ignore-document-click')) {
                Metro.getPlugin(el, 'dropdown').close();
            }
        });
    });
}(Metro, m4q));


/* global Metro, METRO_LOCALE */
(function(Metro, $) {
    'use strict';
    var Utils = Metro.utils;
    var DialogDefaultConfig = {
        dialogDeferred: 0,
        closeButton: false,
        leaveOverlayOnClose: false,
        toTop: false,
        toBottom: false,
        locale: METRO_LOCALE,
        title: "",
        content: "",
        actions: {},
        actionsAlign: "right",
        defaultAction: true,
        overlay: true,
        overlayColor: '#000000',
        overlayAlpha: .5,
        overlayClickClose: false,
        width: '480',
        height: 'auto',
        shadow: true,
        closeAction: true,
        clsDialog: "",
        clsTitle: "",
        clsContent: "",
        clsAction: "",
        clsDefaultAction: "",
        clsOverlay: "",
        autoHide: 0,
        removeOnClose: false,
        show: false,

        _runtime: false,

        onShow: Metro.noop,
        onHide: Metro.noop,
        onOpen: Metro.noop,
        onClose: Metro.noop,
        onDialogCreate: Metro.noop
    };

    Metro.dialogSetup = function (options) {
        DialogDefaultConfig = $.extend({}, DialogDefaultConfig, options);
    };

    if (typeof window["metroDialogSetup"] !== undefined) {
        Metro.dialogSetup(window["metroDialogSetup"]);
    }

    Metro.Component('dialog', {
        _counter: 0,

        init: function( options, elem ) {
            this._super(elem, options, DialogDefaultConfig, {
                interval: null,
                overlay: null,
                id: Utils.elementId("dialog")
            });

            return this;
        },

        _create: function(){
            var o = this.options;
            this.locale = Metro.locales[o.locale] !== undefined ? Metro.locales[o.locale] : Metro.locales["en-US"];
            this._build();
        },

        _build: function(){
            var that = this, element = this.element, o = this.options;
            var body = $("body");
            var overlay;

            element.addClass("dialog");

            if (o.shadow === true) {
                element.addClass("shadow-on");
            }

            if (o.title !== "") {
                this.setTitle(o.title);
            }

            if (o.content !== "") {
                this.setContent(o.content);
            }

            if (o.defaultAction === true || (o.actions !== false && typeof o.actions === 'object' && Utils.objectLength(o.actions) > 0)) {
                var buttons = element.find(".dialog-actions");
                var button;

                if (buttons.length === 0) {
                    buttons = $("<div>").addClass("dialog-actions").addClass("text-"+o.actionsAlign).appendTo(element);
                }

                if (o.defaultAction === true && (Utils.objectLength(o.actions) === 0 && element.find(".dialog-actions > *").length === 0)) {
                    button = $("<button>").addClass("button js-dialog-close").addClass(o.clsDefaultAction).html(this.locale["buttons"]["ok"]);
                    button.appendTo(buttons);
                }

                if (Utils.isObject(o.actions)) $.each(Utils.isObject(o.actions), function(){
                    var item = this;
                    button = $("<button>").addClass("button").addClass(item.cls).html(item.caption);
                    if (item.onclick !== undefined) button.on(Metro.events.click, function(){
                        Utils.exec(item.onclick, [element]);
                    });
                    button.appendTo(buttons);
                });
            }

            if (o.overlay === true) {
                overlay  = this._overlay();
                this.overlay = overlay;
            }

            if (o.closeAction === true) {
                element.on(Metro.events.click, ".js-dialog-close", function(){
                    that.close();
                });
            }

            var closer = element.find("closer");
            if (closer.length === 0) {
                closer = $("<span>").addClass("button square closer js-dialog-close");
                closer.appendTo(element);
            }
            if (o.closeButton !== true) {
                closer.hide();
            }

            element.css({
                width: o.width,
                height: o.height,
                visibility: "hidden",
                top: '100%',
                left: ( $(window).width() - element.outerWidth() ) / 2
            });

            element.addClass(o.clsDialog);
            element.find(".dialog-title").addClass(o.clsTitle);
            element.find(".dialog-content").addClass(o.clsContent);
            element.find(".dialog-actions").addClass(o.clsAction);

            element.appendTo(body);

            if (o.show) {
                this.open();
            }

            $(window).on(Metro.events.resize, function(){
                that.setPosition();
            }, {ns: this.id});

            this._fireEvent("dialog-create", {
                element: element
            });
        },

        _overlay: function(){
            var o = this.options;

            var overlay = $("<div>");
            overlay.addClass("overlay").addClass(o.clsOverlay);

            if (o.overlayColor === 'transparent') {
                overlay.addClass("transparent");
            } else {
                overlay.css({
                    background: Metro.colors.toRGBA(o.overlayColor, o.overlayAlpha)
                });
            }

            return overlay;
        },

        hide: function(callback){
            var element = this.element, o = this.options;
            var timeout = 0;
            if (o.onHide !== Metro.noop) {
                timeout = 500;

                this._fireEvent("hide");
            }
            setTimeout(function(){
                Utils.exec(callback, null, element[0]);
                element.css({
                    visibility: "hidden",
                    top: "100%"
                });
            }, timeout);
        },

        show: function(callback){
            var element = this.element;
            this.setPosition();
            element.css({
                visibility: "visible"
            });

            this._fireEvent("show");

            Utils.exec(callback, null, element[0]);
        },

        setPosition: function(){
            var element = this.element, o = this.options;
            var top, bottom;
            if (o.toTop !== true && o.toBottom !== true) {
                top = ( $(window).height() - element.outerHeight() ) / 2;
                if (top < 0) {
                    top = 0;
                }
                bottom = "auto";
            } else {
                if (o.toTop === true) {
                    top = 0;
                    bottom = "auto";
                }
                if (o.toTop !== true && o.toBottom === true) {
                    bottom = 0;
                    top = "auto";
                }
            }
            element.css({
                top: top,
                bottom: bottom,
                left: ( $(window).width() - element.outerWidth() ) / 2
            });
        },

        setContent: function(c){
            var element = this.element;
            var content = element.find(".dialog-content");
            if (content.length === 0) {
                content = $("<div>").addClass("dialog-content");
                content.appendTo(element);
            }

            if (!Utils.isQ(c) && Utils.isFunc(c)) {
                c = Utils.exec(c);
            }

            if (Utils.isQ(c)) {
                c.appendTo(content);
            } else {
                content.html(c);
            }
        },

        setTitle: function(t){
            var element = this.element;
            var title = element.find(".dialog-title");
            if (title.length === 0) {
                title = $("<div>").addClass("dialog-title");
                title.appendTo(element);
            }
            title.html(t);
        },

        close: function(){
            var that = this, element = this.element, o = this.options;

            if (!Utils.bool(o.leaveOverlayOnClose)) {
                $('body').find('.overlay').remove();
            }

            this.hide(function(){
                element.data("open", false);

                that._fireEvent("close")

                if (o.removeOnClose === true) {
                    element.remove();
                }
            });
        },

        open: function(){
            var that = this, element = this.element, o = this.options;

            if (o.overlay === true && $(".overlay").length === 0) {
                this.overlay.appendTo($("body"));
                if (o.overlayClickClose === true) {
                    this.overlay.on(Metro.events.click, function(){
                        that.close();
                    });
                }
            }

            this.show(function(){

                that._fireEvent("open");

                element.data("open", true);
                if (parseInt(o.autoHide) > 0) {
                    setTimeout(function(){
                        that.close();
                    }, parseInt(o.autoHide));
                }
            });
        },

        toggle: function(){
            var element = this.element;
            if (element.data('open')) {
                this.close();
            } else {
                this.open();
            }
        },

        isOpen: function(){
            return this.element.data('open') === true;
        },

        /* eslint-disable-next-line */
        changeAttribute: function(attributeName){
        },

        destroy: function(){
            var element = this.element;

            element.off(Metro.events.click, ".js-dialog-close");
            element.find(".button").off(Metro.events.click);
            $(window).off(Metro.events.resize,{ns: this.id});

            return element;
        }
    });

    Metro.dialog = {
        isDialog: function(el){
            return Utils.isMetroObject(el, "dialog");
        },

        open: function(el, content, title){
            if (!this.isDialog(el)) {
                return false;
            }
            var dialog = Metro.getPlugin(el, "dialog");
            if (title !== undefined) {
                dialog.setTitle(title);
            }
            if (content !== undefined) {
                dialog.setContent(content);
            }
            dialog.open();
        },

        close: function(el){
            if (!this.isDialog(el)) {
                return false;
            }
            Metro.getPlugin($(el)[0], "dialog").close();
        },

        toggle: function(el){
            if (!this.isDialog(el)) {
                return false;
            }
            Metro.getPlugin($(el)[0], "dialog").toggle();
        },

        isOpen: function(el){
            if (!this.isDialog(el)) {
                return false;
            }
            Metro.getPlugin($(el)[0], "dialog").isOpen();
        },

        remove: function(el){
            if (!this.isDialog(el)) {
                return false;
            }
            var dialog = Metro.getPlugin($(el)[0], "dialog");
            dialog.options.removeOnClose = true;
            dialog.close();
        },

        create: function(options){
            var dlg;

            dlg = $("<div>").appendTo($("body"));

            var dlg_options = $.extend({}, {
                show: true,
                closeAction: true,
                removeOnClose: true
            }, (options !== undefined ? options : {}));

            dlg_options._runtime = true;

            return Metro.makePlugin(dlg, "dialog", dlg_options);
        }
    };
}(Metro, m4q));


/* global Metro */
(function(Metro, $) {
    'use strict';
    var Types = {
        HEX: "hex",
        HEXA: "hexa",
        RGB: "rgb",
        RGBA: "rgba",
        HSV: "hsv",
        HSL: "hsl",
        HSLA: "hsla",
        CMYK: "cmyk",
        UNKNOWN: "unknown"
    };

    Metro.colorsSetup = function (options) {
        ColorsDefaultConfig = $.extend({}, ColorsDefaultConfig, options);
    };

    if (typeof window["metroColorsSetup"] !== undefined) {
        Metro.colorsSetup(window["metroColorsSetup"]);
    }

    var ColorsDefaultConfig = {
        angle: 30,
        resultType: 'hex',
        results: 6,
        baseLight: "#ffffff",
        baseDark: "self"
    };

    // function HEX(r, g, b) {
    //     this.r = r || "00";
    //     this.g = g || "00";
    //     this.b = b || "00";
    // }
    //
    // HEX.prototype.toString = function(){
    //     return "#" + [this.r, this.g, this.b].join("");
    // }

    // function dec2hex(d){
    //     return Math.round(parseFloat(d) * 255).toString(16);
    // }
    //
    // function hex2dec(h){
    //     return (parseInt(h, 16) / 255);
    // }

    function shift(h, angle){
        h += angle;
        while (h >= 360.0) h -= 360.0;
        while (h < 0.0) h += 360.0;
        return h;
    }

    function clamp(val){
        return Math.min(1, Math.max(0, val));
    }

    function RGB(r, g, b){
        this.r = r || 0;
        this.g = g || 0;
        this.b = b || 0;
    }

    RGB.prototype.toString = function(){
        return "rgb(" + [this.r, this.g, this.b].join(", ") + ")";
    }

    function RGBA(r, g, b, a){
        this.r = r || 0;
        this.g = g || 0;
        this.b = b || 0;
        this.a = a === 0 ? 0 : a || 1;
    }

    RGBA.prototype.toString = function(){
        return "rgba(" + [this.r, this.g, this.b, parseFloat(this.a).toFixed(2)].join(", ") + ")";
    }

    function HSV(h, s, v){
        this.h = h || 0;
        this.s = s || 0;
        this.v = v || 0;
    }

    HSV.prototype.toString2 = function(){
        return "hsv(" + [this.h, this.s, this.v].join(", ") + ")";
    }

    HSV.prototype.toString = function(){
        return "hsv(" + [Math.round(this.h), Math.round(this.s*100)+"%", Math.round(this.v*100)+"%"].join(", ") + ")";
    }

    function HSL(h, s, l){
        this.h = h || 0;
        this.s = s || 0;
        this.l = l || 0;
    }

    HSL.prototype.toString2 = function(){
        return "hsl(" + [this.h, this.s, this.l].join(", ") + ")";
    }

    HSL.prototype.toString = function(){
        return "hsl(" + [Math.round(this.h), Math.round(this.s*100)+"%", Math.round(this.l*100)+"%"].join(", ") + ")";
    }

    function HSLA(h, s, l, a){
        this.h = h || 0;
        this.s = s || 0;
        this.l = l || 0;
        this.a = a === 0 ? 0 : a || 1;
    }

    HSLA.prototype.toString2 = function(){
        return "hsla(" + [this.h, this.s, this.l, this.a].join(", ") + ")";
    }

    HSLA.prototype.toString = function(){
        return "hsla(" + [Math.round(this.h), Math.round(this.s*100)+"%", Math.round(this.l*100)+"%", parseFloat(this.a).toFixed(2)].join(", ") + ")";
    }

    function CMYK(c, m, y, k){
        this.c = c || 0;
        this.m = m || 0;
        this.y = y || 0;
        this.k = k || 0;
    }

    CMYK.prototype.toString = function(){
        return "cmyk(" + [this.c, this.m, this.y, this.k].join(", ") + ")";
    }

    var Colors = {

        PALETTES: {
            ALL: "all",
            METRO: "metro",
            STANDARD: "standard"
        },

        metro: {
            lime: '#a4c400',
            green: '#60a917',
            emerald: '#008a00',
            blue: '#00AFF0',
            teal: '#00aba9',
            cyan: '#1ba1e2',
            cobalt: '#0050ef',
            indigo: '#6a00ff',
            violet: '#aa00ff',
            pink: '#dc4fad',
            magenta: '#d80073',
            crimson: '#a20025',
            red: '#CE352C',
            orange: '#fa6800',
            amber: '#f0a30a',
            yellow: '#fff000',
            brown: '#825a2c',
            olive: '#6d8764',
            steel: '#647687',
            mauve: '#76608a',
            taupe: '#87794e'
        },

        standard: {
            aliceblue: "#f0f8ff",
            antiquewhite: "#faebd7",
            aqua: "#00ffff",
            aquamarine: "#7fffd4",
            azure: "#f0ffff",
            beige: "#f5f5dc",
            bisque: "#ffe4c4",
            black: "#000000",
            blanchedalmond: "#ffebcd",
            blue: "#0000ff",
            blueviolet: "#8a2be2",
            brown: "#a52a2a",
            burlywood: "#deb887",
            cadetblue: "#5f9ea0",
            chartreuse: "#7fff00",
            chocolate: "#d2691e",
            coral: "#ff7f50",
            cornflowerblue: "#6495ed",
            cornsilk: "#fff8dc",
            crimson: "#dc143c",
            cyan: "#00ffff",
            darkblue: "#00008b",
            darkcyan: "#008b8b",
            darkgoldenrod: "#b8860b",
            darkgray: "#a9a9a9",
            darkgreen: "#006400",
            darkkhaki: "#bdb76b",
            darkmagenta: "#8b008b",
            darkolivegreen: "#556b2f",
            darkorange: "#ff8c00",
            darkorchid: "#9932cc",
            darkred: "#8b0000",
            darksalmon: "#e9967a",
            darkseagreen: "#8fbc8f",
            darkslateblue: "#483d8b",
            darkslategray: "#2f4f4f",
            darkturquoise: "#00ced1",
            darkviolet: "#9400d3",
            deeppink: "#ff1493",
            deepskyblue: "#00bfff",
            dimgray: "#696969",
            dodgerblue: "#1e90ff",
            firebrick: "#b22222",
            floralwhite: "#fffaf0",
            forestgreen: "#228b22",
            fuchsia: "#ff00ff",
            gainsboro: "#DCDCDC",
            ghostwhite: "#F8F8FF",
            gold: "#ffd700",
            goldenrod: "#daa520",
            gray: "#808080",
            green: "#008000",
            greenyellow: "#adff2f",
            honeydew: "#f0fff0",
            hotpink: "#ff69b4",
            indianred: "#cd5c5c",
            indigo: "#4b0082",
            ivory: "#fffff0",
            khaki: "#f0e68c",
            lavender: "#e6e6fa",
            lavenderblush: "#fff0f5",
            lawngreen: "#7cfc00",
            lemonchiffon: "#fffacd",
            lightblue: "#add8e6",
            lightcoral: "#f08080",
            lightcyan: "#e0ffff",
            lightgoldenrodyellow: "#fafad2",
            lightgray: "#d3d3d3",
            lightgreen: "#90ee90",
            lightpink: "#ffb6c1",
            lightsalmon: "#ffa07a",
            lightseagreen: "#20b2aa",
            lightskyblue: "#87cefa",
            lightslategray: "#778899",
            lightsteelblue: "#b0c4de",
            lightyellow: "#ffffe0",
            lime: "#00ff00",
            limegreen: "#32dc32",
            linen: "#faf0e6",
            magenta: "#ff00ff",
            maroon: "#800000",
            mediumaquamarine: "#66cdaa",
            mediumblue: "#0000cd",
            mediumorchid: "#ba55d3",
            mediumpurple: "#9370db",
            mediumseagreen: "#3cb371",
            mediumslateblue: "#7b68ee",
            mediumspringgreen: "#00fa9a",
            mediumturquoise: "#48d1cc",
            mediumvioletred: "#c71585",
            midnightblue: "#191970",
            mintcream: "#f5fffa",
            mistyrose: "#ffe4e1",
            moccasin: "#ffe4b5",
            navajowhite: "#ffdead",
            navy: "#000080",
            oldlace: "#fdd5e6",
            olive: "#808000",
            olivedrab: "#6b8e23",
            orange: "#ffa500",
            orangered: "#ff4500",
            orchid: "#da70d6",
            palegoldenrod: "#eee8aa",
            palegreen: "#98fb98",
            paleturquoise: "#afeeee",
            palevioletred: "#db7093",
            papayawhip: "#ffefd5",
            peachpuff: "#ffdab9",
            peru: "#cd853f",
            pink: "#ffc0cb",
            plum: "#dda0dd",
            powderblue: "#b0e0e6",
            purple: "#800080",
            rebeccapurple: "#663399",
            red: "#ff0000",
            rosybrown: "#bc8f8f",
            royalblue: "#4169e1",
            saddlebrown: "#8b4513",
            salmon: "#fa8072",
            sandybrown: "#f4a460",
            seagreen: "#2e8b57",
            seashell: "#fff5ee",
            sienna: "#a0522d",
            silver: "#c0c0c0",
            slyblue: "#87ceeb",
            slateblue: "#6a5acd",
            slategray: "#708090",
            snow: "#fffafa",
            springgreen: "#00ff7f",
            steelblue: "#4682b4",
            tan: "#d2b48c",
            teal: "#008080",
            thistle: "#d8bfd8",
            tomato: "#ff6347",
            turquoise: "#40e0d0",
            violet: "#ee82ee",
            wheat: "#f5deb3",
            white: "#ffffff",
            whitesmoke: "#f5f5f5",
            yellow: "#ffff00",
            yellowgreen: "#9acd32"
        },

        all: {},

        init: function(){
            this.all = $.extend( {}, this.standard, this.metro );
            return this;
        },

        color: function(name, palette){
            palette = palette || this.PALETTES.ALL;
            return this[palette][name] !== undefined ? this[palette][name] : false;
        },

        palette: function(palette){
            palette = palette || this.PALETTES.ALL;
            return Object.keys(this[palette]);
        },

        expandHexColor: function(hex){
            if (typeof hex !== "string") {
                throw new Error("Value is not a string!");
            }
            if (hex[0] === "#" && hex.length === 4) {
                var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                return (
                    "#" +
                    hex.replace(shorthandRegex, function(m, r, g, b) {
                        return r + r + g + g + b + b;
                    })
                );
            }
            return hex[0] === "#" ? hex : "#" + hex;
        },

        colors: function(palette){
            palette = palette || this.PALETTES.ALL;
            return Object.values(this[palette]);
        },

        random: function(colorType, alpha){
            colorType = colorType || Types.HEX;
            alpha = typeof alpha !== "undefined" ? alpha : 1;

            var hex, r, g, b;

            r = $.random(0, 255);
            g = $.random(0, 255);
            b = $.random(0, 255);

            hex = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);

            return colorType === "hex" ? hex : this.toColor(hex, colorType, alpha);
        },

        parse: function(color){
            var _color = color.toLowerCase().trim();

            var a = _color
                .replace(/[^%\d.,]/g, "")
                .split(",")
                .map(function(v) {
                    if (v.indexOf('%') > -1) {
                        v = ""+parseInt(v)/100;
                    }
                    return v.indexOf(".") > -1 ? parseFloat(v) : parseInt(v);
                });

            if (this.metro[_color]) {
                return this.expandHexColor(this.metro[_color]);
            }

            if (this.standard[_color]) {
                return this.expandHexColor(this.standard[_color]);
            }

            if (_color[0] === "#") {
                return this.expandHexColor(_color);
            }

            if (_color.indexOf("rgba") === 0 && a.length === 4) {
                return new RGBA(a[0], a[1], a[2], a[3]);
            }
            if (_color.indexOf("rgb") === 0 && a.length === 3) {
                return new RGB(a[0], a[1], a[2]);
            }
            if (_color.indexOf("cmyk") === 0 && a.length === 4) {
                return new CMYK(a[0], a[1], a[2], a[3]);
            }
            if (_color.indexOf("hsv") === 0 && a.length === 3) {
                return new HSV(a[0], a[1], a[2]);
            }
            if (_color.indexOf("hsla") === 0 && a.length === 4) {
                return new HSLA(a[0], a[1], a[2], a[3]);
            }
            if (_color.indexOf("hsl")  === 0 && a.length === 3) {
                return new HSL(a[0], a[1], a[2]);
            }
            return undefined;
        },

        createColor: function(colorType, from){
            colorType = colorType || "hex";
            from = from || "#000000";

            var baseColor;

            if (typeof from === "string") {
                baseColor = this.parse(from);
            }

            if (!this.isColor(baseColor)) {
                baseColor = "#000000";
            }

            return this.toColor(baseColor, colorType.toLowerCase());
        },

        isDark: function(color){
            if (!this.isColor(color)) return;
            var rgb = this.toRGB(color);
            var YIQ = (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;
            return YIQ < 128;
        },

        isLight: function(color){
            return !this.isDark(color);
        },

        isHSV: function(color){
            return color instanceof HSV;
        },

        isHSL: function(color){
            return color instanceof HSL;
        },

        isHSLA: function(color){
            return color instanceof HSLA;
        },

        isRGB: function(color){
            return color instanceof RGB;
        },

        isRGBA: function(color){
            return color instanceof RGBA;
        },

        isCMYK: function(color){
            return color instanceof CMYK;
        },

        isHEX: function(color){
            return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(color);
        },

        isColor: function(val){
            var color = typeof val === "string" ? this.parse(val) : val;

            return !color
                ? false
                : this.isHEX(color) ||
                  this.isRGB(color) ||
                  this.isRGBA(color) ||
                  this.isHSV(color) ||
                  this.isHSL(color) ||
                  this.isHSLA(color) ||
                  this.isCMYK(color);
        },

        check: function(color, type){
            var that = this, checkFor = typeof type === "string" ? [type] : type;
            var result = false;

            $.each(checkFor, function(){
                if (that["is"+this.toUpperCase()](color)) {
                    result = true;
                }
            });

            if (!result) {
                throw new Error("Value is not a " + type + " color type!");
            }
        },

        colorType: function(color){
            if (this.isHEX(color)) return Types.HEX;
            if (this.isRGB(color)) return Types.RGB;
            if (this.isRGBA(color)) return Types.RGBA;
            if (this.isHSV(color)) return Types.HSV;
            if (this.isHSL(color)) return Types.HSL;
            if (this.isHSLA(color)) return Types.HSLA;
            if (this.isCMYK(color)) return Types.CMYK;

            return Types.UNKNOWN;
        },

        equal: function(color1, color2){
            if (!this.isColor(color1) || !this.isColor(color2)) {
                return false;
            }

            return this.toHEX(color1) === this.toHEX(color2);
        },

        colorToString: function(color){
            return color.toString();
        },

        hex2rgb: function(color){
            if (typeof color !== "string") {
                throw new Error("Value is not a string!")
            }
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(
                this.expandHexColor(color)
            );
            var rgb = [
                parseInt(result[1], 16),
                parseInt(result[2], 16),
                parseInt(result[3], 16)
            ];
            return result ? new RGB(rgb[0], rgb[1], rgb[2]) : null;
        },

        rgb2hex: function(color){
            this.check(color, "rgb");
            return (
                "#" +
                ((1 << 24) + (color.r << 16) + (color.g << 8) + color.b).toString(16).slice(1)
            );
        },

        rgb2hsv: function(color){
            this.check(color, "rgb");
            var hsv = new HSV();
            var h, s, v;
            var r = color.r / 255,
                g = color.g / 255,
                b = color.b / 255;

            var max = Math.max(r, g, b);
            var min = Math.min(r, g, b);
            var delta = max - min;

            v = max;

            if (max === 0) {
                s = 0;
            } else {
                s = 1 - min / max;
            }

            if (max === min) {
                h = 0;
            } else if (max === r && g >= b) {
                h = 60 * ((g - b) / delta);
            } else if (max === r && g < b) {
                h = 60 * ((g - b) / delta) + 360;
            } else if (max === g) {
                h = 60 * ((b - r) / delta) + 120;
            } else if (max === b) {
                h = 60 * ((r - g) / delta) + 240;
            } else {
                h = 0;
            }

            hsv.h = h;
            hsv.s = s;
            hsv.v = v;

            return hsv;
        },

        hsv2rgb: function(color){
            this.check(color, "hsv");
            var r, g, b;
            var h = color.h,
                s = color.s * 100,
                v = color.v * 100;
            var Hi = Math.floor(h / 60);
            var Vmin = ((100 - s) * v) / 100;
            var alpha = (v - Vmin) * ((h % 60) / 60);
            var Vinc = Vmin + alpha;
            var Vdec = v - alpha;

            switch (Hi) {
                case 0:
                    r = v;
                    g = Vinc;
                    b = Vmin;
                    break;
                case 1:
                    r = Vdec;
                    g = v;
                    b = Vmin;
                    break;
                case 2:
                    r = Vmin;
                    g = v;
                    b = Vinc;
                    break;
                case 3:
                    r = Vmin;
                    g = Vdec;
                    b = v;
                    break;
                case 4:
                    r = Vinc;
                    g = Vmin;
                    b = v;
                    break;
                case 5:
                    r = v;
                    g = Vmin;
                    b = Vdec;
                    break;
            }

            return new RGB(
                Math.round((r * 255) / 100),
                Math.round((g * 255) / 100),
                Math.round((b * 255) / 100)
            );
        },

        hsv2hex: function(color){
            this.check(color, "hsv");
            return this.rgb2hex(this.hsv2rgb(color));
        },

        hex2hsv: function(color){
            this.check(color, "hex");
            return this.rgb2hsv(this.hex2rgb(color));
        },

        rgb2cmyk: function(color){
            this.check(color, "rgb");
            var cmyk = new CMYK();

            var r = color.r / 255;
            var g = color.g / 255;
            var b = color.b / 255;

            cmyk.k = Math.min(1 - r, 1 - g, 1 - b);

            cmyk.c = 1 - cmyk.k === 0 ? 0 : (1 - r - cmyk.k) / (1 - cmyk.k);
            cmyk.m = 1 - cmyk.k === 0 ? 0 : (1 - g - cmyk.k) / (1 - cmyk.k);
            cmyk.y = 1 - cmyk.k === 0 ? 0 : (1 - b - cmyk.k) / (1 - cmyk.k);

            cmyk.c = Math.round(cmyk.c * 100);
            cmyk.m = Math.round(cmyk.m * 100);
            cmyk.y = Math.round(cmyk.y * 100);
            cmyk.k = Math.round(cmyk.k * 100);

            return cmyk;
        },

        cmyk2rgb: function(color){
            this.check(color, "cmyk");
            var r = Math.floor(255 * (1 - color.c / 100) * (1 - color.k / 100));
            var g = Math.ceil(255 * (1 - color.m / 100) * (1 - color.k / 100));
            var b = Math.ceil(255 * (1 - color.y / 100) * (1 - color.k / 100));

            return new RGB(r, g, b);
        },

        hsv2hsl: function(color){
            this.check(color, "hsv");
            var h, s, l, d;
            h = color.h;
            l = (2 - color.s) * color.v;
            s = color.s * color.v;
            if (l === 0) {
                s = 0;
            } else {
                d = l <= 1 ? l : 2 - l;
                if (d === 0) {
                    s = 0;
                } else {
                    s /= d;
                }
            }
            l /= 2;
            return new HSL(h, s, l);
        },

        hsl2hsv: function(color){
            this.check(color, ["hsl", "hsla"]);
            var h, s, v, l;
            h = color.h;
            l = color.l * 2;
            s = color.s * (l <= 1 ? l : 2 - l);

            v = (l + s) / 2;

            if (l + s === 0) {
                s = 0;
            } else {
                s = (2 * s) / (l + s);
            }

            return new HSV(h, s, v);
        },

        rgb2websafe: function(color){
            this.check(color, "rgb");
            return new RGB(
                Math.round(color.r / 51) * 51,
                Math.round(color.g / 51) * 51,
                Math.round(color.b / 51) * 51
            );
        },

        rgba2websafe: function(color){
            this.check(color, "rgba");
            var rgbWebSafe = this.rgb2websafe(color);
            return new RGBA(rgbWebSafe.r, rgbWebSafe.g, rgbWebSafe.b, color.a);
        },

        hex2websafe: function(color){
            this.check(color, "hex");
            return this.rgb2hex(this.rgb2websafe(this.hex2rgb(color)));
        },

        hsv2websafe: function(color){
            this.check(color, "hsv");
            return this.rgb2hsv(this.rgb2websafe(this.toRGB(color)));
        },

        hsl2websafe: function(color){
           this.check(color, "hsl");
            return this.hsv2hsl(this.rgb2hsv(this.rgb2websafe(this.toRGB(color))));
        },

        cmyk2websafe: function(color){
            this.check(color, "cmyk");
            return this.rgb2cmyk(this.rgb2websafe(this.cmyk2rgb(color)));
        },

        websafe: function(color){
            if (this.isHEX(color)) return this.hex2websafe(color);
            if (this.isRGB(color)) return this.rgb2websafe(color);
            if (this.isRGBA(color)) return this.rgba2websafe(color);
            if (this.isHSV(color)) return this.hsv2websafe(color);
            if (this.isHSL(color)) return this.hsl2websafe(color);
            if (this.isCMYK(color)) return this.cmyk2websafe(color);

            return color;
        },

        toColor: function(color, type, alpha){
            var result;
            switch (type.toLowerCase()) {
                case "hex":
                    result = this.toHEX(color);
                    break;
                case "rgb":
                    result = this.toRGB(color);
                    break;
                case "rgba":
                    result = this.toRGBA(color, alpha);
                    break;
                case "hsl":
                    result = this.toHSL(color);
                    break;
                case "hsla":
                    result = this.toHSLA(color, alpha);
                    break;
                case "hsv":
                    result = this.toHSV(color);
                    break;
                case "cmyk":
                    result = this.toCMYK(color);
                    break;
                default:
                    result = color;
            }
            return result;
        },

        toHEX: function(val){
            var color = typeof val === "string" ? this.parse(val) : val;

            if (!color) {
                throw new Error("Unknown color format!");
            }

            return typeof color === "string"
                ? color
                : this.rgb2hex(this.toRGB(color));
        },

        toRGB: function(val){
            var color = typeof val === "string" ? this.parse(val) : val;

            if (this.isRGB(color)) return color;
            if (this.isRGBA(color)) return new RGB(color.r, color.g, color.b);
            if (this.isHSV(color)) return this.hsv2rgb(color);
            if (this.isHSL(color)) return this.hsv2rgb(this.hsl2hsv(color));
            if (this.isHSLA(color)) return this.hsv2rgb(this.hsl2hsv(color));
            if (this.isHEX(color)) return this.hex2rgb(color);
            if (this.isCMYK(color)) return this.cmyk2rgb(color);

            throw new Error("Unknown color format!");
        },

        toRGBA: function(color, alpha){
            if (this.isRGBA(color)) {
                if (alpha) {
                    color.a = alpha;
                }
                return color;
            }
            var rgb = this.toRGB(color);
            return new RGBA(rgb.r, rgb.g, rgb.b, alpha);
        },

        toHSV: function(color){
            return this.rgb2hsv(this.toRGB(color));
        },

        toHSL: function(color){
            return this.hsv2hsl(this.rgb2hsv(this.toRGB(color)));
        },

        toHSLA: function(color, alpha){
            if (this.isHSLA(color)) {
                if (alpha) {
                    color.a = alpha;
                }
                return color;
            }
            var hsla = this.hsv2hsl(this.rgb2hsv(this.toRGB(color)));
            hsla.a = alpha;
            return new HSLA(hsla.h, hsla.s, hsla.l, hsla.a);
        },

        toCMYK: function(color){
            return this.rgb2cmyk(this.toRGB(color));
        },

        grayscale: function(color){
            return this.desaturate(color, 100);
        },

        lighten: function(color, amount){
            var hsl, type, alpha;

            if (!this.isColor(color)) {
                throw new Error(color + " is not a valid color value!");
            }

            amount = (amount === 0) ? 0 : (amount || 10);
            hsl = this.toHSL(color);
            hsl.l += amount / 100;
            hsl.l = clamp(hsl.l);

            type = this.colorType(color).toLowerCase();

            if (type === Types.RGBA || type === Types.HSLA) {
                alpha = color.a;
            }

            return this.toColor(hsl, type, alpha);
        },

        darken: function(color, amount){
            return this.lighten(color, -amount);
        },

        spin: function(color, amount){
            var hsl, type, alpha, hue;

            if (!this.isColor(color)) {
                throw new Error(color + " is not a valid color value!");
            }

            hsl = this.toHSL(color);
            hue = (hsl.h + amount) % 360;
            hsl.h = hue < 0 ? 360 + hue : hue;

            type = this.colorType(color).toLowerCase();

            if (type === Types.RGBA || type === Types.HSLA) {
                alpha = color.a;
            }

            return this.toColor(hsl, type, alpha);
        },

        brighten: function(color, amount){
            var rgb, type, alpha;

            if (!this.isColor(color)) {
                throw new Error(color + " is not a valid color value!");
            }

            rgb = this.toRGB(color);
            rgb.r = Math.max(0, Math.min(255, rgb.r - Math.round(255 * - (amount / 100))));
            rgb.g = Math.max(0, Math.min(255, rgb.g - Math.round(255 * - (amount / 100))));
            rgb.b = Math.max(0, Math.min(255, rgb.b - Math.round(255 * - (amount / 100))));

            type = this.colorType(color).toLowerCase();

            if (type === Types.RGBA || type === Types.HSLA) {
                alpha = color.a;
            }

            return this.toColor(rgb, type, alpha);
        },

        saturate: function(color, amount){
            var hsl, type, alpha;

            if (!this.isColor(color)) {
                throw new Error(color + " is not a valid color value!");
            }

            hsl = this.toHSL(color);
            hsl.s += amount / 100;
            hsl.s = clamp(hsl.s);

            type = this.colorType(color).toLowerCase();

            if (type === Types.RGBA || type === Types.HSLA) {
                alpha = color.a;
            }

            return this.toColor(hsl, type, alpha);
        },

        desaturate: function(color, amount){
            var hsl, type, alpha;

            if (!this.isColor(color)) {
                throw new Error(color + " is not a valid color value!");
            }

            hsl = this.toHSL(color);
            hsl.s -= amount / 100;
            hsl.s = clamp(hsl.s);

            type = this.colorType(color).toLowerCase();

            if (type === Types.RGBA || type === Types.HSLA) {
                alpha = color.a;
            }

            return this.toColor(hsl, type, alpha);
        },

        hueShift: function(color, hue, saturation, value){
            var hsv = this.toHSV(color);
            var type = this.colorType(color).toLowerCase();
            var h = hsv.h;
            var alpha;
            var _h = hue || 0;
            var _s = saturation || 0;
            var _v = value || 0;

            h += _h;
            while (h >= 360.0) h -= 360.0;
            while (h < 0.0) h += 360.0;
            hsv.h = h;

            hsv.s += _s;
            if (hsv.s > 1) {hsv.s = 1;}
            if (hsv.s < 0) {hsv.s = 0;}

            hsv.v += _v;
            if (hsv.v > 1) {hsv.v = 1;}
            if (hsv.v < 0) {hsv.v = 0;}

            if (type === Types.RGBA || type === Types.HSLA) {
                alpha = color.a;
            }

            return this.toColor(hsv, type, alpha);
        },

        shade: function(color, amount){
            if (!this.isColor(color)) {
                throw new Error(color + " is not a valid color value!");
            }

            amount /= 100;

            var type = this.colorType(color).toLowerCase();
            var rgb = this.toRGB(color);
            var t = amount < 0 ? 0 : 255;
            var p = amount < 0 ? amount * -1 : amount;
            var r, g, b, a;

            r = (Math.round((t - rgb.r) * p) + rgb.r);
            g = (Math.round((t - rgb.g) * p) + rgb.g);
            b = (Math.round((t - rgb.b) * p) + rgb.b);

            if (type === Types.RGBA || type === Types.HSLA) {
                a = color.a;
            }

            return this.toColor(new RGB(r, g, b), type, a);
        },

        mix: function(color1, color2, amount){

            amount = (amount === 0) ? 0 : (amount || 50);

            var rgb = new RGB(0,0,0);
            var rgb1 = this.toRGB(color1);
            var rgb2 = this.toRGB(color2);

            var p = amount / 100;

            rgb.r = Math.round(((rgb2.r - rgb1.r) * p) + rgb1.r);
            rgb.g = Math.round(((rgb2.g - rgb1.g) * p) + rgb1.g);
            rgb.b = Math.round(((rgb2.b - rgb1.b) * p) + rgb1.b);

            return this.toHEX(rgb);
        },

        multiply: function(color1, color2){
            var rgb1 = this.toRGB(color1);
            var rgb2 = this.toRGB(color2);
            var rgb = new RGB();

            rgb1.b = Math.floor(rgb1.b * rgb2.b / 255);
            rgb1.g = Math.floor(rgb1.g * rgb2.g / 255);
            rgb1.r = Math.floor(rgb1.r * rgb2.r / 255);

            return this.toHEX(rgb);
        },

        materialPalette: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var baseLight = opt.baseLight;
            var baseDark = opt.baseDark === "self" || !opt.baseDark ? this.multiply(color, color) : opt.baseDark;

            return {
                "50": this.mix(baseLight, color, 10),
                "100": this.mix(baseLight, color, 30),
                "200": this.mix(baseLight, color, 50),
                "300": this.mix(baseLight, color, 70),
                "400": this.mix(baseLight, color, 85),
                "500": this.mix(baseLight, color, 100),
                "600": this.mix(baseDark, color, 92),
                "700": this.mix(baseDark, color, 83),
                "800": this.mix(baseDark, color, 74),
                "900": this.mix(baseDark, color, 65),

                "A100": this.lighten(this.saturate(this.mix(baseDark, color, 15), 80), 65),
                "A200": this.lighten(this.saturate(this.mix(baseDark, color, 15), 80), 55),
                "A400": this.lighten(this.saturate(this.mix(baseLight, color, 100), 55), 10),
                "A700": this.lighten(this.saturate(this.mix(baseDark, color, 83), 65), 10)
            };
        },

        monochromatic: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var returnAs = opt.resultType;
            var results = opt.results;
            var hsv = this.toHSV(color);
            var h = hsv.h,
                s = hsv.s,
                v = hsv.v;
            var result = [];
            var mod = 1 / results;
            var self = this;

            while (results--) {
                result.push(new HSV(h, s, v));
                v = (v + mod) % 1;
            }

            return result.map(function(el){
                return self["to"+returnAs.toUpperCase()](el);
            });
        },

        complementary: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var hsl = this.toHSL(color);
            var result;
            var self = this;

            var returnAs = opt.resultType;

            result = [
                hsl,
                new HSL(shift(hsl.h, 180), hsl.s, hsl.l)
            ];

            return result.map(function(el){
                return self["to"+returnAs.toUpperCase()](el);
            });
        },

        splitComplementary: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var hsl = this.toHSL(color);
            var h = hsl.h;
            var result, self = this;

            var returnAs = opt.resultType;
            var angle = opt.angle;

            result = [
                hsl,
                new HSL(shift(h, 180 - angle), hsl.s, hsl.l ),
                new HSL(shift(h, 180 + angle), hsl.s, hsl.l )
            ];

            return result.map(function(el){
                return self["to"+returnAs.toUpperCase()](el);
            });
        },

        doubleComplementary: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var returnAs = opt.resultType;
            var angle = opt.angle;
            var hsl = this.toHSL(color);
            var h = hsl.h;
            var result, self = this;

            result = [
                hsl,
                new HSL(shift(h, 180), hsl.s, hsl.l ),
                new HSL(shift(h, angle), hsl.s, hsl.l ),
                new HSL(shift(h, 180 + angle), hsl.s, hsl.l )
            ];

            return result.map(function(el){
                return self["to"+returnAs.toUpperCase()](el);
            });
        },

        square: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var returnAs = opt.resultType;
            var result = [], i;
            var hsl = this.toHSL(color);
            var h = hsl.h , self = this;

            result.push(hsl);

            for (i = 1; i < 4; i++) {
                h = shift(h, 90.0);
                result.push(new HSL(h, hsl.s, hsl.l));
            }

            return result.map(function(el){
                return self["to"+returnAs.toUpperCase()](el);
            });
        },

        tetradic: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var returnAs = opt.resultType;
            var angle = opt.angle;
            var result;
            var hsl = this.toHSL(color);
            var h = hsl.h;
            var self = this;

            result = [
                hsl,
                new HSL(shift(h, 180), hsl.s, hsl.l),
                new HSL(shift(h, 180 - angle), hsl.s, hsl.l),
                new HSL(shift(h, -angle), hsl.s, hsl.l)
            ];

            return result.map(function(el){
                return self["to"+returnAs.toUpperCase()](el);
            });
        },

        triadic: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var returnAs = opt.resultType;
            var result;
            var hsl = this.toHSL(color);
            var h = hsl.h;
            var self = this;

            result = [
                hsl,
                new HSL(shift(h,120), hsl.s, hsl.l),
                new HSL(shift(h,240), hsl.s, hsl.l)
            ];

            return result.map(function(el){
                return self["to"+returnAs.toUpperCase()](el);
            });
        },

        analogous: function(color, options){
            var opt = $.extend({}, ColorsDefaultConfig, options);
            var returnAs = opt.resultType;
            var angle = opt.angle;

            var hsl = this.toHSL(color);
            var result, self = this;

            result = [
                hsl,
                new HSL(shift(hsl.h, -angle), hsl.s, hsl.l),
                new HSL(shift(hsl.h, +angle), hsl.s, hsl.l)
            ];

            return result.map(function(el){
                return self["to"+returnAs.toUpperCase()](el);
            });
        },

        createScheme: function(color, name, options){
            switch (name.toLowerCase()) {
                case "analogous":
                case "analog": return this.analogous(color, options);

                case "triadic":
                case "triad": return this.triadic(color, options);

                case "tetradic":
                case "tetra": return this.tetradic(color, options);

                case "monochromatic":
                case "mono": return this.monochromatic(color, options);

                case "complementary":
                case "complement":
                case "comp": return this.complementary(color, options);

                case "double-complementary":
                case "double-complement":
                case "double": return this.doubleComplementary(color, options);

                case "split-complementary":
                case "split-complement":
                case "split": return this.splitComplementary(color, options);

                case "square": return this.square(color, options);
                case "material": return this.materialPalette(color, options);
            }
        },

        getScheme: function(){
            return this.createScheme.apply(this, arguments)
        },

        add: function(val1, val2, returnAs){
            var color1 = typeof val1 === "string" ? this.parse(val1) : val1;
            var color2 = typeof val2 === "string" ? this.parse(val2) : val2;
            var c1 = this.toRGBA(color1);
            var c2 = this.toRGBA(color2);
            var result = new RGBA();
            var to = (""+returnAs).toLowerCase() || "hex";

            result.r = Math.round((c1.r + c2.r) / 2);
            result.g = Math.round((c1.g + c2.g) / 2);
            result.b = Math.round((c1.b + c2.b) / 2);
            result.a = Math.round((c1.a + c2.a) / 2);

            return this["to"+to.toUpperCase()](result);
        }
    };

    var Color = function(color, options){
        this._setValue(color);
        this._setOptions(options);
    }

    Color.prototype = {
        _setValue: function(color){
            var _color;

            if (typeof color === "string") {
                _color = Colors.parse(color);
            } else {
                _color = color;
            }

            if (!Colors.isColor(_color)) {
                _color = "#000000";
            }

            this._value = _color;
            this._type = Colors.colorType(this._value);
        },

        _setOptions: function(options){
            options = typeof options === "object" ? options : {};
            this._options = $.extend({}, ColorsDefaultConfig, options);
        },

        getOptions: function(){
            return this._options;
        },

        setOptions: function(options){
            this._setOptions(options);
        },

        setValue: function(color){
            this._setValue(color);
        },

        getValue: function(){
            return this._value;
        },

        channel: function(ch, val){
            var currentType = this._type.toUpperCase();

            if (["red", "green", "blue"].indexOf(ch) > -1) {
                this.toRGB();
                this._value[ch[0]] = val;
                this["to"+currentType]();
            }
            if (ch === "alpha" && this._value.a) {
                this._value.a = val;
            }
            if (["hue", "saturation", "value"].indexOf(ch) > -1) {
                this.toHSV();
                this._value[ch[0]] = val;
                this["to"+currentType]();
            }
            if (["lightness"].indexOf(ch) > -1) {
                this.toHSL();
                this._value[ch[0]] = val;
                this["to"+currentType]();
            }
            if (["cyan", "magenta", "yellow", "black"].indexOf(ch) > -1) {
                this.toCMYK();
                this._value[ch[0]] = val;
                this["to"+currentType]();
            }

            return this;
        },

        channels: function(obj){
            var that = this;

            $.each(obj, function(key, val){
                that.channel(key, val);
            });

            return this;
        },

        toRGB: function() {
            this._value = Colors.toRGB(this._value);
            this._type = Types.RGB;
            return this;
        },

        rgb: function(){
            return this._value ? new Color(Colors.toRGB(this._value)) : undefined;
        },

        toRGBA: function(alpha) {
            if (Colors.isRGBA(this._value)) {
                if (alpha) {
                    this._value = Colors.toRGBA(this._value, alpha);
                }
            } else {
                this._value = Colors.toRGBA(this._value, alpha);
            }
            this._type = Types.RGBA;
            return this;
        },

        rgba: function(alpha) {
            return this._value ? new Color(Colors.toRGBA(this._value, alpha)) : undefined;
        },

        toHEX: function() {
            this._value = Colors.toHEX(this._value);
            this._type = Types.HEX;
            return this;
        },

        hex: function() {
            return this._value ? new Color(Colors.toHEX(this._value)) : undefined;
        },

        toHSV: function() {
            this._value = Colors.toHSV(this._value);
            this._type = Types.HSV;
            return this;
        },

        hsv: function() {
            return this._value ? new Color(Colors.toHSV(this._value)) : undefined;
        },

        toHSL: function() {
            this._value = Colors.toHSL(this._value);
            this._type = Types.HSL;
            return this;
        },

        hsl: function() {
            return this._value ? new Color(Colors.toHSL(this._value)) : undefined;
        },

        toHSLA: function(alpha) {
            if (Colors.isHSLA(this._value)) {
                if (alpha) {
                    this._value = Colors.toHSLA(this._value, alpha);
                }
            } else {
                this._value = Colors.toHSLA(this._value, alpha);
            }
            this._type = Types.HSLA;
            return this;
        },

        hsla: function(alpha) {
            return this._value ? new Color(Colors.toHSLA(this._value, alpha)) : undefined;
        },

        toCMYK: function() {
            this._value = Colors.toCMYK(this._value);
            this._type = Types.CMYK;
            return this;
        },

        cmyk: function() {
            return this._value ? new Color(Colors.toCMYK(this._value)) : undefined;
        },

        toWebsafe: function() {
            this._value = Colors.websafe(this._value);
            this._type = Colors.colorType(this._value);
            return this;
        },

        websafe: function() {
            return this._value ? new Color(Colors.websafe(this._value)) : undefined;
        },

        toString: function() {
            return this._value ? Colors.colorToString(this._value) : "undefined";
        },

        toDarken: function(amount) {
            this._value = Colors.darken(this._value, amount);
            return this;
        },

        darken: function(amount){
            return new Color(Colors.darken(this._value, amount));
        },

        toLighten: function(amount) {
            this._value = Colors.lighten(this._value, amount);
            return this;
        },

        lighten: function(amount){
            return new Color(Colors.lighten(this._value, amount))
        },

        isDark: function() {
            return this._value ? Colors.isDark(this._value) : undefined;
        },

        isLight: function() {
            return this._value ? Colors.isLight(this._value) : undefined;
        },

        toHueShift: function(hue, saturation, value) {
            this._value = Colors.hueShift(this._value, hue, saturation, value);
            return this;
        },

        hueShift: function (hue, saturation, value) {
            return new Color(Colors.hueShift(this._value, hue, saturation, value));
        },

        toGrayscale: function() {
            this._value = Colors.grayscale(this._value, this._type);
            return this;
        },

        grayscale: function(){
            return new Color(Colors.grayscale(this._value, this._type));
        },

        type: function() {
            return Colors.colorType(this._value);
        },

        createScheme: function(name, format, options) {
            return this._value
                ? Colors.createScheme(this._value, name, format, options)
                : undefined;
        },

        getScheme: function(){
            return this.createScheme.apply(this, arguments);
        },

        equal: function(color) {
            return Colors.equal(this._value, color);
        },

        toAdd: function(color){
            this._value = Colors.add(this._value, color, this._type);
            return this;
        },

        add: function(color){
            return new Color(Colors.add(this._value, color, this._type));
        }
    }

    Metro.colors = Colors.init();
    window.Color = Metro.Color = Color;
    window.ColorPrimitive = Metro.colorPrimitive = {
        RGB: RGB,
        RGBA: RGBA,
        HSV: HSV,
        HSL: HSL,
        HSLA: HSLA,
        CMYK: CMYK
    };

    if (window.METRO_GLOBAL_COMMON === true) {
        window.Colors = Metro.colors;
    }

}(Metro, m4q));


/* global Metro */
(function(Metro, $) {
    'use strict';
    var Utils = Metro.utils;
    var CheckboxDefaultConfig = {
        checkboxDeferred: 0,
        transition: true,
        style: 1,
        caption: "",
        captionPosition: "right",
        indeterminate: false,
        clsCheckbox: "",
        clsCheck: "",
        clsCaption: "",
        onCheckboxCreate: Metro.noop
    };

    Metro.checkboxSetup = function (options) {
        CheckboxDefaultConfig = $.extend({}, CheckboxDefaultConfig, options);
    };

    if (typeof window["metroCheckboxSetup"] !== undefined) {
        Metro.checkboxSetup(window["metroCheckboxSetup"]);
    }

    Metro.Component('checkbox', {
        init: function( options, elem ) {
            this._super(elem, options, CheckboxDefaultConfig, {
                origin: {
                    className: ""
                }
            });

            return this;
        },

        _create: function(){
            this._createStructure();
            this._createEvents();
            this._fireEvent("checkbox-create");
        },

        _createStructure: function(){
            var element = this.element, o = this.options;
            var checkbox;
            var check = $("<span>").addClass("check");
            var caption = $("<span>").addClass("caption").html(o.caption);

            element.attr("type", "checkbox");

            if (element.attr("readonly") !== undefined) {
                element.on("click", function(e){
                    e.preventDefault();
                })
            }

            checkbox = element
                .wrap("<label>")
                .addClass("checkbox " + element[0].className)
                .addClass(o.style === 2 ? "style2" : "");

            check.appendTo(checkbox);
            caption.appendTo(checkbox);

            if (o.transition === true) {
                checkbox.addClass("transition-on");
            }

            if (o.captionPosition === 'left') {
                checkbox.addClass("caption-left");
            }

            this.origin.className = element[0].className;
            element[0].className = '';

            checkbox.addClass(o.clsCheckbox);
            caption.addClass(o.clsCaption);
            check.addClass(o.clsCheck);

            if (o.indeterminate) {
                element[0].indeterminate = true;
            }

            if (element.is(':disabled')) {
                this.disable();
            } else {
                this.enable();
            }
        },

        _createEvents: function(){
            var element = this.element, check = element.siblings(".check");

            element.on("focus", function(){
                check.addClass("focused");
            });

            element.on("blur", function(){
                check.removeClass("focused");
            });
        },

        indeterminate: function(v){
            var element = this.element;

            v = Utils.isNull(v) ? true : Utils.bool(v);

            element[0].indeterminate = v;
            element.attr("data-indeterminate", v);
        },

        disable: function(){
            this.element.data("disabled", true);
            this.element.parent().addClass("disabled");
        },

        enable: function(){
            this.element.data("disabled", false);
            this.element.parent().removeClass("disabled");
        },

        toggleState: function(){
            if (this.elem.disabled) {
                this.disable();
            } else {
                this.enable();
            }
        },

        toggle: function(v){
            var element = this.element;

            this.indeterminate(false);

            if (!Utils.isValue(v)) {
                element.prop("checked", !Utils.bool(element.prop("checked")));
            } else {
                if (v === -1) {
                    this.indeterminate(true);
                } else {
                    element.prop("checked", v === 1);
                }
            }
            return this;
        },

        changeAttribute: function(attributeName){
            var element = this.element, o = this.options;
            var parent = element.parent();

            var changeStyle = function(){
                var new_style = parseInt(element.attr("data-style"));

                if (!Utils.isInt(new_style)) return;

                o.style = new_style;
                parent.removeClass("style1 style2").addClass("style"+new_style);
            };

            var indeterminateState = function(){
                element[0].indeterminate = JSON.parse(element.attr("data-indeterminate")) === true;
            };

            switch (attributeName) {
                case 'disabled': this.toggleState(); break;
                case 'data-indeterminate': indeterminateState(); break;
                case 'data-style': changeStyle(); break;
            }
        },

        destroy: function(){
            var element = this.element;
            element.off("focus");
            element.off("blur");
            return element;
        }
    });
}(Metro, m4q));


/* global Metro */
(function(Metro, $) {
    'use strict';
    var ActivityDefaultConfig = {
        activityDeferred: 0,
        type: "ring",
        style: "light",
        size: 64,
        radius: 20,
        onActivityCreate: Metro.noop
    };

    Metro.activitySetup = function(options){
        ActivityDefaultConfig = $.extend({}, ActivityDefaultConfig, options);
    };

    if (typeof window["metroActivitySetup"] !== undefined) {
        Metro.activitySetup(window["metroActivitySetup"]);
    }

    Metro.Component('activity', {
        init: function( options, elem ) {
            this._super(elem, options, ActivityDefaultConfig);
            return this;
        },

        _create: function(){
            var element = this.element, o = this.options;
            var i, wrap;

            element
                .html('')
                .addClass(o.style + "-style")
                .addClass("activity-" + o.type);

            function _metro(){
                for(i = 0; i < 5 ; i++) {
                    $("<div/>").addClass('circle').appendTo(element);
                }
            }

            function _square(){
                for(i = 0; i < 4 ; i++) {
                    $("<div/>").addClass('square').appendTo(element);
                }
            }

            function _cycle(){
                $("<div/>").addClass('cycle').appendTo(element);
            }

            function _ring(){
                for(i = 0; i < 5 ; i++) {
                    wrap = $("<div/>").addClass('wrap').appendTo(element);
                    $("<div/>").addClass('circle').appendTo(wrap);
                }
            }

            function _simple(){
                $('<svg class="circular"><circle class="path" cx="'+o.size/2+'" cy="'+o.size/2+'" r="'+o.radius+'" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>').appendTo(element);
            }

            function _atom(){
                for(i = 0; i < 3 ; i++) {
                    $("<span/>").addClass('electron').appendTo(element);
                }
            }

            function _bars(){
                for(i = 0; i < 6 ; i++) {
                    $("<span/>").addClass('bar').appendTo(element);
                }
            }

            switch (o.type) {
                case 'metro': _metro(); break;
                case 'square': _square(); break;
                case 'cycle': _cycle(); break;
                case 'simple': _simple(); break;
                case 'atom': _atom(); break;
                case 'bars': _bars(); break;
                default: _ring();
            }

            this._fireEvent("activity-create", {
                element: element
            })
        },

        /*eslint-disable-next-line*/
        changeAttribute: function(attributeName){
        },

        destroy: function(){
            return this.element;
        }
    });

    Metro.activity = {
        open: function(options){
            var o = options || {};
            var activity = '<div data-role="activity" data-type="'+( o.type ? o.type : 'cycle' )+'" data-style="'+( o.style ? o.style : 'color' )+'"></div>';
            var text = o.text ? '<div class="text-center">'+o.text+'</div>' : '';

            return Metro.dialog.create({
                content: activity + text,
                defaultAction: false,
                clsContent: "d-flex flex-column flex-justify-center flex-align-center bg-transparent no-shadow w-auto",
                clsDialog: "no-border no-shadow bg-transparent global-dialog",
                autoHide: o.autoHide ? o.autoHide : 0,
                overlayClickClose: o.overlayClickClose === true,
                overlayColor: o.overlayColor ? o.overlayColor : '#000000',
                overlayAlpha: o.overlayAlpha ? o.overlayAlpha : 0.5,
                clsOverlay: "global-overlay"
            });
        },

        close: function(a){
            Metro.dialog.close(a);
        }
    };
}(Metro, m4q));

