import React, {Component} from 'react';

class Form extends Component {
    constructor(props) {
        super(props);
        
        this.initialState = {
            name: '',
            job: '',
            position:'',
            years:''
        };

        this.state = this.initialState;
    }

    handleChange = event => {
        const { name, value } = event.target;
        this.setState({value: event.target.value});

        this.setState({
            [name] : value
        });
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        
        this.props.handleSubmit(this.state);
        this.setState(this.initialState);
        alert('姓名: ' + this.state.name +'\n工作: '+this.state.job+'\n職位: '+this.state.position+'\n年資'+this.state.years);
        // alert(this.state.value);
    }

    render() {
        const { name, job ,position,years} = this.state; 

        return (
            <form onSubmit={this.onFormSubmit}>
            <table>
                <tr>
                    <td><label for="name">Name</label></td>
                    <td>
                        <input 
                        type="text" 
                        name="name" 
                        id="name"
                        value={name} 
                        // required
                        onChange={this.handleChange} 
                        />
                    </td>
                </tr>
                    <tr>
                        <td><label for="job">Job</label></td>
                        <td><input 
                            type="text" 
                            name="job" 
                            id="job"
                            value={job} 
                            // required
                            onChange={this.handleChange} 
                            />
                        </td>
                    </tr>        
                <tr>
                <td><label for="Years">Years</label></td>
                    <td>
                    <input 
                        type="text" 
                        name="years" 
                        id="years"
                        value={years} 
                        // required
                        onChange={this.handleChange} 
                        />
                    </td>
                </tr>
                <tr>
                    <td> Position:</td>
                    <td>
                        <select name="position"  value={this.state.value} onChange={this.handleChange}>
                        <option>-選擇-</option>
                        <option value="協理">協理</option>
                        <option value="經理">經理</option>
                        <option value="副理">副理</option>
                        <option value="專員">專員</option>
                        </select>
                    </td>
                </tr>
                </table><p></p>
                <button type="submit">
                    Submit
                </button>
            </form>
        );
    }
}

export default Form;