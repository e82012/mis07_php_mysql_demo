class Toggle extends React.Component {
    constructor(props) {
      super(props);
      this.state = {isToggleOn: true,
                    tick:new Date().toLocaleTimeString()
                    };
  
      // This binding is necessary to make `this` work in the callback
      this.handleClick = this.handleClick.bind(this);
      this.tick = this.tick.bind(this);
      this.updateTime = this.updateTime.bind(this);


    }
    // 取得時間
    tick(){
        const element = new Date().toLocaleTimeString() 
        this.setState({
            tick: element
          })
        //   console.log(element)
        }
    //更新時間
    updateTime(){
        setInterval(()=>{this.tick()},1000)  
        // console.log("element")
    }

    handleClick() {
        
      this.setState(prevState => ({
        isToggleOn: !prevState.isToggleOn
      }));
    }
  
    render() {
      return (
          <div>
            <h5>{this.state.tick}</h5>
            {/* <input type ='text' value ={this.state.tick} ></input> */}
            <button onClick={this.updateTime} >
          {this.state.isToggleOn ? 'ON' : 'OFF'}
        </button>
        </div>
      );
    }
  }

  
  ReactDOM.render(
    <Toggle />,
    document.getElementById('reactDiv')
  );