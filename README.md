## **PHP Yii1.1 框架**
** 功能測試用**
>  - 上傳聯信帳務(imap)  
>> ![nccc_email_trans](https://gitlab.com/e82012/mis07/-/raw/master/images/flow/NCCC_email_trans.png)  

>  - 上傳一卡通帳務(SFTP)  
>> ![ipass_sftp_trans](https://gitlab.com/e82012/mis07/-/raw/master/images/flow/Ipass_sftp_trans.png)

>  - 上傳悠遊卡帳務(FTP)  
>> ![easy_ftp_trans](https://gitlab.com/e82012/mis07/-/raw/master/images/flow/easy_ftp_trans.png)
 
> - websocket  
> - linepay sandbox  
> - ETH 區塊鍊交易操作
>> - web3
>> - mainnet.infura.io 
> -  **其他功能測試**  
>>  - 資料轉出試算表(csv、xls)  
>>  - 資料轉出PDF  
>>  - 檔案上傳下載、資料分析處理(json、xls、csv、txt)  
>>  - SELECT、INPUT、INPUT元件測試(單選、多選、日期、連動)  
>>  - QRCODE測試(html5-qrcode)(票卷分析)  
>>  - Google + fullcalendar 應用(檢視、新增事件)  
>>  - **圖表工具應用**  
>>>  - Highcharts  
>>>  - bootstrap-table  
>>>  - tabulator-tables  
>>>  - datatables  
>>
>>

- webSocket  
>   2021/11/26
>  - https://www.letswrite.tw/websocket/#%e5%8f%83%e8%80%83%e8%b3%87%e6%ba%90  
>  - npm install ws //WebSocket  
>  - npm install Nodemon //執行webSocket  

- <p>資料夾下建立index.js</p>
<details><summary>程式碼</summary>

```
const WebSocket = require('ws');  
const wss = new WebSocket.Server({  
    port: 8088  
});  
var date = new Date();  
wss.on('connection', function connection(ws) {  
    console.log('server connection')  

    ws.on('message', function incoming(message) {  
    console.log('received: %s', message);  
    });  

    var data = { name: "Augustus", blog: "Let's Write" };  

    setInterval(function() {  
    ws.send(JSON.stringify(date));  
    }, 5000);  
    
    ws.send('som??????????????ng');  
});  
```  
</details> 
 
-  <p>指令執行 $nodemon index.js </p>

